
Description
============================
This project consists of a ChocoPy compiler. ChocoPy is a language that acts as a simplified version of Python. The compiler does not handle the whole ChocoPy language yet and this document will therefore provide example programs that act as a description of what is implemented.

______________________________________________________________________________________________
______________________________________________________________________________________________
Installation
============================

Steps to build, test and run ChocoPy compiler:

  1. Clone or download repository
  2. Open project
  3. Build
  4. Run tests
  5. Construct and run your own tests.



1. Clone repository/download
==============
This only works for linux.
Make sure that you have git installed. If you do not have git, install it by following the instructions at: https://git-scm.com/book/en/v2/Getting-Started-Installing-Git.

Open the terminal and write the following command where userName is your username:
 git clone https://userName@bitbucket.org/edan70/chocopy-tobias.git

______________________________________________________________________________________________
2. Open project
==============
Open the ChocoPy compiler in the preferred environment.
______________________________________________________________________________________________
3. Build
==============
Open the terminal and type the following: ./gradlew build.
______________________________________________________________________________________________
4. Run test
==============
You can run the constructed tests by opening the terminal and type the following: ./gradlew test
______________________________________________________________________________________________
5. Construct and run your own tests
==============
You can run the constructed tests by opening the terminal and type the following: ./gradlew test
There are constructed tests for assembly code generation, name analysis, parser, and type analysis.

If you want to create your test file you can just add it under the folder that you are interested in testing. If you, for example, want to test the assembly code, you can create it under the following folder: testfiles/asm, in the following way: tesfile.in. After adding the file that is to be tested, just run the following command: ./gradlew test
You can also go to the demoF.in file and enter the program that you want to test and then run the command: ./testFile.sh
______________________________________________________________________________________________
______________________________________________________________________________________________

Example programs
==============

As of now, not the whole language is implemented for the compiler, there will be example programs that one can test that function as a description of what is possible. If the examples below don't provide enough information, there are more examples under the folder: testfiles.

1 [Example 1](https://bitbucket.org/edan70/chocopy-tobias/src/master/testfiles/asm/funcDefInitTest.in). Expected output:[7 2450 16 25 36]

______________________________________________________________________________________________


2 [Example 2](https://bitbucket.org/edan70/chocopy-tobias/src/master/testfiles/asm/sqrTest2.in).  Expected output:[16 36 25 16] 

______________________________________________________________________________________________

3 [Example 3](https://bitbucket.org/edan70/chocopy-tobias/src/master/testfiles/asm/mathT.in). Expected output:[7 8 9 8 9] 
______________________________________________________________________________________________
______________________________________________________________________________________________

License
==============
This repository is covered by the license BSD 2-clause, see file LICENSE.
