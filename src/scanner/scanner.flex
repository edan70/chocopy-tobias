package lang.ast; // The generated scanner will belong to the package lang.ast

import lang.ast.LangParser.Terminals; // The terminals are implicitly defined in the parser
import lang.ast.LangParser.SyntaxError;

%%

// define the signature for the generated scanner
%public
%final
%class LangScanner
%extends beaver.Scanner

// the interface between the scanner and the parser is the nextToken() method
%type beaver.Symbol 
%function nextToken 

// store line and column information in the tokens
%line
%column

// this code will be inlined in the body of the generated scanner class
%{
  private beaver.Symbol sym(short id) {
    return new beaver.Symbol(id, yyline + 1, yycolumn + 1, yylength(), yytext());
  }
%}



// macros
WhiteSpace = [ ] | \t | \f | \n | \r
Comment = "#"[^\n\r]*
ID = [_]*[a-zA-Z][a-zA-Z0-9]* [_]*
Numeral = [0-9]+
Str = [\"]

%%
// discard whitespace information
{WhiteSpace}  { }
{Comment}     { }


// token definitions
"("           { return sym(Terminals.LPAREN); }
")"           { return sym(Terminals.RPAREN); }
"="           { return sym(Terminals.ASSIGN); }
","           { return sym(Terminals.COMMA); }
"+"           { return sym(Terminals.PLUS); }
"-"           { return sym(Terminals.MINUS); }
":"           { return sym(Terminals.DOTS); }
"."           { return sym(Terminals.DOT); }

"*"           { return sym(Terminals.MUL); }
"/"           { return sym(Terminals.DIV); }
"%"           { return sym(Terminals.MOD); }
"=="          { return sym(Terminals.EQ); }
"!="          { return sym(Terminals.NE); }
"<="          { return sym(Terminals.LE); }
"->"          { return sym(Terminals.ARROW); }
"<"           { return sym(Terminals.LT); }
">"           { return sym(Terminals.GT); }
">="          { return sym(Terminals.GE); }
"if"          { return sym(Terminals.IF); }
"else"        { return sym(Terminals.ELSE); }
"True"        { return sym(Terminals.TRUE); }
"False"        { return sym(Terminals.FALSE); }
"string"        { return sym(Terminals.STRINGT); }
"class"        { return sym(Terminals.CLASS); }
"elif"        { return sym(Terminals.ELSEIF); }
"idDecl"          { return sym(Terminals.IDDECL); }
"and"        { return sym(Terminals.AND); }
"or"        { return sym(Terminals.OR); }
"not"        { return sym(Terminals.NOT); }
"for"        { return sym(Terminals.FOR); }
"in"        { return sym(Terminals.IN); }
"pass"       { return sym(Terminals.PASS); }
"while"       { return sym(Terminals.WHILE); }
"indent"       { return sym(Terminals.INDENT); }
"return"       { return sym(Terminals.RETURN); }
"dedent"       { return sym(Terminals.DEDENT); }
"int"         { return sym(Terminals.INT); }
"def"         { return sym(Terminals.DEF); }
"bool"         { return sym(Terminals.BOOLEAN); }
"newline"     { return sym(Terminals.NEWLINE); }
{Str}     { return sym(Terminals.STR); }
{Numeral}     { return sym(Terminals.NUMERAL); }
{ID}          { return sym(Terminals.ID); }
<<EOF>>       { return sym(Terminals.EOF); }



/* error fallback */
[^]           { throw new SyntaxError("Illegal character <"+yytext()+">"); }
