%class "LangParser";
%package "lang.ast";

%embed {:
	static public class SyntaxError extends RuntimeException { public SyntaxError(String msg) {super(msg);}}
	// Disable syntax error recovery
	protected void recoverFromError(Symbol token, TokenStream in) {
		throw new SyntaxError("Cannot recover from the syntax error");
	}
:};

%terminals ID, TYPE, NUMERAL, IF, ELSE, WHILE,LPAREN, RPAREN, ASSIGN, COMMA,
PLUS, MINUS, MUL, DIV, MOD,EQ, NE, LT, GT, LE, GE,DOTS,NEWLINE,NONE,TRUE,FALSE,DEF,
INDENT,DEDENT,ELSEIF,DOT,ARROW,FOR,IN,CLASS,PASS,AND,OR,RETURN,NOT,IDDECL,BOOLEAN,INT,STR,STRINGT;

%typeof program = "Program";
%typeof funcbody = "func_body";
%typeof ch = "Opt<opttypedVar>";
%typeof chCall = "Opt<optExpr>";
%typeof stmt_list = "List";
%typeof vfc_list = "List";
%typeof argument_list_opt = "List<typedVar>";
%typeof argument_list = "List<typedVar>";
%typeof opttypedvar = "opttypedVar";
%typeof elif_list = "List";
%typeof parameter_list = "List";
%typeof vfc = "vfc";
%typeof funcdef = "func_def";
%typeof vardef = "var_def";
%typeof typedvar = "typedVar";
%typeof typevar = "typedVar";
%typeof tempLit = "literal";
%typeof retType= "Opt";
%typeof opt = "Expr";
%typeof termCh = "Expr";
%typeof term1 = "Term1";
%typeof term2 = "Term2";
%typeof typen = "Typen";
%typeof fac = "Expr";
%typeof fac1 = "Factor1";
%typeof fac2 = "Factor2";
%typeof numeral = "Numeral";
%typeof elif_list_opt = "List";
%typeof else_opt = "Opt<optExprBlock>";
%typeof param_opt = "List";
%typeof optexpr = "optExpr";
%typeof stmt = "stmt";
%typeof simpstmt = "simpStmt";
%typeof passstmt = "passStmt";
%typeof assignstmt = "assignStmt";
%typeof ifstmt = "ifStmt";
%typeof whilestmt = "whileStmt";
%typeof forstmt = "forStmt";
%typeof truestmt = "trueStmt";
%typeof falsestmt = "falseStmt";
%typeof nonestmt = "noneStmt";
%typeof exprblock = "exprBlock";
%typeof optexprblock = "optExprBlock";
%typeof functioncall = "functionCall";
%typeof cexpr = "cexpr";
%typeof arithmetic_expr = "cexpr";
%typeof term = "cexpr";
%typeof factor = "cexpr";
%typeof binary = "cexpr";
%typeof idcexpr = "idCexpr";
%typeof expr = "Expr";
%typeof notexpr = "notExpr";
%typeof andorexpr = "andOrExpr";
%typeof ifelseexpr = "ifElseExpr";
%typeof typei = "IntType";
%typeof typeb = "BoolType";
%typeof typeu = "UnknownType";
%typeof add = "Add";
%typeof sub = "Sub";
%typeof mul = "Mul";
%typeof div = "Mul";
%typeof mod = "Mod";
%typeof eq = "Eq";
%typeof ne = "Ne";
%typeof lt = "Lt";
%typeof gt = "gt";
%typeof le = "Le";
%typeof ge = "Ge";
%typeof exprstmt = "exprStmt";
%typeof retstmt = "retStmt";
%typeof optRet = "Opt<Expr>";
%typeof id_use = "IdUse";
%typeof unary_minus = "UnaryMinus";
%typeof id_decl = "IdDecl";
%typeof target = "Target";
%typeof block = "Block";
%typeof stringstmt = "stringStmt";
%typeof vardefLi = "List<var_def>";
%typeof typeo = "ObjType";
%typeof classdef = "class_def";
%typeof classbody = "class_body";
%typeof optvf = "List<var_def>";
%typeof optfdef = "List<func_def>";
%typeof memexpr = "member_expr";
%typeof cltype = "clType";






program =  vfc_list.a stmt_list.a2 {: return new Program(a,a2); :} ;


vfc_list ={: return new List(); :}
	|vfc_list.list vfc.a {: return list.add(a); :} ;

vfc = vardef|funcdef|classdef;

classdef =CLASS id_decl.id LPAREN cltype.ids RPAREN DOTS NEWLINE INDENT classbody.cb DEDENT  {: return new class_def(id,ids,cb); :} ;
classbody = vardef.v optvf.ov optfdef.ovf {: return new class_body(v,ov,ovf); :} ;

optvf ={: return new List(); :}
	|optvf.list vardef.a {: return list.add(a); :} ;
optfdef ={: return new List(); :}
	|optfdef.list funcdef.a {: return list.add(a); :} ;

cltype = ID.id {: return new clType(id); :} ;

memexpr =id_use.xe DOT id_use.id LPAREN chCall.e RPAREN {: return new member_expr(xe,id,e); :} ; 


funcdef =DEF id_decl.id LPAREN ch.opt RPAREN retType.rt DOTS NEWLINE INDENT funcbody.fb DEDENT {: return new func_def(id,opt,rt,fb); :} ;
funcbody = vardefLi.vf stmt.st stmt_list.st2 {: return new func_body(vf,st,st2); :} ;

vardefLi ={: return new List(); :}
	|vardefLi.list vardef.a {: return list.add(a); :} ;

ch = {: return new Opt(); :}
	|opttypedvar;
opttypedvar = typevar.a argument_list_opt.t {: return new Opt(new opttypedVar(a,t)); :} ;
vardef = typedvar.v ASSIGN cexpr.a NEWLINE{: return new var_def(v,a); :} ;
typedvar = IDDECL id_decl.id DOTS typen.t {: return new typedVar(id,t); :} ;
typevar = id_decl.id DOTS typen.t {: return new typedVar(id,t); :} ;

retType =
	{: return new Opt(); :}
	|ARROW typen.t{: return new Opt(t); :}  ;
	
argument_list_opt =
	{: return new List(); :}
	| argument_list ;
argument_list =
	COMMA typevar.a {: return new List(a); :}
	| argument_list.list COMMA typevar.a {: return list.add(a); :} ;


id_decl =  ID.id {: return new IdDecl( id); :} ;


typen = typei| typeb|types|typeo ;
typei = INT.a {: return new IntType(); :} ;
typeb = BOOLEAN.a {: return new BoolType(); :} ;
types = STRINGT.a {: return new StringType(); :} ;
typeo = id_use.a {: return new ObjType(a); :} ;



stmt_list =
	{: return new List(); :}
	| stmt_list.list stmt.a {: return list.add(a); :} ;

stmt = simpOrAssign | passstmt | ifstmt | while_stmt | forstmt;

passstmt=PASS NEWLINE {: return new passStmt(); :}  ;
while_stmt= WHILE cexpr.e DOTS block.b  {: return new whileStmt(e,b); :} ;
forstmt= FOR id_use.id IN expr.e DOTS block.b  {: return new forStmt(id,e,b); :} ;
ifstmt= IF cexpr.e DOTS block.b elif_list_opt.l1 else_opt.els  {: return new ifStmt(e,b,l1,els); :} ;
simpOrAssign = assignstmt | simpstmt;

simpstmt = exprstmt|retstmt;
retstmt=  RETURN optRet.a  NEWLINE {: return new retStmt(a); :}  ;
optRet = {: return new Opt(); :}
	| expr.e {: return new Opt(e); :} ;

exprstmt=  expr.e NEWLINE {: return new exprStmt(e); :}  ;
assignstmt = target.t ASSIGN  expr.e  NEWLINE{: return new assignStmt(t,e); :};


elif_list_opt =
	{: return new List(); :}
	| elif_list ;
elif_list =
	ELSEIF exprblock.eb {: return new List(eb); :}
	| elif_list.list ELSEIF exprblock.eb {: return list.add(eb); :} ;
exprblock= cexpr.e DOTS block.b {: return new exprBlock(e,b); :};


else_opt = 
	{: return new Opt(); :}
	| optexprblock.b {: return new Opt(b); :};
optexprblock = ELSE DOTS block.b {: return new optExprBlock(b); :};


block= NEWLINE INDENT stmt.s stmt_list.sl DEDENT {: return new Block(s,sl); :};


expr = termCh; 
termCh = term1 |term2  ; 
term1 = termCh.t andor fac.f {: return new Term1(t,f); :};
term2 =  fac.f {: return new Term2(f); :};
andor = AND|OR;

fac = fac1 | cexpr  ; 
fac1 = NOT opt.f  {: return new Factor1(f); :}; 
opt = fac  | LPAREN expr RPAREN ; 

cexpr =  binary|memexpr ;
functioncall = idcexpr.id LPAREN chCall.e  RPAREN {: return new functionCall(id,(e)); :};
chCall = {: return new Opt(); :}
	|optexpr ;
optexpr = expr.a param_opt.t {: return new Opt(new optExpr(a,t)); :}  ;

param_opt = 	{: return new List(); :}
	| parameter_list;
parameter_list =
	COMMA expr.e {: return new List(e); :}
	| parameter_list.list COMMA expr.e {: return list.add(e); :} ;

idcexpr = id_use.id {: return new idCexpr(id); :};
binary = eq | ne | lt | gt | le | ge | arithmetic_expr;
arithmetic_expr = add | sub | term;
term = mul | div | mod |factor  | functioncall;
factor =  idcexpr| numeral| unary_minus | tempLit ;
unary_minus = MINUS factor.a {: return new UnaryMinus(a); :} ;


tempLit = truestmt | tem2;
tem2= falsestmt | tem3;
tem3 = nonestmt | tem4 ;
tem4 = stringstmt;
truestmt = TRUE {: return new trueStmt(); :} ;
falsestmt =FALSE {: return new falseStmt(); :} ;
nonestmt = NONE {: return new noneStmt(); :} ;
stringstmt = STR ID.a STR {: return new stringStmt(a); :} ;

numeral = NUMERAL.a {: return new Numeral(a); :} ;
id_decl = TYPE ID.id {: return new IdDecl(id); :} ;
id_use = ID.id {: return new IdUse(id); :} ;
target = id_use.id {: return new Target(id); :} ;

add = arithmetic_expr.a PLUS term.b {: return new Add(a, b); :} ;
sub = arithmetic_expr.a MINUS term.b {: return new Sub(a, b); :} ;
mul = term.a MUL factor.b {: return new Mul(a, b); :} ;
div = term.a DIV factor.b {: return new Div(a, b); :} ;
mod = term.a MOD factor.b {: return new Mod(a, b); :} ;

eq = arithmetic_expr.a EQ factor.b {: return new Eq(a, b); :} ;
ne = arithmetic_expr.a NE factor.b {: return new Ne(a, b); :} ;
lt = arithmetic_expr.a LT factor.b {: return new Lt(a, b); :} ;
gt = arithmetic_expr.a GT factor.b {: return new Gt(a, b); :} ;
le = arithmetic_expr.a LE factor.b {: return new Le(a, b); :} ;
ge = arithmetic_expr.a GE factor.b {: return new Ge(a, b); :} ;