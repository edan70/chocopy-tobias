/* This file was generated with JastAdd2 (http://jastadd.org) version 2.3.4 */
package lang.ast;
import java.util.*;
import java.io.PrintStream;
import java.util.Set;
import java.util.TreeSet;
import java.util.ArrayList;
import java.io.ByteArrayOutputStream;
import java.lang.reflect.InvocationTargetException;
import java.util.HashSet;
/**
 * @ast node
 * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/lang.ast:38
 * @astdecl cexpr : Expr;
 * @production cexpr : {@link Expr};

 */
public abstract class cexpr extends Expr implements Cloneable {
  /**
   * @aspect CodeGen
   * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/CodeGen.jrag:507
   */
  public void genEval(PrintStream out, IdDecl id) {
		throw new UnsupportedOperationException();
	}
  /**
   * @aspect CodeGen
   * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/CodeGen.jrag:674
   */
  public void genConditionalJump(PrintStream out,IdDecl id,String falseLabel) {
		throw new UnsupportedOperationException();
	}
  /**
   * @declaredat ASTNode:1
   */
  public cexpr() {
    super();
  }
  /**
   * Initializes the child array to the correct size.
   * Initializes List and Opt nta children.
   * @apilevel internal
   * @ast method
   * @declaredat ASTNode:10
   */
  public void init$Children() {
  }
  /** @apilevel low-level 
   * @declaredat ASTNode:13
   */
  protected int numChildren() {
    return 0;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:17
   */
  public void flushAttrCache() {
    super.flushAttrCache();
    paramse_reset();
    nbrP_reset();
    gettypens_reset();
    decle_reset();
  }
  /** @apilevel internal 
   * @declaredat ASTNode:25
   */
  public void flushCollectionCache() {
    super.flushCollectionCache();

  }
  /** @apilevel internal 
   * @declaredat ASTNode:30
   */
  public cexpr clone() throws CloneNotSupportedException {
    cexpr node = (cexpr) super.clone();
    return node;
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @deprecated Please use treeCopy or treeCopyNoTransform instead
   * @declaredat ASTNode:41
   */
  @Deprecated
  public abstract cexpr fullCopy();
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @declaredat ASTNode:49
   */
  public abstract cexpr treeCopyNoTransform();
  /**
   * Create a deep copy of the AST subtree at this node.
   * The subtree of this node is traversed to trigger rewrites before copy.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @declaredat ASTNode:57
   */
  public abstract cexpr treeCopy();
/** @apilevel internal */
protected boolean paramse_visited = false;
  /** @apilevel internal */
  private void paramse_reset() {
    paramse_computed = false;
    
    paramse_value = null;
    paramse_visited = false;
  }
  /** @apilevel internal */
  protected boolean paramse_computed = false;

  /** @apilevel internal */
  protected List<Expr> paramse_value;

  /**
   * @attribute syn
   * @aspect checkFuncCall
   * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/checkFuncCall.jrag:43
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="checkFuncCall", declaredAt="/home/tobias_carlsson/chocopy-tobias/src/jastadd/checkFuncCall.jrag:43")
  public List<Expr> paramse() {
    ASTState state = state();
    if (paramse_computed) {
      return paramse_value;
    }
    if (paramse_visited) {
      throw new RuntimeException("Circular definition of attribute cexpr.paramse().");
    }
    paramse_visited = true;
    state().enterLazyAttribute();
    paramse_value = new List<Expr>();
    paramse_computed = true;
    state().leaveLazyAttribute();
    paramse_visited = false;
    return paramse_value;
  }
/** @apilevel internal */
protected boolean nbrP_visited = false;
  /** @apilevel internal */
  private void nbrP_reset() {
    nbrP_computed = false;
    nbrP_visited = false;
  }
  /** @apilevel internal */
  protected boolean nbrP_computed = false;

  /** @apilevel internal */
  protected int nbrP_value;

  /**
   * @attribute syn
   * @aspect checkFuncCall
   * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/checkFuncCall.jrag:63
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="checkFuncCall", declaredAt="/home/tobias_carlsson/chocopy-tobias/src/jastadd/checkFuncCall.jrag:63")
  public int nbrP() {
    ASTState state = state();
    if (nbrP_computed) {
      return nbrP_value;
    }
    if (nbrP_visited) {
      throw new RuntimeException("Circular definition of attribute cexpr.nbrP().");
    }
    nbrP_visited = true;
    state().enterLazyAttribute();
    nbrP_value = 0;
    nbrP_computed = true;
    state().leaveLazyAttribute();
    nbrP_visited = false;
    return nbrP_value;
  }
/** @apilevel internal */
protected boolean gettypens_visited = false;
  /** @apilevel internal */
  private void gettypens_reset() {
    gettypens_computed = false;
    
    gettypens_value = null;
    gettypens_visited = false;
  }
  /** @apilevel internal */
  protected boolean gettypens_computed = false;

  /** @apilevel internal */
  protected HashMap<Integer, String> gettypens_value;

  /**
   * @attribute syn
   * @aspect NameAnalysis
   * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/nameanalysis.jrag:37
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="NameAnalysis", declaredAt="/home/tobias_carlsson/chocopy-tobias/src/jastadd/nameanalysis.jrag:37")
  public HashMap<Integer, String> gettypens() {
    ASTState state = state();
    if (gettypens_computed) {
      return gettypens_value;
    }
    if (gettypens_visited) {
      throw new RuntimeException("Circular definition of attribute cexpr.gettypens().");
    }
    gettypens_visited = true;
    state().enterLazyAttribute();
    gettypens_value = new  HashMap<Integer, String>();
    gettypens_computed = true;
    state().leaveLazyAttribute();
    gettypens_visited = false;
    return gettypens_value;
  }
  /**
   * @attribute inh
   * @aspect paramAdrInd
   * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/paramAdrInd.jrag:86
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.INH)
  @ASTNodeAnnotation.Source(aspect="paramAdrInd", declaredAt="/home/tobias_carlsson/chocopy-tobias/src/jastadd/paramAdrInd.jrag:86")
  public IdDecl decle() {
    ASTState state = state();
    if (decle_computed) {
      return decle_value;
    }
    if (decle_visited) {
      throw new RuntimeException("Circular definition of attribute cexpr.decle().");
    }
    decle_visited = true;
    state().enterLazyAttribute();
    decle_value = getParent().Define_decle(this, null);
    decle_computed = true;
    state().leaveLazyAttribute();
    decle_visited = false;
    return decle_value;
  }
/** @apilevel internal */
protected boolean decle_visited = false;
  /** @apilevel internal */
  private void decle_reset() {
    decle_computed = false;
    
    decle_value = null;
    decle_visited = false;
  }
  /** @apilevel internal */
  protected boolean decle_computed = false;

  /** @apilevel internal */
  protected IdDecl decle_value;


}
