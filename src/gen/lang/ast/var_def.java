/* This file was generated with JastAdd2 (http://jastadd.org) version 2.3.4 */
package lang.ast;
import java.util.*;
import java.io.PrintStream;
import java.util.Set;
import java.util.TreeSet;
import java.util.ArrayList;
import java.io.ByteArrayOutputStream;
import java.lang.reflect.InvocationTargetException;
import java.util.HashSet;
/**
 * @ast node
 * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/lang.ast:4
 * @astdecl var_def : vfc ::= typedVar cexpr;
 * @production var_def : {@link vfc} ::= <span class="component">{@link typedVar}</span> <span class="component">{@link cexpr}</span>;

 */
public class var_def extends vfc implements Cloneable {
  /**
   * @aspect CodeGen
   * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/CodeGen.jrag:271
   */
  public void genCode(PrintStream out, IdDecl id) {

		if(gettypedVar().getTypen().toString().equals("objecttype")){

			typedVar tp = gettypedVar();

			int count =tp.getIdDecl().nbrLoc()-1;
			class_def c = lookupCl(gettypedVar().getTypen().idif());
			for(Map.Entry mapElement : tp.getIdDecl().objvars().entrySet()){
				int ind = (int)mapElement.getValue(); 
				var_def vd = (var_def)mapElement.getKey(); 
				out.println("        subq $" + (1*8) + ", %rsp");
				vd.getcexpr().genEval(out,gettypedVar().getIdDecl());
				out.println("        movq %rax, " +"-"+((ind)*8)+"(%rbp)"+
						"	# Local var:  "+", instance of: ");
			}
	
			c = lookupCl(gettypedVar().getTypen().idif());



			if(getcexpr().nbrP()>0){
				for(Expr e: getcexpr().paramse()){
					e.genEval(out,gettypedVar().getIdDecl());
					out.println("        pushq %rax");

				}
			out.println("        call " +gettypedVar().getIdDecl().getID()+(gettypedVar().getTypen().idif())+ "__init__");
			out.println("        addq $" + ((getcexpr().nbrP()) * 8) + ", %rsp");

			}



			out.println("        jmp "  + gettypedVar().getIdDecl().getID()+"ejump");
			for(func_def fd : c.getclass_body().getoptfuncs()){
				out.println( gettypedVar().getIdDecl().getID() +c.getIdDecl().getID() +fd.getIdDecl().getID() + ':');

							out.println("        pushq %R15");
			out.println("        movq %rsp, %R15");
				func_body fb = fd.getfunc_body();
				for(var_def p : fb.getlocdefs()){
					for(Map.Entry mapElement : tp.getIdDecl().objvars().entrySet()){
						int ind = (int)mapElement.getValue(); 
						var_def vd = (var_def)mapElement.getKey(); 
						if(vd.gettypedVar().getIdDecl().getID().equals(p.gettypedVar().getIdDecl().getID())){
							p.getcexpr().genEval(out,gettypedVar().getIdDecl());
							out.println("        movq %rax, " +"-"+((ind)*8)+"(%rbp)");
						}
					}
				}

				fd.getfunc_body().getstmt().genCode(out,gettypedVar().getIdDecl());
				for (stmt s : fd.getfunc_body().getoptListList()) {
					s.genCode(out,gettypedVar().getIdDecl());

				}
				out.println("        movq $0, %rax");
				out.println(gettypedVar().getIdDecl().getID()+fd.getIdDecl().getID() + "_ret:");
				out.println("        popq %R15");
				out.println("        ret");
			}


			out.println(gettypedVar().getIdDecl().getID()+"ejump:");
		}
		else{
			out.println("        subq $" + (1*8) + ", %rsp");
			getcexpr().genEval(out,gettypedVar().getIdDecl());
			if(insideCl()&& gettypedVar().getIdDecl().declInFunc()){
				out.println("        movq %rax, " + gettypedVar().getIdDecl().address());

			}
			else if(insideCl()){
				out.println("        movq %rax, " + gettypedVar().getIdDecl().addressC());
			}
			else if(types()==objType()){
			}
			else{
				out.println("        movq %rax, " + gettypedVar().getIdDecl().address());
			}
		}


	}
  /**
   * @aspect NameAnalysis
   * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/nameanalysis.jrag:139
   */
  public IdDecl lookup(String name) {
		if (gettypedVar().getIdDecl().getID().equals(name)) {
			return gettypedVar().getIdDecl();
		}
		return unknownDecl();
	}
  /**
   * @declaredat ASTNode:1
   */
  public var_def() {
    super();
  }
  /**
   * Initializes the child array to the correct size.
   * Initializes List and Opt nta children.
   * @apilevel internal
   * @ast method
   * @declaredat ASTNode:10
   */
  public void init$Children() {
    children = new ASTNode[2];
  }
  /**
   * @declaredat ASTNode:13
   */
  @ASTNodeAnnotation.Constructor(
    name = {"typedVar", "cexpr"},
    type = {"typedVar", "cexpr"},
    kind = {"Child", "Child"}
  )
  public var_def(typedVar p0, cexpr p1) {
    setChild(p0, 0);
    setChild(p1, 1);
  }
  /** @apilevel low-level 
   * @declaredat ASTNode:23
   */
  protected int numChildren() {
    return 2;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:27
   */
  public void flushAttrCache() {
    super.flushAttrCache();
    okayClassD_reset();
    okayClassDFc_reset();
    type_reset();
    current_reset();
    expectedType_reset();
    types_reset();
    lookupCl_String_reset();
    lookupCl_String_reset();
    globalLookup_String_reset();
    localIndexc_reset();
    insideFunc_reset();
    insideClass_reset();
    localIndexe_reset();
    insideCl_reset();
  }
  /** @apilevel internal 
   * @declaredat ASTNode:45
   */
  public void flushCollectionCache() {
    super.flushCollectionCache();

  }
  /** @apilevel internal 
   * @declaredat ASTNode:50
   */
  public var_def clone() throws CloneNotSupportedException {
    var_def node = (var_def) super.clone();
    return node;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:55
   */
  public var_def copy() {
    try {
      var_def node = (var_def) clone();
      node.parent = null;
      if (children != null) {
        node.children = (ASTNode[]) children.clone();
      }
      return node;
    } catch (CloneNotSupportedException e) {
      throw new Error("Error: clone not supported for " + getClass().getName());
    }
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @deprecated Please use treeCopy or treeCopyNoTransform instead
   * @declaredat ASTNode:74
   */
  @Deprecated
  public var_def fullCopy() {
    return treeCopyNoTransform();
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @declaredat ASTNode:84
   */
  public var_def treeCopyNoTransform() {
    var_def tree = (var_def) copy();
    if (children != null) {
      for (int i = 0; i < children.length; ++i) {
        ASTNode child = (ASTNode) children[i];
        if (child != null) {
          child = child.treeCopyNoTransform();
          tree.setChild(child, i);
        }
      }
    }
    return tree;
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The subtree of this node is traversed to trigger rewrites before copy.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @declaredat ASTNode:104
   */
  public var_def treeCopy() {
    var_def tree = (var_def) copy();
    if (children != null) {
      for (int i = 0; i < children.length; ++i) {
        ASTNode child = (ASTNode) getChild(i);
        if (child != null) {
          child = child.treeCopy();
          tree.setChild(child, i);
        }
      }
    }
    return tree;
  }
  /**
   * Replaces the typedVar child.
   * @param node The new node to replace the typedVar child.
   * @apilevel high-level
   */
  public var_def settypedVar(typedVar node) {
    setChild(node, 0);
    return this;
  }
  /**
   * Retrieves the typedVar child.
   * @return The current node used as the typedVar child.
   * @apilevel high-level
   */
  @ASTNodeAnnotation.Child(name="typedVar")
  public typedVar gettypedVar() {
    return (typedVar) getChild(0);
  }
  /**
   * Retrieves the typedVar child.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The current node used as the typedVar child.
   * @apilevel low-level
   */
  public typedVar gettypedVarNoTransform() {
    return (typedVar) getChildNoTransform(0);
  }
  /**
   * Replaces the cexpr child.
   * @param node The new node to replace the cexpr child.
   * @apilevel high-level
   */
  public var_def setcexpr(cexpr node) {
    setChild(node, 1);
    return this;
  }
  /**
   * Retrieves the cexpr child.
   * @return The current node used as the cexpr child.
   * @apilevel high-level
   */
  @ASTNodeAnnotation.Child(name="cexpr")
  public cexpr getcexpr() {
    return (cexpr) getChild(1);
  }
  /**
   * Retrieves the cexpr child.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The current node used as the cexpr child.
   * @apilevel low-level
   */
  public cexpr getcexprNoTransform() {
    return (cexpr) getChildNoTransform(1);
  }
/** @apilevel internal */
protected boolean okayClassD_visited = false;
  /** @apilevel internal */
  private void okayClassD_reset() {
    okayClassD_computed = false;
    okayClassD_visited = false;
  }
  /** @apilevel internal */
  protected boolean okayClassD_computed = false;

  /** @apilevel internal */
  protected boolean okayClassD_value;

  /**
   * @attribute syn
   * @aspect NameAnalysis
   * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/nameanalysis.jrag:27
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="NameAnalysis", declaredAt="/home/tobias_carlsson/chocopy-tobias/src/jastadd/nameanalysis.jrag:27")
  public boolean okayClassD() {
    ASTState state = state();
    if (okayClassD_computed) {
      return okayClassD_value;
    }
    if (okayClassD_visited) {
      throw new RuntimeException("Circular definition of attribute var_def.okayClassD().");
    }
    okayClassD_visited = true;
    state().enterLazyAttribute();
    okayClassD_value = okayClassD_compute();
    okayClassD_computed = true;
    state().leaveLazyAttribute();
    okayClassD_visited = false;
    return okayClassD_value;
  }
  /** @apilevel internal */
  private boolean okayClassD_compute() {
  		if(!types().toString().equals("objecttype") ){
  			return true;
  		}
  		IdDecl idc = gettypedVar().getIdDecl();
  		int nbrC = getcexpr().nbrP();
  		int nbrCl = globalLookup(gettypedVar().getTypen().idif()).nbrInit();
  		return nbrC == nbrCl;
  	}
/** @apilevel internal */
protected boolean okayClassDFc_visited = false;
  /** @apilevel internal */
  private void okayClassDFc_reset() {
    okayClassDFc_computed = false;
    okayClassDFc_visited = false;
  }
  /** @apilevel internal */
  protected boolean okayClassDFc_computed = false;

  /** @apilevel internal */
  protected boolean okayClassDFc_value;

  /**
   * @attribute syn
   * @aspect NameAnalysis
   * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/nameanalysis.jrag:55
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="NameAnalysis", declaredAt="/home/tobias_carlsson/chocopy-tobias/src/jastadd/nameanalysis.jrag:55")
  public boolean okayClassDFc() {
    ASTState state = state();
    if (okayClassDFc_computed) {
      return okayClassDFc_value;
    }
    if (okayClassDFc_visited) {
      throw new RuntimeException("Circular definition of attribute var_def.okayClassDFc().");
    }
    okayClassDFc_visited = true;
    state().enterLazyAttribute();
    okayClassDFc_value = okayClassDFc_compute();
    okayClassDFc_computed = true;
    state().leaveLazyAttribute();
    okayClassDFc_visited = false;
    return okayClassDFc_value;
  }
  /** @apilevel internal */
  private boolean okayClassDFc_compute() {
  		if(!types().toString().equals("objecttype") ){
  			return true;
  		}
  		IdDecl idc = gettypedVar().getIdDecl();
  		HashMap<Integer, String> hash_map = getcexpr().gettypens();
  
  		int nbrC = getcexpr().nbrP();
  		class_def cld = lookupCl(gettypedVar().getTypen().idif());
  		class_body cb = cld.getclass_body();
  		int count = 0;
  		for(func_def fb : cb.getoptfuncs()){
  			if(fb.getIdDecl().getID().equals("__init__")){
  				if(fb.hasopttypedVar()){
  					opttypedVar v = fb.getopttypedVar();
  					if(!v.gettypedVar().getTypen().toString().equals(hash_map.get(count))){
  						return false;
  					}
  					count++;
  				}
  			}
  		}
  
  		return true;
  	}
/** @apilevel internal */
protected boolean type_visited = false;
  /** @apilevel internal */
  private void type_reset() {
    type_computed = false;
    
    type_value = null;
    type_visited = false;
  }
  /** @apilevel internal */
  protected boolean type_computed = false;

  /** @apilevel internal */
  protected String type_value;

  /**
   * @attribute syn
   * @aspect NameAnalysis
   * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/nameanalysis.jrag:182
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="NameAnalysis", declaredAt="/home/tobias_carlsson/chocopy-tobias/src/jastadd/nameanalysis.jrag:182")
  public String type() {
    ASTState state = state();
    if (type_computed) {
      return type_value;
    }
    if (type_visited) {
      throw new RuntimeException("Circular definition of attribute var_def.type().");
    }
    type_visited = true;
    state().enterLazyAttribute();
    type_value = "vardef";
    type_computed = true;
    state().leaveLazyAttribute();
    type_visited = false;
    return type_value;
  }
/** @apilevel internal */
protected boolean current_visited = false;
  /** @apilevel internal */
  private void current_reset() {
    current_computed = false;
    
    current_value = null;
    current_visited = false;
  }
  /** @apilevel internal */
  protected boolean current_computed = false;

  /** @apilevel internal */
  protected Typen current_value;

  /**
   * @attribute syn
   * @aspect NameAnalysis
   * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/nameanalysis.jrag:188
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="NameAnalysis", declaredAt="/home/tobias_carlsson/chocopy-tobias/src/jastadd/nameanalysis.jrag:188")
  public Typen current() {
    ASTState state = state();
    if (current_computed) {
      return current_value;
    }
    if (current_visited) {
      throw new RuntimeException("Circular definition of attribute var_def.current().");
    }
    current_visited = true;
    state().enterLazyAttribute();
    current_value = gettypedVar().getTypen();
    current_computed = true;
    state().leaveLazyAttribute();
    current_visited = false;
    return current_value;
  }
/** @apilevel internal */
protected boolean expectedType_visited = false;
  /** @apilevel internal */
  private void expectedType_reset() {
    expectedType_computed = false;
    
    expectedType_value = null;
    expectedType_visited = false;
  }
  /** @apilevel internal */
  protected boolean expectedType_computed = false;

  /** @apilevel internal */
  protected Typen expectedType_value;

  /**
   * @attribute syn
   * @aspect typeChecker
   * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/typeChecker.jrag:112
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="typeChecker", declaredAt="/home/tobias_carlsson/chocopy-tobias/src/jastadd/typeChecker.jrag:112")
  public Typen expectedType() {
    ASTState state = state();
    if (expectedType_computed) {
      return expectedType_value;
    }
    if (expectedType_visited) {
      throw new RuntimeException("Circular definition of attribute var_def.expectedType().");
    }
    expectedType_visited = true;
    state().enterLazyAttribute();
    expectedType_value = gettypedVar().getTypen();
    expectedType_computed = true;
    state().leaveLazyAttribute();
    expectedType_visited = false;
    return expectedType_value;
  }
/** @apilevel internal */
protected boolean types_visited = false;
  /** @apilevel internal */
  private void types_reset() {
    types_computed = false;
    
    types_value = null;
    types_visited = false;
  }
  /** @apilevel internal */
  protected boolean types_computed = false;

  /** @apilevel internal */
  protected Typen types_value;

  /**
   * @attribute syn
   * @aspect typeChecker
   * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/typeChecker.jrag:113
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="typeChecker", declaredAt="/home/tobias_carlsson/chocopy-tobias/src/jastadd/typeChecker.jrag:113")
  public Typen types() {
    ASTState state = state();
    if (types_computed) {
      return types_value;
    }
    if (types_visited) {
      throw new RuntimeException("Circular definition of attribute var_def.types().");
    }
    types_visited = true;
    state().enterLazyAttribute();
    types_value = getcexpr().types();
    types_computed = true;
    state().leaveLazyAttribute();
    types_visited = false;
    return types_value;
  }
  /**
   * @attribute inh
   * @aspect NameAnalysis
   * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/nameanalysis.jrag:12
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.INH)
  @ASTNodeAnnotation.Source(aspect="NameAnalysis", declaredAt="/home/tobias_carlsson/chocopy-tobias/src/jastadd/nameanalysis.jrag:12")
  public class_def lookupCl(String name) {
    Object _parameters = name;
    if (lookupCl_String_visited == null) lookupCl_String_visited = new java.util.HashSet(4);
    if (lookupCl_String_values == null) lookupCl_String_values = new java.util.HashMap(4);
    ASTState state = state();
    if (lookupCl_String_values.containsKey(_parameters)) {
      return (class_def) lookupCl_String_values.get(_parameters);
    }
    if (lookupCl_String_visited.contains(_parameters)) {
      throw new RuntimeException("Circular definition of attribute var_def.lookupCl(String).");
    }
    lookupCl_String_visited.add(_parameters);
    state().enterLazyAttribute();
    class_def lookupCl_String_value = getParent().Define_lookupCl(this, null, name);
    lookupCl_String_values.put(_parameters, lookupCl_String_value);
    state().leaveLazyAttribute();
    lookupCl_String_visited.remove(_parameters);
    return lookupCl_String_value;
  }
/** @apilevel internal */
protected java.util.Set lookupCl_String_visited;
  /** @apilevel internal */
  private void lookupCl_String_reset() {
    lookupCl_String_values = null;
    lookupCl_String_visited = null;
  }
  /** @apilevel internal */
  protected java.util.Map lookupCl_String_values;

  /**
   * @attribute inh
   * @aspect NameAnalysis
   * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/nameanalysis.jrag:331
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.INH)
  @ASTNodeAnnotation.Source(aspect="NameAnalysis", declaredAt="/home/tobias_carlsson/chocopy-tobias/src/jastadd/nameanalysis.jrag:331")
  public IdDecl globalLookup(String name) {
    Object _parameters = name;
    if (globalLookup_String_visited == null) globalLookup_String_visited = new java.util.HashSet(4);
    if (globalLookup_String_values == null) globalLookup_String_values = new java.util.HashMap(4);
    ASTState state = state();
    if (globalLookup_String_values.containsKey(_parameters)) {
      return (IdDecl) globalLookup_String_values.get(_parameters);
    }
    if (globalLookup_String_visited.contains(_parameters)) {
      throw new RuntimeException("Circular definition of attribute var_def.globalLookup(String).");
    }
    globalLookup_String_visited.add(_parameters);
    state().enterLazyAttribute();
    IdDecl globalLookup_String_value = getParent().Define_globalLookup(this, null, name);
    globalLookup_String_values.put(_parameters, globalLookup_String_value);
    state().leaveLazyAttribute();
    globalLookup_String_visited.remove(_parameters);
    return globalLookup_String_value;
  }
/** @apilevel internal */
protected java.util.Set globalLookup_String_visited;
  /** @apilevel internal */
  private void globalLookup_String_reset() {
    globalLookup_String_values = null;
    globalLookup_String_visited = null;
  }
  /** @apilevel internal */
  protected java.util.Map globalLookup_String_values;

  /**
   * @attribute inh
   * @aspect paramAdrInd
   * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/paramAdrInd.jrag:49
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.INH)
  @ASTNodeAnnotation.Source(aspect="paramAdrInd", declaredAt="/home/tobias_carlsson/chocopy-tobias/src/jastadd/paramAdrInd.jrag:49")
  public int localIndexc() {
    ASTState state = state();
    if (localIndexc_computed) {
      return localIndexc_value;
    }
    if (localIndexc_visited) {
      throw new RuntimeException("Circular definition of attribute var_def.localIndexc().");
    }
    localIndexc_visited = true;
    state().enterLazyAttribute();
    localIndexc_value = getParent().Define_localIndexc(this, null);
    localIndexc_computed = true;
    state().leaveLazyAttribute();
    localIndexc_visited = false;
    return localIndexc_value;
  }
/** @apilevel internal */
protected boolean localIndexc_visited = false;
  /** @apilevel internal */
  private void localIndexc_reset() {
    localIndexc_computed = false;
    localIndexc_visited = false;
  }
  /** @apilevel internal */
  protected boolean localIndexc_computed = false;

  /** @apilevel internal */
  protected int localIndexc_value;

  /**
   * @attribute inh
   * @aspect paramAdrInd
   * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/paramAdrInd.jrag:65
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.INH)
  @ASTNodeAnnotation.Source(aspect="paramAdrInd", declaredAt="/home/tobias_carlsson/chocopy-tobias/src/jastadd/paramAdrInd.jrag:65")
  public boolean insideFunc() {
    ASTState state = state();
    if (insideFunc_computed) {
      return insideFunc_value;
    }
    if (insideFunc_visited) {
      throw new RuntimeException("Circular definition of attribute var_def.insideFunc().");
    }
    insideFunc_visited = true;
    state().enterLazyAttribute();
    insideFunc_value = getParent().Define_insideFunc(this, null);
    insideFunc_computed = true;
    state().leaveLazyAttribute();
    insideFunc_visited = false;
    return insideFunc_value;
  }
/** @apilevel internal */
protected boolean insideFunc_visited = false;
  /** @apilevel internal */
  private void insideFunc_reset() {
    insideFunc_computed = false;
    insideFunc_visited = false;
  }
  /** @apilevel internal */
  protected boolean insideFunc_computed = false;

  /** @apilevel internal */
  protected boolean insideFunc_value;

  /**
   * @attribute inh
   * @aspect paramAdrInd
   * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/paramAdrInd.jrag:72
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.INH)
  @ASTNodeAnnotation.Source(aspect="paramAdrInd", declaredAt="/home/tobias_carlsson/chocopy-tobias/src/jastadd/paramAdrInd.jrag:72")
  public boolean insideClass() {
    ASTState state = state();
    if (insideClass_computed) {
      return insideClass_value;
    }
    if (insideClass_visited) {
      throw new RuntimeException("Circular definition of attribute var_def.insideClass().");
    }
    insideClass_visited = true;
    state().enterLazyAttribute();
    insideClass_value = getParent().Define_insideClass(this, null);
    insideClass_computed = true;
    state().leaveLazyAttribute();
    insideClass_visited = false;
    return insideClass_value;
  }
/** @apilevel internal */
protected boolean insideClass_visited = false;
  /** @apilevel internal */
  private void insideClass_reset() {
    insideClass_computed = false;
    insideClass_visited = false;
  }
  /** @apilevel internal */
  protected boolean insideClass_computed = false;

  /** @apilevel internal */
  protected boolean insideClass_value;

  /**
   * @attribute inh
   * @aspect paramAdrInd
   * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/paramAdrInd.jrag:81
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.INH)
  @ASTNodeAnnotation.Source(aspect="paramAdrInd", declaredAt="/home/tobias_carlsson/chocopy-tobias/src/jastadd/paramAdrInd.jrag:81")
  public int localIndexe() {
    ASTState state = state();
    if (localIndexe_computed) {
      return localIndexe_value;
    }
    if (localIndexe_visited) {
      throw new RuntimeException("Circular definition of attribute var_def.localIndexe().");
    }
    localIndexe_visited = true;
    state().enterLazyAttribute();
    localIndexe_value = getParent().Define_localIndexe(this, null);
    localIndexe_computed = true;
    state().leaveLazyAttribute();
    localIndexe_visited = false;
    return localIndexe_value;
  }
/** @apilevel internal */
protected boolean localIndexe_visited = false;
  /** @apilevel internal */
  private void localIndexe_reset() {
    localIndexe_computed = false;
    localIndexe_visited = false;
  }
  /** @apilevel internal */
  protected boolean localIndexe_computed = false;

  /** @apilevel internal */
  protected int localIndexe_value;

  /**
   * @attribute inh
   * @aspect NameAnalysis
   * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/rndFix.jrag:11
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.INH)
  @ASTNodeAnnotation.Source(aspect="NameAnalysis", declaredAt="/home/tobias_carlsson/chocopy-tobias/src/jastadd/rndFix.jrag:11")
  public boolean insideCl() {
    ASTState state = state();
    if (insideCl_computed) {
      return insideCl_value;
    }
    if (insideCl_visited) {
      throw new RuntimeException("Circular definition of attribute var_def.insideCl().");
    }
    insideCl_visited = true;
    state().enterLazyAttribute();
    insideCl_value = getParent().Define_insideCl(this, null);
    insideCl_computed = true;
    state().leaveLazyAttribute();
    insideCl_visited = false;
    return insideCl_value;
  }
/** @apilevel internal */
protected boolean insideCl_visited = false;
  /** @apilevel internal */
  private void insideCl_reset() {
    insideCl_computed = false;
    insideCl_visited = false;
  }
  /** @apilevel internal */
  protected boolean insideCl_computed = false;

  /** @apilevel internal */
  protected boolean insideCl_value;

  /**
   * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/paramAdrInd.jrag:29
   * @apilevel internal
   */
  public int Define_localIndex(ASTNode _callerNode, ASTNode _childNode) {
    int childIndex = this.getIndexOfChild(_callerNode);
    {
    		if(insideFunc()&&insideClass()){
    			return  localIndex();
    		}
    		else if(types()==objType()){
    			return  gettypedVar().getIdDecl().nbrLoc()+gettypedVar().getIdDecl().nbrLocF()+prevNode().localIndex() ;
    		}
    		return  prevNode().localIndex() +1;
    	}
  }
  /**
   * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/paramAdrInd.jrag:29
   * @apilevel internal
   * @return {@code true} if this node has an equation for the inherited attribute localIndex
   */
  protected boolean canDefine_localIndex(ASTNode _callerNode, ASTNode _childNode) {
    return true;
  }
  /**
   * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/paramAdrInd.jrag:59
   * @apilevel internal
   */
  public boolean Define_declInFunc(ASTNode _callerNode, ASTNode _childNode) {
    int childIndex = this.getIndexOfChild(_callerNode);
    return insideFunc();
  }
  /**
   * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/paramAdrInd.jrag:59
   * @apilevel internal
   * @return {@code true} if this node has an equation for the inherited attribute declInFunc
   */
  protected boolean canDefine_declInFunc(ASTNode _callerNode, ASTNode _childNode) {
    return true;
  }
  /**
   * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/paramAdrInd.jrag:90
   * @apilevel internal
   */
  public int Define_nbrLoc(ASTNode _callerNode, ASTNode _childNode) {
    int childIndex = this.getIndexOfChild(_callerNode);
    {
    		if(gettypedVar().getTypen().toString().equals("objecttype")){
    			return getcexpr().decle().nbrLoc();
    		}
    		return 0;
    	}
  }
  /**
   * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/paramAdrInd.jrag:90
   * @apilevel internal
   * @return {@code true} if this node has an equation for the inherited attribute nbrLoc
   */
  protected boolean canDefine_nbrLoc(ASTNode _callerNode, ASTNode _childNode) {
    return true;
  }
  /**
   * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/paramAdrInd.jrag:106
   * @apilevel internal
   */
  public int Define_nbrLocF(ASTNode _callerNode, ASTNode _childNode) {
    int childIndex = this.getIndexOfChild(_callerNode);
    {
    		if(gettypedVar().getTypen().toString().equals("objecttype")){
    			return getcexpr().decle().nbrLocF();
    		}
    		return 0;
    	}
  }
  /**
   * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/paramAdrInd.jrag:106
   * @apilevel internal
   * @return {@code true} if this node has an equation for the inherited attribute nbrLocF
   */
  protected boolean canDefine_nbrLocF(ASTNode _callerNode, ASTNode _childNode) {
    return true;
  }
  /**
   * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/typeChecker.jrag:21
   * @apilevel internal
   */
  public Typen Define_types(ASTNode _callerNode, ASTNode _childNode) {
    if (_callerNode == gettypedVarNoTransform()) {
      // @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/typeChecker.jrag:23
      return gettypedVar().getTypen();
    }
    else {
      return getParent().Define_types(this, _callerNode);
    }
  }
  /**
   * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/typeChecker.jrag:21
   * @apilevel internal
   * @return {@code true} if this node has an equation for the inherited attribute types
   */
  protected boolean canDefine_types(ASTNode _callerNode, ASTNode _childNode) {
    return true;
  }
  /** @apilevel internal */
  protected void collect_contributors_Program_errors(Program _root, java.util.Map<ASTNode, java.util.Set<ASTNode>> _map) {
    // @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/Errors.jrag:60
    if (!okayClassD()) {
      {
        Program target = (Program) (program());
        java.util.Set<ASTNode> contributors = _map.get(target);
        if (contributors == null) {
          contributors = new java.util.LinkedHashSet<ASTNode>();
          _map.put((ASTNode) target, contributors);
        }
        contributors.add(this);
      }
    }
    // @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/Errors.jrag:63
    if (!okayClassDFc()) {
      {
        Program target = (Program) (program());
        java.util.Set<ASTNode> contributors = _map.get(target);
        if (contributors == null) {
          contributors = new java.util.LinkedHashSet<ASTNode>();
          _map.put((ASTNode) target, contributors);
        }
        contributors.add(this);
      }
    }
    // @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/Errors.jrag:124
    if (!types().compatibleType(expectedType())) {
      {
        Program target = (Program) (program());
        java.util.Set<ASTNode> contributors = _map.get(target);
        if (contributors == null) {
          contributors = new java.util.LinkedHashSet<ASTNode>();
          _map.put((ASTNode) target, contributors);
        }
        contributors.add(this);
      }
    }
    super.collect_contributors_Program_errors(_root, _map);
  }
  /** @apilevel internal */
  protected void contributeTo_Program_errors(Set<ErrorMessage> collection) {
    super.contributeTo_Program_errors(collection);
    if (!okayClassD()) {
      collection.add(error("Number of parameters when inititalizing: " + gettypedVar().getIdDecl().getID()+", for class: "+ gettypedVar().getTypen().idif() + ", is wrong." ));
    }
    if (!okayClassDFc()) {
      collection.add(error("Type of parameters when inititalizing: " + gettypedVar().getIdDecl().getID()+", for class: "+ gettypedVar().getTypen().idif() + ", is wrong." ));
    }
    if (!types().compatibleType(expectedType())) {
      collection.add(error("Expected " + expectedType() + " but received " + types()));
    }
  }

}
