/* This file was generated with JastAdd2 (http://jastadd.org) version 2.3.4 */
package lang.ast;
import java.util.*;
import java.io.PrintStream;
import java.util.Set;
import java.util.TreeSet;
import java.util.ArrayList;
import java.io.ByteArrayOutputStream;
import java.lang.reflect.InvocationTargetException;
import java.util.HashSet;
/**
 * @ast node
 * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/lang.ast:18
 * @astdecl UnknownType : Typen;
 * @production UnknownType : {@link Typen};

 */
public class UnknownType extends Typen implements Cloneable {
  /**
   * @aspect UnknownType
   * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/Typen.jrag:48
   */
  public String toString() {
		return "<unknown>";
	}
  /**
   * @declaredat ASTNode:1
   */
  public UnknownType() {
    super();
  }
  /**
   * Initializes the child array to the correct size.
   * Initializes List and Opt nta children.
   * @apilevel internal
   * @ast method
   * @declaredat ASTNode:10
   */
  public void init$Children() {
  }
  /** @apilevel low-level 
   * @declaredat ASTNode:13
   */
  protected int numChildren() {
    return 0;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:17
   */
  public void flushAttrCache() {
    super.flushAttrCache();
    compatibleType_Typen_reset();
  }
  /** @apilevel internal 
   * @declaredat ASTNode:22
   */
  public void flushCollectionCache() {
    super.flushCollectionCache();

  }
  /** @apilevel internal 
   * @declaredat ASTNode:27
   */
  public UnknownType clone() throws CloneNotSupportedException {
    UnknownType node = (UnknownType) super.clone();
    return node;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:32
   */
  public UnknownType copy() {
    try {
      UnknownType node = (UnknownType) clone();
      node.parent = null;
      if (children != null) {
        node.children = (ASTNode[]) children.clone();
      }
      return node;
    } catch (CloneNotSupportedException e) {
      throw new Error("Error: clone not supported for " + getClass().getName());
    }
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @deprecated Please use treeCopy or treeCopyNoTransform instead
   * @declaredat ASTNode:51
   */
  @Deprecated
  public UnknownType fullCopy() {
    return treeCopyNoTransform();
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @declaredat ASTNode:61
   */
  public UnknownType treeCopyNoTransform() {
    UnknownType tree = (UnknownType) copy();
    if (children != null) {
      for (int i = 0; i < children.length; ++i) {
        ASTNode child = (ASTNode) children[i];
        if (child != null) {
          child = child.treeCopyNoTransform();
          tree.setChild(child, i);
        }
      }
    }
    return tree;
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The subtree of this node is traversed to trigger rewrites before copy.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @declaredat ASTNode:81
   */
  public UnknownType treeCopy() {
    UnknownType tree = (UnknownType) copy();
    if (children != null) {
      for (int i = 0; i < children.length; ++i) {
        ASTNode child = (ASTNode) getChild(i);
        if (child != null) {
          child = child.treeCopy();
          tree.setChild(child, i);
        }
      }
    }
    return tree;
  }
/** @apilevel internal */
protected java.util.Set compatibleType_Typen_visited;
  /** @apilevel internal */
  private void compatibleType_Typen_reset() {
    compatibleType_Typen_values = null;
    compatibleType_Typen_visited = null;
  }
  /** @apilevel internal */
  protected java.util.Map compatibleType_Typen_values;

  /**
   * @attribute syn
   * @aspect typeChecker
   * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/typeChecker.jrag:116
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="typeChecker", declaredAt="/home/tobias_carlsson/chocopy-tobias/src/jastadd/typeChecker.jrag:116")
  public boolean compatibleType(Typen type) {
    Object _parameters = type;
    if (compatibleType_Typen_visited == null) compatibleType_Typen_visited = new java.util.HashSet(4);
    if (compatibleType_Typen_values == null) compatibleType_Typen_values = new java.util.HashMap(4);
    ASTState state = state();
    if (compatibleType_Typen_values.containsKey(_parameters)) {
      return (Boolean) compatibleType_Typen_values.get(_parameters);
    }
    if (compatibleType_Typen_visited.contains(_parameters)) {
      throw new RuntimeException("Circular definition of attribute UnknownType.compatibleType(Typen).");
    }
    compatibleType_Typen_visited.add(_parameters);
    state().enterLazyAttribute();
    boolean compatibleType_Typen_value = this.toString().equals(type.toString());
    compatibleType_Typen_values.put(_parameters, compatibleType_Typen_value);
    state().leaveLazyAttribute();
    compatibleType_Typen_visited.remove(_parameters);
    return compatibleType_Typen_value;
  }

}
