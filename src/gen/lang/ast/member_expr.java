/* This file was generated with JastAdd2 (http://jastadd.org) version 2.3.4 */
package lang.ast;
import java.util.*;
import java.io.PrintStream;
import java.util.Set;
import java.util.TreeSet;
import java.util.ArrayList;
import java.io.ByteArrayOutputStream;
import java.lang.reflect.InvocationTargetException;
import java.util.HashSet;
/**
 * @ast node
 * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/lang.ast:53
 * @astdecl member_expr : Expr ::= IdUse call:IdUse [optExpr];
 * @production member_expr : {@link Expr} ::= <span class="component">{@link IdUse}</span> <span class="component">call:{@link IdUse}</span> <span class="component">[{@link optExpr}]</span>;

 */
public class member_expr extends Expr implements Cloneable {
  /**
   * @aspect CodeGen
   * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/CodeGen.jrag:510
   */
  public void genEval(PrintStream out, IdDecl id) {		

		if(hasoptExpr()){

				if( getoptExpr().hasOpt()){
					for (int i = getoptExpr().getNumOpt() - 1; i >= 0; i--) {
						getoptExpr().getOpt(i).genEval(out,id);
						out.println("        pushq %rax");
					}
				}
				getoptExpr().getExpr().genEval(out,id);
				out.println("        pushq %rax");

			}



		out.println("        call " +getIdUse().getID()+globalLookup(getIdUse().getID()).types().idif()+ getcall().getID());


		if(hasoptExpr()){
				out.println("        addq $" + ((getoptExpr().getNumOpt()+1) * 8) + ", %rsp");
			}


	}
  /**
   * @declaredat ASTNode:1
   */
  public member_expr() {
    super();
  }
  /**
   * Initializes the child array to the correct size.
   * Initializes List and Opt nta children.
   * @apilevel internal
   * @ast method
   * @declaredat ASTNode:10
   */
  public void init$Children() {
    children = new ASTNode[3];
    setChild(new Opt(), 2);
  }
  /**
   * @declaredat ASTNode:14
   */
  @ASTNodeAnnotation.Constructor(
    name = {"IdUse", "call", "optExpr"},
    type = {"IdUse", "IdUse", "Opt<optExpr>"},
    kind = {"Child", "Child", "Opt"}
  )
  public member_expr(IdUse p0, IdUse p1, Opt<optExpr> p2) {
    setChild(p0, 0);
    setChild(p1, 1);
    setChild(p2, 2);
  }
  /** @apilevel low-level 
   * @declaredat ASTNode:25
   */
  protected int numChildren() {
    return 3;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:29
   */
  public void flushAttrCache() {
    super.flushAttrCache();
    corrNbrParam_reset();
    types_reset();
    lookupFunc_String_reset();
    globalLookup_String_reset();
  }
  /** @apilevel internal 
   * @declaredat ASTNode:37
   */
  public void flushCollectionCache() {
    super.flushCollectionCache();

  }
  /** @apilevel internal 
   * @declaredat ASTNode:42
   */
  public member_expr clone() throws CloneNotSupportedException {
    member_expr node = (member_expr) super.clone();
    return node;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:47
   */
  public member_expr copy() {
    try {
      member_expr node = (member_expr) clone();
      node.parent = null;
      if (children != null) {
        node.children = (ASTNode[]) children.clone();
      }
      return node;
    } catch (CloneNotSupportedException e) {
      throw new Error("Error: clone not supported for " + getClass().getName());
    }
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @deprecated Please use treeCopy or treeCopyNoTransform instead
   * @declaredat ASTNode:66
   */
  @Deprecated
  public member_expr fullCopy() {
    return treeCopyNoTransform();
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @declaredat ASTNode:76
   */
  public member_expr treeCopyNoTransform() {
    member_expr tree = (member_expr) copy();
    if (children != null) {
      for (int i = 0; i < children.length; ++i) {
        ASTNode child = (ASTNode) children[i];
        if (child != null) {
          child = child.treeCopyNoTransform();
          tree.setChild(child, i);
        }
      }
    }
    return tree;
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The subtree of this node is traversed to trigger rewrites before copy.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @declaredat ASTNode:96
   */
  public member_expr treeCopy() {
    member_expr tree = (member_expr) copy();
    if (children != null) {
      for (int i = 0; i < children.length; ++i) {
        ASTNode child = (ASTNode) getChild(i);
        if (child != null) {
          child = child.treeCopy();
          tree.setChild(child, i);
        }
      }
    }
    return tree;
  }
  /**
   * Replaces the IdUse child.
   * @param node The new node to replace the IdUse child.
   * @apilevel high-level
   */
  public member_expr setIdUse(IdUse node) {
    setChild(node, 0);
    return this;
  }
  /**
   * Retrieves the IdUse child.
   * @return The current node used as the IdUse child.
   * @apilevel high-level
   */
  @ASTNodeAnnotation.Child(name="IdUse")
  public IdUse getIdUse() {
    return (IdUse) getChild(0);
  }
  /**
   * Retrieves the IdUse child.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The current node used as the IdUse child.
   * @apilevel low-level
   */
  public IdUse getIdUseNoTransform() {
    return (IdUse) getChildNoTransform(0);
  }
  /**
   * Replaces the call child.
   * @param node The new node to replace the call child.
   * @apilevel high-level
   */
  public member_expr setcall(IdUse node) {
    setChild(node, 1);
    return this;
  }
  /**
   * Retrieves the call child.
   * @return The current node used as the call child.
   * @apilevel high-level
   */
  @ASTNodeAnnotation.Child(name="call")
  public IdUse getcall() {
    return (IdUse) getChild(1);
  }
  /**
   * Retrieves the call child.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The current node used as the call child.
   * @apilevel low-level
   */
  public IdUse getcallNoTransform() {
    return (IdUse) getChildNoTransform(1);
  }
  /**
   * Replaces the optional node for the optExpr child. This is the <code>Opt</code>
   * node containing the child optExpr, not the actual child!
   * @param opt The new node to be used as the optional node for the optExpr child.
   * @apilevel low-level
   */
  public member_expr setoptExprOpt(Opt<optExpr> opt) {
    setChild(opt, 2);
    return this;
  }
  /**
   * Replaces the (optional) optExpr child.
   * @param node The new node to be used as the optExpr child.
   * @apilevel high-level
   */
  public member_expr setoptExpr(optExpr node) {
    getoptExprOpt().setChild(node, 0);
    return this;
  }
  /**
   * Check whether the optional optExpr child exists.
   * @return {@code true} if the optional optExpr child exists, {@code false} if it does not.
   * @apilevel high-level
   */
  public boolean hasoptExpr() {
    return getoptExprOpt().getNumChild() != 0;
  }
  /**
   * Retrieves the (optional) optExpr child.
   * @return The optExpr child, if it exists. Returns {@code null} otherwise.
   * @apilevel low-level
   */
  public optExpr getoptExpr() {
    return (optExpr) getoptExprOpt().getChild(0);
  }
  /**
   * Retrieves the optional node for the optExpr child. This is the <code>Opt</code> node containing the child optExpr, not the actual child!
   * @return The optional node for child the optExpr child.
   * @apilevel low-level
   */
  @ASTNodeAnnotation.OptChild(name="optExpr")
  public Opt<optExpr> getoptExprOpt() {
    return (Opt<optExpr>) getChild(2);
  }
  /**
   * Retrieves the optional node for child optExpr. This is the <code>Opt</code> node containing the child optExpr, not the actual child!
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The optional node for child optExpr.
   * @apilevel low-level
   */
  public Opt<optExpr> getoptExprOptNoTransform() {
    return (Opt<optExpr>) getChildNoTransform(2);
  }
/** @apilevel internal */
protected boolean corrNbrParam_visited = false;
  /** @apilevel internal */
  private void corrNbrParam_reset() {
    corrNbrParam_computed = false;
    corrNbrParam_visited = false;
  }
  /** @apilevel internal */
  protected boolean corrNbrParam_computed = false;

  /** @apilevel internal */
  protected boolean corrNbrParam_value;

  /**
   * @attribute syn
   * @aspect checkFuncCall
   * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/checkFuncCall.jrag:23
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="checkFuncCall", declaredAt="/home/tobias_carlsson/chocopy-tobias/src/jastadd/checkFuncCall.jrag:23")
  public boolean corrNbrParam() {
    ASTState state = state();
    if (corrNbrParam_computed) {
      return corrNbrParam_value;
    }
    if (corrNbrParam_visited) {
      throw new RuntimeException("Circular definition of attribute member_expr.corrNbrParam().");
    }
    corrNbrParam_visited = true;
    state().enterLazyAttribute();
    corrNbrParam_value = corrNbrParam_compute();
    corrNbrParam_computed = true;
    state().leaveLazyAttribute();
    corrNbrParam_visited = false;
    return corrNbrParam_value;
  }
  /** @apilevel internal */
  private boolean corrNbrParam_compute() { 
  		IdDecl j = lookupFunc( globalLookup(getIdUse().getID()).types().idif());
  		HashMap<String, Integer> g = j.objNum();
  		for(Map.Entry mapElement : g.entrySet()){
  				int ind = (int)mapElement.getValue(); 
  				String vd = (String)mapElement.getKey(); 
  				if(vd.equals(getcall().getID())){
  					if(hasoptExpr()){
  						
  					return  getoptExpr().getNumOpt() +1 == ind;
  
  					}
  					return  0 == ind;
  				}
  
  		}
  		
  		return false;
  	}
/** @apilevel internal */
protected boolean types_visited = false;
  /** @apilevel internal */
  private void types_reset() {
    types_computed = false;
    
    types_value = null;
    types_visited = false;
  }
  /** @apilevel internal */
  protected boolean types_computed = false;

  /** @apilevel internal */
  protected Typen types_value;

  /**
   * @attribute syn
   * @aspect typeChecker
   * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/typeChecker.jrag:43
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="typeChecker", declaredAt="/home/tobias_carlsson/chocopy-tobias/src/jastadd/typeChecker.jrag:43")
  public Typen types() {
    ASTState state = state();
    if (types_computed) {
      return types_value;
    }
    if (types_visited) {
      throw new RuntimeException("Circular definition of attribute member_expr.types().");
    }
    types_visited = true;
    state().enterLazyAttribute();
    types_value = intType();
    types_computed = true;
    state().leaveLazyAttribute();
    types_visited = false;
    return types_value;
  }
  /**
   * @attribute inh
   * @aspect NameAnalysis
   * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/nameanalysis.jrag:113
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.INH)
  @ASTNodeAnnotation.Source(aspect="NameAnalysis", declaredAt="/home/tobias_carlsson/chocopy-tobias/src/jastadd/nameanalysis.jrag:113")
  public IdDecl lookupFunc(String name) {
    Object _parameters = name;
    if (lookupFunc_String_visited == null) lookupFunc_String_visited = new java.util.HashSet(4);
    if (lookupFunc_String_values == null) lookupFunc_String_values = new java.util.HashMap(4);
    ASTState state = state();
    if (lookupFunc_String_values.containsKey(_parameters)) {
      return (IdDecl) lookupFunc_String_values.get(_parameters);
    }
    if (lookupFunc_String_visited.contains(_parameters)) {
      throw new RuntimeException("Circular definition of attribute member_expr.lookupFunc(String).");
    }
    lookupFunc_String_visited.add(_parameters);
    state().enterLazyAttribute();
    IdDecl lookupFunc_String_value = getParent().Define_lookupFunc(this, null, name);
    lookupFunc_String_values.put(_parameters, lookupFunc_String_value);
    state().leaveLazyAttribute();
    lookupFunc_String_visited.remove(_parameters);
    return lookupFunc_String_value;
  }
/** @apilevel internal */
protected java.util.Set lookupFunc_String_visited;
  /** @apilevel internal */
  private void lookupFunc_String_reset() {
    lookupFunc_String_values = null;
    lookupFunc_String_visited = null;
  }
  /** @apilevel internal */
  protected java.util.Map lookupFunc_String_values;

  /**
   * @attribute inh
   * @aspect NameAnalysis
   * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/nameanalysis.jrag:330
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.INH)
  @ASTNodeAnnotation.Source(aspect="NameAnalysis", declaredAt="/home/tobias_carlsson/chocopy-tobias/src/jastadd/nameanalysis.jrag:330")
  public IdDecl globalLookup(String name) {
    Object _parameters = name;
    if (globalLookup_String_visited == null) globalLookup_String_visited = new java.util.HashSet(4);
    if (globalLookup_String_values == null) globalLookup_String_values = new java.util.HashMap(4);
    ASTState state = state();
    if (globalLookup_String_values.containsKey(_parameters)) {
      return (IdDecl) globalLookup_String_values.get(_parameters);
    }
    if (globalLookup_String_visited.contains(_parameters)) {
      throw new RuntimeException("Circular definition of attribute member_expr.globalLookup(String).");
    }
    globalLookup_String_visited.add(_parameters);
    state().enterLazyAttribute();
    IdDecl globalLookup_String_value = getParent().Define_globalLookup(this, null, name);
    globalLookup_String_values.put(_parameters, globalLookup_String_value);
    state().leaveLazyAttribute();
    globalLookup_String_visited.remove(_parameters);
    return globalLookup_String_value;
  }
/** @apilevel internal */
protected java.util.Set globalLookup_String_visited;
  /** @apilevel internal */
  private void globalLookup_String_reset() {
    globalLookup_String_values = null;
    globalLookup_String_visited = null;
  }
  /** @apilevel internal */
  protected java.util.Map globalLookup_String_values;

  /**
   * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/nameanalysis.jrag:207
   * @apilevel internal
   */
  public IdDecl Define_lookup(ASTNode _callerNode, ASTNode _childNode, String name) {
    if (_callerNode == getcallNoTransform()) {
      // @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/nameanalysis.jrag:255
      return globalLookup(getIdUse().getID()).loclookup(name);
    }
    else if (_callerNode == getIdUseNoTransform()) {
      // @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/nameanalysis.jrag:240
      return globalLookup(name);
    }
    else {
      return getParent().Define_lookup(this, _callerNode, name);
    }
  }
  /**
   * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/nameanalysis.jrag:207
   * @apilevel internal
   * @return {@code true} if this node has an equation for the inherited attribute lookup
   */
  protected boolean canDefine_lookup(ASTNode _callerNode, ASTNode _childNode, String name) {
    return true;
  }
  /** @apilevel internal */
  protected void collect_contributors_Program_errors(Program _root, java.util.Map<ASTNode, java.util.Set<ASTNode>> _map) {
    // @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/Errors.jrag:70
    if (!corrNbrParam()) {
      {
        Program target = (Program) (program());
        java.util.Set<ASTNode> contributors = _map.get(target);
        if (contributors == null) {
          contributors = new java.util.LinkedHashSet<ASTNode>();
          _map.put((ASTNode) target, contributors);
        }
        contributors.add(this);
      }
    }
    super.collect_contributors_Program_errors(_root, _map);
  }
  /** @apilevel internal */
  protected void contributeTo_Program_errors(Set<ErrorMessage> collection) {
    super.contributeTo_Program_errors(collection);
    if (!corrNbrParam()) {
      collection.add(error("The call to: '" + getIdUse().getID()+"-"+getcall().getID() + "' does not have the correct number of parameters!"));
    }
  }

}
