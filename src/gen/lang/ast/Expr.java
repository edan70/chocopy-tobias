/* This file was generated with JastAdd2 (http://jastadd.org) version 2.3.4 */
package lang.ast;
import java.util.*;
import java.io.PrintStream;
import java.util.Set;
import java.util.TreeSet;
import java.util.ArrayList;
import java.io.ByteArrayOutputStream;
import java.lang.reflect.InvocationTargetException;
import java.util.HashSet;
/**
 * @ast node
 * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/lang.ast:32
 * @astdecl Expr : ASTNode;
 * @production Expr : {@link ASTNode};

 */
public abstract class Expr extends ASTNode<ASTNode> implements Cloneable {
  /**
   * @aspect CodeGen
   * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/CodeGen.jrag:503
   */
  public void genEval(PrintStream out,IdDecl id) {
		throw new UnsupportedOperationException();
	}
  /**
   * @declaredat ASTNode:1
   */
  public Expr() {
    super();
  }
  /**
   * Initializes the child array to the correct size.
   * Initializes List and Opt nta children.
   * @apilevel internal
   * @ast method
   * @declaredat ASTNode:10
   */
  public void init$Children() {
  }
  /** @apilevel low-level 
   * @declaredat ASTNode:13
   */
  protected int numChildren() {
    return 0;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:17
   */
  public void flushAttrCache() {
    super.flushAttrCache();
    getCexpr_reset();
    isExpr_reset();
    enclosingFunction_reset();
    isFunction_reset();
    insideClass_reset();
  }
  /** @apilevel internal 
   * @declaredat ASTNode:26
   */
  public void flushCollectionCache() {
    super.flushCollectionCache();

  }
  /** @apilevel internal 
   * @declaredat ASTNode:31
   */
  public Expr clone() throws CloneNotSupportedException {
    Expr node = (Expr) super.clone();
    return node;
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @deprecated Please use treeCopy or treeCopyNoTransform instead
   * @declaredat ASTNode:42
   */
  @Deprecated
  public abstract Expr fullCopy();
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @declaredat ASTNode:50
   */
  public abstract Expr treeCopyNoTransform();
  /**
   * Create a deep copy of the AST subtree at this node.
   * The subtree of this node is traversed to trigger rewrites before copy.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @declaredat ASTNode:58
   */
  public abstract Expr treeCopy();
  /**
   * @attribute syn
   * @aspect typeChecker
   * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/typeChecker.jrag:19
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="typeChecker", declaredAt="/home/tobias_carlsson/chocopy-tobias/src/jastadd/typeChecker.jrag:19")
  public abstract Typen types();
/** @apilevel internal */
protected boolean getCexpr_visited = false;
  /** @apilevel internal */
  private void getCexpr_reset() {
    getCexpr_computed = false;
    
    getCexpr_value = null;
    getCexpr_visited = false;
  }
  /** @apilevel internal */
  protected boolean getCexpr_computed = false;

  /** @apilevel internal */
  protected idCexpr getCexpr_value;

  /**
   * @attribute syn
   * @aspect NameAnalysis
   * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/nameanalysis.jrag:81
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="NameAnalysis", declaredAt="/home/tobias_carlsson/chocopy-tobias/src/jastadd/nameanalysis.jrag:81")
  public idCexpr getCexpr() {
    ASTState state = state();
    if (getCexpr_computed) {
      return getCexpr_value;
    }
    if (getCexpr_visited) {
      throw new RuntimeException("Circular definition of attribute Expr.getCexpr().");
    }
    getCexpr_visited = true;
    state().enterLazyAttribute();
    getCexpr_value = null;
    getCexpr_computed = true;
    state().leaveLazyAttribute();
    getCexpr_visited = false;
    return getCexpr_value;
  }
/** @apilevel internal */
protected boolean isExpr_visited = false;
  /** @apilevel internal */
  private void isExpr_reset() {
    isExpr_computed = false;
    isExpr_visited = false;
  }
  /** @apilevel internal */
  protected boolean isExpr_computed = false;

  /** @apilevel internal */
  protected boolean isExpr_value;

  /**
   * @attribute syn
   * @aspect paramAdrInd
   * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/paramAdrInd.jrag:204
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="paramAdrInd", declaredAt="/home/tobias_carlsson/chocopy-tobias/src/jastadd/paramAdrInd.jrag:203")
  public boolean isExpr() {
    ASTState state = state();
    if (isExpr_computed) {
      return isExpr_value;
    }
    if (isExpr_visited) {
      throw new RuntimeException("Circular definition of attribute ASTNode.isExpr().");
    }
    isExpr_visited = true;
    state().enterLazyAttribute();
    isExpr_value = true;
    isExpr_computed = true;
    state().leaveLazyAttribute();
    isExpr_visited = false;
    return isExpr_value;
  }
  /**
   * @attribute inh
   * @aspect CallGraph
   * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/CallGraph.jrag:5
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.INH)
  @ASTNodeAnnotation.Source(aspect="CallGraph", declaredAt="/home/tobias_carlsson/chocopy-tobias/src/jastadd/CallGraph.jrag:5")
  public func_def enclosingFunction() {
    ASTState state = state();
    if (enclosingFunction_computed) {
      return enclosingFunction_value;
    }
    if (enclosingFunction_visited) {
      throw new RuntimeException("Circular definition of attribute Expr.enclosingFunction().");
    }
    enclosingFunction_visited = true;
    state().enterLazyAttribute();
    enclosingFunction_value = getParent().Define_enclosingFunction(this, null);
    enclosingFunction_computed = true;
    state().leaveLazyAttribute();
    enclosingFunction_visited = false;
    return enclosingFunction_value;
  }
/** @apilevel internal */
protected boolean enclosingFunction_visited = false;
  /** @apilevel internal */
  private void enclosingFunction_reset() {
    enclosingFunction_computed = false;
    
    enclosingFunction_value = null;
    enclosingFunction_visited = false;
  }
  /** @apilevel internal */
  protected boolean enclosingFunction_computed = false;

  /** @apilevel internal */
  protected func_def enclosingFunction_value;

  /**
   * @attribute inh
   * @aspect FuncOrVarAnalysis
   * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/funcOrVarCh.jrag:2
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.INH)
  @ASTNodeAnnotation.Source(aspect="FuncOrVarAnalysis", declaredAt="/home/tobias_carlsson/chocopy-tobias/src/jastadd/funcOrVarCh.jrag:2")
  public boolean isFunction() {
    ASTState state = state();
    if (isFunction_computed) {
      return isFunction_value;
    }
    if (isFunction_visited) {
      throw new RuntimeException("Circular definition of attribute Expr.isFunction().");
    }
    isFunction_visited = true;
    state().enterLazyAttribute();
    isFunction_value = getParent().Define_isFunction(this, null);
    isFunction_computed = true;
    state().leaveLazyAttribute();
    isFunction_visited = false;
    return isFunction_value;
  }
/** @apilevel internal */
protected boolean isFunction_visited = false;
  /** @apilevel internal */
  private void isFunction_reset() {
    isFunction_computed = false;
    isFunction_visited = false;
  }
  /** @apilevel internal */
  protected boolean isFunction_computed = false;

  /** @apilevel internal */
  protected boolean isFunction_value;

  /**
   * @attribute inh
   * @aspect paramAdrInd
   * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/paramAdrInd.jrag:73
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.INH)
  @ASTNodeAnnotation.Source(aspect="paramAdrInd", declaredAt="/home/tobias_carlsson/chocopy-tobias/src/jastadd/paramAdrInd.jrag:73")
  public boolean insideClass() {
    ASTState state = state();
    if (insideClass_computed) {
      return insideClass_value;
    }
    if (insideClass_visited) {
      throw new RuntimeException("Circular definition of attribute Expr.insideClass().");
    }
    insideClass_visited = true;
    state().enterLazyAttribute();
    insideClass_value = getParent().Define_insideClass(this, null);
    insideClass_computed = true;
    state().leaveLazyAttribute();
    insideClass_visited = false;
    return insideClass_value;
  }
/** @apilevel internal */
protected boolean insideClass_visited = false;
  /** @apilevel internal */
  private void insideClass_reset() {
    insideClass_computed = false;
    insideClass_visited = false;
  }
  /** @apilevel internal */
  protected boolean insideClass_computed = false;

  /** @apilevel internal */
  protected boolean insideClass_value;


}
