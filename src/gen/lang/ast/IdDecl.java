/* This file was generated with JastAdd2 (http://jastadd.org) version 2.3.4 */
package lang.ast;
import java.util.*;
import java.io.PrintStream;
import java.util.Set;
import java.util.TreeSet;
import java.util.ArrayList;
import java.io.ByteArrayOutputStream;
import java.lang.reflect.InvocationTargetException;
import java.util.HashSet;
/**
 * @ast node
 * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/lang.ast:76
 * @astdecl IdDecl : ASTNode ::= <ID:String>;
 * @production IdDecl : {@link ASTNode} ::= <span class="component">&lt;ID:{@link String}&gt;</span>;

 */
public class IdDecl extends ASTNode<ASTNode> implements Cloneable {
  /**
   * @declaredat ASTNode:1
   */
  public IdDecl() {
    super();
  }
  /**
   * Initializes the child array to the correct size.
   * Initializes List and Opt nta children.
   * @apilevel internal
   * @ast method
   * @declaredat ASTNode:10
   */
  public void init$Children() {
  }
  /**
   * @declaredat ASTNode:12
   */
  @ASTNodeAnnotation.Constructor(
    name = {"ID"},
    type = {"String"},
    kind = {"Token"}
  )
  public IdDecl(String p0) {
    setID(p0);
  }
  /**
   * @declaredat ASTNode:20
   */
  public IdDecl(beaver.Symbol p0) {
    setID(p0);
  }
  /** @apilevel low-level 
   * @declaredat ASTNode:24
   */
  protected int numChildren() {
    return 0;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:28
   */
  public void flushAttrCache() {
    super.flushAttrCache();
    uniqueName_reset();
    isMultiDeclared_reset();
    address_reset();
    addressC_reset();
    addresse_reset();
    isUnknown_reset();
    isFunction_reset();
    nbrParams_reset();
    params_reset();
    stmtId_reset();
    isClass_reset();
    lookup_String_reset();
    loclookup_String_reset();
    possAssign_reset();
    isParameter_reset();
    parameterIndex_reset();
    localIndex_reset();
    localIndexc_reset();
    declInFunc_reset();
    insideFunc_reset();
    insideClass_reset();
    localIndexe_reset();
    nbrLoc_reset();
    nbrLocF_reset();
    objvars_reset();
    objNum_reset();
    hasInit_reset();
    nbrInit_reset();
    locvars_reset();
    locvarsF_reset();
    types_reset();
  }
  /** @apilevel internal 
   * @declaredat ASTNode:63
   */
  public void flushCollectionCache() {
    super.flushCollectionCache();

  }
  /** @apilevel internal 
   * @declaredat ASTNode:68
   */
  public IdDecl clone() throws CloneNotSupportedException {
    IdDecl node = (IdDecl) super.clone();
    return node;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:73
   */
  public IdDecl copy() {
    try {
      IdDecl node = (IdDecl) clone();
      node.parent = null;
      if (children != null) {
        node.children = (ASTNode[]) children.clone();
      }
      return node;
    } catch (CloneNotSupportedException e) {
      throw new Error("Error: clone not supported for " + getClass().getName());
    }
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @deprecated Please use treeCopy or treeCopyNoTransform instead
   * @declaredat ASTNode:92
   */
  @Deprecated
  public IdDecl fullCopy() {
    return treeCopyNoTransform();
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @declaredat ASTNode:102
   */
  public IdDecl treeCopyNoTransform() {
    IdDecl tree = (IdDecl) copy();
    if (children != null) {
      for (int i = 0; i < children.length; ++i) {
        ASTNode child = (ASTNode) children[i];
        if (child != null) {
          child = child.treeCopyNoTransform();
          tree.setChild(child, i);
        }
      }
    }
    return tree;
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The subtree of this node is traversed to trigger rewrites before copy.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @declaredat ASTNode:122
   */
  public IdDecl treeCopy() {
    IdDecl tree = (IdDecl) copy();
    if (children != null) {
      for (int i = 0; i < children.length; ++i) {
        ASTNode child = (ASTNode) getChild(i);
        if (child != null) {
          child = child.treeCopy();
          tree.setChild(child, i);
        }
      }
    }
    return tree;
  }
  /**
   * Replaces the lexeme ID.
   * @param value The new value for the lexeme ID.
   * @apilevel high-level
   */
  public IdDecl setID(String value) {
    tokenString_ID = value;
    return this;
  }
  /** @apilevel internal 
   */
  protected String tokenString_ID;
  /**
   */
  public int IDstart;
  /**
   */
  public int IDend;
  /**
   * JastAdd-internal setter for lexeme ID using the Beaver parser.
   * @param symbol Symbol containing the new value for the lexeme ID
   * @apilevel internal
   */
  public IdDecl setID(beaver.Symbol symbol) {
    if (symbol.value != null && !(symbol.value instanceof String))
    throw new UnsupportedOperationException("setID is only valid for String lexemes");
    tokenString_ID = (String)symbol.value;
    IDstart = symbol.getStart();
    IDend = symbol.getEnd();
    return this;
  }
  /**
   * Retrieves the value for the lexeme ID.
   * @return The value for the lexeme ID.
   * @apilevel high-level
   */
  @ASTNodeAnnotation.Token(name="ID")
  public String getID() {
    return tokenString_ID != null ? tokenString_ID : "";
  }
/** @apilevel internal */
protected boolean uniqueName_visited = false;
  /** @apilevel internal */
  private void uniqueName_reset() {
    uniqueName_computed = false;
    
    uniqueName_value = null;
    uniqueName_visited = false;
  }
  /** @apilevel internal */
  protected boolean uniqueName_computed = false;

  /** @apilevel internal */
  protected String uniqueName_value;

  /**
   * @attribute syn
   * @aspect UniqueName
   * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/genUniqueName.jrag:16
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="UniqueName", declaredAt="/home/tobias_carlsson/chocopy-tobias/src/jastadd/genUniqueName.jrag:16")
  public String uniqueName() {
    ASTState state = state();
    if (uniqueName_computed) {
      return uniqueName_value;
    }
    if (uniqueName_visited) {
      throw new RuntimeException("Circular definition of attribute IdDecl.uniqueName().");
    }
    uniqueName_visited = true;
    state().enterLazyAttribute();
    uniqueName_value = stmtId() + getID();
    uniqueName_computed = true;
    state().leaveLazyAttribute();
    uniqueName_visited = false;
    return uniqueName_value;
  }
/** @apilevel internal */
protected boolean isMultiDeclared_visited = false;
  /** @apilevel internal */
  private void isMultiDeclared_reset() {
    isMultiDeclared_computed = false;
    isMultiDeclared_visited = false;
  }
  /** @apilevel internal */
  protected boolean isMultiDeclared_computed = false;

  /** @apilevel internal */
  protected boolean isMultiDeclared_value;

  /**
   * @attribute syn
   * @aspect NameAnalysis
   * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/nameanalysis.jrag:355
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="NameAnalysis", declaredAt="/home/tobias_carlsson/chocopy-tobias/src/jastadd/nameanalysis.jrag:355")
  public boolean isMultiDeclared() {
    ASTState state = state();
    if (isMultiDeclared_computed) {
      return isMultiDeclared_value;
    }
    if (isMultiDeclared_visited) {
      throw new RuntimeException("Circular definition of attribute IdDecl.isMultiDeclared().");
    }
    isMultiDeclared_visited = true;
    state().enterLazyAttribute();
    isMultiDeclared_value = isMultiDeclared_compute();
    isMultiDeclared_computed = true;
    state().leaveLazyAttribute();
    isMultiDeclared_visited = false;
    return isMultiDeclared_value;
  }
  /** @apilevel internal */
  private boolean isMultiDeclared_compute() {
  		if(lookup(getID())!=this){
  			if(lookup(getID())!=unknownDecl()){
  				return true;
  			}
  		}
  		return false;
  	}
/** @apilevel internal */
protected boolean address_visited = false;
  /** @apilevel internal */
  private void address_reset() {
    address_computed = false;
    
    address_value = null;
    address_visited = false;
  }
  /** @apilevel internal */
  protected boolean address_computed = false;

  /** @apilevel internal */
  protected String address_value;

  /**
   * @attribute syn
   * @aspect paramAdrInd
   * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/paramAdrInd.jrag:2
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="paramAdrInd", declaredAt="/home/tobias_carlsson/chocopy-tobias/src/jastadd/paramAdrInd.jrag:2")
  public String address() {
    ASTState state = state();
    if (address_computed) {
      return address_value;
    }
    if (address_visited) {
      throw new RuntimeException("Circular definition of attribute IdDecl.address().");
    }
    address_visited = true;
    state().enterLazyAttribute();
    address_value = address_compute();
    address_computed = true;
    state().leaveLazyAttribute();
    address_visited = false;
    return address_value;
  }
  /** @apilevel internal */
  private String address_compute() {
  		if(isParameter()){
  			if(insideClass())
  			{
  				return  (16 + parameterIndex()*8)+"(%R15)";
  			}
  			return  (16 + parameterIndex()*8)+"(%rbp)";
  		}
  	return  "-"+((localIndex())*8)+"(%rbp)";
  
  	}
/** @apilevel internal */
protected boolean addressC_visited = false;
  /** @apilevel internal */
  private void addressC_reset() {
    addressC_computed = false;
    
    addressC_value = null;
    addressC_visited = false;
  }
  /** @apilevel internal */
  protected boolean addressC_computed = false;

  /** @apilevel internal */
  protected String addressC_value;

  /**
   * @attribute syn
   * @aspect paramAdrInd
   * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/paramAdrInd.jrag:13
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="paramAdrInd", declaredAt="/home/tobias_carlsson/chocopy-tobias/src/jastadd/paramAdrInd.jrag:13")
  public String addressC() {
    ASTState state = state();
    if (addressC_computed) {
      return addressC_value;
    }
    if (addressC_visited) {
      throw new RuntimeException("Circular definition of attribute IdDecl.addressC().");
    }
    addressC_visited = true;
    state().enterLazyAttribute();
    addressC_value = "-"+((localIndexc())*8)+"(%rbp)";
    addressC_computed = true;
    state().leaveLazyAttribute();
    addressC_visited = false;
    return addressC_value;
  }
/** @apilevel internal */
protected boolean addresse_visited = false;
  /** @apilevel internal */
  private void addresse_reset() {
    addresse_computed = false;
    
    addresse_value = null;
    addresse_visited = false;
  }
  /** @apilevel internal */
  protected boolean addresse_computed = false;

  /** @apilevel internal */
  protected String addresse_value;

  /**
   * @attribute syn
   * @aspect paramAdrInd
   * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/paramAdrInd.jrag:14
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="paramAdrInd", declaredAt="/home/tobias_carlsson/chocopy-tobias/src/jastadd/paramAdrInd.jrag:14")
  public String addresse() {
    ASTState state = state();
    if (addresse_computed) {
      return addresse_value;
    }
    if (addresse_visited) {
      throw new RuntimeException("Circular definition of attribute IdDecl.addresse().");
    }
    addresse_visited = true;
    state().enterLazyAttribute();
    addresse_value = "-"+((localIndexe())*8)+"(%rbp)";
    addresse_computed = true;
    state().leaveLazyAttribute();
    addresse_visited = false;
    return addresse_value;
  }
/** @apilevel internal */
protected boolean isUnknown_visited = false;
  /** @apilevel internal */
  private void isUnknown_reset() {
    isUnknown_computed = false;
    isUnknown_visited = false;
  }
  /** @apilevel internal */
  protected boolean isUnknown_computed = false;

  /** @apilevel internal */
  protected boolean isUnknown_value;

  /**
   * @attribute syn
   * @aspect UnknownDecl
   * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/UnknownDecl.jrag:7
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="UnknownDecl", declaredAt="/home/tobias_carlsson/chocopy-tobias/src/jastadd/UnknownDecl.jrag:7")
  public boolean isUnknown() {
    ASTState state = state();
    if (isUnknown_computed) {
      return isUnknown_value;
    }
    if (isUnknown_visited) {
      throw new RuntimeException("Circular definition of attribute IdDecl.isUnknown().");
    }
    isUnknown_visited = true;
    state().enterLazyAttribute();
    isUnknown_value = false;
    isUnknown_computed = true;
    state().leaveLazyAttribute();
    isUnknown_visited = false;
    return isUnknown_value;
  }
  /**
   * @attribute inh
   * @aspect FuncOrVarAnalysis
   * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/funcOrVarCh.jrag:3
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.INH)
  @ASTNodeAnnotation.Source(aspect="FuncOrVarAnalysis", declaredAt="/home/tobias_carlsson/chocopy-tobias/src/jastadd/funcOrVarCh.jrag:3")
  public boolean isFunction() {
    ASTState state = state();
    if (isFunction_computed) {
      return isFunction_value;
    }
    if (isFunction_visited) {
      throw new RuntimeException("Circular definition of attribute IdDecl.isFunction().");
    }
    isFunction_visited = true;
    state().enterLazyAttribute();
    isFunction_value = getParent().Define_isFunction(this, null);
    isFunction_computed = true;
    state().leaveLazyAttribute();
    isFunction_visited = false;
    return isFunction_value;
  }
/** @apilevel internal */
protected boolean isFunction_visited = false;
  /** @apilevel internal */
  private void isFunction_reset() {
    isFunction_computed = false;
    isFunction_visited = false;
  }
  /** @apilevel internal */
  protected boolean isFunction_computed = false;

  /** @apilevel internal */
  protected boolean isFunction_value;

  /**
   * @attribute inh
   * @aspect FuncOrVarAnalysis
   * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/funcOrVarCh.jrag:15
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.INH)
  @ASTNodeAnnotation.Source(aspect="FuncOrVarAnalysis", declaredAt="/home/tobias_carlsson/chocopy-tobias/src/jastadd/funcOrVarCh.jrag:15")
  public int nbrParams() {
    ASTState state = state();
    if (nbrParams_computed) {
      return nbrParams_value;
    }
    if (nbrParams_visited) {
      throw new RuntimeException("Circular definition of attribute IdDecl.nbrParams().");
    }
    nbrParams_visited = true;
    state().enterLazyAttribute();
    nbrParams_value = getParent().Define_nbrParams(this, null);
    nbrParams_computed = true;
    state().leaveLazyAttribute();
    nbrParams_visited = false;
    return nbrParams_value;
  }
/** @apilevel internal */
protected boolean nbrParams_visited = false;
  /** @apilevel internal */
  private void nbrParams_reset() {
    nbrParams_computed = false;
    nbrParams_visited = false;
  }
  /** @apilevel internal */
  protected boolean nbrParams_computed = false;

  /** @apilevel internal */
  protected int nbrParams_value;

  /**
   * @attribute inh
   * @aspect FuncOrVarAnalysis
   * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/funcOrVarCh.jrag:40
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.INH)
  @ASTNodeAnnotation.Source(aspect="FuncOrVarAnalysis", declaredAt="/home/tobias_carlsson/chocopy-tobias/src/jastadd/funcOrVarCh.jrag:40")
  public List<Typen> params() {
    ASTState state = state();
    if (params_computed) {
      return params_value;
    }
    if (params_visited) {
      throw new RuntimeException("Circular definition of attribute IdDecl.params().");
    }
    params_visited = true;
    state().enterLazyAttribute();
    params_value = getParent().Define_params(this, null);
    params_computed = true;
    state().leaveLazyAttribute();
    params_visited = false;
    return params_value;
  }
/** @apilevel internal */
protected boolean params_visited = false;
  /** @apilevel internal */
  private void params_reset() {
    params_computed = false;
    
    params_value = null;
    params_visited = false;
  }
  /** @apilevel internal */
  protected boolean params_computed = false;

  /** @apilevel internal */
  protected List<Typen> params_value;

  /**
   * @attribute inh
   * @aspect UniqueName
   * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/genUniqueName.jrag:4
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.INH)
  @ASTNodeAnnotation.Source(aspect="UniqueName", declaredAt="/home/tobias_carlsson/chocopy-tobias/src/jastadd/genUniqueName.jrag:4")
  public String stmtId() {
    ASTState state = state();
    if (stmtId_computed) {
      return stmtId_value;
    }
    if (stmtId_visited) {
      throw new RuntimeException("Circular definition of attribute IdDecl.stmtId().");
    }
    stmtId_visited = true;
    state().enterLazyAttribute();
    stmtId_value = getParent().Define_stmtId(this, null);
    stmtId_computed = true;
    state().leaveLazyAttribute();
    stmtId_visited = false;
    return stmtId_value;
  }
/** @apilevel internal */
protected boolean stmtId_visited = false;
  /** @apilevel internal */
  private void stmtId_reset() {
    stmtId_computed = false;
    
    stmtId_value = null;
    stmtId_visited = false;
  }
  /** @apilevel internal */
  protected boolean stmtId_computed = false;

  /** @apilevel internal */
  protected String stmtId_value;

  /**
   * @attribute inh
   * @aspect NameAnalysis
   * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/nameanalysis.jrag:109
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.INH)
  @ASTNodeAnnotation.Source(aspect="NameAnalysis", declaredAt="/home/tobias_carlsson/chocopy-tobias/src/jastadd/nameanalysis.jrag:109")
  public boolean isClass() {
    ASTState state = state();
    if (isClass_computed) {
      return isClass_value;
    }
    if (isClass_visited) {
      throw new RuntimeException("Circular definition of attribute IdDecl.isClass().");
    }
    isClass_visited = true;
    state().enterLazyAttribute();
    isClass_value = getParent().Define_isClass(this, null);
    isClass_computed = true;
    state().leaveLazyAttribute();
    isClass_visited = false;
    return isClass_value;
  }
/** @apilevel internal */
protected boolean isClass_visited = false;
  /** @apilevel internal */
  private void isClass_reset() {
    isClass_computed = false;
    isClass_visited = false;
  }
  /** @apilevel internal */
  protected boolean isClass_computed = false;

  /** @apilevel internal */
  protected boolean isClass_value;

  /**
   * @attribute inh
   * @aspect NameAnalysis
   * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/nameanalysis.jrag:208
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.INH)
  @ASTNodeAnnotation.Source(aspect="NameAnalysis", declaredAt="/home/tobias_carlsson/chocopy-tobias/src/jastadd/nameanalysis.jrag:208")
  public IdDecl lookup(String name) {
    Object _parameters = name;
    if (lookup_String_visited == null) lookup_String_visited = new java.util.HashSet(4);
    if (lookup_String_values == null) lookup_String_values = new java.util.HashMap(4);
    ASTState state = state();
    if (lookup_String_values.containsKey(_parameters)) {
      return (IdDecl) lookup_String_values.get(_parameters);
    }
    if (lookup_String_visited.contains(_parameters)) {
      throw new RuntimeException("Circular definition of attribute IdDecl.lookup(String).");
    }
    lookup_String_visited.add(_parameters);
    state().enterLazyAttribute();
    IdDecl lookup_String_value = getParent().Define_lookup(this, null, name);
    lookup_String_values.put(_parameters, lookup_String_value);
    state().leaveLazyAttribute();
    lookup_String_visited.remove(_parameters);
    return lookup_String_value;
  }
/** @apilevel internal */
protected java.util.Set lookup_String_visited;
  /** @apilevel internal */
  private void lookup_String_reset() {
    lookup_String_values = null;
    lookup_String_visited = null;
  }
  /** @apilevel internal */
  protected java.util.Map lookup_String_values;

  /**
   * @attribute inh
   * @aspect NameAnalysis
   * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/nameanalysis.jrag:254
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.INH)
  @ASTNodeAnnotation.Source(aspect="NameAnalysis", declaredAt="/home/tobias_carlsson/chocopy-tobias/src/jastadd/nameanalysis.jrag:254")
  public IdDecl loclookup(String name) {
    Object _parameters = name;
    if (loclookup_String_visited == null) loclookup_String_visited = new java.util.HashSet(4);
    if (loclookup_String_values == null) loclookup_String_values = new java.util.HashMap(4);
    ASTState state = state();
    if (loclookup_String_values.containsKey(_parameters)) {
      return (IdDecl) loclookup_String_values.get(_parameters);
    }
    if (loclookup_String_visited.contains(_parameters)) {
      throw new RuntimeException("Circular definition of attribute IdDecl.loclookup(String).");
    }
    loclookup_String_visited.add(_parameters);
    state().enterLazyAttribute();
    IdDecl loclookup_String_value = getParent().Define_loclookup(this, null, name);
    loclookup_String_values.put(_parameters, loclookup_String_value);
    state().leaveLazyAttribute();
    loclookup_String_visited.remove(_parameters);
    return loclookup_String_value;
  }
/** @apilevel internal */
protected java.util.Set loclookup_String_visited;
  /** @apilevel internal */
  private void loclookup_String_reset() {
    loclookup_String_values = null;
    loclookup_String_visited = null;
  }
  /** @apilevel internal */
  protected java.util.Map loclookup_String_values;

  /**
   * @attribute inh
   * @aspect NameAnalysis
   * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/nameanalysis.jrag:351
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.INH)
  @ASTNodeAnnotation.Source(aspect="NameAnalysis", declaredAt="/home/tobias_carlsson/chocopy-tobias/src/jastadd/nameanalysis.jrag:351")
  public boolean possAssign() {
    ASTState state = state();
    if (possAssign_computed) {
      return possAssign_value;
    }
    if (possAssign_visited) {
      throw new RuntimeException("Circular definition of attribute IdDecl.possAssign().");
    }
    possAssign_visited = true;
    state().enterLazyAttribute();
    possAssign_value = getParent().Define_possAssign(this, null);
    possAssign_computed = true;
    state().leaveLazyAttribute();
    possAssign_visited = false;
    return possAssign_value;
  }
/** @apilevel internal */
protected boolean possAssign_visited = false;
  /** @apilevel internal */
  private void possAssign_reset() {
    possAssign_computed = false;
    possAssign_visited = false;
  }
  /** @apilevel internal */
  protected boolean possAssign_computed = false;

  /** @apilevel internal */
  protected boolean possAssign_value;

  /**
   * @attribute inh
   * @aspect paramAdrInd
   * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/paramAdrInd.jrag:16
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.INH)
  @ASTNodeAnnotation.Source(aspect="paramAdrInd", declaredAt="/home/tobias_carlsson/chocopy-tobias/src/jastadd/paramAdrInd.jrag:16")
  public boolean isParameter() {
    ASTState state = state();
    if (isParameter_computed) {
      return isParameter_value;
    }
    if (isParameter_visited) {
      throw new RuntimeException("Circular definition of attribute IdDecl.isParameter().");
    }
    isParameter_visited = true;
    state().enterLazyAttribute();
    isParameter_value = getParent().Define_isParameter(this, null);
    isParameter_computed = true;
    state().leaveLazyAttribute();
    isParameter_visited = false;
    return isParameter_value;
  }
/** @apilevel internal */
protected boolean isParameter_visited = false;
  /** @apilevel internal */
  private void isParameter_reset() {
    isParameter_computed = false;
    isParameter_visited = false;
  }
  /** @apilevel internal */
  protected boolean isParameter_computed = false;

  /** @apilevel internal */
  protected boolean isParameter_value;

  /**
   * @attribute inh
   * @aspect paramAdrInd
   * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/paramAdrInd.jrag:23
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.INH)
  @ASTNodeAnnotation.Source(aspect="paramAdrInd", declaredAt="/home/tobias_carlsson/chocopy-tobias/src/jastadd/paramAdrInd.jrag:23")
  public int parameterIndex() {
    ASTState state = state();
    if (parameterIndex_computed) {
      return parameterIndex_value;
    }
    if (parameterIndex_visited) {
      throw new RuntimeException("Circular definition of attribute IdDecl.parameterIndex().");
    }
    parameterIndex_visited = true;
    state().enterLazyAttribute();
    parameterIndex_value = getParent().Define_parameterIndex(this, null);
    parameterIndex_computed = true;
    state().leaveLazyAttribute();
    parameterIndex_visited = false;
    return parameterIndex_value;
  }
/** @apilevel internal */
protected boolean parameterIndex_visited = false;
  /** @apilevel internal */
  private void parameterIndex_reset() {
    parameterIndex_computed = false;
    parameterIndex_visited = false;
  }
  /** @apilevel internal */
  protected boolean parameterIndex_computed = false;

  /** @apilevel internal */
  protected int parameterIndex_value;

  /**
   * @attribute inh
   * @aspect paramAdrInd
   * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/paramAdrInd.jrag:29
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.INH)
  @ASTNodeAnnotation.Source(aspect="paramAdrInd", declaredAt="/home/tobias_carlsson/chocopy-tobias/src/jastadd/paramAdrInd.jrag:29")
  public int localIndex() {
    ASTState state = state();
    if (localIndex_computed) {
      return localIndex_value;
    }
    if (localIndex_visited) {
      throw new RuntimeException("Circular definition of attribute IdDecl.localIndex().");
    }
    localIndex_visited = true;
    state().enterLazyAttribute();
    localIndex_value = getParent().Define_localIndex(this, null);
    localIndex_computed = true;
    state().leaveLazyAttribute();
    localIndex_visited = false;
    return localIndex_value;
  }
/** @apilevel internal */
protected boolean localIndex_visited = false;
  /** @apilevel internal */
  private void localIndex_reset() {
    localIndex_computed = false;
    localIndex_visited = false;
  }
  /** @apilevel internal */
  protected boolean localIndex_computed = false;

  /** @apilevel internal */
  protected int localIndex_value;

  /**
   * @attribute inh
   * @aspect paramAdrInd
   * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/paramAdrInd.jrag:52
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.INH)
  @ASTNodeAnnotation.Source(aspect="paramAdrInd", declaredAt="/home/tobias_carlsson/chocopy-tobias/src/jastadd/paramAdrInd.jrag:52")
  public int localIndexc() {
    ASTState state = state();
    if (localIndexc_computed) {
      return localIndexc_value;
    }
    if (localIndexc_visited) {
      throw new RuntimeException("Circular definition of attribute IdDecl.localIndexc().");
    }
    localIndexc_visited = true;
    state().enterLazyAttribute();
    localIndexc_value = getParent().Define_localIndexc(this, null);
    localIndexc_computed = true;
    state().leaveLazyAttribute();
    localIndexc_visited = false;
    return localIndexc_value;
  }
/** @apilevel internal */
protected boolean localIndexc_visited = false;
  /** @apilevel internal */
  private void localIndexc_reset() {
    localIndexc_computed = false;
    localIndexc_visited = false;
  }
  /** @apilevel internal */
  protected boolean localIndexc_computed = false;

  /** @apilevel internal */
  protected int localIndexc_value;

  /**
   * @attribute inh
   * @aspect paramAdrInd
   * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/paramAdrInd.jrag:59
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.INH)
  @ASTNodeAnnotation.Source(aspect="paramAdrInd", declaredAt="/home/tobias_carlsson/chocopy-tobias/src/jastadd/paramAdrInd.jrag:59")
  public boolean declInFunc() {
    ASTState state = state();
    if (declInFunc_computed) {
      return declInFunc_value;
    }
    if (declInFunc_visited) {
      throw new RuntimeException("Circular definition of attribute IdDecl.declInFunc().");
    }
    declInFunc_visited = true;
    state().enterLazyAttribute();
    declInFunc_value = getParent().Define_declInFunc(this, null);
    declInFunc_computed = true;
    state().leaveLazyAttribute();
    declInFunc_visited = false;
    return declInFunc_value;
  }
/** @apilevel internal */
protected boolean declInFunc_visited = false;
  /** @apilevel internal */
  private void declInFunc_reset() {
    declInFunc_computed = false;
    declInFunc_visited = false;
  }
  /** @apilevel internal */
  protected boolean declInFunc_computed = false;

  /** @apilevel internal */
  protected boolean declInFunc_value;

  /**
   * @attribute inh
   * @aspect paramAdrInd
   * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/paramAdrInd.jrag:64
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.INH)
  @ASTNodeAnnotation.Source(aspect="paramAdrInd", declaredAt="/home/tobias_carlsson/chocopy-tobias/src/jastadd/paramAdrInd.jrag:64")
  public boolean insideFunc() {
    ASTState state = state();
    if (insideFunc_computed) {
      return insideFunc_value;
    }
    if (insideFunc_visited) {
      throw new RuntimeException("Circular definition of attribute IdDecl.insideFunc().");
    }
    insideFunc_visited = true;
    state().enterLazyAttribute();
    insideFunc_value = getParent().Define_insideFunc(this, null);
    insideFunc_computed = true;
    state().leaveLazyAttribute();
    insideFunc_visited = false;
    return insideFunc_value;
  }
/** @apilevel internal */
protected boolean insideFunc_visited = false;
  /** @apilevel internal */
  private void insideFunc_reset() {
    insideFunc_computed = false;
    insideFunc_visited = false;
  }
  /** @apilevel internal */
  protected boolean insideFunc_computed = false;

  /** @apilevel internal */
  protected boolean insideFunc_value;

  /**
   * @attribute inh
   * @aspect paramAdrInd
   * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/paramAdrInd.jrag:70
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.INH)
  @ASTNodeAnnotation.Source(aspect="paramAdrInd", declaredAt="/home/tobias_carlsson/chocopy-tobias/src/jastadd/paramAdrInd.jrag:70")
  public boolean insideClass() {
    ASTState state = state();
    if (insideClass_computed) {
      return insideClass_value;
    }
    if (insideClass_visited) {
      throw new RuntimeException("Circular definition of attribute IdDecl.insideClass().");
    }
    insideClass_visited = true;
    state().enterLazyAttribute();
    insideClass_value = getParent().Define_insideClass(this, null);
    insideClass_computed = true;
    state().leaveLazyAttribute();
    insideClass_visited = false;
    return insideClass_value;
  }
/** @apilevel internal */
protected boolean insideClass_visited = false;
  /** @apilevel internal */
  private void insideClass_reset() {
    insideClass_computed = false;
    insideClass_visited = false;
  }
  /** @apilevel internal */
  protected boolean insideClass_computed = false;

  /** @apilevel internal */
  protected boolean insideClass_value;

  /**
   * @attribute inh
   * @aspect paramAdrInd
   * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/paramAdrInd.jrag:80
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.INH)
  @ASTNodeAnnotation.Source(aspect="paramAdrInd", declaredAt="/home/tobias_carlsson/chocopy-tobias/src/jastadd/paramAdrInd.jrag:80")
  public int localIndexe() {
    ASTState state = state();
    if (localIndexe_computed) {
      return localIndexe_value;
    }
    if (localIndexe_visited) {
      throw new RuntimeException("Circular definition of attribute IdDecl.localIndexe().");
    }
    localIndexe_visited = true;
    state().enterLazyAttribute();
    localIndexe_value = getParent().Define_localIndexe(this, null);
    localIndexe_computed = true;
    state().leaveLazyAttribute();
    localIndexe_visited = false;
    return localIndexe_value;
  }
/** @apilevel internal */
protected boolean localIndexe_visited = false;
  /** @apilevel internal */
  private void localIndexe_reset() {
    localIndexe_computed = false;
    localIndexe_visited = false;
  }
  /** @apilevel internal */
  protected boolean localIndexe_computed = false;

  /** @apilevel internal */
  protected int localIndexe_value;

  /**
   * @attribute inh
   * @aspect paramAdrInd
   * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/paramAdrInd.jrag:90
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.INH)
  @ASTNodeAnnotation.Source(aspect="paramAdrInd", declaredAt="/home/tobias_carlsson/chocopy-tobias/src/jastadd/paramAdrInd.jrag:90")
  public int nbrLoc() {
    ASTState state = state();
    if (nbrLoc_computed) {
      return nbrLoc_value;
    }
    if (nbrLoc_visited) {
      throw new RuntimeException("Circular definition of attribute IdDecl.nbrLoc().");
    }
    nbrLoc_visited = true;
    state().enterLazyAttribute();
    nbrLoc_value = getParent().Define_nbrLoc(this, null);
    nbrLoc_computed = true;
    state().leaveLazyAttribute();
    nbrLoc_visited = false;
    return nbrLoc_value;
  }
/** @apilevel internal */
protected boolean nbrLoc_visited = false;
  /** @apilevel internal */
  private void nbrLoc_reset() {
    nbrLoc_computed = false;
    nbrLoc_visited = false;
  }
  /** @apilevel internal */
  protected boolean nbrLoc_computed = false;

  /** @apilevel internal */
  protected int nbrLoc_value;

  /**
   * @attribute inh
   * @aspect paramAdrInd
   * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/paramAdrInd.jrag:106
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.INH)
  @ASTNodeAnnotation.Source(aspect="paramAdrInd", declaredAt="/home/tobias_carlsson/chocopy-tobias/src/jastadd/paramAdrInd.jrag:106")
  public int nbrLocF() {
    ASTState state = state();
    if (nbrLocF_computed) {
      return nbrLocF_value;
    }
    if (nbrLocF_visited) {
      throw new RuntimeException("Circular definition of attribute IdDecl.nbrLocF().");
    }
    nbrLocF_visited = true;
    state().enterLazyAttribute();
    nbrLocF_value = getParent().Define_nbrLocF(this, null);
    nbrLocF_computed = true;
    state().leaveLazyAttribute();
    nbrLocF_visited = false;
    return nbrLocF_value;
  }
/** @apilevel internal */
protected boolean nbrLocF_visited = false;
  /** @apilevel internal */
  private void nbrLocF_reset() {
    nbrLocF_computed = false;
    nbrLocF_visited = false;
  }
  /** @apilevel internal */
  protected boolean nbrLocF_computed = false;

  /** @apilevel internal */
  protected int nbrLocF_value;

  /**
   * @attribute inh
   * @aspect paramAdrInd
   * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/paramAdrInd.jrag:124
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.INH)
  @ASTNodeAnnotation.Source(aspect="paramAdrInd", declaredAt="/home/tobias_carlsson/chocopy-tobias/src/jastadd/paramAdrInd.jrag:124")
  public HashMap<var_def, Integer> objvars() {
    ASTState state = state();
    if (objvars_computed) {
      return objvars_value;
    }
    if (objvars_visited) {
      throw new RuntimeException("Circular definition of attribute IdDecl.objvars().");
    }
    objvars_visited = true;
    state().enterLazyAttribute();
    objvars_value = getParent().Define_objvars(this, null);
    objvars_computed = true;
    state().leaveLazyAttribute();
    objvars_visited = false;
    return objvars_value;
  }
/** @apilevel internal */
protected boolean objvars_visited = false;
  /** @apilevel internal */
  private void objvars_reset() {
    objvars_computed = false;
    
    objvars_value = null;
    objvars_visited = false;
  }
  /** @apilevel internal */
  protected boolean objvars_computed = false;

  /** @apilevel internal */
  protected HashMap<var_def, Integer> objvars_value;

  /**
   * @attribute inh
   * @aspect paramAdrInd
   * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/paramAdrInd.jrag:128
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.INH)
  @ASTNodeAnnotation.Source(aspect="paramAdrInd", declaredAt="/home/tobias_carlsson/chocopy-tobias/src/jastadd/paramAdrInd.jrag:128")
  public HashMap<String, Integer> objNum() {
    ASTState state = state();
    if (objNum_computed) {
      return objNum_value;
    }
    if (objNum_visited) {
      throw new RuntimeException("Circular definition of attribute IdDecl.objNum().");
    }
    objNum_visited = true;
    state().enterLazyAttribute();
    objNum_value = getParent().Define_objNum(this, null);
    objNum_computed = true;
    state().leaveLazyAttribute();
    objNum_visited = false;
    return objNum_value;
  }
/** @apilevel internal */
protected boolean objNum_visited = false;
  /** @apilevel internal */
  private void objNum_reset() {
    objNum_computed = false;
    
    objNum_value = null;
    objNum_visited = false;
  }
  /** @apilevel internal */
  protected boolean objNum_computed = false;

  /** @apilevel internal */
  protected HashMap<String, Integer> objNum_value;

  /**
   * @attribute inh
   * @aspect paramAdrInd
   * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/paramAdrInd.jrag:129
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.INH)
  @ASTNodeAnnotation.Source(aspect="paramAdrInd", declaredAt="/home/tobias_carlsson/chocopy-tobias/src/jastadd/paramAdrInd.jrag:129")
  public boolean hasInit() {
    ASTState state = state();
    if (hasInit_computed) {
      return hasInit_value;
    }
    if (hasInit_visited) {
      throw new RuntimeException("Circular definition of attribute IdDecl.hasInit().");
    }
    hasInit_visited = true;
    state().enterLazyAttribute();
    hasInit_value = getParent().Define_hasInit(this, null);
    hasInit_computed = true;
    state().leaveLazyAttribute();
    hasInit_visited = false;
    return hasInit_value;
  }
/** @apilevel internal */
protected boolean hasInit_visited = false;
  /** @apilevel internal */
  private void hasInit_reset() {
    hasInit_computed = false;
    hasInit_visited = false;
  }
  /** @apilevel internal */
  protected boolean hasInit_computed = false;

  /** @apilevel internal */
  protected boolean hasInit_value;

  /**
   * @attribute inh
   * @aspect paramAdrInd
   * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/paramAdrInd.jrag:132
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.INH)
  @ASTNodeAnnotation.Source(aspect="paramAdrInd", declaredAt="/home/tobias_carlsson/chocopy-tobias/src/jastadd/paramAdrInd.jrag:132")
  public int nbrInit() {
    ASTState state = state();
    if (nbrInit_computed) {
      return nbrInit_value;
    }
    if (nbrInit_visited) {
      throw new RuntimeException("Circular definition of attribute IdDecl.nbrInit().");
    }
    nbrInit_visited = true;
    state().enterLazyAttribute();
    nbrInit_value = getParent().Define_nbrInit(this, null);
    nbrInit_computed = true;
    state().leaveLazyAttribute();
    nbrInit_visited = false;
    return nbrInit_value;
  }
/** @apilevel internal */
protected boolean nbrInit_visited = false;
  /** @apilevel internal */
  private void nbrInit_reset() {
    nbrInit_computed = false;
    nbrInit_visited = false;
  }
  /** @apilevel internal */
  protected boolean nbrInit_computed = false;

  /** @apilevel internal */
  protected int nbrInit_value;

  /**
   * @attribute inh
   * @aspect paramAdrInd
   * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/paramAdrInd.jrag:179
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.INH)
  @ASTNodeAnnotation.Source(aspect="paramAdrInd", declaredAt="/home/tobias_carlsson/chocopy-tobias/src/jastadd/paramAdrInd.jrag:179")
  public List<var_def> locvars() {
    ASTState state = state();
    if (locvars_computed) {
      return locvars_value;
    }
    if (locvars_visited) {
      throw new RuntimeException("Circular definition of attribute IdDecl.locvars().");
    }
    locvars_visited = true;
    state().enterLazyAttribute();
    locvars_value = getParent().Define_locvars(this, null);
    locvars_computed = true;
    state().leaveLazyAttribute();
    locvars_visited = false;
    return locvars_value;
  }
/** @apilevel internal */
protected boolean locvars_visited = false;
  /** @apilevel internal */
  private void locvars_reset() {
    locvars_computed = false;
    
    locvars_value = null;
    locvars_visited = false;
  }
  /** @apilevel internal */
  protected boolean locvars_computed = false;

  /** @apilevel internal */
  protected List<var_def> locvars_value;

  /**
   * @attribute inh
   * @aspect paramAdrInd
   * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/paramAdrInd.jrag:180
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.INH)
  @ASTNodeAnnotation.Source(aspect="paramAdrInd", declaredAt="/home/tobias_carlsson/chocopy-tobias/src/jastadd/paramAdrInd.jrag:180")
  public List<var_def> locvarsF() {
    ASTState state = state();
    if (locvarsF_computed) {
      return locvarsF_value;
    }
    if (locvarsF_visited) {
      throw new RuntimeException("Circular definition of attribute IdDecl.locvarsF().");
    }
    locvarsF_visited = true;
    state().enterLazyAttribute();
    locvarsF_value = getParent().Define_locvarsF(this, null);
    locvarsF_computed = true;
    state().leaveLazyAttribute();
    locvarsF_visited = false;
    return locvarsF_value;
  }
/** @apilevel internal */
protected boolean locvarsF_visited = false;
  /** @apilevel internal */
  private void locvarsF_reset() {
    locvarsF_computed = false;
    
    locvarsF_value = null;
    locvarsF_visited = false;
  }
  /** @apilevel internal */
  protected boolean locvarsF_computed = false;

  /** @apilevel internal */
  protected List<var_def> locvarsF_value;

  /**
   * @attribute inh
   * @aspect typeChecker
   * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/typeChecker.jrag:21
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.INH)
  @ASTNodeAnnotation.Source(aspect="typeChecker", declaredAt="/home/tobias_carlsson/chocopy-tobias/src/jastadd/typeChecker.jrag:21")
  public Typen types() {
    ASTState state = state();
    if (types_computed) {
      return types_value;
    }
    if (types_visited) {
      throw new RuntimeException("Circular definition of attribute IdDecl.types().");
    }
    types_visited = true;
    state().enterLazyAttribute();
    types_value = getParent().Define_types(this, null);
    types_computed = true;
    state().leaveLazyAttribute();
    types_visited = false;
    return types_value;
  }
/** @apilevel internal */
protected boolean types_visited = false;
  /** @apilevel internal */
  private void types_reset() {
    types_computed = false;
    
    types_value = null;
    types_visited = false;
  }
  /** @apilevel internal */
  protected boolean types_computed = false;

  /** @apilevel internal */
  protected Typen types_value;

  /** @apilevel internal */
  protected void collect_contributors_Program_errors(Program _root, java.util.Map<ASTNode, java.util.Set<ASTNode>> _map) {
    // @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/Errors.jrag:50
    if (isMultiDeclared()) {
      {
        Program target = (Program) (program());
        java.util.Set<ASTNode> contributors = _map.get(target);
        if (contributors == null) {
          contributors = new java.util.LinkedHashSet<ASTNode>();
          _map.put((ASTNode) target, contributors);
        }
        contributors.add(this);
      }
    }
    super.collect_contributors_Program_errors(_root, _map);
  }
  /** @apilevel internal */
  protected void contributeTo_Program_errors(Set<ErrorMessage> collection) {
    super.contributeTo_Program_errors(collection);
    if (isMultiDeclared()) {
      collection.add(error("symbol '" + getID() + "' is already declared!"));
    }
  }

}
