/* This file was generated with JastAdd2 (http://jastadd.org) version 2.3.4 */
package lang.ast;
import java.util.*;
import java.io.PrintStream;
import java.util.Set;
import java.util.TreeSet;
import java.util.ArrayList;
import java.io.ByteArrayOutputStream;
import java.lang.reflect.InvocationTargetException;
import java.util.HashSet;
/**
 * @ast node
 * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/lang.ast:11
 * @astdecl fd : vf ::= func_def;
 * @production fd : {@link vf} ::= <span class="component">{@link func_def}</span>;

 */
public class fd extends vf implements Cloneable {
  /**
   * @aspect CodeGen
   * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/CodeGen.jrag:267
   */
  public void genCode(PrintStream out, IdDecl id) {
		getfunc_def().genCode(out,id);
	}
  /**
   * @declaredat ASTNode:1
   */
  public fd() {
    super();
  }
  /**
   * Initializes the child array to the correct size.
   * Initializes List and Opt nta children.
   * @apilevel internal
   * @ast method
   * @declaredat ASTNode:10
   */
  public void init$Children() {
    children = new ASTNode[1];
  }
  /**
   * @declaredat ASTNode:13
   */
  @ASTNodeAnnotation.Constructor(
    name = {"func_def"},
    type = {"func_def"},
    kind = {"Child"}
  )
  public fd(func_def p0) {
    setChild(p0, 0);
  }
  /** @apilevel low-level 
   * @declaredat ASTNode:22
   */
  protected int numChildren() {
    return 1;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:26
   */
  public void flushAttrCache() {
    super.flushAttrCache();

  }
  /** @apilevel internal 
   * @declaredat ASTNode:31
   */
  public void flushCollectionCache() {
    super.flushCollectionCache();

  }
  /** @apilevel internal 
   * @declaredat ASTNode:36
   */
  public fd clone() throws CloneNotSupportedException {
    fd node = (fd) super.clone();
    return node;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:41
   */
  public fd copy() {
    try {
      fd node = (fd) clone();
      node.parent = null;
      if (children != null) {
        node.children = (ASTNode[]) children.clone();
      }
      return node;
    } catch (CloneNotSupportedException e) {
      throw new Error("Error: clone not supported for " + getClass().getName());
    }
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @deprecated Please use treeCopy or treeCopyNoTransform instead
   * @declaredat ASTNode:60
   */
  @Deprecated
  public fd fullCopy() {
    return treeCopyNoTransform();
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @declaredat ASTNode:70
   */
  public fd treeCopyNoTransform() {
    fd tree = (fd) copy();
    if (children != null) {
      for (int i = 0; i < children.length; ++i) {
        ASTNode child = (ASTNode) children[i];
        if (child != null) {
          child = child.treeCopyNoTransform();
          tree.setChild(child, i);
        }
      }
    }
    return tree;
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The subtree of this node is traversed to trigger rewrites before copy.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @declaredat ASTNode:90
   */
  public fd treeCopy() {
    fd tree = (fd) copy();
    if (children != null) {
      for (int i = 0; i < children.length; ++i) {
        ASTNode child = (ASTNode) getChild(i);
        if (child != null) {
          child = child.treeCopy();
          tree.setChild(child, i);
        }
      }
    }
    return tree;
  }
  /**
   * Replaces the func_def child.
   * @param node The new node to replace the func_def child.
   * @apilevel high-level
   */
  public fd setfunc_def(func_def node) {
    setChild(node, 0);
    return this;
  }
  /**
   * Retrieves the func_def child.
   * @return The current node used as the func_def child.
   * @apilevel high-level
   */
  @ASTNodeAnnotation.Child(name="func_def")
  public func_def getfunc_def() {
    return (func_def) getChild(0);
  }
  /**
   * Retrieves the func_def child.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The current node used as the func_def child.
   * @apilevel low-level
   */
  public func_def getfunc_defNoTransform() {
    return (func_def) getChildNoTransform(0);
  }
  /**
   * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/funcOrVarCh.jrag:11
   * @apilevel internal
   */
  public boolean Define_declIsFunction(ASTNode _callerNode, ASTNode _childNode) {
    int childIndex = this.getIndexOfChild(_callerNode);
    return true;
  }
  /**
   * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/funcOrVarCh.jrag:11
   * @apilevel internal
   * @return {@code true} if this node has an equation for the inherited attribute declIsFunction
   */
  protected boolean canDefine_declIsFunction(ASTNode _callerNode, ASTNode _childNode) {
    return true;
  }

}
