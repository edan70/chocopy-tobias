/* This file was generated with JastAdd2 (http://jastadd.org) version 2.3.4 */
package lang.ast;
import java.util.*;
import java.io.PrintStream;
import java.util.Set;
import java.util.TreeSet;
import java.util.ArrayList;
import java.io.ByteArrayOutputStream;
import java.lang.reflect.InvocationTargetException;
import java.util.HashSet;
/**
 * @ast node
 * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/lang.ast:49
 * @astdecl idsD : idOrInt ::= <Str:String>;
 * @production idsD : {@link idOrInt} ::= <span class="component">&lt;Str:{@link String}&gt;</span>;

 */
public class idsD extends idOrInt implements Cloneable {
  /**
   * @aspect CodeGen
   * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/CodeGen.jrag:356
   */
  public void genEval(PrintStream out, IdDecl id) {
		out.println("        movq $ \"" + getStr() + "\", %rax");
	}
  /**
   * @declaredat ASTNode:1
   */
  public idsD() {
    super();
  }
  /**
   * Initializes the child array to the correct size.
   * Initializes List and Opt nta children.
   * @apilevel internal
   * @ast method
   * @declaredat ASTNode:10
   */
  public void init$Children() {
  }
  /**
   * @declaredat ASTNode:12
   */
  @ASTNodeAnnotation.Constructor(
    name = {"Str"},
    type = {"String"},
    kind = {"Token"}
  )
  public idsD(String p0) {
    setStr(p0);
  }
  /**
   * @declaredat ASTNode:20
   */
  public idsD(beaver.Symbol p0) {
    setStr(p0);
  }
  /** @apilevel low-level 
   * @declaredat ASTNode:24
   */
  protected int numChildren() {
    return 0;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:28
   */
  public void flushAttrCache() {
    super.flushAttrCache();
    types_reset();
  }
  /** @apilevel internal 
   * @declaredat ASTNode:33
   */
  public void flushCollectionCache() {
    super.flushCollectionCache();

  }
  /** @apilevel internal 
   * @declaredat ASTNode:38
   */
  public idsD clone() throws CloneNotSupportedException {
    idsD node = (idsD) super.clone();
    return node;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:43
   */
  public idsD copy() {
    try {
      idsD node = (idsD) clone();
      node.parent = null;
      if (children != null) {
        node.children = (ASTNode[]) children.clone();
      }
      return node;
    } catch (CloneNotSupportedException e) {
      throw new Error("Error: clone not supported for " + getClass().getName());
    }
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @deprecated Please use treeCopy or treeCopyNoTransform instead
   * @declaredat ASTNode:62
   */
  @Deprecated
  public idsD fullCopy() {
    return treeCopyNoTransform();
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @declaredat ASTNode:72
   */
  public idsD treeCopyNoTransform() {
    idsD tree = (idsD) copy();
    if (children != null) {
      for (int i = 0; i < children.length; ++i) {
        ASTNode child = (ASTNode) children[i];
        if (child != null) {
          child = child.treeCopyNoTransform();
          tree.setChild(child, i);
        }
      }
    }
    return tree;
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The subtree of this node is traversed to trigger rewrites before copy.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @declaredat ASTNode:92
   */
  public idsD treeCopy() {
    idsD tree = (idsD) copy();
    if (children != null) {
      for (int i = 0; i < children.length; ++i) {
        ASTNode child = (ASTNode) getChild(i);
        if (child != null) {
          child = child.treeCopy();
          tree.setChild(child, i);
        }
      }
    }
    return tree;
  }
  /**
   * Replaces the lexeme Str.
   * @param value The new value for the lexeme Str.
   * @apilevel high-level
   */
  public idsD setStr(String value) {
    tokenString_Str = value;
    return this;
  }
  /** @apilevel internal 
   */
  protected String tokenString_Str;
  /**
   */
  public int Strstart;
  /**
   */
  public int Strend;
  /**
   * JastAdd-internal setter for lexeme Str using the Beaver parser.
   * @param symbol Symbol containing the new value for the lexeme Str
   * @apilevel internal
   */
  public idsD setStr(beaver.Symbol symbol) {
    if (symbol.value != null && !(symbol.value instanceof String))
    throw new UnsupportedOperationException("setStr is only valid for String lexemes");
    tokenString_Str = (String)symbol.value;
    Strstart = symbol.getStart();
    Strend = symbol.getEnd();
    return this;
  }
  /**
   * Retrieves the value for the lexeme Str.
   * @return The value for the lexeme Str.
   * @apilevel high-level
   */
  @ASTNodeAnnotation.Token(name="Str")
  public String getStr() {
    return tokenString_Str != null ? tokenString_Str : "";
  }
/** @apilevel internal */
protected boolean types_visited = false;
  /** @apilevel internal */
  private void types_reset() {
    types_computed = false;
    
    types_value = null;
    types_visited = false;
  }
  /** @apilevel internal */
  protected boolean types_computed = false;

  /** @apilevel internal */
  protected Typen types_value;

  /**
   * @attribute syn
   * @aspect typeChecker
   * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/typeChecker.jrag:75
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="typeChecker", declaredAt="/home/tobias_carlsson/chocopy-tobias/src/jastadd/typeChecker.jrag:75")
  public Typen types() {
    ASTState state = state();
    if (types_computed) {
      return types_value;
    }
    if (types_visited) {
      throw new RuntimeException("Circular definition of attribute idsD.types().");
    }
    types_visited = true;
    state().enterLazyAttribute();
    types_value = stringType();
    types_computed = true;
    state().leaveLazyAttribute();
    types_visited = false;
    return types_value;
  }

}
