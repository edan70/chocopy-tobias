/* This file was generated with JastAdd2 (http://jastadd.org) version 2.3.4 */
package lang.ast;
import java.util.*;
import java.io.PrintStream;
import java.util.Set;
import java.util.TreeSet;
import java.util.ArrayList;
import java.io.ByteArrayOutputStream;
import java.lang.reflect.InvocationTargetException;
import java.util.HashSet;
/**
 * @ast node
 * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/lang.ast:14
 * @astdecl Typen : ASTNode;
 * @production Typen : {@link ASTNode};

 */
public abstract class Typen extends ASTNode<ASTNode> implements Cloneable {
  /**
   * @declaredat ASTNode:1
   */
  public Typen() {
    super();
  }
  /**
   * Initializes the child array to the correct size.
   * Initializes List and Opt nta children.
   * @apilevel internal
   * @ast method
   * @declaredat ASTNode:10
   */
  public void init$Children() {
  }
  /** @apilevel low-level 
   * @declaredat ASTNode:13
   */
  protected int numChildren() {
    return 0;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:17
   */
  public void flushAttrCache() {
    super.flushAttrCache();
    idif_reset();
    compatibleType_Typen_reset();
  }
  /** @apilevel internal 
   * @declaredat ASTNode:23
   */
  public void flushCollectionCache() {
    super.flushCollectionCache();

  }
  /** @apilevel internal 
   * @declaredat ASTNode:28
   */
  public Typen clone() throws CloneNotSupportedException {
    Typen node = (Typen) super.clone();
    return node;
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @deprecated Please use treeCopy or treeCopyNoTransform instead
   * @declaredat ASTNode:39
   */
  @Deprecated
  public abstract Typen fullCopy();
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @declaredat ASTNode:47
   */
  public abstract Typen treeCopyNoTransform();
  /**
   * Create a deep copy of the AST subtree at this node.
   * The subtree of this node is traversed to trigger rewrites before copy.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @declaredat ASTNode:55
   */
  public abstract Typen treeCopy();
/** @apilevel internal */
protected boolean idif_visited = false;
  /** @apilevel internal */
  private void idif_reset() {
    idif_computed = false;
    
    idif_value = null;
    idif_visited = false;
  }
  /** @apilevel internal */
  protected boolean idif_computed = false;

  /** @apilevel internal */
  protected String idif_value;

  /**
   * @attribute syn
   * @aspect NameAnalysis
   * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/nameanalysis.jrag:163
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="NameAnalysis", declaredAt="/home/tobias_carlsson/chocopy-tobias/src/jastadd/nameanalysis.jrag:163")
  public String idif() {
    ASTState state = state();
    if (idif_computed) {
      return idif_value;
    }
    if (idif_visited) {
      throw new RuntimeException("Circular definition of attribute Typen.idif().");
    }
    idif_visited = true;
    state().enterLazyAttribute();
    idif_value = "-";
    idif_computed = true;
    state().leaveLazyAttribute();
    idif_visited = false;
    return idif_value;
  }
/** @apilevel internal */
protected java.util.Set compatibleType_Typen_visited;
  /** @apilevel internal */
  private void compatibleType_Typen_reset() {
    compatibleType_Typen_values = null;
    compatibleType_Typen_visited = null;
  }
  /** @apilevel internal */
  protected java.util.Map compatibleType_Typen_values;

  /**
   * @attribute syn
   * @aspect typeChecker
   * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/typeChecker.jrag:115
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="typeChecker", declaredAt="/home/tobias_carlsson/chocopy-tobias/src/jastadd/typeChecker.jrag:115")
  public boolean compatibleType(Typen type) {
    Object _parameters = type;
    if (compatibleType_Typen_visited == null) compatibleType_Typen_visited = new java.util.HashSet(4);
    if (compatibleType_Typen_values == null) compatibleType_Typen_values = new java.util.HashMap(4);
    ASTState state = state();
    if (compatibleType_Typen_values.containsKey(_parameters)) {
      return (Boolean) compatibleType_Typen_values.get(_parameters);
    }
    if (compatibleType_Typen_visited.contains(_parameters)) {
      throw new RuntimeException("Circular definition of attribute Typen.compatibleType(Typen).");
    }
    compatibleType_Typen_visited.add(_parameters);
    state().enterLazyAttribute();
    boolean compatibleType_Typen_value = this.toString().equals(type.toString());
    compatibleType_Typen_values.put(_parameters, compatibleType_Typen_value);
    state().leaveLazyAttribute();
    compatibleType_Typen_visited.remove(_parameters);
    return compatibleType_Typen_value;
  }

}
