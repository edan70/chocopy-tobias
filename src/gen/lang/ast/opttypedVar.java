/* This file was generated with JastAdd2 (http://jastadd.org) version 2.3.4 */
package lang.ast;
import java.util.*;
import java.io.PrintStream;
import java.util.Set;
import java.util.TreeSet;
import java.util.ArrayList;
import java.io.ByteArrayOutputStream;
import java.lang.reflect.InvocationTargetException;
import java.util.HashSet;
/**
 * @ast node
 * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/lang.ast:73
 * @astdecl opttypedVar : ASTNode ::= typedVar opt:typedVar*;
 * @production opttypedVar : {@link ASTNode} ::= <span class="component">{@link typedVar}</span> <span class="component">opt:{@link typedVar}*</span>;

 */
public class opttypedVar extends ASTNode<ASTNode> implements Cloneable {
  /**
   * @declaredat ASTNode:1
   */
  public opttypedVar() {
    super();
  }
  /**
   * Initializes the child array to the correct size.
   * Initializes List and Opt nta children.
   * @apilevel internal
   * @ast method
   * @declaredat ASTNode:10
   */
  public void init$Children() {
    children = new ASTNode[2];
    setChild(new List(), 1);
  }
  /**
   * @declaredat ASTNode:14
   */
  @ASTNodeAnnotation.Constructor(
    name = {"typedVar", "opt"},
    type = {"typedVar", "List<typedVar>"},
    kind = {"Child", "List"}
  )
  public opttypedVar(typedVar p0, List<typedVar> p1) {
    setChild(p0, 0);
    setChild(p1, 1);
  }
  /** @apilevel low-level 
   * @declaredat ASTNode:24
   */
  protected int numChildren() {
    return 2;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:28
   */
  public void flushAttrCache() {
    super.flushAttrCache();
    globalLookup_String_reset();
  }
  /** @apilevel internal 
   * @declaredat ASTNode:33
   */
  public void flushCollectionCache() {
    super.flushCollectionCache();

  }
  /** @apilevel internal 
   * @declaredat ASTNode:38
   */
  public opttypedVar clone() throws CloneNotSupportedException {
    opttypedVar node = (opttypedVar) super.clone();
    return node;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:43
   */
  public opttypedVar copy() {
    try {
      opttypedVar node = (opttypedVar) clone();
      node.parent = null;
      if (children != null) {
        node.children = (ASTNode[]) children.clone();
      }
      return node;
    } catch (CloneNotSupportedException e) {
      throw new Error("Error: clone not supported for " + getClass().getName());
    }
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @deprecated Please use treeCopy or treeCopyNoTransform instead
   * @declaredat ASTNode:62
   */
  @Deprecated
  public opttypedVar fullCopy() {
    return treeCopyNoTransform();
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @declaredat ASTNode:72
   */
  public opttypedVar treeCopyNoTransform() {
    opttypedVar tree = (opttypedVar) copy();
    if (children != null) {
      for (int i = 0; i < children.length; ++i) {
        ASTNode child = (ASTNode) children[i];
        if (child != null) {
          child = child.treeCopyNoTransform();
          tree.setChild(child, i);
        }
      }
    }
    return tree;
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The subtree of this node is traversed to trigger rewrites before copy.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @declaredat ASTNode:92
   */
  public opttypedVar treeCopy() {
    opttypedVar tree = (opttypedVar) copy();
    if (children != null) {
      for (int i = 0; i < children.length; ++i) {
        ASTNode child = (ASTNode) getChild(i);
        if (child != null) {
          child = child.treeCopy();
          tree.setChild(child, i);
        }
      }
    }
    return tree;
  }
  /**
   * Replaces the typedVar child.
   * @param node The new node to replace the typedVar child.
   * @apilevel high-level
   */
  public opttypedVar settypedVar(typedVar node) {
    setChild(node, 0);
    return this;
  }
  /**
   * Retrieves the typedVar child.
   * @return The current node used as the typedVar child.
   * @apilevel high-level
   */
  @ASTNodeAnnotation.Child(name="typedVar")
  public typedVar gettypedVar() {
    return (typedVar) getChild(0);
  }
  /**
   * Retrieves the typedVar child.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The current node used as the typedVar child.
   * @apilevel low-level
   */
  public typedVar gettypedVarNoTransform() {
    return (typedVar) getChildNoTransform(0);
  }
  /**
   * Replaces the opt list.
   * @param list The new list node to be used as the opt list.
   * @apilevel high-level
   */
  public opttypedVar setoptList(List<typedVar> list) {
    setChild(list, 1);
    return this;
  }
  /**
   * Retrieves the number of children in the opt list.
   * @return Number of children in the opt list.
   * @apilevel high-level
   */
  public int getNumopt() {
    return getoptList().getNumChild();
  }
  /**
   * Retrieves the number of children in the opt list.
   * Calling this method will not trigger rewrites.
   * @return Number of children in the opt list.
   * @apilevel low-level
   */
  public int getNumoptNoTransform() {
    return getoptListNoTransform().getNumChildNoTransform();
  }
  /**
   * Retrieves the element at index {@code i} in the opt list.
   * @param i Index of the element to return.
   * @return The element at position {@code i} in the opt list.
   * @apilevel high-level
   */
  public typedVar getopt(int i) {
    return (typedVar) getoptList().getChild(i);
  }
  /**
   * Check whether the opt list has any children.
   * @return {@code true} if it has at least one child, {@code false} otherwise.
   * @apilevel high-level
   */
  public boolean hasopt() {
    return getoptList().getNumChild() != 0;
  }
  /**
   * Append an element to the opt list.
   * @param node The element to append to the opt list.
   * @apilevel high-level
   */
  public opttypedVar addopt(typedVar node) {
    List<typedVar> list = (parent == null) ? getoptListNoTransform() : getoptList();
    list.addChild(node);
    return this;
  }
  /** @apilevel low-level 
   */
  public opttypedVar addoptNoTransform(typedVar node) {
    List<typedVar> list = getoptListNoTransform();
    list.addChild(node);
    return this;
  }
  /**
   * Replaces the opt list element at index {@code i} with the new node {@code node}.
   * @param node The new node to replace the old list element.
   * @param i The list index of the node to be replaced.
   * @apilevel high-level
   */
  public opttypedVar setopt(typedVar node, int i) {
    List<typedVar> list = getoptList();
    list.setChild(node, i);
    return this;
  }
  /**
   * Retrieves the opt list.
   * @return The node representing the opt list.
   * @apilevel high-level
   */
  @ASTNodeAnnotation.ListChild(name="opt")
  public List<typedVar> getoptList() {
    List<typedVar> list = (List<typedVar>) getChild(1);
    return list;
  }
  /**
   * Retrieves the opt list.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The node representing the opt list.
   * @apilevel low-level
   */
  public List<typedVar> getoptListNoTransform() {
    return (List<typedVar>) getChildNoTransform(1);
  }
  /**
   * @return the element at index {@code i} in the opt list without
   * triggering rewrites.
   */
  public typedVar getoptNoTransform(int i) {
    return (typedVar) getoptListNoTransform().getChildNoTransform(i);
  }
  /**
   * Retrieves the opt list.
   * @return The node representing the opt list.
   * @apilevel high-level
   */
  public List<typedVar> getopts() {
    return getoptList();
  }
  /**
   * Retrieves the opt list.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The node representing the opt list.
   * @apilevel low-level
   */
  public List<typedVar> getoptsNoTransform() {
    return getoptListNoTransform();
  }
  /**
   * @attribute inh
   * @aspect NameAnalysis
   * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/nameanalysis.jrag:336
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.INH)
  @ASTNodeAnnotation.Source(aspect="NameAnalysis", declaredAt="/home/tobias_carlsson/chocopy-tobias/src/jastadd/nameanalysis.jrag:336")
  public IdDecl globalLookup(String name) {
    Object _parameters = name;
    if (globalLookup_String_visited == null) globalLookup_String_visited = new java.util.HashSet(4);
    if (globalLookup_String_values == null) globalLookup_String_values = new java.util.HashMap(4);
    ASTState state = state();
    if (globalLookup_String_values.containsKey(_parameters)) {
      return (IdDecl) globalLookup_String_values.get(_parameters);
    }
    if (globalLookup_String_visited.contains(_parameters)) {
      throw new RuntimeException("Circular definition of attribute opttypedVar.globalLookup(String).");
    }
    globalLookup_String_visited.add(_parameters);
    state().enterLazyAttribute();
    IdDecl globalLookup_String_value = getParent().Define_globalLookup(this, null, name);
    globalLookup_String_values.put(_parameters, globalLookup_String_value);
    state().leaveLazyAttribute();
    globalLookup_String_visited.remove(_parameters);
    return globalLookup_String_value;
  }
/** @apilevel internal */
protected java.util.Set globalLookup_String_visited;
  /** @apilevel internal */
  private void globalLookup_String_reset() {
    globalLookup_String_values = null;
    globalLookup_String_visited = null;
  }
  /** @apilevel internal */
  protected java.util.Map globalLookup_String_values;

  /**
   * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/nameanalysis.jrag:334
   * @apilevel internal
   */
  public IdDecl Define_globalLookup(ASTNode _callerNode, ASTNode _childNode, String name) {
    if (_callerNode == gettypedVarNoTransform()) {
      // @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/nameanalysis.jrag:341
      return globalLookup(name);
    }
    else {
      return getParent().Define_globalLookup(this, _callerNode, name);
    }
  }
  /**
   * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/nameanalysis.jrag:334
   * @apilevel internal
   * @return {@code true} if this node has an equation for the inherited attribute globalLookup
   */
  protected boolean canDefine_globalLookup(ASTNode _callerNode, ASTNode _childNode, String name) {
    return true;
  }
  /**
   * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/paramAdrInd.jrag:16
   * @apilevel internal
   */
  public boolean Define_isParameter(ASTNode _callerNode, ASTNode _childNode) {
    int childIndex = this.getIndexOfChild(_callerNode);
    return true;
  }
  /**
   * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/paramAdrInd.jrag:16
   * @apilevel internal
   * @return {@code true} if this node has an equation for the inherited attribute isParameter
   */
  protected boolean canDefine_isParameter(ASTNode _callerNode, ASTNode _childNode) {
    return true;
  }
  /**
   * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/paramAdrInd.jrag:23
   * @apilevel internal
   */
  public int Define_parameterIndex(ASTNode _callerNode, ASTNode _childNode) {
    if (_callerNode == getoptListNoTransform()) {
      // @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/paramAdrInd.jrag:26
      int i = _callerNode.getIndexOfChild(_childNode);
      return i+1;
    }
    else if (_callerNode == gettypedVarNoTransform()) {
      // @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/paramAdrInd.jrag:25
      return 0;
    }
    else {
      return getParent().Define_parameterIndex(this, _callerNode);
    }
  }
  /**
   * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/paramAdrInd.jrag:23
   * @apilevel internal
   * @return {@code true} if this node has an equation for the inherited attribute parameterIndex
   */
  protected boolean canDefine_parameterIndex(ASTNode _callerNode, ASTNode _childNode) {
    return true;
  }
  /**
   * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/typeChecker.jrag:21
   * @apilevel internal
   */
  public Typen Define_types(ASTNode _callerNode, ASTNode _childNode) {
    if (_callerNode == getoptListNoTransform()) {
      // @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/typeChecker.jrag:42
      int i = _callerNode.getIndexOfChild(_childNode);
      return getopt(i).getTypen();
    }
    else if (_callerNode == gettypedVarNoTransform()) {
      // @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/typeChecker.jrag:41
      return gettypedVar().getTypen();
    }
    else {
      return getParent().Define_types(this, _callerNode);
    }
  }
  /**
   * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/typeChecker.jrag:21
   * @apilevel internal
   * @return {@code true} if this node has an equation for the inherited attribute types
   */
  protected boolean canDefine_types(ASTNode _callerNode, ASTNode _childNode) {
    return true;
  }

}
