/* This file was generated with JastAdd2 (http://jastadd.org) version 2.3.4 */
package lang.ast;
import java.util.*;
import java.io.PrintStream;
import java.util.Set;
import java.util.TreeSet;
import java.util.ArrayList;
import java.io.ByteArrayOutputStream;
import java.lang.reflect.InvocationTargetException;
import java.util.HashSet;
/**
 * @ast node
 * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/lang.ast:39
 * @astdecl idCexpr : cexpr ::= IdUse;
 * @production idCexpr : {@link cexpr} ::= <span class="component">{@link IdUse}</span>;

 */
public class idCexpr extends cexpr implements Cloneable {
  /**
   * @aspect CodeGen
   * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/CodeGen.jrag:536
   */
  public void genEval(PrintStream out, IdDecl id) {

		if(id!=null ){

			int count =id.nbrLoc()-1;
			int ch=0;
			for(Map.Entry mapElement : id.objvars().entrySet()){
				int ind = (int)mapElement.getValue(); 
				var_def vd = (var_def)mapElement.getKey(); 
				if(vd.gettypedVar().getIdDecl().getID().equals(getIdUse().getID())){
					ch++;
					out.println("        movq " +"-"+((ind)*8)+"(%rbp)"+ ", %rax");
				}
				count--;
			}
			if(ch==0){
				out.println("        movq " + getIdUse().decl().address() + ", %rax");
			}
		}
		else if(insideCl()&&insideFunc())
		{				
			out.println("        movq " + getIdUse().decl().address() + ", %rax");
		}
		else if(insideCl())
		{				
			out.println("        movq " + getIdUse().decl().addresse() + ", %rax");
		}else{
			out.println("        movq " + getIdUse().decl().address() + ", %rax");
		}
	}
  /**
   * @aspect CodeGen
   * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/CodeGen.jrag:698
   */
  public void genConditionalJump(PrintStream out, IdDecl id, String falseLabel) {


		getIdUse().genEval(out,id);
		out.println("        movq $TRUE, %rbx");
		out.println("        cmpq %rbx, %rax");

		out.println("        jne " + falseLabel);

	}
  /**
   * @declaredat ASTNode:1
   */
  public idCexpr() {
    super();
  }
  /**
   * Initializes the child array to the correct size.
   * Initializes List and Opt nta children.
   * @apilevel internal
   * @ast method
   * @declaredat ASTNode:10
   */
  public void init$Children() {
    children = new ASTNode[1];
  }
  /**
   * @declaredat ASTNode:13
   */
  @ASTNodeAnnotation.Constructor(
    name = {"IdUse"},
    type = {"IdUse"},
    kind = {"Child"}
  )
  public idCexpr(IdUse p0) {
    setChild(p0, 0);
  }
  /** @apilevel low-level 
   * @declaredat ASTNode:22
   */
  protected int numChildren() {
    return 1;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:26
   */
  public void flushAttrCache() {
    super.flushAttrCache();
    getCexpr_reset();
    lookup_String_reset();
    types_reset();
    isFunction_reset();
    lookupNN_String_reset();
    localLookups_String_reset();
    globalLookup_String_reset();
    insideFunc_reset();
    insideCl_reset();
  }
  /** @apilevel internal 
   * @declaredat ASTNode:39
   */
  public void flushCollectionCache() {
    super.flushCollectionCache();

  }
  /** @apilevel internal 
   * @declaredat ASTNode:44
   */
  public idCexpr clone() throws CloneNotSupportedException {
    idCexpr node = (idCexpr) super.clone();
    return node;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:49
   */
  public idCexpr copy() {
    try {
      idCexpr node = (idCexpr) clone();
      node.parent = null;
      if (children != null) {
        node.children = (ASTNode[]) children.clone();
      }
      return node;
    } catch (CloneNotSupportedException e) {
      throw new Error("Error: clone not supported for " + getClass().getName());
    }
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @deprecated Please use treeCopy or treeCopyNoTransform instead
   * @declaredat ASTNode:68
   */
  @Deprecated
  public idCexpr fullCopy() {
    return treeCopyNoTransform();
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @declaredat ASTNode:78
   */
  public idCexpr treeCopyNoTransform() {
    idCexpr tree = (idCexpr) copy();
    if (children != null) {
      for (int i = 0; i < children.length; ++i) {
        ASTNode child = (ASTNode) children[i];
        if (child != null) {
          child = child.treeCopyNoTransform();
          tree.setChild(child, i);
        }
      }
    }
    return tree;
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The subtree of this node is traversed to trigger rewrites before copy.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @declaredat ASTNode:98
   */
  public idCexpr treeCopy() {
    idCexpr tree = (idCexpr) copy();
    if (children != null) {
      for (int i = 0; i < children.length; ++i) {
        ASTNode child = (ASTNode) getChild(i);
        if (child != null) {
          child = child.treeCopy();
          tree.setChild(child, i);
        }
      }
    }
    return tree;
  }
  /**
   * Replaces the IdUse child.
   * @param node The new node to replace the IdUse child.
   * @apilevel high-level
   */
  public idCexpr setIdUse(IdUse node) {
    setChild(node, 0);
    return this;
  }
  /**
   * Retrieves the IdUse child.
   * @return The current node used as the IdUse child.
   * @apilevel high-level
   */
  @ASTNodeAnnotation.Child(name="IdUse")
  public IdUse getIdUse() {
    return (IdUse) getChild(0);
  }
  /**
   * Retrieves the IdUse child.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The current node used as the IdUse child.
   * @apilevel low-level
   */
  public IdUse getIdUseNoTransform() {
    return (IdUse) getChildNoTransform(0);
  }
/** @apilevel internal */
protected boolean getCexpr_visited = false;
  /** @apilevel internal */
  private void getCexpr_reset() {
    getCexpr_computed = false;
    
    getCexpr_value = null;
    getCexpr_visited = false;
  }
  /** @apilevel internal */
  protected boolean getCexpr_computed = false;

  /** @apilevel internal */
  protected idCexpr getCexpr_value;

  /**
   * @attribute syn
   * @aspect NameAnalysis
   * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/nameanalysis.jrag:82
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="NameAnalysis", declaredAt="/home/tobias_carlsson/chocopy-tobias/src/jastadd/nameanalysis.jrag:81")
  public idCexpr getCexpr() {
    ASTState state = state();
    if (getCexpr_computed) {
      return getCexpr_value;
    }
    if (getCexpr_visited) {
      throw new RuntimeException("Circular definition of attribute Expr.getCexpr().");
    }
    getCexpr_visited = true;
    state().enterLazyAttribute();
    getCexpr_value = this;
    getCexpr_computed = true;
    state().leaveLazyAttribute();
    getCexpr_visited = false;
    return getCexpr_value;
  }
/** @apilevel internal */
protected java.util.Set lookup_String_visited;
  /** @apilevel internal */
  private void lookup_String_reset() {
    lookup_String_values = null;
    lookup_String_visited = null;
  }
  /** @apilevel internal */
  protected java.util.Map lookup_String_values;

  /**
   * @attribute syn
   * @aspect NameAnalysis
   * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/nameanalysis.jrag:198
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="NameAnalysis", declaredAt="/home/tobias_carlsson/chocopy-tobias/src/jastadd/nameanalysis.jrag:198")
  public IdDecl lookup(String name) {
    Object _parameters = name;
    if (lookup_String_visited == null) lookup_String_visited = new java.util.HashSet(4);
    if (lookup_String_values == null) lookup_String_values = new java.util.HashMap(4);
    ASTState state = state();
    if (lookup_String_values.containsKey(_parameters)) {
      return (IdDecl) lookup_String_values.get(_parameters);
    }
    if (lookup_String_visited.contains(_parameters)) {
      throw new RuntimeException("Circular definition of attribute idCexpr.lookup(String).");
    }
    lookup_String_visited.add(_parameters);
    state().enterLazyAttribute();
    IdDecl lookup_String_value = lookup_compute(name);
    lookup_String_values.put(_parameters, lookup_String_value);
    state().leaveLazyAttribute();
    lookup_String_visited.remove(_parameters);
    return lookup_String_value;
  }
  /** @apilevel internal */
  private IdDecl lookup_compute(String name) {
  		if(insideClass()){
  			if(insideFunc()){
  			return globalLookup(name);
  			}
  			return lookupNN(name);
  		}
  		return globalLookup(name);
  	}
/** @apilevel internal */
protected boolean types_visited = false;
  /** @apilevel internal */
  private void types_reset() {
    types_computed = false;
    
    types_value = null;
    types_visited = false;
  }
  /** @apilevel internal */
  protected boolean types_computed = false;

  /** @apilevel internal */
  protected Typen types_value;

  /**
   * @attribute syn
   * @aspect typeChecker
   * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/typeChecker.jrag:68
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="typeChecker", declaredAt="/home/tobias_carlsson/chocopy-tobias/src/jastadd/typeChecker.jrag:68")
  public Typen types() {
    ASTState state = state();
    if (types_computed) {
      return types_value;
    }
    if (types_visited) {
      throw new RuntimeException("Circular definition of attribute idCexpr.types().");
    }
    types_visited = true;
    state().enterLazyAttribute();
    types_value = getIdUse().types();
    types_computed = true;
    state().leaveLazyAttribute();
    types_visited = false;
    return types_value;
  }
  /**
   * @attribute inh
   * @aspect FuncOrVarAnalysis
   * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/funcOrVarCh.jrag:7
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.INH)
  @ASTNodeAnnotation.Source(aspect="FuncOrVarAnalysis", declaredAt="/home/tobias_carlsson/chocopy-tobias/src/jastadd/funcOrVarCh.jrag:7")
  public boolean isFunction() {
    ASTState state = state();
    if (isFunction_computed) {
      return isFunction_value;
    }
    if (isFunction_visited) {
      throw new RuntimeException("Circular definition of attribute idCexpr.isFunction().");
    }
    isFunction_visited = true;
    state().enterLazyAttribute();
    isFunction_value = getParent().Define_isFunction(this, null);
    isFunction_computed = true;
    state().leaveLazyAttribute();
    isFunction_visited = false;
    return isFunction_value;
  }
/** @apilevel internal */
protected boolean isFunction_visited = false;
  /** @apilevel internal */
  private void isFunction_reset() {
    isFunction_computed = false;
    isFunction_visited = false;
  }
  /** @apilevel internal */
  protected boolean isFunction_computed = false;

  /** @apilevel internal */
  protected boolean isFunction_value;

  /**
   * @attribute inh
   * @aspect NameAnalysis
   * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/nameanalysis.jrag:225
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.INH)
  @ASTNodeAnnotation.Source(aspect="NameAnalysis", declaredAt="/home/tobias_carlsson/chocopy-tobias/src/jastadd/nameanalysis.jrag:225")
  public IdDecl lookupNN(String name) {
    Object _parameters = name;
    if (lookupNN_String_visited == null) lookupNN_String_visited = new java.util.HashSet(4);
    if (lookupNN_String_values == null) lookupNN_String_values = new java.util.HashMap(4);
    ASTState state = state();
    if (lookupNN_String_values.containsKey(_parameters)) {
      return (IdDecl) lookupNN_String_values.get(_parameters);
    }
    if (lookupNN_String_visited.contains(_parameters)) {
      throw new RuntimeException("Circular definition of attribute idCexpr.lookupNN(String).");
    }
    lookupNN_String_visited.add(_parameters);
    state().enterLazyAttribute();
    IdDecl lookupNN_String_value = getParent().Define_lookupNN(this, null, name);
    lookupNN_String_values.put(_parameters, lookupNN_String_value);
    state().leaveLazyAttribute();
    lookupNN_String_visited.remove(_parameters);
    return lookupNN_String_value;
  }
/** @apilevel internal */
protected java.util.Set lookupNN_String_visited;
  /** @apilevel internal */
  private void lookupNN_String_reset() {
    lookupNN_String_values = null;
    lookupNN_String_visited = null;
  }
  /** @apilevel internal */
  protected java.util.Map lookupNN_String_values;

  /**
   * @attribute inh
   * @aspect NameAnalysis
   * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/nameanalysis.jrag:285
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.INH)
  @ASTNodeAnnotation.Source(aspect="NameAnalysis", declaredAt="/home/tobias_carlsson/chocopy-tobias/src/jastadd/nameanalysis.jrag:285")
  public IdDecl localLookups(String name) {
    Object _parameters = name;
    if (localLookups_String_visited == null) localLookups_String_visited = new java.util.HashSet(4);
    if (localLookups_String_values == null) localLookups_String_values = new java.util.HashMap(4);
    ASTState state = state();
    if (localLookups_String_values.containsKey(_parameters)) {
      return (IdDecl) localLookups_String_values.get(_parameters);
    }
    if (localLookups_String_visited.contains(_parameters)) {
      throw new RuntimeException("Circular definition of attribute idCexpr.localLookups(String).");
    }
    localLookups_String_visited.add(_parameters);
    state().enterLazyAttribute();
    IdDecl localLookups_String_value = getParent().Define_localLookups(this, null, name);
    localLookups_String_values.put(_parameters, localLookups_String_value);
    state().leaveLazyAttribute();
    localLookups_String_visited.remove(_parameters);
    return localLookups_String_value;
  }
/** @apilevel internal */
protected java.util.Set localLookups_String_visited;
  /** @apilevel internal */
  private void localLookups_String_reset() {
    localLookups_String_values = null;
    localLookups_String_visited = null;
  }
  /** @apilevel internal */
  protected java.util.Map localLookups_String_values;

  /**
   * @attribute inh
   * @aspect NameAnalysis
   * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/nameanalysis.jrag:333
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.INH)
  @ASTNodeAnnotation.Source(aspect="NameAnalysis", declaredAt="/home/tobias_carlsson/chocopy-tobias/src/jastadd/nameanalysis.jrag:333")
  public IdDecl globalLookup(String name) {
    Object _parameters = name;
    if (globalLookup_String_visited == null) globalLookup_String_visited = new java.util.HashSet(4);
    if (globalLookup_String_values == null) globalLookup_String_values = new java.util.HashMap(4);
    ASTState state = state();
    if (globalLookup_String_values.containsKey(_parameters)) {
      return (IdDecl) globalLookup_String_values.get(_parameters);
    }
    if (globalLookup_String_visited.contains(_parameters)) {
      throw new RuntimeException("Circular definition of attribute idCexpr.globalLookup(String).");
    }
    globalLookup_String_visited.add(_parameters);
    state().enterLazyAttribute();
    IdDecl globalLookup_String_value = getParent().Define_globalLookup(this, null, name);
    globalLookup_String_values.put(_parameters, globalLookup_String_value);
    state().leaveLazyAttribute();
    globalLookup_String_visited.remove(_parameters);
    return globalLookup_String_value;
  }
/** @apilevel internal */
protected java.util.Set globalLookup_String_visited;
  /** @apilevel internal */
  private void globalLookup_String_reset() {
    globalLookup_String_values = null;
    globalLookup_String_visited = null;
  }
  /** @apilevel internal */
  protected java.util.Map globalLookup_String_values;

  /**
   * @attribute inh
   * @aspect paramAdrInd
   * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/paramAdrInd.jrag:63
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.INH)
  @ASTNodeAnnotation.Source(aspect="paramAdrInd", declaredAt="/home/tobias_carlsson/chocopy-tobias/src/jastadd/paramAdrInd.jrag:63")
  public boolean insideFunc() {
    ASTState state = state();
    if (insideFunc_computed) {
      return insideFunc_value;
    }
    if (insideFunc_visited) {
      throw new RuntimeException("Circular definition of attribute idCexpr.insideFunc().");
    }
    insideFunc_visited = true;
    state().enterLazyAttribute();
    insideFunc_value = getParent().Define_insideFunc(this, null);
    insideFunc_computed = true;
    state().leaveLazyAttribute();
    insideFunc_visited = false;
    return insideFunc_value;
  }
/** @apilevel internal */
protected boolean insideFunc_visited = false;
  /** @apilevel internal */
  private void insideFunc_reset() {
    insideFunc_computed = false;
    insideFunc_visited = false;
  }
  /** @apilevel internal */
  protected boolean insideFunc_computed = false;

  /** @apilevel internal */
  protected boolean insideFunc_value;

  /**
   * @attribute inh
   * @aspect NameAnalysis
   * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/rndFix.jrag:8
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.INH)
  @ASTNodeAnnotation.Source(aspect="NameAnalysis", declaredAt="/home/tobias_carlsson/chocopy-tobias/src/jastadd/rndFix.jrag:8")
  public boolean insideCl() {
    ASTState state = state();
    if (insideCl_computed) {
      return insideCl_value;
    }
    if (insideCl_visited) {
      throw new RuntimeException("Circular definition of attribute idCexpr.insideCl().");
    }
    insideCl_visited = true;
    state().enterLazyAttribute();
    insideCl_value = getParent().Define_insideCl(this, null);
    insideCl_computed = true;
    state().leaveLazyAttribute();
    insideCl_visited = false;
    return insideCl_value;
  }
/** @apilevel internal */
protected boolean insideCl_visited = false;
  /** @apilevel internal */
  private void insideCl_reset() {
    insideCl_computed = false;
    insideCl_visited = false;
  }
  /** @apilevel internal */
  protected boolean insideCl_computed = false;

  /** @apilevel internal */
  protected boolean insideCl_value;

  /**
   * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/funcOrVarCh.jrag:6
   * @apilevel internal
   */
  public boolean Define_isFunction(ASTNode _callerNode, ASTNode _childNode) {
    if (_callerNode == getIdUseNoTransform()) {
      // @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/funcOrVarCh.jrag:9
      return isFunction();
    }
    else {
      return getParent().Define_isFunction(this, _callerNode);
    }
  }
  /**
   * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/funcOrVarCh.jrag:6
   * @apilevel internal
   * @return {@code true} if this node has an equation for the inherited attribute isFunction
   */
  protected boolean canDefine_isFunction(ASTNode _callerNode, ASTNode _childNode) {
    return true;
  }
  /**
   * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/funcOrVarCh.jrag:11
   * @apilevel internal
   */
  public boolean Define_declIsFunction(ASTNode _callerNode, ASTNode _childNode) {
    if (_callerNode == getIdUseNoTransform()) {
      // @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/funcOrVarCh.jrag:13
      return lookup(getIdUse().getID()).isFunction();
    }
    else {
      return getParent().Define_declIsFunction(this, _callerNode);
    }
  }
  /**
   * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/funcOrVarCh.jrag:11
   * @apilevel internal
   * @return {@code true} if this node has an equation for the inherited attribute declIsFunction
   */
  protected boolean canDefine_declIsFunction(ASTNode _callerNode, ASTNode _childNode) {
    return true;
  }
  /**
   * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/nameanalysis.jrag:207
   * @apilevel internal
   */
  public IdDecl Define_lookup(ASTNode _callerNode, ASTNode _childNode, String name) {
    if (_callerNode == getIdUseNoTransform()) {
      // @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/nameanalysis.jrag:241
      return globalLookup(name);
    }
    else {
      return getParent().Define_lookup(this, _callerNode, name);
    }
  }
  /**
   * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/nameanalysis.jrag:207
   * @apilevel internal
   * @return {@code true} if this node has an equation for the inherited attribute lookup
   */
  protected boolean canDefine_lookup(ASTNode _callerNode, ASTNode _childNode, String name) {
    return true;
  }
  /** @apilevel internal */
  protected void collect_contributors_Program_errors(Program _root, java.util.Map<ASTNode, java.util.Set<ASTNode>> _map) {
    // @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/Errors.jrag:39
    if (lookup(getIdUse().getID()).isUnknown()) {
      {
        Program target = (Program) (program());
        java.util.Set<ASTNode> contributors = _map.get(target);
        if (contributors == null) {
          contributors = new java.util.LinkedHashSet<ASTNode>();
          _map.put((ASTNode) target, contributors);
        }
        contributors.add(this);
      }
    }
    // @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/Errors.jrag:105
    if (isFunction() && ! getIdUse().declIsFunction() && lookup(getIdUse().getID())!=unknownDecl() && !lookup(getIdUse().getID()).isClass()) {
      {
        Program target = (Program) (program());
        java.util.Set<ASTNode> contributors = _map.get(target);
        if (contributors == null) {
          contributors = new java.util.LinkedHashSet<ASTNode>();
          _map.put((ASTNode) target, contributors);
        }
        contributors.add(this);
      }
    }
    // @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/Errors.jrag:110
    if (isFunction() && ! getIdUse().declIsFunction() && insideCl()) {
      {
        Program target = (Program) (program());
        java.util.Set<ASTNode> contributors = _map.get(target);
        if (contributors == null) {
          contributors = new java.util.LinkedHashSet<ASTNode>();
          _map.put((ASTNode) target, contributors);
        }
        contributors.add(this);
      }
    }
    super.collect_contributors_Program_errors(_root, _map);
  }
  /** @apilevel internal */
  protected void contributeTo_Program_errors(Set<ErrorMessage> collection) {
    super.contributeTo_Program_errors(collection);
    if (lookup(getIdUse().getID()).isUnknown()) {
      collection.add(error("The symbol: "+getIdUse().getID() + ", is not defined here" ));
    }
    if (isFunction() && ! getIdUse().declIsFunction() && lookup(getIdUse().getID())!=unknownDecl() && !lookup(getIdUse().getID()).isClass()) {
      collection.add(error("Expected a function but received variable " + getIdUse().getID()));
    }
    if (isFunction() && ! getIdUse().declIsFunction() && insideCl()) {
      collection.add(error("This call is not defined in this scope" ));
    }
  }

}
