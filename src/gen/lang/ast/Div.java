/* This file was generated with JastAdd2 (http://jastadd.org) version 2.3.4 */
package lang.ast;
import java.util.*;
import java.io.PrintStream;
import java.util.Set;
import java.util.TreeSet;
import java.util.ArrayList;
import java.io.ByteArrayOutputStream;
import java.lang.reflect.InvocationTargetException;
import java.util.HashSet;
/**
 * @ast node
 * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/lang.ast:59
 * @astdecl Div : binary ::= Left:cexpr Right:cexpr;
 * @production Div : {@link binary};

 */
public class Div extends binary implements Cloneable {
  /**
   * @aspect CodeGen
   * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/CodeGen.jrag:654
   */
  public void genEval(PrintStream out, IdDecl id) {
		getLeft().genEval(out,id);
		out.println("        pushq %rax");
		getRight().genEval(out,id);
		out.println("        movq %rax, %rbx");
		out.println("        popq %rax");
		out.println("        movq $0, %rdx");// NB: clear RDX to prepare division RDX:RAX / RBX
		out.println("        idivq %rbx");
	}
  /**
   * @declaredat ASTNode:1
   */
  public Div() {
    super();
  }
  /**
   * Initializes the child array to the correct size.
   * Initializes List and Opt nta children.
   * @apilevel internal
   * @ast method
   * @declaredat ASTNode:10
   */
  public void init$Children() {
    children = new ASTNode[2];
  }
  /**
   * @declaredat ASTNode:13
   */
  @ASTNodeAnnotation.Constructor(
    name = {"Left", "Right"},
    type = {"cexpr", "cexpr"},
    kind = {"Child", "Child"}
  )
  public Div(cexpr p0, cexpr p1) {
    setChild(p0, 0);
    setChild(p1, 1);
  }
  /** @apilevel low-level 
   * @declaredat ASTNode:23
   */
  protected int numChildren() {
    return 2;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:27
   */
  public void flushAttrCache() {
    super.flushAttrCache();
    types_reset();
  }
  /** @apilevel internal 
   * @declaredat ASTNode:32
   */
  public void flushCollectionCache() {
    super.flushCollectionCache();

  }
  /** @apilevel internal 
   * @declaredat ASTNode:37
   */
  public Div clone() throws CloneNotSupportedException {
    Div node = (Div) super.clone();
    return node;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:42
   */
  public Div copy() {
    try {
      Div node = (Div) clone();
      node.parent = null;
      if (children != null) {
        node.children = (ASTNode[]) children.clone();
      }
      return node;
    } catch (CloneNotSupportedException e) {
      throw new Error("Error: clone not supported for " + getClass().getName());
    }
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @deprecated Please use treeCopy or treeCopyNoTransform instead
   * @declaredat ASTNode:61
   */
  @Deprecated
  public Div fullCopy() {
    return treeCopyNoTransform();
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @declaredat ASTNode:71
   */
  public Div treeCopyNoTransform() {
    Div tree = (Div) copy();
    if (children != null) {
      for (int i = 0; i < children.length; ++i) {
        ASTNode child = (ASTNode) children[i];
        if (child != null) {
          child = child.treeCopyNoTransform();
          tree.setChild(child, i);
        }
      }
    }
    return tree;
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The subtree of this node is traversed to trigger rewrites before copy.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @declaredat ASTNode:91
   */
  public Div treeCopy() {
    Div tree = (Div) copy();
    if (children != null) {
      for (int i = 0; i < children.length; ++i) {
        ASTNode child = (ASTNode) getChild(i);
        if (child != null) {
          child = child.treeCopy();
          tree.setChild(child, i);
        }
      }
    }
    return tree;
  }
  /**
   * Replaces the Left child.
   * @param node The new node to replace the Left child.
   * @apilevel high-level
   */
  public Div setLeft(cexpr node) {
    setChild(node, 0);
    return this;
  }
  /**
   * Retrieves the Left child.
   * @return The current node used as the Left child.
   * @apilevel high-level
   */
  @ASTNodeAnnotation.Child(name="Left")
  public cexpr getLeft() {
    return (cexpr) getChild(0);
  }
  /**
   * Retrieves the Left child.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The current node used as the Left child.
   * @apilevel low-level
   */
  public cexpr getLeftNoTransform() {
    return (cexpr) getChildNoTransform(0);
  }
  /**
   * Replaces the Right child.
   * @param node The new node to replace the Right child.
   * @apilevel high-level
   */
  public Div setRight(cexpr node) {
    setChild(node, 1);
    return this;
  }
  /**
   * Retrieves the Right child.
   * @return The current node used as the Right child.
   * @apilevel high-level
   */
  @ASTNodeAnnotation.Child(name="Right")
  public cexpr getRight() {
    return (cexpr) getChild(1);
  }
  /**
   * Retrieves the Right child.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The current node used as the Right child.
   * @apilevel low-level
   */
  public cexpr getRightNoTransform() {
    return (cexpr) getChildNoTransform(1);
  }
/** @apilevel internal */
protected boolean types_visited = false;
  /** @apilevel internal */
  private void types_reset() {
    types_computed = false;
    
    types_value = null;
    types_visited = false;
  }
  /** @apilevel internal */
  protected boolean types_computed = false;

  /** @apilevel internal */
  protected Typen types_value;

  /**
   * @attribute syn
   * @aspect typeChecker
   * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/typeChecker.jrag:50
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="typeChecker", declaredAt="/home/tobias_carlsson/chocopy-tobias/src/jastadd/typeChecker.jrag:50")
  public Typen types() {
    ASTState state = state();
    if (types_computed) {
      return types_value;
    }
    if (types_visited) {
      throw new RuntimeException("Circular definition of attribute Div.types().");
    }
    types_visited = true;
    state().enterLazyAttribute();
    types_value = intType();
    types_computed = true;
    state().leaveLazyAttribute();
    types_visited = false;
    return types_value;
  }

}
