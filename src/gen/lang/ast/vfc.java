/* This file was generated with JastAdd2 (http://jastadd.org) version 2.3.4 */
package lang.ast;
import java.util.*;
import java.io.PrintStream;
import java.util.Set;
import java.util.TreeSet;
import java.util.ArrayList;
import java.io.ByteArrayOutputStream;
import java.lang.reflect.InvocationTargetException;
import java.util.HashSet;
/**
 * @ast node
 * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/lang.ast:3
 * @astdecl vfc : ASTNode;
 * @production vfc : {@link ASTNode};

 */
public abstract class vfc extends ASTNode<ASTNode> implements Cloneable {
  /**
   * @aspect CodeGen
   * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/CodeGen.jrag:162
   */
  abstract public void genCode(PrintStream out, IdDecl id);
  /**
   * @aspect NameAnalysis
   * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/nameanalysis.jrag:209
   */
  abstract public IdDecl lookup(String name);
  /**
   * @declaredat ASTNode:1
   */
  public vfc() {
    super();
  }
  /**
   * Initializes the child array to the correct size.
   * Initializes List and Opt nta children.
   * @apilevel internal
   * @ast method
   * @declaredat ASTNode:10
   */
  public void init$Children() {
  }
  /** @apilevel low-level 
   * @declaredat ASTNode:13
   */
  protected int numChildren() {
    return 0;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:17
   */
  public void flushAttrCache() {
    super.flushAttrCache();
    name_reset();
    isClasse_reset();
    getDecl_reset();
    isClass_reset();
    type_reset();
    current_reset();
  }
  /** @apilevel internal 
   * @declaredat ASTNode:27
   */
  public void flushCollectionCache() {
    super.flushCollectionCache();

  }
  /** @apilevel internal 
   * @declaredat ASTNode:32
   */
  public vfc clone() throws CloneNotSupportedException {
    vfc node = (vfc) super.clone();
    return node;
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @deprecated Please use treeCopy or treeCopyNoTransform instead
   * @declaredat ASTNode:43
   */
  @Deprecated
  public abstract vfc fullCopy();
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @declaredat ASTNode:51
   */
  public abstract vfc treeCopyNoTransform();
  /**
   * Create a deep copy of the AST subtree at this node.
   * The subtree of this node is traversed to trigger rewrites before copy.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @declaredat ASTNode:59
   */
  public abstract vfc treeCopy();
/** @apilevel internal */
protected boolean name_visited = false;
  /** @apilevel internal */
  private void name_reset() {
    name_computed = false;
    
    name_value = null;
    name_visited = false;
  }
  /** @apilevel internal */
  protected boolean name_computed = false;

  /** @apilevel internal */
  protected String name_value;

  /**
   * @attribute syn
   * @aspect NameAnalysis
   * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/nameanalysis.jrag:84
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="NameAnalysis", declaredAt="/home/tobias_carlsson/chocopy-tobias/src/jastadd/nameanalysis.jrag:84")
  public String name() {
    ASTState state = state();
    if (name_computed) {
      return name_value;
    }
    if (name_visited) {
      throw new RuntimeException("Circular definition of attribute vfc.name().");
    }
    name_visited = true;
    state().enterLazyAttribute();
    name_value = "-";
    name_computed = true;
    state().leaveLazyAttribute();
    name_visited = false;
    return name_value;
  }
/** @apilevel internal */
protected boolean isClasse_visited = false;
  /** @apilevel internal */
  private void isClasse_reset() {
    isClasse_computed = false;
    isClasse_visited = false;
  }
  /** @apilevel internal */
  protected boolean isClasse_computed = false;

  /** @apilevel internal */
  protected boolean isClasse_value;

  /**
   * @attribute syn
   * @aspect NameAnalysis
   * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/nameanalysis.jrag:87
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="NameAnalysis", declaredAt="/home/tobias_carlsson/chocopy-tobias/src/jastadd/nameanalysis.jrag:87")
  public boolean isClasse() {
    ASTState state = state();
    if (isClasse_computed) {
      return isClasse_value;
    }
    if (isClasse_visited) {
      throw new RuntimeException("Circular definition of attribute vfc.isClasse().");
    }
    isClasse_visited = true;
    state().enterLazyAttribute();
    isClasse_value = false;
    isClasse_computed = true;
    state().leaveLazyAttribute();
    isClasse_visited = false;
    return isClasse_value;
  }
  /**
   * @attribute inh
   * @aspect NameAnalysis
   * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/nameanalysis.jrag:104
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.INH)
  @ASTNodeAnnotation.Source(aspect="NameAnalysis", declaredAt="/home/tobias_carlsson/chocopy-tobias/src/jastadd/nameanalysis.jrag:104")
  public IdDecl getDecl() {
    ASTState state = state();
    if (getDecl_computed) {
      return getDecl_value;
    }
    if (getDecl_visited) {
      throw new RuntimeException("Circular definition of attribute vfc.getDecl().");
    }
    getDecl_visited = true;
    state().enterLazyAttribute();
    getDecl_value = getParent().Define_getDecl(this, null);
    getDecl_computed = true;
    state().leaveLazyAttribute();
    getDecl_visited = false;
    return getDecl_value;
  }
/** @apilevel internal */
protected boolean getDecl_visited = false;
  /** @apilevel internal */
  private void getDecl_reset() {
    getDecl_computed = false;
    
    getDecl_value = null;
    getDecl_visited = false;
  }
  /** @apilevel internal */
  protected boolean getDecl_computed = false;

  /** @apilevel internal */
  protected IdDecl getDecl_value;

  /**
   * @attribute inh
   * @aspect NameAnalysis
   * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/nameanalysis.jrag:108
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.INH)
  @ASTNodeAnnotation.Source(aspect="NameAnalysis", declaredAt="/home/tobias_carlsson/chocopy-tobias/src/jastadd/nameanalysis.jrag:108")
  public boolean isClass() {
    ASTState state = state();
    if (isClass_computed) {
      return isClass_value;
    }
    if (isClass_visited) {
      throw new RuntimeException("Circular definition of attribute vfc.isClass().");
    }
    isClass_visited = true;
    state().enterLazyAttribute();
    isClass_value = getParent().Define_isClass(this, null);
    isClass_computed = true;
    state().leaveLazyAttribute();
    isClass_visited = false;
    return isClass_value;
  }
/** @apilevel internal */
protected boolean isClass_visited = false;
  /** @apilevel internal */
  private void isClass_reset() {
    isClass_computed = false;
    isClass_visited = false;
  }
  /** @apilevel internal */
  protected boolean isClass_computed = false;

  /** @apilevel internal */
  protected boolean isClass_value;

  /**
   * @attribute inh
   * @aspect NameAnalysis
   * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/nameanalysis.jrag:180
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.INH)
  @ASTNodeAnnotation.Source(aspect="NameAnalysis", declaredAt="/home/tobias_carlsson/chocopy-tobias/src/jastadd/nameanalysis.jrag:180")
  public String type() {
    ASTState state = state();
    if (type_computed) {
      return type_value;
    }
    if (type_visited) {
      throw new RuntimeException("Circular definition of attribute vfc.type().");
    }
    type_visited = true;
    state().enterLazyAttribute();
    type_value = getParent().Define_type(this, null);
    type_computed = true;
    state().leaveLazyAttribute();
    type_visited = false;
    return type_value;
  }
/** @apilevel internal */
protected boolean type_visited = false;
  /** @apilevel internal */
  private void type_reset() {
    type_computed = false;
    
    type_value = null;
    type_visited = false;
  }
  /** @apilevel internal */
  protected boolean type_computed = false;

  /** @apilevel internal */
  protected String type_value;

  /**
   * @attribute inh
   * @aspect NameAnalysis
   * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/nameanalysis.jrag:186
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.INH)
  @ASTNodeAnnotation.Source(aspect="NameAnalysis", declaredAt="/home/tobias_carlsson/chocopy-tobias/src/jastadd/nameanalysis.jrag:186")
  public Typen current() {
    ASTState state = state();
    if (current_computed) {
      return current_value;
    }
    if (current_visited) {
      throw new RuntimeException("Circular definition of attribute vfc.current().");
    }
    current_visited = true;
    state().enterLazyAttribute();
    current_value = getParent().Define_current(this, null);
    current_computed = true;
    state().leaveLazyAttribute();
    current_visited = false;
    return current_value;
  }
/** @apilevel internal */
protected boolean current_visited = false;
  /** @apilevel internal */
  private void current_reset() {
    current_computed = false;
    
    current_value = null;
    current_visited = false;
  }
  /** @apilevel internal */
  protected boolean current_computed = false;

  /** @apilevel internal */
  protected Typen current_value;


}
