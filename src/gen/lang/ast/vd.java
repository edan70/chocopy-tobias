/* This file was generated with JastAdd2 (http://jastadd.org) version 2.3.4 */
package lang.ast;
import java.util.*;
import java.io.PrintStream;
import java.util.Set;
import java.util.TreeSet;
import java.util.ArrayList;
import java.io.ByteArrayOutputStream;
import java.lang.reflect.InvocationTargetException;
import java.util.HashSet;
/**
 * @ast node
 * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/lang.ast:10
 * @astdecl vd : vf ::= var_def;
 * @production vd : {@link vf} ::= <span class="component">{@link var_def}</span>;

 */
public class vd extends vf implements Cloneable {
  /**
   * @aspect CodeGen
   * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/CodeGen.jrag:263
   */
  public void genCode(PrintStream out, IdDecl id) {
		getvar_def().genCode(out,id);
	}
  /**
   * @declaredat ASTNode:1
   */
  public vd() {
    super();
  }
  /**
   * Initializes the child array to the correct size.
   * Initializes List and Opt nta children.
   * @apilevel internal
   * @ast method
   * @declaredat ASTNode:10
   */
  public void init$Children() {
    children = new ASTNode[1];
  }
  /**
   * @declaredat ASTNode:13
   */
  @ASTNodeAnnotation.Constructor(
    name = {"var_def"},
    type = {"var_def"},
    kind = {"Child"}
  )
  public vd(var_def p0) {
    setChild(p0, 0);
  }
  /** @apilevel low-level 
   * @declaredat ASTNode:22
   */
  protected int numChildren() {
    return 1;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:26
   */
  public void flushAttrCache() {
    super.flushAttrCache();

  }
  /** @apilevel internal 
   * @declaredat ASTNode:31
   */
  public void flushCollectionCache() {
    super.flushCollectionCache();

  }
  /** @apilevel internal 
   * @declaredat ASTNode:36
   */
  public vd clone() throws CloneNotSupportedException {
    vd node = (vd) super.clone();
    return node;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:41
   */
  public vd copy() {
    try {
      vd node = (vd) clone();
      node.parent = null;
      if (children != null) {
        node.children = (ASTNode[]) children.clone();
      }
      return node;
    } catch (CloneNotSupportedException e) {
      throw new Error("Error: clone not supported for " + getClass().getName());
    }
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @deprecated Please use treeCopy or treeCopyNoTransform instead
   * @declaredat ASTNode:60
   */
  @Deprecated
  public vd fullCopy() {
    return treeCopyNoTransform();
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @declaredat ASTNode:70
   */
  public vd treeCopyNoTransform() {
    vd tree = (vd) copy();
    if (children != null) {
      for (int i = 0; i < children.length; ++i) {
        ASTNode child = (ASTNode) children[i];
        if (child != null) {
          child = child.treeCopyNoTransform();
          tree.setChild(child, i);
        }
      }
    }
    return tree;
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The subtree of this node is traversed to trigger rewrites before copy.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @declaredat ASTNode:90
   */
  public vd treeCopy() {
    vd tree = (vd) copy();
    if (children != null) {
      for (int i = 0; i < children.length; ++i) {
        ASTNode child = (ASTNode) getChild(i);
        if (child != null) {
          child = child.treeCopy();
          tree.setChild(child, i);
        }
      }
    }
    return tree;
  }
  /**
   * Replaces the var_def child.
   * @param node The new node to replace the var_def child.
   * @apilevel high-level
   */
  public vd setvar_def(var_def node) {
    setChild(node, 0);
    return this;
  }
  /**
   * Retrieves the var_def child.
   * @return The current node used as the var_def child.
   * @apilevel high-level
   */
  @ASTNodeAnnotation.Child(name="var_def")
  public var_def getvar_def() {
    return (var_def) getChild(0);
  }
  /**
   * Retrieves the var_def child.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The current node used as the var_def child.
   * @apilevel low-level
   */
  public var_def getvar_defNoTransform() {
    return (var_def) getChildNoTransform(0);
  }
  /**
   * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/funcOrVarCh.jrag:11
   * @apilevel internal
   */
  public boolean Define_declIsFunction(ASTNode _callerNode, ASTNode _childNode) {
    int childIndex = this.getIndexOfChild(_callerNode);
    return false;
  }
  /**
   * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/funcOrVarCh.jrag:11
   * @apilevel internal
   * @return {@code true} if this node has an equation for the inherited attribute declIsFunction
   */
  protected boolean canDefine_declIsFunction(ASTNode _callerNode, ASTNode _childNode) {
    return true;
  }

}
