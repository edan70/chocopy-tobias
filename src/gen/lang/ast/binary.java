/* This file was generated with JastAdd2 (http://jastadd.org) version 2.3.4 */
package lang.ast;
import java.util.*;
import java.io.PrintStream;
import java.util.Set;
import java.util.TreeSet;
import java.util.ArrayList;
import java.io.ByteArrayOutputStream;
import java.lang.reflect.InvocationTargetException;
import java.util.HashSet;
/**
 * @ast node
 * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/lang.ast:55
 * @astdecl binary : cexpr ::= Left:cexpr Right:cexpr;
 * @production binary : {@link cexpr} ::= <span class="component">Left:{@link cexpr}</span> <span class="component">Right:{@link cexpr}</span>;

 */
public abstract class binary extends cexpr implements Cloneable {
  /**
   * @declaredat ASTNode:1
   */
  public binary() {
    super();
  }
  /**
   * Initializes the child array to the correct size.
   * Initializes List and Opt nta children.
   * @apilevel internal
   * @ast method
   * @declaredat ASTNode:10
   */
  public void init$Children() {
    children = new ASTNode[2];
  }
  /**
   * @declaredat ASTNode:13
   */
  @ASTNodeAnnotation.Constructor(
    name = {"Left", "Right"},
    type = {"cexpr", "cexpr"},
    kind = {"Child", "Child"}
  )
  public binary(cexpr p0, cexpr p1) {
    setChild(p0, 0);
    setChild(p1, 1);
  }
  /** @apilevel low-level 
   * @declaredat ASTNode:23
   */
  protected int numChildren() {
    return 2;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:27
   */
  public void flushAttrCache() {
    super.flushAttrCache();
    typesL_reset();
    typesR_reset();
    expectedType_reset();
  }
  /** @apilevel internal 
   * @declaredat ASTNode:34
   */
  public void flushCollectionCache() {
    super.flushCollectionCache();

  }
  /** @apilevel internal 
   * @declaredat ASTNode:39
   */
  public binary clone() throws CloneNotSupportedException {
    binary node = (binary) super.clone();
    return node;
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @deprecated Please use treeCopy or treeCopyNoTransform instead
   * @declaredat ASTNode:50
   */
  @Deprecated
  public abstract binary fullCopy();
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @declaredat ASTNode:58
   */
  public abstract binary treeCopyNoTransform();
  /**
   * Create a deep copy of the AST subtree at this node.
   * The subtree of this node is traversed to trigger rewrites before copy.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @declaredat ASTNode:66
   */
  public abstract binary treeCopy();
  /**
   * Replaces the Left child.
   * @param node The new node to replace the Left child.
   * @apilevel high-level
   */
  public binary setLeft(cexpr node) {
    setChild(node, 0);
    return this;
  }
  /**
   * Retrieves the Left child.
   * @return The current node used as the Left child.
   * @apilevel high-level
   */
  @ASTNodeAnnotation.Child(name="Left")
  public cexpr getLeft() {
    return (cexpr) getChild(0);
  }
  /**
   * Retrieves the Left child.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The current node used as the Left child.
   * @apilevel low-level
   */
  public cexpr getLeftNoTransform() {
    return (cexpr) getChildNoTransform(0);
  }
  /**
   * Replaces the Right child.
   * @param node The new node to replace the Right child.
   * @apilevel high-level
   */
  public binary setRight(cexpr node) {
    setChild(node, 1);
    return this;
  }
  /**
   * Retrieves the Right child.
   * @return The current node used as the Right child.
   * @apilevel high-level
   */
  @ASTNodeAnnotation.Child(name="Right")
  public cexpr getRight() {
    return (cexpr) getChild(1);
  }
  /**
   * Retrieves the Right child.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The current node used as the Right child.
   * @apilevel low-level
   */
  public cexpr getRightNoTransform() {
    return (cexpr) getChildNoTransform(1);
  }
/** @apilevel internal */
protected boolean typesL_visited = false;
  /** @apilevel internal */
  private void typesL_reset() {
    typesL_computed = false;
    
    typesL_value = null;
    typesL_visited = false;
  }
  /** @apilevel internal */
  protected boolean typesL_computed = false;

  /** @apilevel internal */
  protected Typen typesL_value;

  /**
   * @attribute syn
   * @aspect typeChecker
   * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/typeChecker.jrag:78
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="typeChecker", declaredAt="/home/tobias_carlsson/chocopy-tobias/src/jastadd/typeChecker.jrag:78")
  public Typen typesL() {
    ASTState state = state();
    if (typesL_computed) {
      return typesL_value;
    }
    if (typesL_visited) {
      throw new RuntimeException("Circular definition of attribute binary.typesL().");
    }
    typesL_visited = true;
    state().enterLazyAttribute();
    typesL_value = getLeft().types();
    typesL_computed = true;
    state().leaveLazyAttribute();
    typesL_visited = false;
    return typesL_value;
  }
/** @apilevel internal */
protected boolean typesR_visited = false;
  /** @apilevel internal */
  private void typesR_reset() {
    typesR_computed = false;
    
    typesR_value = null;
    typesR_visited = false;
  }
  /** @apilevel internal */
  protected boolean typesR_computed = false;

  /** @apilevel internal */
  protected Typen typesR_value;

  /**
   * @attribute syn
   * @aspect typeChecker
   * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/typeChecker.jrag:79
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="typeChecker", declaredAt="/home/tobias_carlsson/chocopy-tobias/src/jastadd/typeChecker.jrag:79")
  public Typen typesR() {
    ASTState state = state();
    if (typesR_computed) {
      return typesR_value;
    }
    if (typesR_visited) {
      throw new RuntimeException("Circular definition of attribute binary.typesR().");
    }
    typesR_visited = true;
    state().enterLazyAttribute();
    typesR_value = getRight().types();
    typesR_computed = true;
    state().leaveLazyAttribute();
    typesR_visited = false;
    return typesR_value;
  }
/** @apilevel internal */
protected boolean expectedType_visited = false;
  /** @apilevel internal */
  private void expectedType_reset() {
    expectedType_computed = false;
    
    expectedType_value = null;
    expectedType_visited = false;
  }
  /** @apilevel internal */
  protected boolean expectedType_computed = false;

  /** @apilevel internal */
  protected Typen expectedType_value;

  /**
   * @attribute syn
   * @aspect typeChecker
   * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/typeChecker.jrag:84
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="typeChecker", declaredAt="/home/tobias_carlsson/chocopy-tobias/src/jastadd/typeChecker.jrag:84")
  public Typen expectedType() {
    ASTState state = state();
    if (expectedType_computed) {
      return expectedType_value;
    }
    if (expectedType_visited) {
      throw new RuntimeException("Circular definition of attribute binary.expectedType().");
    }
    expectedType_visited = true;
    state().enterLazyAttribute();
    expectedType_value = intType();
    expectedType_computed = true;
    state().leaveLazyAttribute();
    expectedType_visited = false;
    return expectedType_value;
  }
  /** @apilevel internal */
  protected void collect_contributors_Program_errors(Program _root, java.util.Map<ASTNode, java.util.Set<ASTNode>> _map) {
    // @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/Errors.jrag:89
    if (!typesL().compatibleType(expectedType())) {
      {
        Program target = (Program) (program());
        java.util.Set<ASTNode> contributors = _map.get(target);
        if (contributors == null) {
          contributors = new java.util.LinkedHashSet<ASTNode>();
          _map.put((ASTNode) target, contributors);
        }
        contributors.add(this);
      }
    }
    // @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/Errors.jrag:92
    if (!typesR().compatibleType(expectedType())) {
      {
        Program target = (Program) (program());
        java.util.Set<ASTNode> contributors = _map.get(target);
        if (contributors == null) {
          contributors = new java.util.LinkedHashSet<ASTNode>();
          _map.put((ASTNode) target, contributors);
        }
        contributors.add(this);
      }
    }
    super.collect_contributors_Program_errors(_root, _map);
  }
  /** @apilevel internal */
  protected void contributeTo_Program_errors(Set<ErrorMessage> collection) {
    super.contributeTo_Program_errors(collection);
    if (!typesL().compatibleType(expectedType())) {
      collection.add(error("Expected: '" +expectedType() + "' but recieved: '"+ typesL()+"'"));
    }
    if (!typesR().compatibleType(expectedType())) {
      collection.add(error("Expected: '" +expectedType() + "' but recieved: '"+ typesR()+"'"));
    }
  }

}
