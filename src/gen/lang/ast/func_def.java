/* This file was generated with JastAdd2 (http://jastadd.org) version 2.3.4 */
package lang.ast;
import java.util.*;
import java.io.PrintStream;
import java.util.Set;
import java.util.TreeSet;
import java.util.ArrayList;
import java.io.ByteArrayOutputStream;
import java.lang.reflect.InvocationTargetException;
import java.util.HashSet;
/**
 * @ast node
 * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/lang.ast:5
 * @astdecl func_def : vfc ::= IdDecl [opttypedVar] [Typen] func_body;
 * @production func_def : {@link vfc} ::= <span class="component">{@link IdDecl}</span> <span class="component">[{@link opttypedVar}]</span> <span class="component">[{@link Typen}]</span> <span class="component">{@link func_body}</span>;

 */
public class func_def extends vfc implements Cloneable {
  /**
   * @aspect CodeGen
   * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/CodeGen.jrag:220
   */
  public void genCode(PrintStream out, IdDecl id) {

		if(!insideCl()){
			out.println(getIdDecl().getID() + ':');
			out.println("        pushq %rbp");
			out.println("        movq %rsp, %rbp");
			getfunc_body().genCode(out,id);
			out.println("        movq $0, %rax");
			out.println(getIdDecl().getID() + "_ret:");
			out.println("        movq %rbp, %rsp");
			out.println("        popq %rbp");
			out.println("        ret");
		}
	}
  /**
   * @aspect NameAnalysis
   * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/nameanalysis.jrag:145
   */
  public IdDecl lookup(String name) {
		if (getIdDecl().getID().equals(name)) {
			return getIdDecl();
		}
		else if (name.equals("print")) {
		}
		return unknownDecl();
	}
  /**
   * @declaredat ASTNode:1
   */
  public func_def() {
    super();
  }
  /**
   * Initializes the child array to the correct size.
   * Initializes List and Opt nta children.
   * @apilevel internal
   * @ast method
   * @declaredat ASTNode:10
   */
  public void init$Children() {
    children = new ASTNode[4];
    setChild(new Opt(), 1);
    setChild(new Opt(), 2);
  }
  /**
   * @declaredat ASTNode:15
   */
  @ASTNodeAnnotation.Constructor(
    name = {"IdDecl", "opttypedVar", "Typen", "func_body"},
    type = {"IdDecl", "Opt<opttypedVar>", "Opt<Typen>", "func_body"},
    kind = {"Child", "Opt", "Opt", "Child"}
  )
  public func_def(IdDecl p0, Opt<opttypedVar> p1, Opt<Typen> p2, func_body p3) {
    setChild(p0, 0);
    setChild(p1, 1);
    setChild(p2, 2);
    setChild(p3, 3);
  }
  /** @apilevel low-level 
   * @declaredat ASTNode:27
   */
  protected int numChildren() {
    return 4;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:31
   */
  public void flushAttrCache() {
    super.flushAttrCache();
    type_reset();
    current_reset();
    localLookup_String_reset();
    nbrLoc_reset();
    wrongRetType_reset();
    types_reset();
    localLookups_String_reset();
    globalLookup_String_reset();
    insideCl_reset();
    objType_reset();
    unknownType_reset();
  }
  /** @apilevel internal 
   * @declaredat ASTNode:46
   */
  public void flushCollectionCache() {
    super.flushCollectionCache();

  }
  /** @apilevel internal 
   * @declaredat ASTNode:51
   */
  public func_def clone() throws CloneNotSupportedException {
    func_def node = (func_def) super.clone();
    return node;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:56
   */
  public func_def copy() {
    try {
      func_def node = (func_def) clone();
      node.parent = null;
      if (children != null) {
        node.children = (ASTNode[]) children.clone();
      }
      return node;
    } catch (CloneNotSupportedException e) {
      throw new Error("Error: clone not supported for " + getClass().getName());
    }
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @deprecated Please use treeCopy or treeCopyNoTransform instead
   * @declaredat ASTNode:75
   */
  @Deprecated
  public func_def fullCopy() {
    return treeCopyNoTransform();
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @declaredat ASTNode:85
   */
  public func_def treeCopyNoTransform() {
    func_def tree = (func_def) copy();
    if (children != null) {
      for (int i = 0; i < children.length; ++i) {
        ASTNode child = (ASTNode) children[i];
        if (child != null) {
          child = child.treeCopyNoTransform();
          tree.setChild(child, i);
        }
      }
    }
    return tree;
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The subtree of this node is traversed to trigger rewrites before copy.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @declaredat ASTNode:105
   */
  public func_def treeCopy() {
    func_def tree = (func_def) copy();
    if (children != null) {
      for (int i = 0; i < children.length; ++i) {
        ASTNode child = (ASTNode) getChild(i);
        if (child != null) {
          child = child.treeCopy();
          tree.setChild(child, i);
        }
      }
    }
    return tree;
  }
  /**
   * Replaces the IdDecl child.
   * @param node The new node to replace the IdDecl child.
   * @apilevel high-level
   */
  public func_def setIdDecl(IdDecl node) {
    setChild(node, 0);
    return this;
  }
  /**
   * Retrieves the IdDecl child.
   * @return The current node used as the IdDecl child.
   * @apilevel high-level
   */
  @ASTNodeAnnotation.Child(name="IdDecl")
  public IdDecl getIdDecl() {
    return (IdDecl) getChild(0);
  }
  /**
   * Retrieves the IdDecl child.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The current node used as the IdDecl child.
   * @apilevel low-level
   */
  public IdDecl getIdDeclNoTransform() {
    return (IdDecl) getChildNoTransform(0);
  }
  /**
   * Replaces the optional node for the opttypedVar child. This is the <code>Opt</code>
   * node containing the child opttypedVar, not the actual child!
   * @param opt The new node to be used as the optional node for the opttypedVar child.
   * @apilevel low-level
   */
  public func_def setopttypedVarOpt(Opt<opttypedVar> opt) {
    setChild(opt, 1);
    return this;
  }
  /**
   * Replaces the (optional) opttypedVar child.
   * @param node The new node to be used as the opttypedVar child.
   * @apilevel high-level
   */
  public func_def setopttypedVar(opttypedVar node) {
    getopttypedVarOpt().setChild(node, 0);
    return this;
  }
  /**
   * Check whether the optional opttypedVar child exists.
   * @return {@code true} if the optional opttypedVar child exists, {@code false} if it does not.
   * @apilevel high-level
   */
  public boolean hasopttypedVar() {
    return getopttypedVarOpt().getNumChild() != 0;
  }
  /**
   * Retrieves the (optional) opttypedVar child.
   * @return The opttypedVar child, if it exists. Returns {@code null} otherwise.
   * @apilevel low-level
   */
  public opttypedVar getopttypedVar() {
    return (opttypedVar) getopttypedVarOpt().getChild(0);
  }
  /**
   * Retrieves the optional node for the opttypedVar child. This is the <code>Opt</code> node containing the child opttypedVar, not the actual child!
   * @return The optional node for child the opttypedVar child.
   * @apilevel low-level
   */
  @ASTNodeAnnotation.OptChild(name="opttypedVar")
  public Opt<opttypedVar> getopttypedVarOpt() {
    return (Opt<opttypedVar>) getChild(1);
  }
  /**
   * Retrieves the optional node for child opttypedVar. This is the <code>Opt</code> node containing the child opttypedVar, not the actual child!
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The optional node for child opttypedVar.
   * @apilevel low-level
   */
  public Opt<opttypedVar> getopttypedVarOptNoTransform() {
    return (Opt<opttypedVar>) getChildNoTransform(1);
  }
  /**
   * Replaces the optional node for the Typen child. This is the <code>Opt</code>
   * node containing the child Typen, not the actual child!
   * @param opt The new node to be used as the optional node for the Typen child.
   * @apilevel low-level
   */
  public func_def setTypenOpt(Opt<Typen> opt) {
    setChild(opt, 2);
    return this;
  }
  /**
   * Replaces the (optional) Typen child.
   * @param node The new node to be used as the Typen child.
   * @apilevel high-level
   */
  public func_def setTypen(Typen node) {
    getTypenOpt().setChild(node, 0);
    return this;
  }
  /**
   * Check whether the optional Typen child exists.
   * @return {@code true} if the optional Typen child exists, {@code false} if it does not.
   * @apilevel high-level
   */
  public boolean hasTypen() {
    return getTypenOpt().getNumChild() != 0;
  }
  /**
   * Retrieves the (optional) Typen child.
   * @return The Typen child, if it exists. Returns {@code null} otherwise.
   * @apilevel low-level
   */
  public Typen getTypen() {
    return (Typen) getTypenOpt().getChild(0);
  }
  /**
   * Retrieves the optional node for the Typen child. This is the <code>Opt</code> node containing the child Typen, not the actual child!
   * @return The optional node for child the Typen child.
   * @apilevel low-level
   */
  @ASTNodeAnnotation.OptChild(name="Typen")
  public Opt<Typen> getTypenOpt() {
    return (Opt<Typen>) getChild(2);
  }
  /**
   * Retrieves the optional node for child Typen. This is the <code>Opt</code> node containing the child Typen, not the actual child!
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The optional node for child Typen.
   * @apilevel low-level
   */
  public Opt<Typen> getTypenOptNoTransform() {
    return (Opt<Typen>) getChildNoTransform(2);
  }
  /**
   * Replaces the func_body child.
   * @param node The new node to replace the func_body child.
   * @apilevel high-level
   */
  public func_def setfunc_body(func_body node) {
    setChild(node, 3);
    return this;
  }
  /**
   * Retrieves the func_body child.
   * @return The current node used as the func_body child.
   * @apilevel high-level
   */
  @ASTNodeAnnotation.Child(name="func_body")
  public func_body getfunc_body() {
    return (func_body) getChild(3);
  }
  /**
   * Retrieves the func_body child.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The current node used as the func_body child.
   * @apilevel low-level
   */
  public func_body getfunc_bodyNoTransform() {
    return (func_body) getChildNoTransform(3);
  }
/** @apilevel internal */
protected boolean type_visited = false;
  /** @apilevel internal */
  private void type_reset() {
    type_computed = false;
    
    type_value = null;
    type_visited = false;
  }
  /** @apilevel internal */
  protected boolean type_computed = false;

  /** @apilevel internal */
  protected String type_value;

  /**
   * @attribute syn
   * @aspect NameAnalysis
   * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/nameanalysis.jrag:184
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="NameAnalysis", declaredAt="/home/tobias_carlsson/chocopy-tobias/src/jastadd/nameanalysis.jrag:184")
  public String type() {
    ASTState state = state();
    if (type_computed) {
      return type_value;
    }
    if (type_visited) {
      throw new RuntimeException("Circular definition of attribute func_def.type().");
    }
    type_visited = true;
    state().enterLazyAttribute();
    type_value = "funcdef";
    type_computed = true;
    state().leaveLazyAttribute();
    type_visited = false;
    return type_value;
  }
/** @apilevel internal */
protected boolean current_visited = false;
  /** @apilevel internal */
  private void current_reset() {
    current_computed = false;
    
    current_value = null;
    current_visited = false;
  }
  /** @apilevel internal */
  protected boolean current_computed = false;

  /** @apilevel internal */
  protected Typen current_value;

  /**
   * @attribute syn
   * @aspect NameAnalysis
   * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/nameanalysis.jrag:189
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="NameAnalysis", declaredAt="/home/tobias_carlsson/chocopy-tobias/src/jastadd/nameanalysis.jrag:189")
  public Typen current() {
    ASTState state = state();
    if (current_computed) {
      return current_value;
    }
    if (current_visited) {
      throw new RuntimeException("Circular definition of attribute func_def.current().");
    }
    current_visited = true;
    state().enterLazyAttribute();
    current_value = getTypen();
    current_computed = true;
    state().leaveLazyAttribute();
    current_visited = false;
    return current_value;
  }
/** @apilevel internal */
protected java.util.Set localLookup_String_visited;
  /** @apilevel internal */
  private void localLookup_String_reset() {
    localLookup_String_values = null;
    localLookup_String_visited = null;
  }
  /** @apilevel internal */
  protected java.util.Map localLookup_String_values;

  /**
   * @attribute syn
   * @aspect NameAnalysis
   * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/nameanalysis.jrag:300
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="NameAnalysis", declaredAt="/home/tobias_carlsson/chocopy-tobias/src/jastadd/nameanalysis.jrag:300")
  public IdDecl localLookup(String name) {
    Object _parameters = name;
    if (localLookup_String_visited == null) localLookup_String_visited = new java.util.HashSet(4);
    if (localLookup_String_values == null) localLookup_String_values = new java.util.HashMap(4);
    ASTState state = state();
    if (localLookup_String_values.containsKey(_parameters)) {
      return (IdDecl) localLookup_String_values.get(_parameters);
    }
    if (localLookup_String_visited.contains(_parameters)) {
      throw new RuntimeException("Circular definition of attribute func_def.localLookup(String).");
    }
    localLookup_String_visited.add(_parameters);
    state().enterLazyAttribute();
    IdDecl localLookup_String_value = localLookup_compute(name);
    localLookup_String_values.put(_parameters, localLookup_String_value);
    state().leaveLazyAttribute();
    localLookup_String_visited.remove(_parameters);
    return localLookup_String_value;
  }
  /** @apilevel internal */
  private IdDecl localLookup_compute(String name) {
  		if(hasopttypedVar()){
  			if(getopttypedVar().gettypedVar().getIdDecl().getID().equals(name)){
  				return getopttypedVar().gettypedVar().getIdDecl();
  			}
  			if(getopttypedVar().hasopt()){
  				for (typedVar tv :getopttypedVar().getoptList()) {
  					if (tv.getIdDecl().getID().equals(name)) {
  						return tv.getIdDecl();
  					}
  				}
  			}
  		}
  		return getfunc_body().localLookup(name);
  	}
/** @apilevel internal */
protected boolean nbrLoc_visited = false;
  /** @apilevel internal */
  private void nbrLoc_reset() {
    nbrLoc_computed = false;
    nbrLoc_visited = false;
  }
  /** @apilevel internal */
  protected boolean nbrLoc_computed = false;

  /** @apilevel internal */
  protected int nbrLoc_value;

  /**
   * @attribute syn
   * @aspect NameAnalysis
   * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/nameanalysis.jrag:326
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="NameAnalysis", declaredAt="/home/tobias_carlsson/chocopy-tobias/src/jastadd/nameanalysis.jrag:326")
  public int nbrLoc() {
    ASTState state = state();
    if (nbrLoc_computed) {
      return nbrLoc_value;
    }
    if (nbrLoc_visited) {
      throw new RuntimeException("Circular definition of attribute func_def.nbrLoc().");
    }
    nbrLoc_visited = true;
    state().enterLazyAttribute();
    nbrLoc_value = nbrLoc_compute();
    nbrLoc_computed = true;
    state().leaveLazyAttribute();
    nbrLoc_visited = false;
    return nbrLoc_value;
  }
  /** @apilevel internal */
  private int nbrLoc_compute() {
  		return (getfunc_body().getNumlocdef());
  	}
/** @apilevel internal */
protected boolean wrongRetType_visited = false;
  /** @apilevel internal */
  private void wrongRetType_reset() {
    wrongRetType_computed = false;
    wrongRetType_visited = false;
  }
  /** @apilevel internal */
  protected boolean wrongRetType_computed = false;

  /** @apilevel internal */
  protected boolean wrongRetType_value;

  /**
   * @attribute syn
   * @aspect NameAnalysis
   * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/nameanalysis.jrag:364
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="NameAnalysis", declaredAt="/home/tobias_carlsson/chocopy-tobias/src/jastadd/nameanalysis.jrag:364")
  public boolean wrongRetType() {
    ASTState state = state();
    if (wrongRetType_computed) {
      return wrongRetType_value;
    }
    if (wrongRetType_visited) {
      throw new RuntimeException("Circular definition of attribute func_def.wrongRetType().");
    }
    wrongRetType_visited = true;
    state().enterLazyAttribute();
    wrongRetType_value = wrongRetType_compute();
    wrongRetType_computed = true;
    state().leaveLazyAttribute();
    wrongRetType_visited = false;
    return wrongRetType_value;
  }
  /** @apilevel internal */
  private boolean wrongRetType_compute() {
  		if(hasTypen()){
  			if(getTypen().toString().equals(unknownType().toString())){
  				return true;
  			}
  		}
  		return false;
  	}
/** @apilevel internal */
protected boolean types_visited = false;
  /** @apilevel internal */
  private void types_reset() {
    types_computed = false;
    
    types_value = null;
    types_visited = false;
  }
  /** @apilevel internal */
  protected boolean types_computed = false;

  /** @apilevel internal */
  protected Typen types_value;

  /**
   * @attribute syn
   * @aspect typeChecker
   * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/typeChecker.jrag:45
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="typeChecker", declaredAt="/home/tobias_carlsson/chocopy-tobias/src/jastadd/typeChecker.jrag:45")
  public Typen types() {
    ASTState state = state();
    if (types_computed) {
      return types_value;
    }
    if (types_visited) {
      throw new RuntimeException("Circular definition of attribute func_def.types().");
    }
    types_visited = true;
    state().enterLazyAttribute();
    types_value = getIdDecl().types();
    types_computed = true;
    state().leaveLazyAttribute();
    types_visited = false;
    return types_value;
  }
  /**
   * @attribute inh
   * @aspect NameAnalysis
   * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/nameanalysis.jrag:286
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.INH)
  @ASTNodeAnnotation.Source(aspect="NameAnalysis", declaredAt="/home/tobias_carlsson/chocopy-tobias/src/jastadd/nameanalysis.jrag:286")
  public IdDecl localLookups(String name) {
    Object _parameters = name;
    if (localLookups_String_visited == null) localLookups_String_visited = new java.util.HashSet(4);
    if (localLookups_String_values == null) localLookups_String_values = new java.util.HashMap(4);
    ASTState state = state();
    if (localLookups_String_values.containsKey(_parameters)) {
      return (IdDecl) localLookups_String_values.get(_parameters);
    }
    if (localLookups_String_visited.contains(_parameters)) {
      throw new RuntimeException("Circular definition of attribute func_def.localLookups(String).");
    }
    localLookups_String_visited.add(_parameters);
    state().enterLazyAttribute();
    IdDecl localLookups_String_value = getParent().Define_localLookups(this, null, name);
    localLookups_String_values.put(_parameters, localLookups_String_value);
    state().leaveLazyAttribute();
    localLookups_String_visited.remove(_parameters);
    return localLookups_String_value;
  }
/** @apilevel internal */
protected java.util.Set localLookups_String_visited;
  /** @apilevel internal */
  private void localLookups_String_reset() {
    localLookups_String_values = null;
    localLookups_String_visited = null;
  }
  /** @apilevel internal */
  protected java.util.Map localLookups_String_values;

  /**
   * @attribute inh
   * @aspect NameAnalysis
   * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/nameanalysis.jrag:335
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.INH)
  @ASTNodeAnnotation.Source(aspect="NameAnalysis", declaredAt="/home/tobias_carlsson/chocopy-tobias/src/jastadd/nameanalysis.jrag:335")
  public IdDecl globalLookup(String name) {
    Object _parameters = name;
    if (globalLookup_String_visited == null) globalLookup_String_visited = new java.util.HashSet(4);
    if (globalLookup_String_values == null) globalLookup_String_values = new java.util.HashMap(4);
    ASTState state = state();
    if (globalLookup_String_values.containsKey(_parameters)) {
      return (IdDecl) globalLookup_String_values.get(_parameters);
    }
    if (globalLookup_String_visited.contains(_parameters)) {
      throw new RuntimeException("Circular definition of attribute func_def.globalLookup(String).");
    }
    globalLookup_String_visited.add(_parameters);
    state().enterLazyAttribute();
    IdDecl globalLookup_String_value = getParent().Define_globalLookup(this, null, name);
    globalLookup_String_values.put(_parameters, globalLookup_String_value);
    state().leaveLazyAttribute();
    globalLookup_String_visited.remove(_parameters);
    return globalLookup_String_value;
  }
/** @apilevel internal */
protected java.util.Set globalLookup_String_visited;
  /** @apilevel internal */
  private void globalLookup_String_reset() {
    globalLookup_String_values = null;
    globalLookup_String_visited = null;
  }
  /** @apilevel internal */
  protected java.util.Map globalLookup_String_values;

  /**
   * @attribute inh
   * @aspect NameAnalysis
   * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/rndFix.jrag:7
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.INH)
  @ASTNodeAnnotation.Source(aspect="NameAnalysis", declaredAt="/home/tobias_carlsson/chocopy-tobias/src/jastadd/rndFix.jrag:7")
  public boolean insideCl() {
    ASTState state = state();
    if (insideCl_computed) {
      return insideCl_value;
    }
    if (insideCl_visited) {
      throw new RuntimeException("Circular definition of attribute func_def.insideCl().");
    }
    insideCl_visited = true;
    state().enterLazyAttribute();
    insideCl_value = getParent().Define_insideCl(this, null);
    insideCl_computed = true;
    state().leaveLazyAttribute();
    insideCl_visited = false;
    return insideCl_value;
  }
/** @apilevel internal */
protected boolean insideCl_visited = false;
  /** @apilevel internal */
  private void insideCl_reset() {
    insideCl_computed = false;
    insideCl_visited = false;
  }
  /** @apilevel internal */
  protected boolean insideCl_computed = false;

  /** @apilevel internal */
  protected boolean insideCl_value;

  /**
   * @attribute inh
   * @aspect ObjType
   * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/Typen.jrag:33
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.INH)
  @ASTNodeAnnotation.Source(aspect="ObjType", declaredAt="/home/tobias_carlsson/chocopy-tobias/src/jastadd/Typen.jrag:33")
  public ObjType objType() {
    ASTState state = state();
    if (objType_computed) {
      return objType_value;
    }
    if (objType_visited) {
      throw new RuntimeException("Circular definition of attribute func_def.objType().");
    }
    objType_visited = true;
    state().enterLazyAttribute();
    objType_value = getParent().Define_objType(this, null);
    objType_computed = true;
    state().leaveLazyAttribute();
    objType_visited = false;
    return objType_value;
  }
/** @apilevel internal */
protected boolean objType_visited = false;
  /** @apilevel internal */
  private void objType_reset() {
    objType_computed = false;
    
    objType_value = null;
    objType_visited = false;
  }
  /** @apilevel internal */
  protected boolean objType_computed = false;

  /** @apilevel internal */
  protected ObjType objType_value;

  /**
   * @attribute inh
   * @aspect UnknownType
   * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/Typen.jrag:44
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.INH)
  @ASTNodeAnnotation.Source(aspect="UnknownType", declaredAt="/home/tobias_carlsson/chocopy-tobias/src/jastadd/Typen.jrag:44")
  public UnknownType unknownType() {
    ASTState state = state();
    if (unknownType_computed) {
      return unknownType_value;
    }
    if (unknownType_visited) {
      throw new RuntimeException("Circular definition of attribute func_def.unknownType().");
    }
    unknownType_visited = true;
    state().enterLazyAttribute();
    unknownType_value = getParent().Define_unknownType(this, null);
    unknownType_computed = true;
    state().leaveLazyAttribute();
    unknownType_visited = false;
    return unknownType_value;
  }
/** @apilevel internal */
protected boolean unknownType_visited = false;
  /** @apilevel internal */
  private void unknownType_reset() {
    unknownType_computed = false;
    
    unknownType_value = null;
    unknownType_visited = false;
  }
  /** @apilevel internal */
  protected boolean unknownType_computed = false;

  /** @apilevel internal */
  protected UnknownType unknownType_value;

  /**
   * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/CallGraph.jrag:5
   * @apilevel internal
   */
  public func_def Define_enclosingFunction(ASTNode _callerNode, ASTNode _childNode) {
    int childIndex = this.getIndexOfChild(_callerNode);
    return this;
  }
  /**
   * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/CallGraph.jrag:5
   * @apilevel internal
   * @return {@code true} if this node has an equation for the inherited attribute enclosingFunction
   */
  protected boolean canDefine_enclosingFunction(ASTNode _callerNode, ASTNode _childNode) {
    return true;
  }
  /**
   * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/funcOrVarCh.jrag:6
   * @apilevel internal
   */
  public boolean Define_isFunction(ASTNode _callerNode, ASTNode _childNode) {
    if (_callerNode == getIdDeclNoTransform()) {
      // @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/funcOrVarCh.jrag:5
      return true;
    }
    else {
      return getParent().Define_isFunction(this, _callerNode);
    }
  }
  /**
   * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/funcOrVarCh.jrag:6
   * @apilevel internal
   * @return {@code true} if this node has an equation for the inherited attribute isFunction
   */
  protected boolean canDefine_isFunction(ASTNode _callerNode, ASTNode _childNode) {
    return true;
  }
  /**
   * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/funcOrVarCh.jrag:15
   * @apilevel internal
   */
  public int Define_nbrParams(ASTNode _callerNode, ASTNode _childNode) {
    if (_callerNode == getIdDeclNoTransform()) {
      // @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/funcOrVarCh.jrag:27
      {
      		int i = 0;
      		if(hasopttypedVar()){
      			i=i+1;
      			if( getopttypedVar().hasopt()){
      				i = i + getopttypedVar().getNumopt();
      			}
      		}
      		return i;
      	}
    }
    else {
      return getParent().Define_nbrParams(this, _callerNode);
    }
  }
  /**
   * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/funcOrVarCh.jrag:15
   * @apilevel internal
   * @return {@code true} if this node has an equation for the inherited attribute nbrParams
   */
  protected boolean canDefine_nbrParams(ASTNode _callerNode, ASTNode _childNode) {
    return true;
  }
  /**
   * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/funcOrVarCh.jrag:40
   * @apilevel internal
   */
  public List<Typen> Define_params(ASTNode _callerNode, ASTNode _childNode) {
    if (_callerNode == getIdDeclNoTransform()) {
      // @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/funcOrVarCh.jrag:42
      {
      		List<Typen> list = new List<Typen>();
      		if(hasopttypedVar()){
      			list.add(getopttypedVar().gettypedVar().getTypen());
      			for(typedVar op : getopttypedVar().getopts()){
      				list.add(op.getTypen());
      
      			}
      
      		}
      		return list;
      	}
    }
    else {
      return getParent().Define_params(this, _callerNode);
    }
  }
  /**
   * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/funcOrVarCh.jrag:40
   * @apilevel internal
   * @return {@code true} if this node has an equation for the inherited attribute params
   */
  protected boolean canDefine_params(ASTNode _callerNode, ASTNode _childNode) {
    return true;
  }
  /**
   * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/nameanalysis.jrag:207
   * @apilevel internal
   */
  public IdDecl Define_lookup(ASTNode _callerNode, ASTNode _childNode, String name) {
    if (_callerNode == getfunc_bodyNoTransform()) {
      // @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/nameanalysis.jrag:247
      { 
      		if(! insideCl() )
      		{return localLookup(name);
      		}
      		return localLookups(name);
      
      	}
    }
    else if (_callerNode == getIdDeclNoTransform()) {
      // @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/nameanalysis.jrag:221
      return globalLookup(name);
    }
    else {
      int childIndex = this.getIndexOfChild(_callerNode);
      return localLookup(name);
    }
  }
  /**
   * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/nameanalysis.jrag:207
   * @apilevel internal
   * @return {@code true} if this node has an equation for the inherited attribute lookup
   */
  protected boolean canDefine_lookup(ASTNode _callerNode, ASTNode _childNode, String name) {
    return true;
  }
  /**
   * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/nameanalysis.jrag:269
   * @apilevel internal
   */
  public IdDecl Define_lookupIt(ASTNode _callerNode, ASTNode _childNode, String name) {
    int childIndex = this.getIndexOfChild(_callerNode);
    { 
    		if(! insideCl() )
    		{return localLookup(name);
    		}
    
    		if(localLookup(name)!= unknownDecl()){
    			return localLookup(name);
    		}
    		return localLookups(name);
    
    	}
  }
  /**
   * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/nameanalysis.jrag:269
   * @apilevel internal
   * @return {@code true} if this node has an equation for the inherited attribute lookupIt
   */
  protected boolean canDefine_lookupIt(ASTNode _callerNode, ASTNode _childNode, String name) {
    return true;
  }
  /**
   * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/nameanalysis.jrag:283
   * @apilevel internal
   */
  public IdDecl Define_localLookups(ASTNode _callerNode, ASTNode _childNode, String name) {
    int childIndex = this.getIndexOfChild(_callerNode);
    return localLookups(name);
  }
  /**
   * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/nameanalysis.jrag:283
   * @apilevel internal
   * @return {@code true} if this node has an equation for the inherited attribute localLookups
   */
  protected boolean canDefine_localLookups(ASTNode _callerNode, ASTNode _childNode, String name) {
    return true;
  }
  /**
   * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/nameanalysis.jrag:334
   * @apilevel internal
   */
  public IdDecl Define_globalLookup(ASTNode _callerNode, ASTNode _childNode, String name) {
    int childIndex = this.getIndexOfChild(_callerNode);
    {
    		if(localLookup(name) != unknownDecl()){
    			return localLookup(name);
    		}
    		else{
    			return globalLookup(name);
    		}
    	}
  }
  /**
   * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/nameanalysis.jrag:334
   * @apilevel internal
   * @return {@code true} if this node has an equation for the inherited attribute globalLookup
   */
  protected boolean canDefine_globalLookup(ASTNode _callerNode, ASTNode _childNode, String name) {
    return true;
  }
  /**
   * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/nameanalysis.jrag:351
   * @apilevel internal
   */
  public boolean Define_possAssign(ASTNode _callerNode, ASTNode _childNode) {
    if (_callerNode == getIdDeclNoTransform()) {
      // @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/nameanalysis.jrag:352
      return false;
    }
    else {
      return getParent().Define_possAssign(this, _callerNode);
    }
  }
  /**
   * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/nameanalysis.jrag:351
   * @apilevel internal
   * @return {@code true} if this node has an equation for the inherited attribute possAssign
   */
  protected boolean canDefine_possAssign(ASTNode _callerNode, ASTNode _childNode) {
    return true;
  }
  /**
   * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/paramAdrInd.jrag:64
   * @apilevel internal
   */
  public boolean Define_insideFunc(ASTNode _callerNode, ASTNode _childNode) {
    int childIndex = this.getIndexOfChild(_callerNode);
    return true;
  }
  /**
   * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/paramAdrInd.jrag:64
   * @apilevel internal
   * @return {@code true} if this node has an equation for the inherited attribute insideFunc
   */
  protected boolean canDefine_insideFunc(ASTNode _callerNode, ASTNode _childNode) {
    return true;
  }
  /**
   * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/typeChecker.jrag:11
   * @apilevel internal
   */
  public Typen Define_expRet(ASTNode _callerNode, ASTNode _childNode) {
    int childIndex = this.getIndexOfChild(_callerNode);
    {
    		if(hasTypen()){
    			return getTypen();
    		}
    		return unknownType();
    	}
  }
  /**
   * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/typeChecker.jrag:11
   * @apilevel internal
   * @return {@code true} if this node has an equation for the inherited attribute expRet
   */
  protected boolean canDefine_expRet(ASTNode _callerNode, ASTNode _childNode) {
    return true;
  }
  /**
   * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/typeChecker.jrag:21
   * @apilevel internal
   */
  public Typen Define_types(ASTNode _callerNode, ASTNode _childNode) {
    if (_callerNode == getIdDeclNoTransform()) {
      // @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/typeChecker.jrag:24
      {
      		if(hasTypen()){
      			return getTypen();
      		}
      		else{
      			return unknownType();
      		}
      	}
    }
    else {
      return getParent().Define_types(this, _callerNode);
    }
  }
  /**
   * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/typeChecker.jrag:21
   * @apilevel internal
   * @return {@code true} if this node has an equation for the inherited attribute types
   */
  protected boolean canDefine_types(ASTNode _callerNode, ASTNode _childNode) {
    return true;
  }
  /** @apilevel internal */
  protected void collect_contributors_Program_errors(Program _root, java.util.Map<ASTNode, java.util.Set<ASTNode>> _map) {
    // @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/Errors.jrag:46
    if (wrongRetType()) {
      {
        Program target = (Program) (program());
        java.util.Set<ASTNode> contributors = _map.get(target);
        if (contributors == null) {
          contributors = new java.util.LinkedHashSet<ASTNode>();
          _map.put((ASTNode) target, contributors);
        }
        contributors.add(this);
      }
    }
    super.collect_contributors_Program_errors(_root, _map);
  }
  /** @apilevel internal */
  protected void contributeTo_Program_errors(Set<ErrorMessage> collection) {
    super.contributeTo_Program_errors(collection);
    if (wrongRetType()) {
      collection.add(error("The return type of function: '" +getIdDecl().getID()+ "' is not valid"));
    }
  }

}
