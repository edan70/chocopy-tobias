/* This file was generated with JastAdd2 (http://jastadd.org) version 2.3.4 */
package lang.ast;
import java.util.*;
import java.io.PrintStream;
import java.util.Set;
import java.util.TreeSet;
import java.util.ArrayList;
import java.io.ByteArrayOutputStream;
import java.lang.reflect.InvocationTargetException;
import java.util.HashSet;
/**
 * @ast node
 * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/lang.ast:68
 * @astdecl Block : ASTNode ::= stmt opt:stmt*;
 * @production Block : {@link ASTNode} ::= <span class="component">{@link stmt}</span> <span class="component">opt:{@link stmt}*</span>;

 */
public class Block extends ASTNode<ASTNode> implements Cloneable {
  /**
   * @aspect CodeGen
   * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/CodeGen.jrag:214
   */
  public void genCode(PrintStream out,IdDecl id) {
		getstmt().genCode(out,id);
		for (stmt s :  getopts() ) {
			s.genCode(out,id);
		}
	}
  /**
   * @declaredat ASTNode:1
   */
  public Block() {
    super();
  }
  /**
   * Initializes the child array to the correct size.
   * Initializes List and Opt nta children.
   * @apilevel internal
   * @ast method
   * @declaredat ASTNode:10
   */
  public void init$Children() {
    children = new ASTNode[2];
    setChild(new List(), 1);
  }
  /**
   * @declaredat ASTNode:14
   */
  @ASTNodeAnnotation.Constructor(
    name = {"stmt", "opt"},
    type = {"stmt", "List<stmt>"},
    kind = {"Child", "List"}
  )
  public Block(stmt p0, List<stmt> p1) {
    setChild(p0, 0);
    setChild(p1, 1);
  }
  /** @apilevel low-level 
   * @declaredat ASTNode:24
   */
  protected int numChildren() {
    return 2;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:28
   */
  public void flushAttrCache() {
    super.flushAttrCache();
    stmtId_reset();
  }
  /** @apilevel internal 
   * @declaredat ASTNode:33
   */
  public void flushCollectionCache() {
    super.flushCollectionCache();

  }
  /** @apilevel internal 
   * @declaredat ASTNode:38
   */
  public Block clone() throws CloneNotSupportedException {
    Block node = (Block) super.clone();
    return node;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:43
   */
  public Block copy() {
    try {
      Block node = (Block) clone();
      node.parent = null;
      if (children != null) {
        node.children = (ASTNode[]) children.clone();
      }
      return node;
    } catch (CloneNotSupportedException e) {
      throw new Error("Error: clone not supported for " + getClass().getName());
    }
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @deprecated Please use treeCopy or treeCopyNoTransform instead
   * @declaredat ASTNode:62
   */
  @Deprecated
  public Block fullCopy() {
    return treeCopyNoTransform();
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @declaredat ASTNode:72
   */
  public Block treeCopyNoTransform() {
    Block tree = (Block) copy();
    if (children != null) {
      for (int i = 0; i < children.length; ++i) {
        ASTNode child = (ASTNode) children[i];
        if (child != null) {
          child = child.treeCopyNoTransform();
          tree.setChild(child, i);
        }
      }
    }
    return tree;
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The subtree of this node is traversed to trigger rewrites before copy.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @declaredat ASTNode:92
   */
  public Block treeCopy() {
    Block tree = (Block) copy();
    if (children != null) {
      for (int i = 0; i < children.length; ++i) {
        ASTNode child = (ASTNode) getChild(i);
        if (child != null) {
          child = child.treeCopy();
          tree.setChild(child, i);
        }
      }
    }
    return tree;
  }
  /**
   * Replaces the stmt child.
   * @param node The new node to replace the stmt child.
   * @apilevel high-level
   */
  public Block setstmt(stmt node) {
    setChild(node, 0);
    return this;
  }
  /**
   * Retrieves the stmt child.
   * @return The current node used as the stmt child.
   * @apilevel high-level
   */
  @ASTNodeAnnotation.Child(name="stmt")
  public stmt getstmt() {
    return (stmt) getChild(0);
  }
  /**
   * Retrieves the stmt child.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The current node used as the stmt child.
   * @apilevel low-level
   */
  public stmt getstmtNoTransform() {
    return (stmt) getChildNoTransform(0);
  }
  /**
   * Replaces the opt list.
   * @param list The new list node to be used as the opt list.
   * @apilevel high-level
   */
  public Block setoptList(List<stmt> list) {
    setChild(list, 1);
    return this;
  }
  /**
   * Retrieves the number of children in the opt list.
   * @return Number of children in the opt list.
   * @apilevel high-level
   */
  public int getNumopt() {
    return getoptList().getNumChild();
  }
  /**
   * Retrieves the number of children in the opt list.
   * Calling this method will not trigger rewrites.
   * @return Number of children in the opt list.
   * @apilevel low-level
   */
  public int getNumoptNoTransform() {
    return getoptListNoTransform().getNumChildNoTransform();
  }
  /**
   * Retrieves the element at index {@code i} in the opt list.
   * @param i Index of the element to return.
   * @return The element at position {@code i} in the opt list.
   * @apilevel high-level
   */
  public stmt getopt(int i) {
    return (stmt) getoptList().getChild(i);
  }
  /**
   * Check whether the opt list has any children.
   * @return {@code true} if it has at least one child, {@code false} otherwise.
   * @apilevel high-level
   */
  public boolean hasopt() {
    return getoptList().getNumChild() != 0;
  }
  /**
   * Append an element to the opt list.
   * @param node The element to append to the opt list.
   * @apilevel high-level
   */
  public Block addopt(stmt node) {
    List<stmt> list = (parent == null) ? getoptListNoTransform() : getoptList();
    list.addChild(node);
    return this;
  }
  /** @apilevel low-level 
   */
  public Block addoptNoTransform(stmt node) {
    List<stmt> list = getoptListNoTransform();
    list.addChild(node);
    return this;
  }
  /**
   * Replaces the opt list element at index {@code i} with the new node {@code node}.
   * @param node The new node to replace the old list element.
   * @param i The list index of the node to be replaced.
   * @apilevel high-level
   */
  public Block setopt(stmt node, int i) {
    List<stmt> list = getoptList();
    list.setChild(node, i);
    return this;
  }
  /**
   * Retrieves the opt list.
   * @return The node representing the opt list.
   * @apilevel high-level
   */
  @ASTNodeAnnotation.ListChild(name="opt")
  public List<stmt> getoptList() {
    List<stmt> list = (List<stmt>) getChild(1);
    return list;
  }
  /**
   * Retrieves the opt list.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The node representing the opt list.
   * @apilevel low-level
   */
  public List<stmt> getoptListNoTransform() {
    return (List<stmt>) getChildNoTransform(1);
  }
  /**
   * @return the element at index {@code i} in the opt list without
   * triggering rewrites.
   */
  public stmt getoptNoTransform(int i) {
    return (stmt) getoptListNoTransform().getChildNoTransform(i);
  }
  /**
   * Retrieves the opt list.
   * @return The node representing the opt list.
   * @apilevel high-level
   */
  public List<stmt> getopts() {
    return getoptList();
  }
  /**
   * Retrieves the opt list.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The node representing the opt list.
   * @apilevel low-level
   */
  public List<stmt> getoptsNoTransform() {
    return getoptListNoTransform();
  }
  /**
   * @attribute inh
   * @aspect UniqueName
   * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/genUniqueName.jrag:2
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.INH)
  @ASTNodeAnnotation.Source(aspect="UniqueName", declaredAt="/home/tobias_carlsson/chocopy-tobias/src/jastadd/genUniqueName.jrag:2")
  public String stmtId() {
    ASTState state = state();
    if (stmtId_computed) {
      return stmtId_value;
    }
    if (stmtId_visited) {
      throw new RuntimeException("Circular definition of attribute Block.stmtId().");
    }
    stmtId_visited = true;
    state().enterLazyAttribute();
    stmtId_value = getParent().Define_stmtId(this, null);
    stmtId_computed = true;
    state().leaveLazyAttribute();
    stmtId_visited = false;
    return stmtId_value;
  }
/** @apilevel internal */
protected boolean stmtId_visited = false;
  /** @apilevel internal */
  private void stmtId_reset() {
    stmtId_computed = false;
    
    stmtId_value = null;
    stmtId_visited = false;
  }
  /** @apilevel internal */
  protected boolean stmtId_computed = false;

  /** @apilevel internal */
  protected String stmtId_value;

  /**
   * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/genUniqueName.jrag:4
   * @apilevel internal
   */
  public String Define_stmtId(ASTNode _callerNode, ASTNode _childNode) {
    if (_callerNode == getoptListNoTransform()) {
      // @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/genUniqueName.jrag:8
      int i = _callerNode.getIndexOfChild(_childNode);
      return stmtId() + i + '_';
    }
    else if (_callerNode == getstmtNoTransform()) {
      // @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/genUniqueName.jrag:7
      return stmtId() + 0 + '_';
    }
    else {
      return getParent().Define_stmtId(this, _callerNode);
    }
  }
  /**
   * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/genUniqueName.jrag:4
   * @apilevel internal
   * @return {@code true} if this node has an equation for the inherited attribute stmtId
   */
  protected boolean canDefine_stmtId(ASTNode _callerNode, ASTNode _childNode) {
    return true;
  }

}
