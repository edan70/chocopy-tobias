/* This file was generated with JastAdd2 (http://jastadd.org) version 2.3.4 */
package lang.ast;
import java.util.*;
import java.io.PrintStream;
import java.util.Set;
import java.util.TreeSet;
import java.util.ArrayList;
import java.io.ByteArrayOutputStream;
import java.lang.reflect.InvocationTargetException;
import java.util.HashSet;
/**
 * @ast node
 * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/lang.ast:40
 * @astdecl functionCall : cexpr ::= idCexpr [optExpr];
 * @production functionCall : {@link cexpr} ::= <span class="component">{@link idCexpr}</span> <span class="component">[{@link optExpr}]</span>;

 */
public class functionCall extends cexpr implements Cloneable {
  /**
   * @aspect CodeGen
   * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/CodeGen.jrag:190
   */
  public void genEval(PrintStream out,IdDecl id) {
		int count =0;
		if (!getidCexpr().getIdUse().declIsFunction()){
		}
		else{
			if(hasoptExpr()){

				if( getoptExpr().hasOpt()){
					for (int i = getoptExpr().getNumOpt() - 1; i >= 0; i--) {
						getoptExpr().getOpt(i).genEval(out,id);
						out.println("        pushq %rax");
					}
				}
				getoptExpr().getExpr().genEval(out,id);
				out.println("        pushq %rax");

			}
			out.println("        call " + getidCexpr().getIdUse().getID());
			if(hasoptExpr()){
				out.println("        addq $" + ((getoptExpr().getNumOpt()+1) * 8) + ", %rsp");
			}
		}	
	}
  /**
   * @aspect CodeGen
   * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/CodeGen.jrag:677
   */
  public void genConditionalJump(PrintStream out, IdDecl id, String falseLabel) {
		if(hasoptExpr()){
			if( getoptExpr().hasOpt()){
				for (int i = getoptExpr().getNumOpt() - 1; i >= 0; i--) {
					getoptExpr().getOpt(i).genEval(out,id);
					out.println("        pushq %rax");
				}
			}
			getoptExpr().getExpr().genEval(out,id);
			out.println("        pushq %rax");
		}
		out.println("        call " + getidCexpr().getIdUse().getID());
		if(hasoptExpr()){
			out.println("        addq $" + ((getoptExpr().getNumOpt()+1) * 8) + ", %rsp");
		}
		out.println("        movq $TRUE, %rbx");
		out.println("        cmpq %rbx, %rax");
		out.println("        jne " + falseLabel);

	}
  /**
   * @declaredat ASTNode:1
   */
  public functionCall() {
    super();
  }
  /**
   * Initializes the child array to the correct size.
   * Initializes List and Opt nta children.
   * @apilevel internal
   * @ast method
   * @declaredat ASTNode:10
   */
  public void init$Children() {
    children = new ASTNode[2];
    setChild(new Opt(), 1);
  }
  /**
   * @declaredat ASTNode:14
   */
  @ASTNodeAnnotation.Constructor(
    name = {"idCexpr", "optExpr"},
    type = {"idCexpr", "Opt<optExpr>"},
    kind = {"Child", "Opt"}
  )
  public functionCall(idCexpr p0, Opt<optExpr> p1) {
    setChild(p0, 0);
    setChild(p1, 1);
  }
  /** @apilevel low-level 
   * @declaredat ASTNode:24
   */
  protected int numChildren() {
    return 2;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:28
   */
  public void flushAttrCache() {
    super.flushAttrCache();
    correctCall_reset();
    corrNbrParam_reset();
    paramse_reset();
    nbrP_reset();
    correctTypeParam_reset();
    noFuncInCall_reset();
    nbrParams_reset();
    gettypens_reset();
    decle_reset();
    types_reset();
    localIndex_reset();
    globalLookup_String_reset();
  }
  /** @apilevel internal 
   * @declaredat ASTNode:44
   */
  public void flushCollectionCache() {
    super.flushCollectionCache();

  }
  /** @apilevel internal 
   * @declaredat ASTNode:49
   */
  public functionCall clone() throws CloneNotSupportedException {
    functionCall node = (functionCall) super.clone();
    return node;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:54
   */
  public functionCall copy() {
    try {
      functionCall node = (functionCall) clone();
      node.parent = null;
      if (children != null) {
        node.children = (ASTNode[]) children.clone();
      }
      return node;
    } catch (CloneNotSupportedException e) {
      throw new Error("Error: clone not supported for " + getClass().getName());
    }
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @deprecated Please use treeCopy or treeCopyNoTransform instead
   * @declaredat ASTNode:73
   */
  @Deprecated
  public functionCall fullCopy() {
    return treeCopyNoTransform();
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @declaredat ASTNode:83
   */
  public functionCall treeCopyNoTransform() {
    functionCall tree = (functionCall) copy();
    if (children != null) {
      for (int i = 0; i < children.length; ++i) {
        ASTNode child = (ASTNode) children[i];
        if (child != null) {
          child = child.treeCopyNoTransform();
          tree.setChild(child, i);
        }
      }
    }
    return tree;
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The subtree of this node is traversed to trigger rewrites before copy.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @declaredat ASTNode:103
   */
  public functionCall treeCopy() {
    functionCall tree = (functionCall) copy();
    if (children != null) {
      for (int i = 0; i < children.length; ++i) {
        ASTNode child = (ASTNode) getChild(i);
        if (child != null) {
          child = child.treeCopy();
          tree.setChild(child, i);
        }
      }
    }
    return tree;
  }
  /**
   * Replaces the idCexpr child.
   * @param node The new node to replace the idCexpr child.
   * @apilevel high-level
   */
  public functionCall setidCexpr(idCexpr node) {
    setChild(node, 0);
    return this;
  }
  /**
   * Retrieves the idCexpr child.
   * @return The current node used as the idCexpr child.
   * @apilevel high-level
   */
  @ASTNodeAnnotation.Child(name="idCexpr")
  public idCexpr getidCexpr() {
    return (idCexpr) getChild(0);
  }
  /**
   * Retrieves the idCexpr child.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The current node used as the idCexpr child.
   * @apilevel low-level
   */
  public idCexpr getidCexprNoTransform() {
    return (idCexpr) getChildNoTransform(0);
  }
  /**
   * Replaces the optional node for the optExpr child. This is the <code>Opt</code>
   * node containing the child optExpr, not the actual child!
   * @param opt The new node to be used as the optional node for the optExpr child.
   * @apilevel low-level
   */
  public functionCall setoptExprOpt(Opt<optExpr> opt) {
    setChild(opt, 1);
    return this;
  }
  /**
   * Replaces the (optional) optExpr child.
   * @param node The new node to be used as the optExpr child.
   * @apilevel high-level
   */
  public functionCall setoptExpr(optExpr node) {
    getoptExprOpt().setChild(node, 0);
    return this;
  }
  /**
   * Check whether the optional optExpr child exists.
   * @return {@code true} if the optional optExpr child exists, {@code false} if it does not.
   * @apilevel high-level
   */
  public boolean hasoptExpr() {
    return getoptExprOpt().getNumChild() != 0;
  }
  /**
   * Retrieves the (optional) optExpr child.
   * @return The optExpr child, if it exists. Returns {@code null} otherwise.
   * @apilevel low-level
   */
  public optExpr getoptExpr() {
    return (optExpr) getoptExprOpt().getChild(0);
  }
  /**
   * Retrieves the optional node for the optExpr child. This is the <code>Opt</code> node containing the child optExpr, not the actual child!
   * @return The optional node for child the optExpr child.
   * @apilevel low-level
   */
  @ASTNodeAnnotation.OptChild(name="optExpr")
  public Opt<optExpr> getoptExprOpt() {
    return (Opt<optExpr>) getChild(1);
  }
  /**
   * Retrieves the optional node for child optExpr. This is the <code>Opt</code> node containing the child optExpr, not the actual child!
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The optional node for child optExpr.
   * @apilevel low-level
   */
  public Opt<optExpr> getoptExprOptNoTransform() {
    return (Opt<optExpr>) getChildNoTransform(1);
  }
/** @apilevel internal */
protected boolean correctCall_visited = false;
  /** @apilevel internal */
  private void correctCall_reset() {
    correctCall_computed = false;
    correctCall_visited = false;
  }
  /** @apilevel internal */
  protected boolean correctCall_computed = false;

  /** @apilevel internal */
  protected boolean correctCall_value;

  /**
   * @attribute syn
   * @aspect checkFuncCall
   * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/checkFuncCall.jrag:5
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="checkFuncCall", declaredAt="/home/tobias_carlsson/chocopy-tobias/src/jastadd/checkFuncCall.jrag:5")
  public boolean correctCall() {
    ASTState state = state();
    if (correctCall_computed) {
      return correctCall_value;
    }
    if (correctCall_visited) {
      throw new RuntimeException("Circular definition of attribute functionCall.correctCall().");
    }
    correctCall_visited = true;
    state().enterLazyAttribute();
    correctCall_value = correctCall_compute();
    correctCall_computed = true;
    state().leaveLazyAttribute();
    correctCall_visited = false;
    return correctCall_value;
  }
  /** @apilevel internal */
  private boolean correctCall_compute() {
  		return getidCexpr().getIdUse().decl().isFunction();
  	}
/** @apilevel internal */
protected boolean corrNbrParam_visited = false;
  /** @apilevel internal */
  private void corrNbrParam_reset() {
    corrNbrParam_computed = false;
    corrNbrParam_visited = false;
  }
  /** @apilevel internal */
  protected boolean corrNbrParam_computed = false;

  /** @apilevel internal */
  protected boolean corrNbrParam_value;

  /**
   * @attribute syn
   * @aspect checkFuncCall
   * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/checkFuncCall.jrag:9
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="checkFuncCall", declaredAt="/home/tobias_carlsson/chocopy-tobias/src/jastadd/checkFuncCall.jrag:9")
  public boolean corrNbrParam() {
    ASTState state = state();
    if (corrNbrParam_computed) {
      return corrNbrParam_value;
    }
    if (corrNbrParam_visited) {
      throw new RuntimeException("Circular definition of attribute functionCall.corrNbrParam().");
    }
    corrNbrParam_visited = true;
    state().enterLazyAttribute();
    corrNbrParam_value = corrNbrParam_compute();
    corrNbrParam_computed = true;
    state().leaveLazyAttribute();
    corrNbrParam_visited = false;
    return corrNbrParam_value;
  }
  /** @apilevel internal */
  private boolean corrNbrParam_compute() { 
  		if(getidCexpr().getIdUse().getID().equals("print")){
  			return nbrParams()==1;
  		}
  		else if(getidCexpr().getIdUse().getID().equals("input")){
  			return nbrParams()==0;
  		}
  		else if(getidCexpr().getIdUse().getID().equals("len")){
  			return nbrParams()==1;
  		}
  		return nbrParams()== getidCexpr().getIdUse().decl().nbrParams();
  	}
/** @apilevel internal */
protected boolean paramse_visited = false;
  /** @apilevel internal */
  private void paramse_reset() {
    paramse_computed = false;
    
    paramse_value = null;
    paramse_visited = false;
  }
  /** @apilevel internal */
  protected boolean paramse_computed = false;

  /** @apilevel internal */
  protected List<Expr> paramse_value;

  /**
   * @attribute syn
   * @aspect checkFuncCall
   * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/checkFuncCall.jrag:45
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="checkFuncCall", declaredAt="/home/tobias_carlsson/chocopy-tobias/src/jastadd/checkFuncCall.jrag:43")
  public List<Expr> paramse() {
    ASTState state = state();
    if (paramse_computed) {
      return paramse_value;
    }
    if (paramse_visited) {
      throw new RuntimeException("Circular definition of attribute cexpr.paramse().");
    }
    paramse_visited = true;
    state().enterLazyAttribute();
    paramse_value = paramse_compute();
    paramse_computed = true;
    state().leaveLazyAttribute();
    paramse_visited = false;
    return paramse_value;
  }
  /** @apilevel internal */
  private List<Expr> paramse_compute() {
  		List<Expr> hash_map = new List<Expr>(); 
  		if(hasoptExpr()){
  			for(int i =getoptExpr().getNumOpt()-1; i>=0; i--){
  
  				hash_map.add(getoptExpr().getOpt(i));
  
  			}
  			hash_map.add(getoptExpr().getExpr());
  		}
  
  	
  			return hash_map;
  		}
/** @apilevel internal */
protected boolean nbrP_visited = false;
  /** @apilevel internal */
  private void nbrP_reset() {
    nbrP_computed = false;
    nbrP_visited = false;
  }
  /** @apilevel internal */
  protected boolean nbrP_computed = false;

  /** @apilevel internal */
  protected int nbrP_value;

  /**
   * @attribute syn
   * @aspect checkFuncCall
   * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/checkFuncCall.jrag:64
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="checkFuncCall", declaredAt="/home/tobias_carlsson/chocopy-tobias/src/jastadd/checkFuncCall.jrag:63")
  public int nbrP() {
    ASTState state = state();
    if (nbrP_computed) {
      return nbrP_value;
    }
    if (nbrP_visited) {
      throw new RuntimeException("Circular definition of attribute cexpr.nbrP().");
    }
    nbrP_visited = true;
    state().enterLazyAttribute();
    nbrP_value = nbrP_compute();
    nbrP_computed = true;
    state().leaveLazyAttribute();
    nbrP_visited = false;
    return nbrP_value;
  }
  /** @apilevel internal */
  private int nbrP_compute() {
  		if(hasoptExpr()){
  			return  getoptExpr().getNumOpt() +1;
  		}
  		return 0;
  
  	}
/** @apilevel internal */
protected boolean correctTypeParam_visited = false;
  /** @apilevel internal */
  private void correctTypeParam_reset() {
    correctTypeParam_computed = false;
    correctTypeParam_visited = false;
  }
  /** @apilevel internal */
  protected boolean correctTypeParam_computed = false;

  /** @apilevel internal */
  protected boolean correctTypeParam_value;

  /**
   * @attribute syn
   * @aspect checkFuncCall
   * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/checkFuncCall.jrag:73
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="checkFuncCall", declaredAt="/home/tobias_carlsson/chocopy-tobias/src/jastadd/checkFuncCall.jrag:73")
  public boolean correctTypeParam() {
    ASTState state = state();
    if (correctTypeParam_computed) {
      return correctTypeParam_value;
    }
    if (correctTypeParam_visited) {
      throw new RuntimeException("Circular definition of attribute functionCall.correctTypeParam().");
    }
    correctTypeParam_visited = true;
    state().enterLazyAttribute();
    correctTypeParam_value = correctTypeParam_compute();
    correctTypeParam_computed = true;
    state().leaveLazyAttribute();
    correctTypeParam_visited = false;
    return correctTypeParam_value;
  }
  /** @apilevel internal */
  private boolean correctTypeParam_compute() {
  		if(getidCexpr().getIdUse().getID().equals("print")){
  			return true;
  		}
  		else if(getidCexpr().getIdUse().getID().equals("input")){
  			return true;
  		}
  		else if(getidCexpr().getIdUse().getID().equals("len")){
  			return true;
  		}
  
  		int i = -1;
  		if(nbrParams() != getidCexpr().getIdUse().decl().nbrParams()){
  			return false;
  		}
  
  		else{
  			if(hasoptExpr()){
  
  				List<Typen> list = getidCexpr().getIdUse().decl().params();
  				for(Typen t: list){
  					if(i==-1){
  						if(!t.toString().equals(getoptExpr().getExpr().types().toString())){
  							return false;
  
  						}
  					}
  					else{
  						if(!(t.toString().equals(getoptExpr().getOpt(i).types().toString()))){
  							return false;
  						}
  
  					}
  
  					i++;
  				}
  			}	
  		}
  		return true;
  	}
/** @apilevel internal */
protected boolean noFuncInCall_visited = false;
  /** @apilevel internal */
  private void noFuncInCall_reset() {
    noFuncInCall_computed = false;
    noFuncInCall_visited = false;
  }
  /** @apilevel internal */
  protected boolean noFuncInCall_computed = false;

  /** @apilevel internal */
  protected boolean noFuncInCall_value;

  /**
   * @attribute syn
   * @aspect checkFuncCall
   * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/checkFuncCall.jrag:114
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="checkFuncCall", declaredAt="/home/tobias_carlsson/chocopy-tobias/src/jastadd/checkFuncCall.jrag:114")
  public boolean noFuncInCall() {
    ASTState state = state();
    if (noFuncInCall_computed) {
      return noFuncInCall_value;
    }
    if (noFuncInCall_visited) {
      throw new RuntimeException("Circular definition of attribute functionCall.noFuncInCall().");
    }
    noFuncInCall_visited = true;
    state().enterLazyAttribute();
    noFuncInCall_value = noFuncInCall_compute();
    noFuncInCall_computed = true;
    state().leaveLazyAttribute();
    noFuncInCall_visited = false;
    return noFuncInCall_value;
  }
  /** @apilevel internal */
  private boolean noFuncInCall_compute() {
  		int i = 0;
  		if(hasoptExpr()){
  			if(getoptExpr().getExpr().isFunction()){
  				return false;
  			}
  			if( getoptExpr().hasOpt()){
  				for (int j = getoptExpr().getNumOpt() - 1; j >= 0; j--) {
  					if(getoptExpr().getOpt(j).isFunction()){
  						return false;
  					}
  				}
  			}
  		}
  		return true;
  	}
/** @apilevel internal */
protected boolean nbrParams_visited = false;
  /** @apilevel internal */
  private void nbrParams_reset() {
    nbrParams_computed = false;
    nbrParams_visited = false;
  }
  /** @apilevel internal */
  protected boolean nbrParams_computed = false;

  /** @apilevel internal */
  protected int nbrParams_value;

  /**
   * @attribute syn
   * @aspect FuncOrVarAnalysis
   * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/funcOrVarCh.jrag:17
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="FuncOrVarAnalysis", declaredAt="/home/tobias_carlsson/chocopy-tobias/src/jastadd/funcOrVarCh.jrag:17")
  public int nbrParams() {
    ASTState state = state();
    if (nbrParams_computed) {
      return nbrParams_value;
    }
    if (nbrParams_visited) {
      throw new RuntimeException("Circular definition of attribute functionCall.nbrParams().");
    }
    nbrParams_visited = true;
    state().enterLazyAttribute();
    nbrParams_value = nbrParams_compute();
    nbrParams_computed = true;
    state().leaveLazyAttribute();
    nbrParams_visited = false;
    return nbrParams_value;
  }
  /** @apilevel internal */
  private int nbrParams_compute() {
  		int i = 0;
  		if(hasoptExpr()){
  			i=i+1;
  			if( getoptExpr().hasOpt()){
  				i = i + getoptExpr().getNumOpt();
  			}
  		}
  		return i;
  	}
/** @apilevel internal */
protected boolean gettypens_visited = false;
  /** @apilevel internal */
  private void gettypens_reset() {
    gettypens_computed = false;
    
    gettypens_value = null;
    gettypens_visited = false;
  }
  /** @apilevel internal */
  protected boolean gettypens_computed = false;

  /** @apilevel internal */
  protected HashMap<Integer, String> gettypens_value;

  /**
   * @attribute syn
   * @aspect NameAnalysis
   * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/nameanalysis.jrag:38
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="NameAnalysis", declaredAt="/home/tobias_carlsson/chocopy-tobias/src/jastadd/nameanalysis.jrag:37")
  public HashMap<Integer, String> gettypens() {
    ASTState state = state();
    if (gettypens_computed) {
      return gettypens_value;
    }
    if (gettypens_visited) {
      throw new RuntimeException("Circular definition of attribute cexpr.gettypens().");
    }
    gettypens_visited = true;
    state().enterLazyAttribute();
    gettypens_value = gettypens_compute();
    gettypens_computed = true;
    state().leaveLazyAttribute();
    gettypens_visited = false;
    return gettypens_value;
  }
  /** @apilevel internal */
  private HashMap<Integer, String> gettypens_compute() {
  		int count = 0;
  		HashMap<Integer, String> hash_map = new HashMap<Integer, String>(); 
  		if(hasoptExpr()){
  			optExpr er = getoptExpr();
  			hash_map.put(count,er.getExpr().types().toString());
  			count++;
  			for(Expr e: er.getOpts()){
  				hash_map.put(count,e.types().toString());
  			count++;
  			}
  		}
  		return hash_map;
  	}
/** @apilevel internal */
protected boolean decle_visited = false;
  /** @apilevel internal */
  private void decle_reset() {
    decle_computed = false;
    
    decle_value = null;
    decle_visited = false;
  }
  /** @apilevel internal */
  protected boolean decle_computed = false;

  /** @apilevel internal */
  protected IdDecl decle_value;

  /**
   * @attribute syn
   * @aspect paramAdrInd
   * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/paramAdrInd.jrag:88
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="paramAdrInd", declaredAt="/home/tobias_carlsson/chocopy-tobias/src/jastadd/paramAdrInd.jrag:88")
  public IdDecl decle() {
    ASTState state = state();
    if (decle_computed) {
      return decle_value;
    }
    if (decle_visited) {
      throw new RuntimeException("Circular definition of attribute functionCall.decle().");
    }
    decle_visited = true;
    state().enterLazyAttribute();
    decle_value = globalLookup(getidCexpr().getIdUse().getID());
    decle_computed = true;
    state().leaveLazyAttribute();
    decle_visited = false;
    return decle_value;
  }
/** @apilevel internal */
protected boolean types_visited = false;
  /** @apilevel internal */
  private void types_reset() {
    types_computed = false;
    
    types_value = null;
    types_visited = false;
  }
  /** @apilevel internal */
  protected boolean types_computed = false;

  /** @apilevel internal */
  protected Typen types_value;

  /**
   * @attribute syn
   * @aspect typeChecker
   * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/typeChecker.jrag:69
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="typeChecker", declaredAt="/home/tobias_carlsson/chocopy-tobias/src/jastadd/typeChecker.jrag:69")
  public Typen types() {
    ASTState state = state();
    if (types_computed) {
      return types_value;
    }
    if (types_visited) {
      throw new RuntimeException("Circular definition of attribute functionCall.types().");
    }
    types_visited = true;
    state().enterLazyAttribute();
    types_value = getidCexpr().getIdUse().decl().types();
    types_computed = true;
    state().leaveLazyAttribute();
    types_visited = false;
    return types_value;
  }
  /**
   * @attribute inh
   * @aspect paramAdrInd
   * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/paramAdrInd.jrag:32
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.INH)
  @ASTNodeAnnotation.Source(aspect="paramAdrInd", declaredAt="/home/tobias_carlsson/chocopy-tobias/src/jastadd/paramAdrInd.jrag:32")
  public int localIndex() {
    ASTState state = state();
    if (localIndex_computed) {
      return localIndex_value;
    }
    if (localIndex_visited) {
      throw new RuntimeException("Circular definition of attribute functionCall.localIndex().");
    }
    localIndex_visited = true;
    state().enterLazyAttribute();
    localIndex_value = getParent().Define_localIndex(this, null);
    localIndex_computed = true;
    state().leaveLazyAttribute();
    localIndex_visited = false;
    return localIndex_value;
  }
/** @apilevel internal */
protected boolean localIndex_visited = false;
  /** @apilevel internal */
  private void localIndex_reset() {
    localIndex_computed = false;
    localIndex_visited = false;
  }
  /** @apilevel internal */
  protected boolean localIndex_computed = false;

  /** @apilevel internal */
  protected int localIndex_value;

  /**
   * @attribute inh
   * @aspect paramAdrInd
   * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/paramAdrInd.jrag:87
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.INH)
  @ASTNodeAnnotation.Source(aspect="paramAdrInd", declaredAt="/home/tobias_carlsson/chocopy-tobias/src/jastadd/paramAdrInd.jrag:87")
  public IdDecl globalLookup(String name) {
    Object _parameters = name;
    if (globalLookup_String_visited == null) globalLookup_String_visited = new java.util.HashSet(4);
    if (globalLookup_String_values == null) globalLookup_String_values = new java.util.HashMap(4);
    ASTState state = state();
    if (globalLookup_String_values.containsKey(_parameters)) {
      return (IdDecl) globalLookup_String_values.get(_parameters);
    }
    if (globalLookup_String_visited.contains(_parameters)) {
      throw new RuntimeException("Circular definition of attribute functionCall.globalLookup(String).");
    }
    globalLookup_String_visited.add(_parameters);
    state().enterLazyAttribute();
    IdDecl globalLookup_String_value = getParent().Define_globalLookup(this, null, name);
    globalLookup_String_values.put(_parameters, globalLookup_String_value);
    state().leaveLazyAttribute();
    globalLookup_String_visited.remove(_parameters);
    return globalLookup_String_value;
  }
/** @apilevel internal */
protected java.util.Set globalLookup_String_visited;
  /** @apilevel internal */
  private void globalLookup_String_reset() {
    globalLookup_String_values = null;
    globalLookup_String_visited = null;
  }
  /** @apilevel internal */
  protected java.util.Map globalLookup_String_values;

  /**
   * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/funcOrVarCh.jrag:6
   * @apilevel internal
   */
  public boolean Define_isFunction(ASTNode _callerNode, ASTNode _childNode) {
    if (_callerNode == getidCexprNoTransform()) {
      // @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/funcOrVarCh.jrag:8
      return true;
    }
    else {
      return getParent().Define_isFunction(this, _callerNode);
    }
  }
  /**
   * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/funcOrVarCh.jrag:6
   * @apilevel internal
   * @return {@code true} if this node has an equation for the inherited attribute isFunction
   */
  protected boolean canDefine_isFunction(ASTNode _callerNode, ASTNode _childNode) {
    return true;
  }
  /**
   * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/paramAdrInd.jrag:29
   * @apilevel internal
   */
  public int Define_localIndex(ASTNode _callerNode, ASTNode _childNode) {
    int childIndex = this.getIndexOfChild(_callerNode);
    return localIndex();
  }
  /**
   * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/paramAdrInd.jrag:29
   * @apilevel internal
   * @return {@code true} if this node has an equation for the inherited attribute localIndex
   */
  protected boolean canDefine_localIndex(ASTNode _callerNode, ASTNode _childNode) {
    return true;
  }
  /** @apilevel internal */
  protected void collect_contributors_Program_errors(Program _root, java.util.Map<ASTNode, java.util.Set<ASTNode>> _map) {
    // @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/Errors.jrag:67
    if (!corrNbrParam() && getidCexpr().getIdUse().declIsFunction()) {
      {
        Program target = (Program) (program());
        java.util.Set<ASTNode> contributors = _map.get(target);
        if (contributors == null) {
          contributors = new java.util.LinkedHashSet<ASTNode>();
          _map.put((ASTNode) target, contributors);
        }
        contributors.add(this);
      }
    }
    // @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/Errors.jrag:81
    if (!correctTypeParam() && getidCexpr().getIdUse().declIsFunction()) {
      {
        Program target = (Program) (program());
        java.util.Set<ASTNode> contributors = _map.get(target);
        if (contributors == null) {
          contributors = new java.util.LinkedHashSet<ASTNode>();
          _map.put((ASTNode) target, contributors);
        }
        contributors.add(this);
      }
    }
    super.collect_contributors_Program_errors(_root, _map);
  }
  /** @apilevel internal */
  protected void contributeTo_Program_errors(Set<ErrorMessage> collection) {
    super.contributeTo_Program_errors(collection);
    if (!corrNbrParam() && getidCexpr().getIdUse().declIsFunction()) {
      collection.add(error("The call to: '" + getidCexpr().getIdUse().getID() + "' does not have the correct number of parameters!"));
    }
    if (!correctTypeParam() && getidCexpr().getIdUse().declIsFunction()) {
      collection.add(error("The call to: '" + getidCexpr().getIdUse().getID() + "' does not have the correct type of parameters!"));
    }
  }

}
