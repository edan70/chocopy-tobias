/* This file was generated with JastAdd2 (http://jastadd.org) version 2.3.4 */
package lang.ast;
import java.util.*;
import java.io.PrintStream;
import java.util.Set;
import java.util.TreeSet;
import java.util.ArrayList;
import java.io.ByteArrayOutputStream;
import java.lang.reflect.InvocationTargetException;
import java.util.HashSet;
/**
 * @ast node
 * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/lang.ast:1
 * @astdecl Program : ASTNode ::= vfc* stmt*;
 * @production Program : {@link ASTNode} ::= <span class="component">{@link vfc}*</span> <span class="component">{@link stmt}*</span>;

 */
public class Program extends ASTNode<ASTNode> implements Cloneable {
  /**
   * @aspect CodeGen
   * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/CodeGen.jrag:5
   */
  public void genCode(PrintStream out, IdDecl id) {

		out.println(".global _start");

		out.println(".data");
		out.println("TRUE: .quad 255");
		out.println("FALSE: .quad 0");

		out.println("ask_message: .ascii \"Please enter a number: \"");
		out.println("ask_msg_len: .quad 23");
		out.println("buf: .skip 1024");
		out.println();
		out.println(".text");
		out.println("_start:");

		out.println("        pushq %rbp");
		out.println("        movq %rsp, %rbp");
		for (vfc f : getvfcList()) {
			if(f.type().equals("vardef")){
				f.genCode(out,id);
			}
		}
		out.println("        call stmtSt");


		out.println("        movq %rbp, %rsp");
		out.println("        popq %rbp");
		// Call sys_exit:

		out.println("        movq $0, %rdi");
		out.println("        movq $60, %rax");
		out.println("        syscall"); // Done!


		for (vfc f : getvfcList()) {
			if(f.type().equals("funcdef")|f.type().equals("classdef")){
				f.genCode(out,id);
			}
		}

		out.println("stmtSt:"); // Done!
		for (stmt s : getstmtList()) {
			s.genCode(out,id);

		}    out.println("        ret");


		// Helper procedures for input/output:
		out.println("# Procedure to print number to stdout.");
		out.println("# C signature: void print(long int)");
		out.println("print:");
		out.println("        pushq %rbp");
		out.println("        movq %rsp, %rbp");
		out.println("        ### Convert integer to string (itoa).");
		out.println("        movq 16(%rbp), %rax");
		out.println("        leaq buf(%rip), %rsi    # RSI = write pointer (starts at end of buffer)");
		out.println("        addq $1023, %rsi");
		out.println("        movb $0x0A, (%rsi)      # insert newline");
		out.println("        movq $1, %rcx           # RCX = string length");
		out.println("        cmpq $0, %rax");
		out.println("        jge itoa_loop");
		out.println("        negq %rax               # negate to make RAX positive");
		out.println("itoa_loop:                      # do.. while (at least one iteration)");
		out.println("        movq $10, %rdi");
		out.println("        movq $0, %rdx");
		out.println("        idivq %rdi              # divide RDX:RAX by 10");
		out.println("        addb $0x30, %dl         # remainder + '0'");
		out.println("        decq %rsi               # move string pointer");
		out.println("        movb %dl, (%rsi)");
		out.println("        incq %rcx               # increment string length");
		out.println("        cmpq $0, %rax");
		out.println("        jg itoa_loop            # produce more digits");
		out.println("itoa_done:");
		out.println("        movq 16(%rbp), %rax");
		out.println("        cmpq $0, %rax");
		out.println("        jge print_end");
		out.println("        decq %rsi");
		out.println("        incq %rcx");
		out.println("        movb $0x2D, (%rsi)");
		out.println("print_end:");
		out.println("        movq $1, %rdi");
		out.println("        movq %rcx, %rdx");
		out.println("        movq $1, %rax");
		out.println("        syscall");
		out.println("        popq %rbp");
		out.println("        ret");
		out.println("");
		out.println("# Procedure to read number from stdin.");
		out.println("# C signature: long long int read(void)");
		out.println("read:");
		out.println("        pushq %rbp");
		out.println("        movq %rsp, %rbp");
		out.println("        ### R9  = sign");
		out.println("        movq $1, %r9            # sign <- 1");
		out.println("        ### R10 = sum");
		out.println("        movq $0, %r10           # sum <- 0");
		out.println("skip_ws: # skip any leading whitespace");
		out.println("        movq $0, %rdi");
		out.println("        leaq buf(%rip), %rsi");
		out.println("        movq $1, %rdx");
		out.println("        movq $0, %rax");
		out.println("        syscall                 # get one char: sys_read(0, buf, 1)");
		out.println("        cmpq $0, %rax");
		out.println("        jle atoi_done           # nchar <= 0");
		out.println("        movb (%rsi), %cl        # c <- current char");
		out.println("        cmp $32, %cl");
		out.println("        je skip_ws              # c == space");
		out.println("        cmp $13, %cl");
		out.println("        je skip_ws              # c == CR");
		out.println("        cmp $10, %cl");
		out.println("        je skip_ws              # c == NL");
		out.println("        cmp $9, %cl");
		out.println("        je skip_ws              # c == tab");
		out.println("        cmp $45, %cl            # check if negative");
		out.println("        jne atoi_loop");
		out.println("        movq $-1, %r9           # sign <- -1");
		out.println("        movq $0, %rdi");
		out.println("        leaq buf(%rip), %rsi");
		out.println("        movq $1, %rdx");
		out.println("        movq $0, %rax");
		out.println("        syscall                 # get one char: sys_read(0, buf, 1)");
		out.println("atoi_loop:");
		out.println("        cmpq $0, %rax           # while (nchar > 0)");
		out.println("        jle atoi_done           # leave loop if nchar <= 0");
		out.println("        movzbq (%rsi), %rcx     # move byte, zero extend to quad-word");
		out.println("        cmpq $0x30, %rcx        # test if < '0'");
		out.println("        jl atoi_done            # character is not numeric");
		out.println("        cmpq $0x39, %rcx        # test if > '9'");
		out.println("        jg atoi_done            # character is not numeric");
		out.println("        imulq $10, %r10         # multiply sum by 10");
		out.println("        subq $0x30, %rcx        # value of character");
		out.println("        addq %rcx, %r10         # add to sum");
		out.println("        movq $0, %rdi");
		out.println("        leaq buf(%rip), %rsi");
		out.println("        movq $1, %rdx");
		out.println("        movq $0, %rax");
		out.println("        syscall                 # get one char: sys_read(0, buf, 1)");
		out.println("        jmp atoi_loop           # loop back");
		out.println("atoi_done:");
		out.println("        imulq %r9, %r10         # sum *= sign");
		out.println("        movq %r10, %rax         # put result value in RAX");
		out.println("        popq %rbp");
		out.println("        ret");
		out.println();
		out.println("print_string:");
		out.println("        pushq %rbp");
		out.println("        movq %rsp, %rbp");
		out.println("        movq $1, %rdi");
		out.println("        movq 16(%rbp), %rsi");
		out.println("        movq 24(%rbp), %rdx");
		out.println("        movq $1, %rax");
		out.println("        syscall");
		out.println("        popq %rbp");
		out.println("        ret");


	}
  /**
   * @declaredat ASTNode:1
   */
  public Program() {
    super();
  }
  /**
   * Initializes the child array to the correct size.
   * Initializes List and Opt nta children.
   * @apilevel internal
   * @ast method
   * @declaredat ASTNode:10
   */
  public void init$Children() {
    children = new ASTNode[2];
    setChild(new List(), 0);
    setChild(new List(), 1);
  }
  /**
   * @declaredat ASTNode:15
   */
  @ASTNodeAnnotation.Constructor(
    name = {"vfc", "stmt"},
    type = {"List<vfc>", "List<stmt>"},
    kind = {"List", "List"}
  )
  public Program(List<vfc> p0, List<stmt> p1) {
    setChild(p0, 0);
    setChild(p1, 1);
  }
  /** @apilevel low-level 
   * @declaredat ASTNode:25
   */
  protected int numChildren() {
    return 2;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:29
   */
  public void flushAttrCache() {
    super.flushAttrCache();
    predefinedFunctions_reset();
    globalLookup_String_reset();
    localIndex_reset();
    boolType_reset();
    intType_reset();
    stringType_reset();
    objType_reset();
    unknownType_reset();
    unknownDecl_reset();
  }
  /** @apilevel internal 
   * @declaredat ASTNode:42
   */
  public void flushCollectionCache() {
    super.flushCollectionCache();
    Program_errors_visited = false;
    Program_errors_computed = false;
    
    Program_errors_value = null;
    contributorMap_Program_errors = null;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:51
   */
  public Program clone() throws CloneNotSupportedException {
    Program node = (Program) super.clone();
    return node;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:56
   */
  public Program copy() {
    try {
      Program node = (Program) clone();
      node.parent = null;
      if (children != null) {
        node.children = (ASTNode[]) children.clone();
      }
      return node;
    } catch (CloneNotSupportedException e) {
      throw new Error("Error: clone not supported for " + getClass().getName());
    }
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @deprecated Please use treeCopy or treeCopyNoTransform instead
   * @declaredat ASTNode:75
   */
  @Deprecated
  public Program fullCopy() {
    return treeCopyNoTransform();
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @declaredat ASTNode:85
   */
  public Program treeCopyNoTransform() {
    Program tree = (Program) copy();
    if (children != null) {
      for (int i = 0; i < children.length; ++i) {
        ASTNode child = (ASTNode) children[i];
        if (child != null) {
          child = child.treeCopyNoTransform();
          tree.setChild(child, i);
        }
      }
    }
    return tree;
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The subtree of this node is traversed to trigger rewrites before copy.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @declaredat ASTNode:105
   */
  public Program treeCopy() {
    Program tree = (Program) copy();
    if (children != null) {
      for (int i = 0; i < children.length; ++i) {
        ASTNode child = (ASTNode) getChild(i);
        if (child != null) {
          child = child.treeCopy();
          tree.setChild(child, i);
        }
      }
    }
    return tree;
  }
  /**
   * Replaces the vfc list.
   * @param list The new list node to be used as the vfc list.
   * @apilevel high-level
   */
  public Program setvfcList(List<vfc> list) {
    setChild(list, 0);
    return this;
  }
  /**
   * Retrieves the number of children in the vfc list.
   * @return Number of children in the vfc list.
   * @apilevel high-level
   */
  public int getNumvfc() {
    return getvfcList().getNumChild();
  }
  /**
   * Retrieves the number of children in the vfc list.
   * Calling this method will not trigger rewrites.
   * @return Number of children in the vfc list.
   * @apilevel low-level
   */
  public int getNumvfcNoTransform() {
    return getvfcListNoTransform().getNumChildNoTransform();
  }
  /**
   * Retrieves the element at index {@code i} in the vfc list.
   * @param i Index of the element to return.
   * @return The element at position {@code i} in the vfc list.
   * @apilevel high-level
   */
  public vfc getvfc(int i) {
    return (vfc) getvfcList().getChild(i);
  }
  /**
   * Check whether the vfc list has any children.
   * @return {@code true} if it has at least one child, {@code false} otherwise.
   * @apilevel high-level
   */
  public boolean hasvfc() {
    return getvfcList().getNumChild() != 0;
  }
  /**
   * Append an element to the vfc list.
   * @param node The element to append to the vfc list.
   * @apilevel high-level
   */
  public Program addvfc(vfc node) {
    List<vfc> list = (parent == null) ? getvfcListNoTransform() : getvfcList();
    list.addChild(node);
    return this;
  }
  /** @apilevel low-level 
   */
  public Program addvfcNoTransform(vfc node) {
    List<vfc> list = getvfcListNoTransform();
    list.addChild(node);
    return this;
  }
  /**
   * Replaces the vfc list element at index {@code i} with the new node {@code node}.
   * @param node The new node to replace the old list element.
   * @param i The list index of the node to be replaced.
   * @apilevel high-level
   */
  public Program setvfc(vfc node, int i) {
    List<vfc> list = getvfcList();
    list.setChild(node, i);
    return this;
  }
  /**
   * Retrieves the vfc list.
   * @return The node representing the vfc list.
   * @apilevel high-level
   */
  @ASTNodeAnnotation.ListChild(name="vfc")
  public List<vfc> getvfcList() {
    List<vfc> list = (List<vfc>) getChild(0);
    return list;
  }
  /**
   * Retrieves the vfc list.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The node representing the vfc list.
   * @apilevel low-level
   */
  public List<vfc> getvfcListNoTransform() {
    return (List<vfc>) getChildNoTransform(0);
  }
  /**
   * @return the element at index {@code i} in the vfc list without
   * triggering rewrites.
   */
  public vfc getvfcNoTransform(int i) {
    return (vfc) getvfcListNoTransform().getChildNoTransform(i);
  }
  /**
   * Retrieves the vfc list.
   * @return The node representing the vfc list.
   * @apilevel high-level
   */
  public List<vfc> getvfcs() {
    return getvfcList();
  }
  /**
   * Retrieves the vfc list.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The node representing the vfc list.
   * @apilevel low-level
   */
  public List<vfc> getvfcsNoTransform() {
    return getvfcListNoTransform();
  }
  /**
   * Replaces the stmt list.
   * @param list The new list node to be used as the stmt list.
   * @apilevel high-level
   */
  public Program setstmtList(List<stmt> list) {
    setChild(list, 1);
    return this;
  }
  /**
   * Retrieves the number of children in the stmt list.
   * @return Number of children in the stmt list.
   * @apilevel high-level
   */
  public int getNumstmt() {
    return getstmtList().getNumChild();
  }
  /**
   * Retrieves the number of children in the stmt list.
   * Calling this method will not trigger rewrites.
   * @return Number of children in the stmt list.
   * @apilevel low-level
   */
  public int getNumstmtNoTransform() {
    return getstmtListNoTransform().getNumChildNoTransform();
  }
  /**
   * Retrieves the element at index {@code i} in the stmt list.
   * @param i Index of the element to return.
   * @return The element at position {@code i} in the stmt list.
   * @apilevel high-level
   */
  public stmt getstmt(int i) {
    return (stmt) getstmtList().getChild(i);
  }
  /**
   * Check whether the stmt list has any children.
   * @return {@code true} if it has at least one child, {@code false} otherwise.
   * @apilevel high-level
   */
  public boolean hasstmt() {
    return getstmtList().getNumChild() != 0;
  }
  /**
   * Append an element to the stmt list.
   * @param node The element to append to the stmt list.
   * @apilevel high-level
   */
  public Program addstmt(stmt node) {
    List<stmt> list = (parent == null) ? getstmtListNoTransform() : getstmtList();
    list.addChild(node);
    return this;
  }
  /** @apilevel low-level 
   */
  public Program addstmtNoTransform(stmt node) {
    List<stmt> list = getstmtListNoTransform();
    list.addChild(node);
    return this;
  }
  /**
   * Replaces the stmt list element at index {@code i} with the new node {@code node}.
   * @param node The new node to replace the old list element.
   * @param i The list index of the node to be replaced.
   * @apilevel high-level
   */
  public Program setstmt(stmt node, int i) {
    List<stmt> list = getstmtList();
    list.setChild(node, i);
    return this;
  }
  /**
   * Retrieves the stmt list.
   * @return The node representing the stmt list.
   * @apilevel high-level
   */
  @ASTNodeAnnotation.ListChild(name="stmt")
  public List<stmt> getstmtList() {
    List<stmt> list = (List<stmt>) getChild(1);
    return list;
  }
  /**
   * Retrieves the stmt list.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The node representing the stmt list.
   * @apilevel low-level
   */
  public List<stmt> getstmtListNoTransform() {
    return (List<stmt>) getChildNoTransform(1);
  }
  /**
   * @return the element at index {@code i} in the stmt list without
   * triggering rewrites.
   */
  public stmt getstmtNoTransform(int i) {
    return (stmt) getstmtListNoTransform().getChildNoTransform(i);
  }
  /**
   * Retrieves the stmt list.
   * @return The node representing the stmt list.
   * @apilevel high-level
   */
  public List<stmt> getstmts() {
    return getstmtList();
  }
  /**
   * Retrieves the stmt list.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The node representing the stmt list.
   * @apilevel low-level
   */
  public List<stmt> getstmtsNoTransform() {
    return getstmtListNoTransform();
  }
  /**
   * @aspect <NoAspect>
   * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/Errors.jrag:26
   */
  /** @apilevel internal */
protected java.util.Map<ASTNode, java.util.Set<ASTNode>> contributorMap_Program_errors = null;

  /** @apilevel internal */
  protected void survey_Program_errors() {
    if (contributorMap_Program_errors == null) {
      contributorMap_Program_errors = new java.util.IdentityHashMap<ASTNode, java.util.Set<ASTNode>>();
      collect_contributors_Program_errors(this, contributorMap_Program_errors);
    }
  }

/** @apilevel internal */
protected boolean predefinedFunctions_visited = false;
  /** @apilevel internal */
  private void predefinedFunctions_reset() {
    predefinedFunctions_computed = false;
    
    predefinedFunctions_value = null;
    predefinedFunctions_visited = false;
  }
  /** @apilevel internal */
  protected boolean predefinedFunctions_computed = false;

  /** @apilevel internal */
  protected List<IdDecl> predefinedFunctions_value;

  /**
   * @attribute syn
   * @aspect NameAnalysis
   * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/nameanalysis.jrag:7
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN, isNTA=true)
  @ASTNodeAnnotation.Source(aspect="NameAnalysis", declaredAt="/home/tobias_carlsson/chocopy-tobias/src/jastadd/nameanalysis.jrag:7")
  public List<IdDecl> predefinedFunctions() {
    ASTState state = state();
    if (predefinedFunctions_computed) {
      return predefinedFunctions_value;
    }
    if (predefinedFunctions_visited) {
      throw new RuntimeException("Circular definition of attribute Program.predefinedFunctions().");
    }
    predefinedFunctions_visited = true;
    state().enterLazyAttribute();
    predefinedFunctions_value = predefinedFunctions_compute();
    predefinedFunctions_value.setParent(this);
    predefinedFunctions_computed = true;
    state().leaveLazyAttribute();
    predefinedFunctions_visited = false;
    return predefinedFunctions_value;
  }
  /** @apilevel internal */
  private List<IdDecl> predefinedFunctions_compute() {
  		List<IdDecl> list = new List<IdDecl>();
  		list.add(new IdDecl("print"));
  		return list;
  	}
/** @apilevel internal */
protected java.util.Set globalLookup_String_visited;
  /** @apilevel internal */
  private void globalLookup_String_reset() {
    globalLookup_String_values = null;
    globalLookup_String_visited = null;
  }
  /** @apilevel internal */
  protected java.util.Map globalLookup_String_values;

  /**
   * @attribute syn
   * @aspect NameAnalysis
   * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/nameanalysis.jrag:90
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="NameAnalysis", declaredAt="/home/tobias_carlsson/chocopy-tobias/src/jastadd/nameanalysis.jrag:90")
  public IdDecl globalLookup(String name) {
    Object _parameters = name;
    if (globalLookup_String_visited == null) globalLookup_String_visited = new java.util.HashSet(4);
    if (globalLookup_String_values == null) globalLookup_String_values = new java.util.HashMap(4);
    ASTState state = state();
    if (globalLookup_String_values.containsKey(_parameters)) {
      return (IdDecl) globalLookup_String_values.get(_parameters);
    }
    if (globalLookup_String_visited.contains(_parameters)) {
      throw new RuntimeException("Circular definition of attribute Program.globalLookup(String).");
    }
    globalLookup_String_visited.add(_parameters);
    state().enterLazyAttribute();
    IdDecl globalLookup_String_value = globalLookup_compute(name);
    globalLookup_String_values.put(_parameters, globalLookup_String_value);
    state().leaveLazyAttribute();
    globalLookup_String_visited.remove(_parameters);
    return globalLookup_String_value;
  }
  /** @apilevel internal */
  private IdDecl globalLookup_compute(String name) {
  		for (vfc v : getvfcs()) {
  			if (v.lookup(name).getID().equals(name)) {
  				return v.lookup(name);
  			}
  		}
  		for (IdDecl v : predefinedFunctions()) {
  			if (v.getID().equals(name)) {
  				return v;
  			}
  		}
  		return unknownDecl();
  	}
/** @apilevel internal */
protected boolean localIndex_visited = false;
  /** @apilevel internal */
  private void localIndex_reset() {
    localIndex_computed = false;
    localIndex_visited = false;
  }
  /** @apilevel internal */
  protected boolean localIndex_computed = false;

  /** @apilevel internal */
  protected int localIndex_value;

  /**
   * @attribute syn
   * @aspect paramAdrInd
   * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/paramAdrInd.jrag:37
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="paramAdrInd", declaredAt="/home/tobias_carlsson/chocopy-tobias/src/jastadd/paramAdrInd.jrag:28")
  public int localIndex() {
    ASTState state = state();
    if (localIndex_computed) {
      return localIndex_value;
    }
    if (localIndex_visited) {
      throw new RuntimeException("Circular definition of attribute ASTNode.localIndex().");
    }
    localIndex_visited = true;
    state().enterLazyAttribute();
    localIndex_value = 0;
    localIndex_computed = true;
    state().leaveLazyAttribute();
    localIndex_visited = false;
    return localIndex_value;
  }
/** @apilevel internal */
protected boolean boolType_visited = false;
  /** @apilevel internal */
  private void boolType_reset() {
    boolType_computed = false;
    
    boolType_value = null;
    boolType_visited = false;
  }
  /** @apilevel internal */
  protected boolean boolType_computed = false;

  /** @apilevel internal */
  protected BoolType boolType_value;

  /**
   * @attribute syn
   * @aspect BoolType
   * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/Typen.jrag:2
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN, isNTA=true)
  @ASTNodeAnnotation.Source(aspect="BoolType", declaredAt="/home/tobias_carlsson/chocopy-tobias/src/jastadd/Typen.jrag:2")
  public BoolType boolType() {
    ASTState state = state();
    if (boolType_computed) {
      return boolType_value;
    }
    if (boolType_visited) {
      throw new RuntimeException("Circular definition of attribute Program.boolType().");
    }
    boolType_visited = true;
    state().enterLazyAttribute();
    boolType_value = new BoolType();
    boolType_value.setParent(this);
    boolType_computed = true;
    state().leaveLazyAttribute();
    boolType_visited = false;
    return boolType_value;
  }
/** @apilevel internal */
protected boolean intType_visited = false;
  /** @apilevel internal */
  private void intType_reset() {
    intType_computed = false;
    
    intType_value = null;
    intType_visited = false;
  }
  /** @apilevel internal */
  protected boolean intType_computed = false;

  /** @apilevel internal */
  protected IntType intType_value;

  /**
   * @attribute syn
   * @aspect IntType
   * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/Typen.jrag:11
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN, isNTA=true)
  @ASTNodeAnnotation.Source(aspect="IntType", declaredAt="/home/tobias_carlsson/chocopy-tobias/src/jastadd/Typen.jrag:11")
  public IntType intType() {
    ASTState state = state();
    if (intType_computed) {
      return intType_value;
    }
    if (intType_visited) {
      throw new RuntimeException("Circular definition of attribute Program.intType().");
    }
    intType_visited = true;
    state().enterLazyAttribute();
    intType_value = new IntType();
    intType_value.setParent(this);
    intType_computed = true;
    state().leaveLazyAttribute();
    intType_visited = false;
    return intType_value;
  }
/** @apilevel internal */
protected boolean stringType_visited = false;
  /** @apilevel internal */
  private void stringType_reset() {
    stringType_computed = false;
    
    stringType_value = null;
    stringType_visited = false;
  }
  /** @apilevel internal */
  protected boolean stringType_computed = false;

  /** @apilevel internal */
  protected StringType stringType_value;

  /**
   * @attribute syn
   * @aspect StringType
   * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/Typen.jrag:21
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN, isNTA=true)
  @ASTNodeAnnotation.Source(aspect="StringType", declaredAt="/home/tobias_carlsson/chocopy-tobias/src/jastadd/Typen.jrag:21")
  public StringType stringType() {
    ASTState state = state();
    if (stringType_computed) {
      return stringType_value;
    }
    if (stringType_visited) {
      throw new RuntimeException("Circular definition of attribute Program.stringType().");
    }
    stringType_visited = true;
    state().enterLazyAttribute();
    stringType_value = new StringType();
    stringType_value.setParent(this);
    stringType_computed = true;
    state().leaveLazyAttribute();
    stringType_visited = false;
    return stringType_value;
  }
/** @apilevel internal */
protected boolean objType_visited = false;
  /** @apilevel internal */
  private void objType_reset() {
    objType_computed = false;
    
    objType_value = null;
    objType_visited = false;
  }
  /** @apilevel internal */
  protected boolean objType_computed = false;

  /** @apilevel internal */
  protected ObjType objType_value;

  /**
   * @attribute syn
   * @aspect ObjType
   * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/Typen.jrag:31
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN, isNTA=true)
  @ASTNodeAnnotation.Source(aspect="ObjType", declaredAt="/home/tobias_carlsson/chocopy-tobias/src/jastadd/Typen.jrag:31")
  public ObjType objType() {
    ASTState state = state();
    if (objType_computed) {
      return objType_value;
    }
    if (objType_visited) {
      throw new RuntimeException("Circular definition of attribute Program.objType().");
    }
    objType_visited = true;
    state().enterLazyAttribute();
    objType_value = new ObjType();
    objType_value.setParent(this);
    objType_computed = true;
    state().leaveLazyAttribute();
    objType_visited = false;
    return objType_value;
  }
/** @apilevel internal */
protected boolean unknownType_visited = false;
  /** @apilevel internal */
  private void unknownType_reset() {
    unknownType_computed = false;
    
    unknownType_value = null;
    unknownType_visited = false;
  }
  /** @apilevel internal */
  protected boolean unknownType_computed = false;

  /** @apilevel internal */
  protected UnknownType unknownType_value;

  /**
   * @attribute syn
   * @aspect UnknownType
   * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/Typen.jrag:42
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN, isNTA=true)
  @ASTNodeAnnotation.Source(aspect="UnknownType", declaredAt="/home/tobias_carlsson/chocopy-tobias/src/jastadd/Typen.jrag:42")
  public UnknownType unknownType() {
    ASTState state = state();
    if (unknownType_computed) {
      return unknownType_value;
    }
    if (unknownType_visited) {
      throw new RuntimeException("Circular definition of attribute Program.unknownType().");
    }
    unknownType_visited = true;
    state().enterLazyAttribute();
    unknownType_value = new UnknownType();
    unknownType_value.setParent(this);
    unknownType_computed = true;
    state().leaveLazyAttribute();
    unknownType_visited = false;
    return unknownType_value;
  }
/** @apilevel internal */
protected boolean unknownDecl_visited = false;
  /** @apilevel internal */
  private void unknownDecl_reset() {
    unknownDecl_computed = false;
    
    unknownDecl_value = null;
    unknownDecl_visited = false;
  }
  /** @apilevel internal */
  protected boolean unknownDecl_computed = false;

  /** @apilevel internal */
  protected UnknownDecl unknownDecl_value;

  /**
   * @attribute syn
   * @aspect UnknownDecl
   * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/UnknownDecl.jrag:2
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN, isNTA=true)
  @ASTNodeAnnotation.Source(aspect="UnknownDecl", declaredAt="/home/tobias_carlsson/chocopy-tobias/src/jastadd/UnknownDecl.jrag:2")
  public UnknownDecl unknownDecl() {
    ASTState state = state();
    if (unknownDecl_computed) {
      return unknownDecl_value;
    }
    if (unknownDecl_visited) {
      throw new RuntimeException("Circular definition of attribute Program.unknownDecl().");
    }
    unknownDecl_visited = true;
    state().enterLazyAttribute();
    unknownDecl_value = new UnknownDecl("<unknown>");
    unknownDecl_value.setParent(this);
    unknownDecl_computed = true;
    state().leaveLazyAttribute();
    unknownDecl_visited = false;
    return unknownDecl_value;
  }
  /**
   * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/Errors.jrag:28
   * @apilevel internal
   */
  public Program Define_program(ASTNode _callerNode, ASTNode _childNode) {
    int childIndex = this.getIndexOfChild(_callerNode);
    return this;
  }
  /**
   * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/Errors.jrag:28
   * @apilevel internal
   * @return {@code true} if this node has an equation for the inherited attribute program
   */
  protected boolean canDefine_program(ASTNode _callerNode, ASTNode _childNode) {
    return true;
  }
  /**
   * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/funcOrVarCh.jrag:6
   * @apilevel internal
   */
  public boolean Define_isFunction(ASTNode _callerNode, ASTNode _childNode) {
    if (_callerNode == predefinedFunctions_value) {
      // @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/funcOrVarCh.jrag:10
      return true;
    }
    else {
      int childIndex = this.getIndexOfChild(_callerNode);
      return false;
    }
  }
  /**
   * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/funcOrVarCh.jrag:6
   * @apilevel internal
   * @return {@code true} if this node has an equation for the inherited attribute isFunction
   */
  protected boolean canDefine_isFunction(ASTNode _callerNode, ASTNode _childNode) {
    return true;
  }
  /**
   * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/funcOrVarCh.jrag:11
   * @apilevel internal
   */
  public boolean Define_declIsFunction(ASTNode _callerNode, ASTNode _childNode) {
    int childIndex = this.getIndexOfChild(_callerNode);
    return false;
  }
  /**
   * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/funcOrVarCh.jrag:11
   * @apilevel internal
   * @return {@code true} if this node has an equation for the inherited attribute declIsFunction
   */
  protected boolean canDefine_declIsFunction(ASTNode _callerNode, ASTNode _childNode) {
    return true;
  }
  /**
   * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/funcOrVarCh.jrag:15
   * @apilevel internal
   */
  public int Define_nbrParams(ASTNode _callerNode, ASTNode _childNode) {
    int childIndex = this.getIndexOfChild(_callerNode);
    return 0;
  }
  /**
   * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/funcOrVarCh.jrag:15
   * @apilevel internal
   * @return {@code true} if this node has an equation for the inherited attribute nbrParams
   */
  protected boolean canDefine_nbrParams(ASTNode _callerNode, ASTNode _childNode) {
    return true;
  }
  /**
   * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/funcOrVarCh.jrag:40
   * @apilevel internal
   */
  public List<Typen> Define_params(ASTNode _callerNode, ASTNode _childNode) {
    int childIndex = this.getIndexOfChild(_callerNode);
    return new List<Typen>();
  }
  /**
   * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/funcOrVarCh.jrag:40
   * @apilevel internal
   * @return {@code true} if this node has an equation for the inherited attribute params
   */
  protected boolean canDefine_params(ASTNode _callerNode, ASTNode _childNode) {
    return true;
  }
  /**
   * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/genUniqueName.jrag:4
   * @apilevel internal
   */
  public String Define_stmtId(ASTNode _callerNode, ASTNode _childNode) {
    if (_callerNode == getstmtListNoTransform()) {
      // @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/genUniqueName.jrag:10
      int i = _callerNode.getIndexOfChild(_childNode);
      return "" +i + '_';
    }
    else {
      int childIndex = this.getIndexOfChild(_callerNode);
      return "";
    }
  }
  /**
   * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/genUniqueName.jrag:4
   * @apilevel internal
   * @return {@code true} if this node has an equation for the inherited attribute stmtId
   */
  protected boolean canDefine_stmtId(ASTNode _callerNode, ASTNode _childNode) {
    return true;
  }
  /**
   * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/nameanalysis.jrag:13
   * @apilevel internal
   */
  public class_def Define_lookupCl(ASTNode _callerNode, ASTNode _childNode, String name) {
    int childIndex = this.getIndexOfChild(_callerNode);
    {
    		for( vfc f : getvfcs()){
    			if(f.isClasse()){
    				if(f.name().equals(name))
    					return (class_def) f;
    			}
    		}
    		return null;
    	}
  }
  /**
   * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/nameanalysis.jrag:13
   * @apilevel internal
   * @return {@code true} if this node has an equation for the inherited attribute lookupCl
   */
  protected boolean canDefine_lookupCl(ASTNode _callerNode, ASTNode _childNode, String name) {
    return true;
  }
  /**
   * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/nameanalysis.jrag:104
   * @apilevel internal
   */
  public IdDecl Define_getDecl(ASTNode _callerNode, ASTNode _childNode) {
    int childIndex = this.getIndexOfChild(_callerNode);
    return unknownDecl();
  }
  /**
   * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/nameanalysis.jrag:104
   * @apilevel internal
   * @return {@code true} if this node has an equation for the inherited attribute getDecl
   */
  protected boolean canDefine_getDecl(ASTNode _callerNode, ASTNode _childNode) {
    return true;
  }
  /**
   * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/nameanalysis.jrag:109
   * @apilevel internal
   */
  public boolean Define_isClass(ASTNode _callerNode, ASTNode _childNode) {
    int childIndex = this.getIndexOfChild(_callerNode);
    return false;
  }
  /**
   * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/nameanalysis.jrag:109
   * @apilevel internal
   * @return {@code true} if this node has an equation for the inherited attribute isClass
   */
  protected boolean canDefine_isClass(ASTNode _callerNode, ASTNode _childNode) {
    return true;
  }
  /**
   * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/nameanalysis.jrag:113
   * @apilevel internal
   */
  public IdDecl Define_lookupFunc(ASTNode _callerNode, ASTNode _childNode, String name) {
    int childIndex = this.getIndexOfChild(_callerNode);
    {
    		for (vfc v : getvfcs()) {
    			if(v.isClass()){
    				if(v.getDecl().getID().equals(name)){
    					return v.getDecl();
    				}
    			}
    		}
    		return unknownDecl();
    	}
  }
  /**
   * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/nameanalysis.jrag:113
   * @apilevel internal
   * @return {@code true} if this node has an equation for the inherited attribute lookupFunc
   */
  protected boolean canDefine_lookupFunc(ASTNode _callerNode, ASTNode _childNode, String name) {
    return true;
  }
  /**
   * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/nameanalysis.jrag:180
   * @apilevel internal
   */
  public String Define_type(ASTNode _callerNode, ASTNode _childNode) {
    if (_callerNode == getvfcListNoTransform()) {
      // @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/nameanalysis.jrag:181
      int i = _callerNode.getIndexOfChild(_childNode);
      return getvfc(i).type();
    }
    else {
      return getParent().Define_type(this, _callerNode);
    }
  }
  /**
   * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/nameanalysis.jrag:180
   * @apilevel internal
   * @return {@code true} if this node has an equation for the inherited attribute type
   */
  protected boolean canDefine_type(ASTNode _callerNode, ASTNode _childNode) {
    return true;
  }
  /**
   * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/nameanalysis.jrag:186
   * @apilevel internal
   */
  public Typen Define_current(ASTNode _callerNode, ASTNode _childNode) {
    if (_callerNode == getvfcListNoTransform()) {
      // @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/nameanalysis.jrag:187
      int i = _callerNode.getIndexOfChild(_childNode);
      return getvfc(i).current();
    }
    else {
      return getParent().Define_current(this, _callerNode);
    }
  }
  /**
   * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/nameanalysis.jrag:186
   * @apilevel internal
   * @return {@code true} if this node has an equation for the inherited attribute current
   */
  protected boolean canDefine_current(ASTNode _callerNode, ASTNode _childNode) {
    return true;
  }
  /**
   * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/nameanalysis.jrag:224
   * @apilevel internal
   */
  public IdDecl Define_lookupNN(ASTNode _callerNode, ASTNode _childNode, String name) {
    int childIndex = this.getIndexOfChild(_callerNode);
    return unknownDecl();
  }
  /**
   * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/nameanalysis.jrag:224
   * @apilevel internal
   * @return {@code true} if this node has an equation for the inherited attribute lookupNN
   */
  protected boolean canDefine_lookupNN(ASTNode _callerNode, ASTNode _childNode, String name) {
    return true;
  }
  /**
   * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/nameanalysis.jrag:269
   * @apilevel internal
   */
  public IdDecl Define_lookupIt(ASTNode _callerNode, ASTNode _childNode, String name) {
    int childIndex = this.getIndexOfChild(_callerNode);
    return globalLookup(name);
  }
  /**
   * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/nameanalysis.jrag:269
   * @apilevel internal
   * @return {@code true} if this node has an equation for the inherited attribute lookupIt
   */
  protected boolean canDefine_lookupIt(ASTNode _callerNode, ASTNode _childNode, String name) {
    return true;
  }
  /**
   * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/nameanalysis.jrag:283
   * @apilevel internal
   */
  public IdDecl Define_localLookups(ASTNode _callerNode, ASTNode _childNode, String name) {
    int childIndex = this.getIndexOfChild(_callerNode);
    return unknownDecl();
  }
  /**
   * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/nameanalysis.jrag:283
   * @apilevel internal
   * @return {@code true} if this node has an equation for the inherited attribute localLookups
   */
  protected boolean canDefine_localLookups(ASTNode _callerNode, ASTNode _childNode, String name) {
    return true;
  }
  /**
   * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/nameanalysis.jrag:334
   * @apilevel internal
   */
  public IdDecl Define_globalLookup(ASTNode _callerNode, ASTNode _childNode, String name) {
    int childIndex = this.getIndexOfChild(_callerNode);
    return globalLookup(name);
  }
  /**
   * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/nameanalysis.jrag:334
   * @apilevel internal
   * @return {@code true} if this node has an equation for the inherited attribute globalLookup
   */
  protected boolean canDefine_globalLookup(ASTNode _callerNode, ASTNode _childNode, String name) {
    return true;
  }
  /**
   * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/paramAdrInd.jrag:16
   * @apilevel internal
   */
  public boolean Define_isParameter(ASTNode _callerNode, ASTNode _childNode) {
    int childIndex = this.getIndexOfChild(_callerNode);
    return false;
  }
  /**
   * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/paramAdrInd.jrag:16
   * @apilevel internal
   * @return {@code true} if this node has an equation for the inherited attribute isParameter
   */
  protected boolean canDefine_isParameter(ASTNode _callerNode, ASTNode _childNode) {
    return true;
  }
  /**
   * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/paramAdrInd.jrag:23
   * @apilevel internal
   */
  public int Define_parameterIndex(ASTNode _callerNode, ASTNode _childNode) {
    int childIndex = this.getIndexOfChild(_callerNode);
    return -1;
  }
  /**
   * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/paramAdrInd.jrag:23
   * @apilevel internal
   * @return {@code true} if this node has an equation for the inherited attribute parameterIndex
   */
  protected boolean canDefine_parameterIndex(ASTNode _callerNode, ASTNode _childNode) {
    return true;
  }
  /**
   * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/paramAdrInd.jrag:29
   * @apilevel internal
   */
  public int Define_localIndex(ASTNode _callerNode, ASTNode _childNode) {
    int childIndex = this.getIndexOfChild(_callerNode);
    return prevNode().localIndex();
  }
  /**
   * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/paramAdrInd.jrag:29
   * @apilevel internal
   * @return {@code true} if this node has an equation for the inherited attribute localIndex
   */
  protected boolean canDefine_localIndex(ASTNode _callerNode, ASTNode _childNode) {
    return true;
  }
  /**
   * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/paramAdrInd.jrag:52
   * @apilevel internal
   */
  public int Define_localIndexc(ASTNode _callerNode, ASTNode _childNode) {
    int childIndex = this.getIndexOfChild(_callerNode);
    return 0;
  }
  /**
   * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/paramAdrInd.jrag:52
   * @apilevel internal
   * @return {@code true} if this node has an equation for the inherited attribute localIndexc
   */
  protected boolean canDefine_localIndexc(ASTNode _callerNode, ASTNode _childNode) {
    return true;
  }
  /**
   * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/paramAdrInd.jrag:59
   * @apilevel internal
   */
  public boolean Define_declInFunc(ASTNode _callerNode, ASTNode _childNode) {
    int childIndex = this.getIndexOfChild(_callerNode);
    return false;
  }
  /**
   * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/paramAdrInd.jrag:59
   * @apilevel internal
   * @return {@code true} if this node has an equation for the inherited attribute declInFunc
   */
  protected boolean canDefine_declInFunc(ASTNode _callerNode, ASTNode _childNode) {
    return true;
  }
  /**
   * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/paramAdrInd.jrag:64
   * @apilevel internal
   */
  public boolean Define_insideFunc(ASTNode _callerNode, ASTNode _childNode) {
    int childIndex = this.getIndexOfChild(_callerNode);
    return false;
  }
  /**
   * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/paramAdrInd.jrag:64
   * @apilevel internal
   * @return {@code true} if this node has an equation for the inherited attribute insideFunc
   */
  protected boolean canDefine_insideFunc(ASTNode _callerNode, ASTNode _childNode) {
    return true;
  }
  /**
   * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/paramAdrInd.jrag:70
   * @apilevel internal
   */
  public boolean Define_insideClass(ASTNode _callerNode, ASTNode _childNode) {
    int childIndex = this.getIndexOfChild(_callerNode);
    return false;
  }
  /**
   * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/paramAdrInd.jrag:70
   * @apilevel internal
   * @return {@code true} if this node has an equation for the inherited attribute insideClass
   */
  protected boolean canDefine_insideClass(ASTNode _callerNode, ASTNode _childNode) {
    return true;
  }
  /**
   * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/paramAdrInd.jrag:86
   * @apilevel internal
   */
  public IdDecl Define_decle(ASTNode _callerNode, ASTNode _childNode) {
    int childIndex = this.getIndexOfChild(_callerNode);
    return unknownDecl();
  }
  /**
   * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/paramAdrInd.jrag:86
   * @apilevel internal
   * @return {@code true} if this node has an equation for the inherited attribute decle
   */
  protected boolean canDefine_decle(ASTNode _callerNode, ASTNode _childNode) {
    return true;
  }
  /**
   * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/paramAdrInd.jrag:124
   * @apilevel internal
   */
  public HashMap<var_def, Integer> Define_objvars(ASTNode _callerNode, ASTNode _childNode) {
    int childIndex = this.getIndexOfChild(_callerNode);
    return new HashMap<var_def, Integer>();
  }
  /**
   * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/paramAdrInd.jrag:124
   * @apilevel internal
   * @return {@code true} if this node has an equation for the inherited attribute objvars
   */
  protected boolean canDefine_objvars(ASTNode _callerNode, ASTNode _childNode) {
    return true;
  }
  /**
   * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/paramAdrInd.jrag:128
   * @apilevel internal
   */
  public HashMap<String, Integer> Define_objNum(ASTNode _callerNode, ASTNode _childNode) {
    int childIndex = this.getIndexOfChild(_callerNode);
    return new HashMap<String, Integer>();
  }
  /**
   * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/paramAdrInd.jrag:128
   * @apilevel internal
   * @return {@code true} if this node has an equation for the inherited attribute objNum
   */
  protected boolean canDefine_objNum(ASTNode _callerNode, ASTNode _childNode) {
    return true;
  }
  /**
   * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/paramAdrInd.jrag:129
   * @apilevel internal
   */
  public boolean Define_hasInit(ASTNode _callerNode, ASTNode _childNode) {
    int childIndex = this.getIndexOfChild(_callerNode);
    return false;
  }
  /**
   * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/paramAdrInd.jrag:129
   * @apilevel internal
   * @return {@code true} if this node has an equation for the inherited attribute hasInit
   */
  protected boolean canDefine_hasInit(ASTNode _callerNode, ASTNode _childNode) {
    return true;
  }
  /**
   * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/paramAdrInd.jrag:132
   * @apilevel internal
   */
  public int Define_nbrInit(ASTNode _callerNode, ASTNode _childNode) {
    int childIndex = this.getIndexOfChild(_callerNode);
    return 0;
  }
  /**
   * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/paramAdrInd.jrag:132
   * @apilevel internal
   * @return {@code true} if this node has an equation for the inherited attribute nbrInit
   */
  protected boolean canDefine_nbrInit(ASTNode _callerNode, ASTNode _childNode) {
    return true;
  }
  /**
   * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/paramAdrInd.jrag:179
   * @apilevel internal
   */
  public List<var_def> Define_locvars(ASTNode _callerNode, ASTNode _childNode) {
    int childIndex = this.getIndexOfChild(_callerNode);
    return new List<var_def>();
  }
  /**
   * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/paramAdrInd.jrag:179
   * @apilevel internal
   * @return {@code true} if this node has an equation for the inherited attribute locvars
   */
  protected boolean canDefine_locvars(ASTNode _callerNode, ASTNode _childNode) {
    return true;
  }
  /**
   * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/paramAdrInd.jrag:180
   * @apilevel internal
   */
  public List<var_def> Define_locvarsF(ASTNode _callerNode, ASTNode _childNode) {
    int childIndex = this.getIndexOfChild(_callerNode);
    return new List<var_def>();
  }
  /**
   * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/paramAdrInd.jrag:180
   * @apilevel internal
   * @return {@code true} if this node has an equation for the inherited attribute locvarsF
   */
  protected boolean canDefine_locvarsF(ASTNode _callerNode, ASTNode _childNode) {
    return true;
  }
  /**
   * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/rndFix.jrag:8
   * @apilevel internal
   */
  public boolean Define_insideCl(ASTNode _callerNode, ASTNode _childNode) {
    int childIndex = this.getIndexOfChild(_callerNode);
    return false;
  }
  /**
   * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/rndFix.jrag:8
   * @apilevel internal
   * @return {@code true} if this node has an equation for the inherited attribute insideCl
   */
  protected boolean canDefine_insideCl(ASTNode _callerNode, ASTNode _childNode) {
    return true;
  }
  /**
   * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/typeChecker.jrag:11
   * @apilevel internal
   */
  public Typen Define_expRet(ASTNode _callerNode, ASTNode _childNode) {
    int childIndex = this.getIndexOfChild(_callerNode);
    return unknownType();
  }
  /**
   * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/typeChecker.jrag:11
   * @apilevel internal
   * @return {@code true} if this node has an equation for the inherited attribute expRet
   */
  protected boolean canDefine_expRet(ASTNode _callerNode, ASTNode _childNode) {
    return true;
  }
  /**
   * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/typeChecker.jrag:85
   * @apilevel internal
   */
  public boolean Define_isDeclOut(ASTNode _callerNode, ASTNode _childNode) {
    int childIndex = this.getIndexOfChild(_callerNode);
    return false;
  }
  /**
   * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/typeChecker.jrag:85
   * @apilevel internal
   * @return {@code true} if this node has an equation for the inherited attribute isDeclOut
   */
  protected boolean canDefine_isDeclOut(ASTNode _callerNode, ASTNode _childNode) {
    return true;
  }
  /**
   * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/typeChecker.jrag:102
   * @apilevel internal
   */
  public boolean Define_assStmt(ASTNode _callerNode, ASTNode _childNode) {
    int childIndex = this.getIndexOfChild(_callerNode);
    return false;
  }
  /**
   * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/typeChecker.jrag:102
   * @apilevel internal
   * @return {@code true} if this node has an equation for the inherited attribute assStmt
   */
  protected boolean canDefine_assStmt(ASTNode _callerNode, ASTNode _childNode) {
    return true;
  }
  /**
   * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/typeChecker.jrag:106
   * @apilevel internal
   */
  public String Define_usedId(ASTNode _callerNode, ASTNode _childNode) {
    int childIndex = this.getIndexOfChild(_callerNode);
    return "none";
  }
  /**
   * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/typeChecker.jrag:106
   * @apilevel internal
   * @return {@code true} if this node has an equation for the inherited attribute usedId
   */
  protected boolean canDefine_usedId(ASTNode _callerNode, ASTNode _childNode) {
    return true;
  }
  /**
   * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/Typen.jrag:3
   * @apilevel internal
   */
  public BoolType Define_boolType(ASTNode _callerNode, ASTNode _childNode) {
    int childIndex = this.getIndexOfChild(_callerNode);
    return boolType();
  }
  /**
   * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/Typen.jrag:3
   * @apilevel internal
   * @return {@code true} if this node has an equation for the inherited attribute boolType
   */
  protected boolean canDefine_boolType(ASTNode _callerNode, ASTNode _childNode) {
    return true;
  }
  /**
   * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/Typen.jrag:12
   * @apilevel internal
   */
  public IntType Define_intType(ASTNode _callerNode, ASTNode _childNode) {
    int childIndex = this.getIndexOfChild(_callerNode);
    return intType();
  }
  /**
   * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/Typen.jrag:12
   * @apilevel internal
   * @return {@code true} if this node has an equation for the inherited attribute intType
   */
  protected boolean canDefine_intType(ASTNode _callerNode, ASTNode _childNode) {
    return true;
  }
  /**
   * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/Typen.jrag:22
   * @apilevel internal
   */
  public StringType Define_stringType(ASTNode _callerNode, ASTNode _childNode) {
    int childIndex = this.getIndexOfChild(_callerNode);
    return stringType();
  }
  /**
   * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/Typen.jrag:22
   * @apilevel internal
   * @return {@code true} if this node has an equation for the inherited attribute stringType
   */
  protected boolean canDefine_stringType(ASTNode _callerNode, ASTNode _childNode) {
    return true;
  }
  /**
   * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/Typen.jrag:33
   * @apilevel internal
   */
  public ObjType Define_objType(ASTNode _callerNode, ASTNode _childNode) {
    int childIndex = this.getIndexOfChild(_callerNode);
    return objType();
  }
  /**
   * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/Typen.jrag:33
   * @apilevel internal
   * @return {@code true} if this node has an equation for the inherited attribute objType
   */
  protected boolean canDefine_objType(ASTNode _callerNode, ASTNode _childNode) {
    return true;
  }
  /**
   * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/Typen.jrag:44
   * @apilevel internal
   */
  public UnknownType Define_unknownType(ASTNode _callerNode, ASTNode _childNode) {
    int childIndex = this.getIndexOfChild(_callerNode);
    return unknownType();
  }
  /**
   * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/Typen.jrag:44
   * @apilevel internal
   * @return {@code true} if this node has an equation for the inherited attribute unknownType
   */
  protected boolean canDefine_unknownType(ASTNode _callerNode, ASTNode _childNode) {
    return true;
  }
  /**
   * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/typeChecker.jrag:21
   * @apilevel internal
   */
  public Typen Define_types(ASTNode _callerNode, ASTNode _childNode) {
    int childIndex = this.getIndexOfChild(_callerNode);
    return unknownType();
  }
  /**
   * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/typeChecker.jrag:21
   * @apilevel internal
   * @return {@code true} if this node has an equation for the inherited attribute types
   */
  protected boolean canDefine_types(ASTNode _callerNode, ASTNode _childNode) {
    return true;
  }
  /**
   * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/UnknownDecl.jrag:3
   * @apilevel internal
   */
  public UnknownDecl Define_unknownDecl(ASTNode _callerNode, ASTNode _childNode) {
    int childIndex = this.getIndexOfChild(_callerNode);
    return unknownDecl();
  }
  /**
   * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/UnknownDecl.jrag:3
   * @apilevel internal
   * @return {@code true} if this node has an equation for the inherited attribute unknownDecl
   */
  protected boolean canDefine_unknownDecl(ASTNode _callerNode, ASTNode _childNode) {
    return true;
  }
/** @apilevel internal */
protected boolean Program_errors_visited = false;
  /**
   * @attribute coll
   * @aspect Errors
   * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/Errors.jrag:26
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.COLL)
  @ASTNodeAnnotation.Source(aspect="Errors", declaredAt="/home/tobias_carlsson/chocopy-tobias/src/jastadd/Errors.jrag:26")
  public Set<ErrorMessage> errors() {
    ASTState state = state();
    if (Program_errors_computed) {
      return Program_errors_value;
    }
    if (Program_errors_visited) {
      throw new RuntimeException("Circular definition of attribute Program.errors().");
    }
    Program_errors_visited = true;
    state().enterLazyAttribute();
    Program_errors_value = errors_compute();
    Program_errors_computed = true;
    state().leaveLazyAttribute();
    Program_errors_visited = false;
    return Program_errors_value;
  }
  /** @apilevel internal */
  private Set<ErrorMessage> errors_compute() {
    ASTNode node = this;
    while (node != null && !(node instanceof Program)) {
      node = node.getParent();
    }
    Program root = (Program) node;
    root.survey_Program_errors();
    Set<ErrorMessage> _computedValue = new TreeSet<ErrorMessage>();
    if (root.contributorMap_Program_errors.containsKey(this)) {
      for (ASTNode contributor : (java.util.Set<ASTNode>) root.contributorMap_Program_errors.get(this)) {
        contributor.contributeTo_Program_errors(_computedValue);
      }
    }
    return _computedValue;
  }
  /** @apilevel internal */
  protected boolean Program_errors_computed = false;

  /** @apilevel internal */
  protected Set<ErrorMessage> Program_errors_value;


}
