/* This file was generated with JastAdd2 (http://jastadd.org) version 2.3.4 */
package lang.ast;
import java.util.*;
import java.io.PrintStream;
import java.util.Set;
import java.util.TreeSet;
import java.util.ArrayList;
import java.io.ByteArrayOutputStream;
import java.lang.reflect.InvocationTargetException;
import java.util.HashSet;
/**
 * @ast node
 * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/lang.ast:6
 * @astdecl class_def : vfc ::= IdDecl clType class_body;
 * @production class_def : {@link vfc} ::= <span class="component">{@link IdDecl}</span> <span class="component">{@link clType}</span> <span class="component">{@link class_body}</span>;

 */
public class class_def extends vfc implements Cloneable {
  /**
   * @aspect CodeGen
   * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/CodeGen.jrag:248
   */
  public void genCode(PrintStream out, IdDecl id) {
	}
  /**
   * @aspect NameAnalysis
   * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/nameanalysis.jrag:171
   */
  public IdDecl lookup(String name) {
		if (getIdDecl().getID().equals(name)) {
			return getIdDecl();
		}
		else if (name.equals("print")) {
		}
		return unknownDecl();
	}
  /**
   * @declaredat ASTNode:1
   */
  public class_def() {
    super();
  }
  /**
   * Initializes the child array to the correct size.
   * Initializes List and Opt nta children.
   * @apilevel internal
   * @ast method
   * @declaredat ASTNode:10
   */
  public void init$Children() {
    children = new ASTNode[3];
  }
  /**
   * @declaredat ASTNode:13
   */
  @ASTNodeAnnotation.Constructor(
    name = {"IdDecl", "clType", "class_body"},
    type = {"IdDecl", "clType", "class_body"},
    kind = {"Child", "Child", "Child"}
  )
  public class_def(IdDecl p0, clType p1, class_body p2) {
    setChild(p0, 0);
    setChild(p1, 1);
    setChild(p2, 2);
  }
  /** @apilevel low-level 
   * @declaredat ASTNode:24
   */
  protected int numChildren() {
    return 3;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:28
   */
  public void flushAttrCache() {
    super.flushAttrCache();
    name_reset();
    isClasse_reset();
    getDecl_reset();
    isClass_reset();
    type_reset();
    types_reset();
    lookupFunc_String_reset();
    globalLookup_String_reset();
  }
  /** @apilevel internal 
   * @declaredat ASTNode:40
   */
  public void flushCollectionCache() {
    super.flushCollectionCache();

  }
  /** @apilevel internal 
   * @declaredat ASTNode:45
   */
  public class_def clone() throws CloneNotSupportedException {
    class_def node = (class_def) super.clone();
    return node;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:50
   */
  public class_def copy() {
    try {
      class_def node = (class_def) clone();
      node.parent = null;
      if (children != null) {
        node.children = (ASTNode[]) children.clone();
      }
      return node;
    } catch (CloneNotSupportedException e) {
      throw new Error("Error: clone not supported for " + getClass().getName());
    }
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @deprecated Please use treeCopy or treeCopyNoTransform instead
   * @declaredat ASTNode:69
   */
  @Deprecated
  public class_def fullCopy() {
    return treeCopyNoTransform();
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @declaredat ASTNode:79
   */
  public class_def treeCopyNoTransform() {
    class_def tree = (class_def) copy();
    if (children != null) {
      for (int i = 0; i < children.length; ++i) {
        ASTNode child = (ASTNode) children[i];
        if (child != null) {
          child = child.treeCopyNoTransform();
          tree.setChild(child, i);
        }
      }
    }
    return tree;
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The subtree of this node is traversed to trigger rewrites before copy.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @declaredat ASTNode:99
   */
  public class_def treeCopy() {
    class_def tree = (class_def) copy();
    if (children != null) {
      for (int i = 0; i < children.length; ++i) {
        ASTNode child = (ASTNode) getChild(i);
        if (child != null) {
          child = child.treeCopy();
          tree.setChild(child, i);
        }
      }
    }
    return tree;
  }
  /**
   * Replaces the IdDecl child.
   * @param node The new node to replace the IdDecl child.
   * @apilevel high-level
   */
  public class_def setIdDecl(IdDecl node) {
    setChild(node, 0);
    return this;
  }
  /**
   * Retrieves the IdDecl child.
   * @return The current node used as the IdDecl child.
   * @apilevel high-level
   */
  @ASTNodeAnnotation.Child(name="IdDecl")
  public IdDecl getIdDecl() {
    return (IdDecl) getChild(0);
  }
  /**
   * Retrieves the IdDecl child.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The current node used as the IdDecl child.
   * @apilevel low-level
   */
  public IdDecl getIdDeclNoTransform() {
    return (IdDecl) getChildNoTransform(0);
  }
  /**
   * Replaces the clType child.
   * @param node The new node to replace the clType child.
   * @apilevel high-level
   */
  public class_def setclType(clType node) {
    setChild(node, 1);
    return this;
  }
  /**
   * Retrieves the clType child.
   * @return The current node used as the clType child.
   * @apilevel high-level
   */
  @ASTNodeAnnotation.Child(name="clType")
  public clType getclType() {
    return (clType) getChild(1);
  }
  /**
   * Retrieves the clType child.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The current node used as the clType child.
   * @apilevel low-level
   */
  public clType getclTypeNoTransform() {
    return (clType) getChildNoTransform(1);
  }
  /**
   * Replaces the class_body child.
   * @param node The new node to replace the class_body child.
   * @apilevel high-level
   */
  public class_def setclass_body(class_body node) {
    setChild(node, 2);
    return this;
  }
  /**
   * Retrieves the class_body child.
   * @return The current node used as the class_body child.
   * @apilevel high-level
   */
  @ASTNodeAnnotation.Child(name="class_body")
  public class_body getclass_body() {
    return (class_body) getChild(2);
  }
  /**
   * Retrieves the class_body child.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The current node used as the class_body child.
   * @apilevel low-level
   */
  public class_body getclass_bodyNoTransform() {
    return (class_body) getChildNoTransform(2);
  }
/** @apilevel internal */
protected boolean name_visited = false;
  /** @apilevel internal */
  private void name_reset() {
    name_computed = false;
    
    name_value = null;
    name_visited = false;
  }
  /** @apilevel internal */
  protected boolean name_computed = false;

  /** @apilevel internal */
  protected String name_value;

  /**
   * @attribute syn
   * @aspect NameAnalysis
   * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/nameanalysis.jrag:85
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="NameAnalysis", declaredAt="/home/tobias_carlsson/chocopy-tobias/src/jastadd/nameanalysis.jrag:84")
  public String name() {
    ASTState state = state();
    if (name_computed) {
      return name_value;
    }
    if (name_visited) {
      throw new RuntimeException("Circular definition of attribute vfc.name().");
    }
    name_visited = true;
    state().enterLazyAttribute();
    name_value = getIdDecl().getID();
    name_computed = true;
    state().leaveLazyAttribute();
    name_visited = false;
    return name_value;
  }
/** @apilevel internal */
protected boolean isClasse_visited = false;
  /** @apilevel internal */
  private void isClasse_reset() {
    isClasse_computed = false;
    isClasse_visited = false;
  }
  /** @apilevel internal */
  protected boolean isClasse_computed = false;

  /** @apilevel internal */
  protected boolean isClasse_value;

  /**
   * @attribute syn
   * @aspect NameAnalysis
   * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/nameanalysis.jrag:88
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="NameAnalysis", declaredAt="/home/tobias_carlsson/chocopy-tobias/src/jastadd/nameanalysis.jrag:87")
  public boolean isClasse() {
    ASTState state = state();
    if (isClasse_computed) {
      return isClasse_value;
    }
    if (isClasse_visited) {
      throw new RuntimeException("Circular definition of attribute vfc.isClasse().");
    }
    isClasse_visited = true;
    state().enterLazyAttribute();
    isClasse_value = true;
    isClasse_computed = true;
    state().leaveLazyAttribute();
    isClasse_visited = false;
    return isClasse_value;
  }
/** @apilevel internal */
protected boolean getDecl_visited = false;
  /** @apilevel internal */
  private void getDecl_reset() {
    getDecl_computed = false;
    
    getDecl_value = null;
    getDecl_visited = false;
  }
  /** @apilevel internal */
  protected boolean getDecl_computed = false;

  /** @apilevel internal */
  protected IdDecl getDecl_value;

  /**
   * @attribute syn
   * @aspect NameAnalysis
   * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/nameanalysis.jrag:105
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="NameAnalysis", declaredAt="/home/tobias_carlsson/chocopy-tobias/src/jastadd/nameanalysis.jrag:105")
  public IdDecl getDecl() {
    ASTState state = state();
    if (getDecl_computed) {
      return getDecl_value;
    }
    if (getDecl_visited) {
      throw new RuntimeException("Circular definition of attribute class_def.getDecl().");
    }
    getDecl_visited = true;
    state().enterLazyAttribute();
    getDecl_value = getIdDecl();
    getDecl_computed = true;
    state().leaveLazyAttribute();
    getDecl_visited = false;
    return getDecl_value;
  }
/** @apilevel internal */
protected boolean isClass_visited = false;
  /** @apilevel internal */
  private void isClass_reset() {
    isClass_computed = false;
    isClass_visited = false;
  }
  /** @apilevel internal */
  protected boolean isClass_computed = false;

  /** @apilevel internal */
  protected boolean isClass_value;

  /**
   * @attribute syn
   * @aspect NameAnalysis
   * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/nameanalysis.jrag:111
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="NameAnalysis", declaredAt="/home/tobias_carlsson/chocopy-tobias/src/jastadd/nameanalysis.jrag:111")
  public boolean isClass() {
    ASTState state = state();
    if (isClass_computed) {
      return isClass_value;
    }
    if (isClass_visited) {
      throw new RuntimeException("Circular definition of attribute class_def.isClass().");
    }
    isClass_visited = true;
    state().enterLazyAttribute();
    isClass_value = true;
    isClass_computed = true;
    state().leaveLazyAttribute();
    isClass_visited = false;
    return isClass_value;
  }
/** @apilevel internal */
protected boolean type_visited = false;
  /** @apilevel internal */
  private void type_reset() {
    type_computed = false;
    
    type_value = null;
    type_visited = false;
  }
  /** @apilevel internal */
  protected boolean type_computed = false;

  /** @apilevel internal */
  protected String type_value;

  /**
   * @attribute syn
   * @aspect NameAnalysis
   * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/nameanalysis.jrag:183
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="NameAnalysis", declaredAt="/home/tobias_carlsson/chocopy-tobias/src/jastadd/nameanalysis.jrag:183")
  public String type() {
    ASTState state = state();
    if (type_computed) {
      return type_value;
    }
    if (type_visited) {
      throw new RuntimeException("Circular definition of attribute class_def.type().");
    }
    type_visited = true;
    state().enterLazyAttribute();
    type_value = "classdef";
    type_computed = true;
    state().leaveLazyAttribute();
    type_visited = false;
    return type_value;
  }
/** @apilevel internal */
protected boolean types_visited = false;
  /** @apilevel internal */
  private void types_reset() {
    types_computed = false;
    
    types_value = null;
    types_visited = false;
  }
  /** @apilevel internal */
  protected boolean types_computed = false;

  /** @apilevel internal */
  protected Typen types_value;

  /**
   * @attribute syn
   * @aspect typeChecker
   * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/typeChecker.jrag:72
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="typeChecker", declaredAt="/home/tobias_carlsson/chocopy-tobias/src/jastadd/typeChecker.jrag:72")
  public Typen types() {
    ASTState state = state();
    if (types_computed) {
      return types_value;
    }
    if (types_visited) {
      throw new RuntimeException("Circular definition of attribute class_def.types().");
    }
    types_visited = true;
    state().enterLazyAttribute();
    types_value = objType();
    types_computed = true;
    state().leaveLazyAttribute();
    types_visited = false;
    return types_value;
  }
  /**
   * @attribute inh
   * @aspect NameAnalysis
   * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/nameanalysis.jrag:114
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.INH)
  @ASTNodeAnnotation.Source(aspect="NameAnalysis", declaredAt="/home/tobias_carlsson/chocopy-tobias/src/jastadd/nameanalysis.jrag:114")
  public IdDecl lookupFunc(String name) {
    Object _parameters = name;
    if (lookupFunc_String_visited == null) lookupFunc_String_visited = new java.util.HashSet(4);
    if (lookupFunc_String_values == null) lookupFunc_String_values = new java.util.HashMap(4);
    ASTState state = state();
    if (lookupFunc_String_values.containsKey(_parameters)) {
      return (IdDecl) lookupFunc_String_values.get(_parameters);
    }
    if (lookupFunc_String_visited.contains(_parameters)) {
      throw new RuntimeException("Circular definition of attribute class_def.lookupFunc(String).");
    }
    lookupFunc_String_visited.add(_parameters);
    state().enterLazyAttribute();
    IdDecl lookupFunc_String_value = getParent().Define_lookupFunc(this, null, name);
    lookupFunc_String_values.put(_parameters, lookupFunc_String_value);
    state().leaveLazyAttribute();
    lookupFunc_String_visited.remove(_parameters);
    return lookupFunc_String_value;
  }
/** @apilevel internal */
protected java.util.Set lookupFunc_String_visited;
  /** @apilevel internal */
  private void lookupFunc_String_reset() {
    lookupFunc_String_values = null;
    lookupFunc_String_visited = null;
  }
  /** @apilevel internal */
  protected java.util.Map lookupFunc_String_values;

  /**
   * @attribute inh
   * @aspect NameAnalysis
   * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/nameanalysis.jrag:329
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.INH)
  @ASTNodeAnnotation.Source(aspect="NameAnalysis", declaredAt="/home/tobias_carlsson/chocopy-tobias/src/jastadd/nameanalysis.jrag:329")
  public IdDecl globalLookup(String name) {
    Object _parameters = name;
    if (globalLookup_String_visited == null) globalLookup_String_visited = new java.util.HashSet(4);
    if (globalLookup_String_values == null) globalLookup_String_values = new java.util.HashMap(4);
    ASTState state = state();
    if (globalLookup_String_values.containsKey(_parameters)) {
      return (IdDecl) globalLookup_String_values.get(_parameters);
    }
    if (globalLookup_String_visited.contains(_parameters)) {
      throw new RuntimeException("Circular definition of attribute class_def.globalLookup(String).");
    }
    globalLookup_String_visited.add(_parameters);
    state().enterLazyAttribute();
    IdDecl globalLookup_String_value = getParent().Define_globalLookup(this, null, name);
    globalLookup_String_values.put(_parameters, globalLookup_String_value);
    state().leaveLazyAttribute();
    globalLookup_String_visited.remove(_parameters);
    return globalLookup_String_value;
  }
/** @apilevel internal */
protected java.util.Set globalLookup_String_visited;
  /** @apilevel internal */
  private void globalLookup_String_reset() {
    globalLookup_String_values = null;
    globalLookup_String_visited = null;
  }
  /** @apilevel internal */
  protected java.util.Map globalLookup_String_values;

  /**
   * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/nameanalysis.jrag:109
   * @apilevel internal
   */
  public boolean Define_isClass(ASTNode _callerNode, ASTNode _childNode) {
    if (_callerNode == getIdDeclNoTransform()) {
      // @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/nameanalysis.jrag:110
      return true;
    }
    else {
      return getParent().Define_isClass(this, _callerNode);
    }
  }
  /**
   * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/nameanalysis.jrag:109
   * @apilevel internal
   * @return {@code true} if this node has an equation for the inherited attribute isClass
   */
  protected boolean canDefine_isClass(ASTNode _callerNode, ASTNode _childNode) {
    return true;
  }
  /**
   * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/nameanalysis.jrag:254
   * @apilevel internal
   */
  public IdDecl Define_loclookup(ASTNode _callerNode, ASTNode _childNode, String name) {
    if (_callerNode == getIdDeclNoTransform()) {
      // @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/nameanalysis.jrag:153
      {
      
      		for(func_def f : getclass_body().getoptfuncs()){
      			if(f.getIdDecl().getID().equals(name)){
      				return f.getIdDecl();
      			}
      		}
      
      		return unknownDecl();
      	}
    }
    else {
      return getParent().Define_loclookup(this, _callerNode, name);
    }
  }
  /**
   * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/nameanalysis.jrag:254
   * @apilevel internal
   * @return {@code true} if this node has an equation for the inherited attribute loclookup
   */
  protected boolean canDefine_loclookup(ASTNode _callerNode, ASTNode _childNode, String name) {
    return true;
  }
  /**
   * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/nameanalysis.jrag:207
   * @apilevel internal
   */
  public IdDecl Define_lookup(ASTNode _callerNode, ASTNode _childNode, String name) {
    if (_callerNode == getIdDeclNoTransform()) {
      // @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/nameanalysis.jrag:239
      return globalLookup(name);
    }
    else {
      return getParent().Define_lookup(this, _callerNode, name);
    }
  }
  /**
   * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/nameanalysis.jrag:207
   * @apilevel internal
   * @return {@code true} if this node has an equation for the inherited attribute lookup
   */
  protected boolean canDefine_lookup(ASTNode _callerNode, ASTNode _childNode, String name) {
    return true;
  }
  /**
   * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/paramAdrInd.jrag:64
   * @apilevel internal
   */
  public boolean Define_insideFunc(ASTNode _callerNode, ASTNode _childNode) {
    int childIndex = this.getIndexOfChild(_callerNode);
    return false;
  }
  /**
   * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/paramAdrInd.jrag:64
   * @apilevel internal
   * @return {@code true} if this node has an equation for the inherited attribute insideFunc
   */
  protected boolean canDefine_insideFunc(ASTNode _callerNode, ASTNode _childNode) {
    return true;
  }
  /**
   * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/paramAdrInd.jrag:70
   * @apilevel internal
   */
  public boolean Define_insideClass(ASTNode _callerNode, ASTNode _childNode) {
    int childIndex = this.getIndexOfChild(_callerNode);
    return true;
  }
  /**
   * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/paramAdrInd.jrag:70
   * @apilevel internal
   * @return {@code true} if this node has an equation for the inherited attribute insideClass
   */
  protected boolean canDefine_insideClass(ASTNode _callerNode, ASTNode _childNode) {
    return true;
  }
  /**
   * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/paramAdrInd.jrag:90
   * @apilevel internal
   */
  public int Define_nbrLoc(ASTNode _callerNode, ASTNode _childNode) {
    if (_callerNode == getIdDeclNoTransform()) {
      // @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/paramAdrInd.jrag:97
      {
      		int count =1;
      		class_body cb = getclass_body();
      		for (var_def f : cb.getoptVfs()) {
      			count++;
      		}
      		return count;
      	}
    }
    else {
      return getParent().Define_nbrLoc(this, _callerNode);
    }
  }
  /**
   * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/paramAdrInd.jrag:90
   * @apilevel internal
   * @return {@code true} if this node has an equation for the inherited attribute nbrLoc
   */
  protected boolean canDefine_nbrLoc(ASTNode _callerNode, ASTNode _childNode) {
    return true;
  }
  /**
   * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/paramAdrInd.jrag:106
   * @apilevel internal
   */
  public int Define_nbrLocF(ASTNode _callerNode, ASTNode _childNode) {
    if (_callerNode == getIdDeclNoTransform()) {
      // @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/paramAdrInd.jrag:113
      {
      		int count =0;
      		class_body cb = getclass_body();
      		for(func_def fc : cb.getoptfuncs()){
      			func_body bb = fc.getfunc_body();
      			for(var_def vv : bb.getlocdefs()){
      				count++;
      			}
      		}
      		return count;
      	}
    }
    else {
      return getParent().Define_nbrLocF(this, _callerNode);
    }
  }
  /**
   * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/paramAdrInd.jrag:106
   * @apilevel internal
   * @return {@code true} if this node has an equation for the inherited attribute nbrLocF
   */
  protected boolean canDefine_nbrLocF(ASTNode _callerNode, ASTNode _childNode) {
    return true;
  }
  /**
   * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/paramAdrInd.jrag:129
   * @apilevel internal
   */
  public boolean Define_hasInit(ASTNode _callerNode, ASTNode _childNode) {
    int childIndex = this.getIndexOfChild(_callerNode);
    return true;
  }
  /**
   * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/paramAdrInd.jrag:129
   * @apilevel internal
   * @return {@code true} if this node has an equation for the inherited attribute hasInit
   */
  protected boolean canDefine_hasInit(ASTNode _callerNode, ASTNode _childNode) {
    return true;
  }
  /**
   * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/paramAdrInd.jrag:132
   * @apilevel internal
   */
  public int Define_nbrInit(ASTNode _callerNode, ASTNode _childNode) {
    int childIndex = this.getIndexOfChild(_callerNode);
    {
    		int count = 0;
    		List<var_def> list = new List<var_def>();
    		class_body cb = getclass_body();
    		for(func_def fb : cb.getoptfuncs()){
    			func_body fbod = fb.getfunc_body();
    			if(fb.getIdDecl().getID().equals("__init__")){
    				if(fb.hasopttypedVar()){
    					count = 1 + fb.getopttypedVar().getNumopt();
    				}
    			}
    		}
    		return count;
    	}
  }
  /**
   * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/paramAdrInd.jrag:132
   * @apilevel internal
   * @return {@code true} if this node has an equation for the inherited attribute nbrInit
   */
  protected boolean canDefine_nbrInit(ASTNode _callerNode, ASTNode _childNode) {
    return true;
  }
  /**
   * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/paramAdrInd.jrag:128
   * @apilevel internal
   */
  public HashMap<String, Integer> Define_objNum(ASTNode _callerNode, ASTNode _childNode) {
    if (_callerNode == getIdDeclNoTransform()) {
      // @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/paramAdrInd.jrag:149
      {
      			class_body cb = getclass_body();
      			HashMap<String, Integer> hash_map = new HashMap<String, Integer>(); 
      			for( func_def f : cb.getoptfuncs()){
      				int count =0;
      				if(f.hasopttypedVar()){
      
      					count = f.getopttypedVar().getNumopt() +1;	
      				}
      				hash_map.put(f.getIdDecl().getID(),count);
      			}
      			return hash_map;
      		}
    }
    else {
      return getParent().Define_objNum(this, _callerNode);
    }
  }
  /**
   * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/paramAdrInd.jrag:128
   * @apilevel internal
   * @return {@code true} if this node has an equation for the inherited attribute objNum
   */
  protected boolean canDefine_objNum(ASTNode _callerNode, ASTNode _childNode) {
    return true;
  }
  /**
   * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/paramAdrInd.jrag:179
   * @apilevel internal
   */
  public List<var_def> Define_locvars(ASTNode _callerNode, ASTNode _childNode) {
    int childIndex = this.getIndexOfChild(_callerNode);
    {
    		List<var_def> list = new List<var_def>();
    		class_body cb = getclass_body();
    		list.add(cb.getvar_def());
    		for (var_def f : cb.getoptVfs()) {
    			list.add(f);
    		}
    		return list;
    	}
  }
  /**
   * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/paramAdrInd.jrag:179
   * @apilevel internal
   * @return {@code true} if this node has an equation for the inherited attribute locvars
   */
  protected boolean canDefine_locvars(ASTNode _callerNode, ASTNode _childNode) {
    return true;
  }
  /**
   * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/paramAdrInd.jrag:180
   * @apilevel internal
   */
  public List<var_def> Define_locvarsF(ASTNode _callerNode, ASTNode _childNode) {
    int childIndex = this.getIndexOfChild(_callerNode);
    {
    		List<var_def> list = new List<var_def>();
    		class_body cb = getclass_body();
    		for(func_def fb : cb.getoptfuncs()){
    			func_body fbod = fb.getfunc_body();
    			for(var_def f : fbod.getlocdefs()){
    				list.add(f);
    			}
    		}
    		return list;
    	}
  }
  /**
   * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/paramAdrInd.jrag:180
   * @apilevel internal
   * @return {@code true} if this node has an equation for the inherited attribute locvarsF
   */
  protected boolean canDefine_locvarsF(ASTNode _callerNode, ASTNode _childNode) {
    return true;
  }
  /**
   * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/rndFix.jrag:8
   * @apilevel internal
   */
  public boolean Define_insideCl(ASTNode _callerNode, ASTNode _childNode) {
    int childIndex = this.getIndexOfChild(_callerNode);
    return true;
  }
  /**
   * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/rndFix.jrag:8
   * @apilevel internal
   * @return {@code true} if this node has an equation for the inherited attribute insideCl
   */
  protected boolean canDefine_insideCl(ASTNode _callerNode, ASTNode _childNode) {
    return true;
  }
  /**
   * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/typeChecker.jrag:21
   * @apilevel internal
   */
  public Typen Define_types(ASTNode _callerNode, ASTNode _childNode) {
    if (_callerNode == getIdDeclNoTransform()) {
      // @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/typeChecker.jrag:32
      {
      		if(getclType().getID().equals("object")){
      			return objType();
      		}
      		else{
      			return lookupFunc(getclType().getID()).types();
      		}
      	}
    }
    else {
      return getParent().Define_types(this, _callerNode);
    }
  }
  /**
   * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/typeChecker.jrag:21
   * @apilevel internal
   * @return {@code true} if this node has an equation for the inherited attribute types
   */
  protected boolean canDefine_types(ASTNode _callerNode, ASTNode _childNode) {
    return true;
  }

}
