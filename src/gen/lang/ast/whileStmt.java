/* This file was generated with JastAdd2 (http://jastadd.org) version 2.3.4 */
package lang.ast;
import java.util.*;
import java.io.PrintStream;
import java.util.Set;
import java.util.TreeSet;
import java.util.ArrayList;
import java.io.ByteArrayOutputStream;
import java.lang.reflect.InvocationTargetException;
import java.util.HashSet;
/**
 * @ast node
 * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/lang.ast:25
 * @astdecl whileStmt : stmt ::= while:cexpr then:Block;
 * @production whileStmt : {@link stmt} ::= <span class="component">while:{@link cexpr}</span> <span class="component">then:{@link Block}</span>;

 */
public class whileStmt extends stmt implements Cloneable {
  /**
   * @aspect CodeGen
   * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/CodeGen.jrag:496
   */
  public void genCode(PrintStream out,IdDecl id) {
		out.println(globalStmtId() + "start:");
		getwhile().genConditionalJump(out,id,globalStmtId() + "end");
		getthen().genCode(out,id);
		out.println("        jmp " + globalStmtId() + "start");
		out.println(globalStmtId() + "end:");
	}
  /**
   * @declaredat ASTNode:1
   */
  public whileStmt() {
    super();
  }
  /**
   * Initializes the child array to the correct size.
   * Initializes List and Opt nta children.
   * @apilevel internal
   * @ast method
   * @declaredat ASTNode:10
   */
  public void init$Children() {
    children = new ASTNode[2];
  }
  /**
   * @declaredat ASTNode:13
   */
  @ASTNodeAnnotation.Constructor(
    name = {"while", "then"},
    type = {"cexpr", "Block"},
    kind = {"Child", "Child"}
  )
  public whileStmt(cexpr p0, Block p1) {
    setChild(p0, 0);
    setChild(p1, 1);
  }
  /** @apilevel low-level 
   * @declaredat ASTNode:23
   */
  protected int numChildren() {
    return 2;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:27
   */
  public void flushAttrCache() {
    super.flushAttrCache();
    globalStmtId_reset();
    types_reset();
    expectedType_reset();
  }
  /** @apilevel internal 
   * @declaredat ASTNode:34
   */
  public void flushCollectionCache() {
    super.flushCollectionCache();

  }
  /** @apilevel internal 
   * @declaredat ASTNode:39
   */
  public whileStmt clone() throws CloneNotSupportedException {
    whileStmt node = (whileStmt) super.clone();
    return node;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:44
   */
  public whileStmt copy() {
    try {
      whileStmt node = (whileStmt) clone();
      node.parent = null;
      if (children != null) {
        node.children = (ASTNode[]) children.clone();
      }
      return node;
    } catch (CloneNotSupportedException e) {
      throw new Error("Error: clone not supported for " + getClass().getName());
    }
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @deprecated Please use treeCopy or treeCopyNoTransform instead
   * @declaredat ASTNode:63
   */
  @Deprecated
  public whileStmt fullCopy() {
    return treeCopyNoTransform();
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @declaredat ASTNode:73
   */
  public whileStmt treeCopyNoTransform() {
    whileStmt tree = (whileStmt) copy();
    if (children != null) {
      for (int i = 0; i < children.length; ++i) {
        ASTNode child = (ASTNode) children[i];
        if (child != null) {
          child = child.treeCopyNoTransform();
          tree.setChild(child, i);
        }
      }
    }
    return tree;
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The subtree of this node is traversed to trigger rewrites before copy.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @declaredat ASTNode:93
   */
  public whileStmt treeCopy() {
    whileStmt tree = (whileStmt) copy();
    if (children != null) {
      for (int i = 0; i < children.length; ++i) {
        ASTNode child = (ASTNode) getChild(i);
        if (child != null) {
          child = child.treeCopy();
          tree.setChild(child, i);
        }
      }
    }
    return tree;
  }
  /**
   * Replaces the while child.
   * @param node The new node to replace the while child.
   * @apilevel high-level
   */
  public whileStmt setwhile(cexpr node) {
    setChild(node, 0);
    return this;
  }
  /**
   * Retrieves the while child.
   * @return The current node used as the while child.
   * @apilevel high-level
   */
  @ASTNodeAnnotation.Child(name="while")
  public cexpr getwhile() {
    return (cexpr) getChild(0);
  }
  /**
   * Retrieves the while child.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The current node used as the while child.
   * @apilevel low-level
   */
  public cexpr getwhileNoTransform() {
    return (cexpr) getChildNoTransform(0);
  }
  /**
   * Replaces the then child.
   * @param node The new node to replace the then child.
   * @apilevel high-level
   */
  public whileStmt setthen(Block node) {
    setChild(node, 1);
    return this;
  }
  /**
   * Retrieves the then child.
   * @return The current node used as the then child.
   * @apilevel high-level
   */
  @ASTNodeAnnotation.Child(name="then")
  public Block getthen() {
    return (Block) getChild(1);
  }
  /**
   * Retrieves the then child.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The current node used as the then child.
   * @apilevel low-level
   */
  public Block getthenNoTransform() {
    return (Block) getChildNoTransform(1);
  }
/** @apilevel internal */
protected boolean globalStmtId_visited = false;
  /** @apilevel internal */
  private void globalStmtId_reset() {
    globalStmtId_computed = false;
    
    globalStmtId_value = null;
    globalStmtId_visited = false;
  }
  /** @apilevel internal */
  protected boolean globalStmtId_computed = false;

  /** @apilevel internal */
  protected String globalStmtId_value;

  /**
   * @attribute syn
   * @aspect UniqueName
   * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/genUniqueName.jrag:12
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="UniqueName", declaredAt="/home/tobias_carlsson/chocopy-tobias/src/jastadd/genUniqueName.jrag:12")
  public String globalStmtId() {
    ASTState state = state();
    if (globalStmtId_computed) {
      return globalStmtId_value;
    }
    if (globalStmtId_visited) {
      throw new RuntimeException("Circular definition of attribute whileStmt.globalStmtId().");
    }
    globalStmtId_visited = true;
    state().enterLazyAttribute();
    globalStmtId_value = "whilestmt" +'_' + stmtId();
    globalStmtId_computed = true;
    state().leaveLazyAttribute();
    globalStmtId_visited = false;
    return globalStmtId_value;
  }
/** @apilevel internal */
protected boolean types_visited = false;
  /** @apilevel internal */
  private void types_reset() {
    types_computed = false;
    
    types_value = null;
    types_visited = false;
  }
  /** @apilevel internal */
  protected boolean types_computed = false;

  /** @apilevel internal */
  protected Typen types_value;

  /**
   * @attribute syn
   * @aspect typeChecker
   * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/typeChecker.jrag:55
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="typeChecker", declaredAt="/home/tobias_carlsson/chocopy-tobias/src/jastadd/typeChecker.jrag:55")
  public Typen types() {
    ASTState state = state();
    if (types_computed) {
      return types_value;
    }
    if (types_visited) {
      throw new RuntimeException("Circular definition of attribute whileStmt.types().");
    }
    types_visited = true;
    state().enterLazyAttribute();
    types_value = getwhile().types();
    types_computed = true;
    state().leaveLazyAttribute();
    types_visited = false;
    return types_value;
  }
/** @apilevel internal */
protected boolean expectedType_visited = false;
  /** @apilevel internal */
  private void expectedType_reset() {
    expectedType_computed = false;
    
    expectedType_value = null;
    expectedType_visited = false;
  }
  /** @apilevel internal */
  protected boolean expectedType_computed = false;

  /** @apilevel internal */
  protected Typen expectedType_value;

  /**
   * @attribute syn
   * @aspect typeChecker
   * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/typeChecker.jrag:81
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="typeChecker", declaredAt="/home/tobias_carlsson/chocopy-tobias/src/jastadd/typeChecker.jrag:81")
  public Typen expectedType() {
    ASTState state = state();
    if (expectedType_computed) {
      return expectedType_value;
    }
    if (expectedType_visited) {
      throw new RuntimeException("Circular definition of attribute whileStmt.expectedType().");
    }
    expectedType_visited = true;
    state().enterLazyAttribute();
    expectedType_value = boolType();
    expectedType_computed = true;
    state().leaveLazyAttribute();
    expectedType_visited = false;
    return expectedType_value;
  }
  /** @apilevel internal */
  protected void collect_contributors_Program_errors(Program _root, java.util.Map<ASTNode, java.util.Set<ASTNode>> _map) {
    // @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/Errors.jrag:85
    if (!types().compatibleType(expectedType())) {
      {
        Program target = (Program) (program());
        java.util.Set<ASTNode> contributors = _map.get(target);
        if (contributors == null) {
          contributors = new java.util.LinkedHashSet<ASTNode>();
          _map.put((ASTNode) target, contributors);
        }
        contributors.add(this);
      }
    }
    super.collect_contributors_Program_errors(_root, _map);
  }
  /** @apilevel internal */
  protected void contributeTo_Program_errors(Set<ErrorMessage> collection) {
    super.contributeTo_Program_errors(collection);
    if (!types().compatibleType(expectedType())) {
      collection.add(error("Expected: '" +expectedType() + "' but recieved: '"+ types()+"'"));
    }
  }

}
