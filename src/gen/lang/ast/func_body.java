/* This file was generated with JastAdd2 (http://jastadd.org) version 2.3.4 */
package lang.ast;
import java.util.*;
import java.io.PrintStream;
import java.util.Set;
import java.util.TreeSet;
import java.util.ArrayList;
import java.io.ByteArrayOutputStream;
import java.lang.reflect.InvocationTargetException;
import java.util.HashSet;
/**
 * @ast node
 * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/lang.ast:13
 * @astdecl func_body : ASTNode ::= locdef:var_def* stmt optList:stmt*;
 * @production func_body : {@link ASTNode} ::= <span class="component">locdef:{@link var_def}*</span> <span class="component">{@link stmt}</span> <span class="component">optList:{@link stmt}*</span>;

 */
public class func_body extends ASTNode<ASTNode> implements Cloneable {
  /**
   * @aspect CodeGen
   * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/CodeGen.jrag:235
   */
  public void genCode(PrintStream out, IdDecl id) {
		if(haslocdef()){
			for(var_def vf: getlocdefs()){
				vf.genCode(out,id);
			}
		}
		getstmt().genCode(out,id);
		for (stmt s : getoptListList()) {
			s.genCode(out,id);

		}
	}
  /**
   * @declaredat ASTNode:1
   */
  public func_body() {
    super();
  }
  /**
   * Initializes the child array to the correct size.
   * Initializes List and Opt nta children.
   * @apilevel internal
   * @ast method
   * @declaredat ASTNode:10
   */
  public void init$Children() {
    children = new ASTNode[3];
    setChild(new List(), 0);
    setChild(new List(), 2);
  }
  /**
   * @declaredat ASTNode:15
   */
  @ASTNodeAnnotation.Constructor(
    name = {"locdef", "stmt", "optList"},
    type = {"List<var_def>", "stmt", "List<stmt>"},
    kind = {"List", "Child", "List"}
  )
  public func_body(List<var_def> p0, stmt p1, List<stmt> p2) {
    setChild(p0, 0);
    setChild(p1, 1);
    setChild(p2, 2);
  }
  /** @apilevel low-level 
   * @declaredat ASTNode:26
   */
  protected int numChildren() {
    return 3;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:30
   */
  public void flushAttrCache() {
    super.flushAttrCache();
    lookUpVar_String_reset();
    localLookup_String_reset();
    lookup_String_reset();
    localIndex_reset();
    insideCl_reset();
  }
  /** @apilevel internal 
   * @declaredat ASTNode:39
   */
  public void flushCollectionCache() {
    super.flushCollectionCache();

  }
  /** @apilevel internal 
   * @declaredat ASTNode:44
   */
  public func_body clone() throws CloneNotSupportedException {
    func_body node = (func_body) super.clone();
    return node;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:49
   */
  public func_body copy() {
    try {
      func_body node = (func_body) clone();
      node.parent = null;
      if (children != null) {
        node.children = (ASTNode[]) children.clone();
      }
      return node;
    } catch (CloneNotSupportedException e) {
      throw new Error("Error: clone not supported for " + getClass().getName());
    }
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @deprecated Please use treeCopy or treeCopyNoTransform instead
   * @declaredat ASTNode:68
   */
  @Deprecated
  public func_body fullCopy() {
    return treeCopyNoTransform();
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @declaredat ASTNode:78
   */
  public func_body treeCopyNoTransform() {
    func_body tree = (func_body) copy();
    if (children != null) {
      for (int i = 0; i < children.length; ++i) {
        ASTNode child = (ASTNode) children[i];
        if (child != null) {
          child = child.treeCopyNoTransform();
          tree.setChild(child, i);
        }
      }
    }
    return tree;
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The subtree of this node is traversed to trigger rewrites before copy.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @declaredat ASTNode:98
   */
  public func_body treeCopy() {
    func_body tree = (func_body) copy();
    if (children != null) {
      for (int i = 0; i < children.length; ++i) {
        ASTNode child = (ASTNode) getChild(i);
        if (child != null) {
          child = child.treeCopy();
          tree.setChild(child, i);
        }
      }
    }
    return tree;
  }
  /**
   * Replaces the locdef list.
   * @param list The new list node to be used as the locdef list.
   * @apilevel high-level
   */
  public func_body setlocdefList(List<var_def> list) {
    setChild(list, 0);
    return this;
  }
  /**
   * Retrieves the number of children in the locdef list.
   * @return Number of children in the locdef list.
   * @apilevel high-level
   */
  public int getNumlocdef() {
    return getlocdefList().getNumChild();
  }
  /**
   * Retrieves the number of children in the locdef list.
   * Calling this method will not trigger rewrites.
   * @return Number of children in the locdef list.
   * @apilevel low-level
   */
  public int getNumlocdefNoTransform() {
    return getlocdefListNoTransform().getNumChildNoTransform();
  }
  /**
   * Retrieves the element at index {@code i} in the locdef list.
   * @param i Index of the element to return.
   * @return The element at position {@code i} in the locdef list.
   * @apilevel high-level
   */
  public var_def getlocdef(int i) {
    return (var_def) getlocdefList().getChild(i);
  }
  /**
   * Check whether the locdef list has any children.
   * @return {@code true} if it has at least one child, {@code false} otherwise.
   * @apilevel high-level
   */
  public boolean haslocdef() {
    return getlocdefList().getNumChild() != 0;
  }
  /**
   * Append an element to the locdef list.
   * @param node The element to append to the locdef list.
   * @apilevel high-level
   */
  public func_body addlocdef(var_def node) {
    List<var_def> list = (parent == null) ? getlocdefListNoTransform() : getlocdefList();
    list.addChild(node);
    return this;
  }
  /** @apilevel low-level 
   */
  public func_body addlocdefNoTransform(var_def node) {
    List<var_def> list = getlocdefListNoTransform();
    list.addChild(node);
    return this;
  }
  /**
   * Replaces the locdef list element at index {@code i} with the new node {@code node}.
   * @param node The new node to replace the old list element.
   * @param i The list index of the node to be replaced.
   * @apilevel high-level
   */
  public func_body setlocdef(var_def node, int i) {
    List<var_def> list = getlocdefList();
    list.setChild(node, i);
    return this;
  }
  /**
   * Retrieves the locdef list.
   * @return The node representing the locdef list.
   * @apilevel high-level
   */
  @ASTNodeAnnotation.ListChild(name="locdef")
  public List<var_def> getlocdefList() {
    List<var_def> list = (List<var_def>) getChild(0);
    return list;
  }
  /**
   * Retrieves the locdef list.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The node representing the locdef list.
   * @apilevel low-level
   */
  public List<var_def> getlocdefListNoTransform() {
    return (List<var_def>) getChildNoTransform(0);
  }
  /**
   * @return the element at index {@code i} in the locdef list without
   * triggering rewrites.
   */
  public var_def getlocdefNoTransform(int i) {
    return (var_def) getlocdefListNoTransform().getChildNoTransform(i);
  }
  /**
   * Retrieves the locdef list.
   * @return The node representing the locdef list.
   * @apilevel high-level
   */
  public List<var_def> getlocdefs() {
    return getlocdefList();
  }
  /**
   * Retrieves the locdef list.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The node representing the locdef list.
   * @apilevel low-level
   */
  public List<var_def> getlocdefsNoTransform() {
    return getlocdefListNoTransform();
  }
  /**
   * Replaces the stmt child.
   * @param node The new node to replace the stmt child.
   * @apilevel high-level
   */
  public func_body setstmt(stmt node) {
    setChild(node, 1);
    return this;
  }
  /**
   * Retrieves the stmt child.
   * @return The current node used as the stmt child.
   * @apilevel high-level
   */
  @ASTNodeAnnotation.Child(name="stmt")
  public stmt getstmt() {
    return (stmt) getChild(1);
  }
  /**
   * Retrieves the stmt child.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The current node used as the stmt child.
   * @apilevel low-level
   */
  public stmt getstmtNoTransform() {
    return (stmt) getChildNoTransform(1);
  }
  /**
   * Replaces the optList list.
   * @param list The new list node to be used as the optList list.
   * @apilevel high-level
   */
  public func_body setoptListList(List<stmt> list) {
    setChild(list, 2);
    return this;
  }
  /**
   * Retrieves the number of children in the optList list.
   * @return Number of children in the optList list.
   * @apilevel high-level
   */
  public int getNumoptList() {
    return getoptListList().getNumChild();
  }
  /**
   * Retrieves the number of children in the optList list.
   * Calling this method will not trigger rewrites.
   * @return Number of children in the optList list.
   * @apilevel low-level
   */
  public int getNumoptListNoTransform() {
    return getoptListListNoTransform().getNumChildNoTransform();
  }
  /**
   * Retrieves the element at index {@code i} in the optList list.
   * @param i Index of the element to return.
   * @return The element at position {@code i} in the optList list.
   * @apilevel high-level
   */
  public stmt getoptList(int i) {
    return (stmt) getoptListList().getChild(i);
  }
  /**
   * Check whether the optList list has any children.
   * @return {@code true} if it has at least one child, {@code false} otherwise.
   * @apilevel high-level
   */
  public boolean hasoptList() {
    return getoptListList().getNumChild() != 0;
  }
  /**
   * Append an element to the optList list.
   * @param node The element to append to the optList list.
   * @apilevel high-level
   */
  public func_body addoptList(stmt node) {
    List<stmt> list = (parent == null) ? getoptListListNoTransform() : getoptListList();
    list.addChild(node);
    return this;
  }
  /** @apilevel low-level 
   */
  public func_body addoptListNoTransform(stmt node) {
    List<stmt> list = getoptListListNoTransform();
    list.addChild(node);
    return this;
  }
  /**
   * Replaces the optList list element at index {@code i} with the new node {@code node}.
   * @param node The new node to replace the old list element.
   * @param i The list index of the node to be replaced.
   * @apilevel high-level
   */
  public func_body setoptList(stmt node, int i) {
    List<stmt> list = getoptListList();
    list.setChild(node, i);
    return this;
  }
  /**
   * Retrieves the optList list.
   * @return The node representing the optList list.
   * @apilevel high-level
   */
  @ASTNodeAnnotation.ListChild(name="optList")
  public List<stmt> getoptListList() {
    List<stmt> list = (List<stmt>) getChild(2);
    return list;
  }
  /**
   * Retrieves the optList list.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The node representing the optList list.
   * @apilevel low-level
   */
  public List<stmt> getoptListListNoTransform() {
    return (List<stmt>) getChildNoTransform(2);
  }
  /**
   * @return the element at index {@code i} in the optList list without
   * triggering rewrites.
   */
  public stmt getoptListNoTransform(int i) {
    return (stmt) getoptListListNoTransform().getChildNoTransform(i);
  }
  /**
   * Retrieves the optList list.
   * @return The node representing the optList list.
   * @apilevel high-level
   */
  public List<stmt> getoptLists() {
    return getoptListList();
  }
  /**
   * Retrieves the optList list.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The node representing the optList list.
   * @apilevel low-level
   */
  public List<stmt> getoptListsNoTransform() {
    return getoptListListNoTransform();
  }
/** @apilevel internal */
protected java.util.Set lookUpVar_String_visited;
  /** @apilevel internal */
  private void lookUpVar_String_reset() {
    lookUpVar_String_values = null;
    lookUpVar_String_visited = null;
  }
  /** @apilevel internal */
  protected java.util.Map lookUpVar_String_values;

  /**
   * @attribute syn
   * @aspect NameAnalysis
   * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/nameanalysis.jrag:128
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="NameAnalysis", declaredAt="/home/tobias_carlsson/chocopy-tobias/src/jastadd/nameanalysis.jrag:128")
  public IdDecl lookUpVar(String name) {
    Object _parameters = name;
    if (lookUpVar_String_visited == null) lookUpVar_String_visited = new java.util.HashSet(4);
    if (lookUpVar_String_values == null) lookUpVar_String_values = new java.util.HashMap(4);
    ASTState state = state();
    if (lookUpVar_String_values.containsKey(_parameters)) {
      return (IdDecl) lookUpVar_String_values.get(_parameters);
    }
    if (lookUpVar_String_visited.contains(_parameters)) {
      throw new RuntimeException("Circular definition of attribute func_body.lookUpVar(String).");
    }
    lookUpVar_String_visited.add(_parameters);
    state().enterLazyAttribute();
    IdDecl lookUpVar_String_value = lookUpVar_compute(name);
    lookUpVar_String_values.put(_parameters, lookUpVar_String_value);
    state().leaveLazyAttribute();
    lookUpVar_String_visited.remove(_parameters);
    return lookUpVar_String_value;
  }
  /** @apilevel internal */
  private IdDecl lookUpVar_compute(String name) {
  
  		for(var_def vd: getlocdefs()){
  			if(vd.gettypedVar().getIdDecl().getID().equals(name)){
  				return vd.gettypedVar().getIdDecl();
  			}
  		}
  		return unknownDecl();
  
  	}
/** @apilevel internal */
protected java.util.Set localLookup_String_visited;
  /** @apilevel internal */
  private void localLookup_String_reset() {
    localLookup_String_values = null;
    localLookup_String_visited = null;
  }
  /** @apilevel internal */
  protected java.util.Map localLookup_String_values;

  /**
   * @attribute syn
   * @aspect NameAnalysis
   * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/nameanalysis.jrag:315
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="NameAnalysis", declaredAt="/home/tobias_carlsson/chocopy-tobias/src/jastadd/nameanalysis.jrag:315")
  public IdDecl localLookup(String name) {
    Object _parameters = name;
    if (localLookup_String_visited == null) localLookup_String_visited = new java.util.HashSet(4);
    if (localLookup_String_values == null) localLookup_String_values = new java.util.HashMap(4);
    ASTState state = state();
    if (localLookup_String_values.containsKey(_parameters)) {
      return (IdDecl) localLookup_String_values.get(_parameters);
    }
    if (localLookup_String_visited.contains(_parameters)) {
      throw new RuntimeException("Circular definition of attribute func_body.localLookup(String).");
    }
    localLookup_String_visited.add(_parameters);
    state().enterLazyAttribute();
    IdDecl localLookup_String_value = localLookup_compute(name);
    localLookup_String_values.put(_parameters, localLookup_String_value);
    state().leaveLazyAttribute();
    localLookup_String_visited.remove(_parameters);
    return localLookup_String_value;
  }
  /** @apilevel internal */
  private IdDecl localLookup_compute(String name) {
  		if(haslocdef()){
  			for (var_def tv :getlocdefs()) {
  				if (tv.gettypedVar().getIdDecl().getID().equals(name)) {
  					return tv.gettypedVar().getIdDecl();
  				}
  			}
  		}
  		return unknownDecl();
  	}
  /**
   * @attribute inh
   * @aspect NameAnalysis
   * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/nameanalysis.jrag:245
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.INH)
  @ASTNodeAnnotation.Source(aspect="NameAnalysis", declaredAt="/home/tobias_carlsson/chocopy-tobias/src/jastadd/nameanalysis.jrag:245")
  public IdDecl lookup(String name) {
    Object _parameters = name;
    if (lookup_String_visited == null) lookup_String_visited = new java.util.HashSet(4);
    if (lookup_String_values == null) lookup_String_values = new java.util.HashMap(4);
    ASTState state = state();
    if (lookup_String_values.containsKey(_parameters)) {
      return (IdDecl) lookup_String_values.get(_parameters);
    }
    if (lookup_String_visited.contains(_parameters)) {
      throw new RuntimeException("Circular definition of attribute func_body.lookup(String).");
    }
    lookup_String_visited.add(_parameters);
    state().enterLazyAttribute();
    IdDecl lookup_String_value = getParent().Define_lookup(this, null, name);
    lookup_String_values.put(_parameters, lookup_String_value);
    state().leaveLazyAttribute();
    lookup_String_visited.remove(_parameters);
    return lookup_String_value;
  }
/** @apilevel internal */
protected java.util.Set lookup_String_visited;
  /** @apilevel internal */
  private void lookup_String_reset() {
    lookup_String_values = null;
    lookup_String_visited = null;
  }
  /** @apilevel internal */
  protected java.util.Map lookup_String_values;

  /**
   * @attribute inh
   * @aspect paramAdrInd
   * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/paramAdrInd.jrag:30
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.INH)
  @ASTNodeAnnotation.Source(aspect="paramAdrInd", declaredAt="/home/tobias_carlsson/chocopy-tobias/src/jastadd/paramAdrInd.jrag:30")
  public int localIndex() {
    ASTState state = state();
    if (localIndex_computed) {
      return localIndex_value;
    }
    if (localIndex_visited) {
      throw new RuntimeException("Circular definition of attribute func_body.localIndex().");
    }
    localIndex_visited = true;
    state().enterLazyAttribute();
    localIndex_value = getParent().Define_localIndex(this, null);
    localIndex_computed = true;
    state().leaveLazyAttribute();
    localIndex_visited = false;
    return localIndex_value;
  }
/** @apilevel internal */
protected boolean localIndex_visited = false;
  /** @apilevel internal */
  private void localIndex_reset() {
    localIndex_computed = false;
    localIndex_visited = false;
  }
  /** @apilevel internal */
  protected boolean localIndex_computed = false;

  /** @apilevel internal */
  protected int localIndex_value;

  /**
   * @attribute inh
   * @aspect NameAnalysis
   * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/rndFix.jrag:10
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.INH)
  @ASTNodeAnnotation.Source(aspect="NameAnalysis", declaredAt="/home/tobias_carlsson/chocopy-tobias/src/jastadd/rndFix.jrag:10")
  public boolean insideCl() {
    ASTState state = state();
    if (insideCl_computed) {
      return insideCl_value;
    }
    if (insideCl_visited) {
      throw new RuntimeException("Circular definition of attribute func_body.insideCl().");
    }
    insideCl_visited = true;
    state().enterLazyAttribute();
    insideCl_value = getParent().Define_insideCl(this, null);
    insideCl_computed = true;
    state().leaveLazyAttribute();
    insideCl_visited = false;
    return insideCl_value;
  }
/** @apilevel internal */
protected boolean insideCl_visited = false;
  /** @apilevel internal */
  private void insideCl_reset() {
    insideCl_computed = false;
    insideCl_visited = false;
  }
  /** @apilevel internal */
  protected boolean insideCl_computed = false;

  /** @apilevel internal */
  protected boolean insideCl_value;

  /**
   * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/nameanalysis.jrag:207
   * @apilevel internal
   */
  public IdDecl Define_lookup(ASTNode _callerNode, ASTNode _childNode, String name) {
    int childIndex = this.getIndexOfChild(_callerNode);
    return lookUpVar(name);
  }
  /**
   * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/nameanalysis.jrag:207
   * @apilevel internal
   * @return {@code true} if this node has an equation for the inherited attribute lookup
   */
  protected boolean canDefine_lookup(ASTNode _callerNode, ASTNode _childNode, String name) {
    return true;
  }
  /**
   * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/paramAdrInd.jrag:16
   * @apilevel internal
   */
  public boolean Define_isParameter(ASTNode _callerNode, ASTNode _childNode) {
    int childIndex = this.getIndexOfChild(_callerNode);
    return false;
  }
  /**
   * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/paramAdrInd.jrag:16
   * @apilevel internal
   * @return {@code true} if this node has an equation for the inherited attribute isParameter
   */
  protected boolean canDefine_isParameter(ASTNode _callerNode, ASTNode _childNode) {
    return true;
  }
  /**
   * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/paramAdrInd.jrag:64
   * @apilevel internal
   */
  public boolean Define_insideFunc(ASTNode _callerNode, ASTNode _childNode) {
    int childIndex = this.getIndexOfChild(_callerNode);
    return true;
  }
  /**
   * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/paramAdrInd.jrag:64
   * @apilevel internal
   * @return {@code true} if this node has an equation for the inherited attribute insideFunc
   */
  protected boolean canDefine_insideFunc(ASTNode _callerNode, ASTNode _childNode) {
    return true;
  }
  /**
   * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/typeChecker.jrag:85
   * @apilevel internal
   */
  public boolean Define_isDeclOut(ASTNode _callerNode, ASTNode _childNode) {
    if (_callerNode == getoptListListNoTransform()) {
      // @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/typeChecker.jrag:93
      int i = _callerNode.getIndexOfChild(_childNode);
      {
      		if(getoptList(i).assStmt()){
      			return lookup(getoptList(i).usedId())== unknownDecl();
      		}
      		return true;
      	}
    }
    else if (_callerNode == getstmtNoTransform()) {
      // @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/typeChecker.jrag:87
      {
      		if(getstmt().assStmt()){
      			return lookup(getstmt().usedId())== unknownDecl();
      		}
      		return true;
      	}
    }
    else {
      return getParent().Define_isDeclOut(this, _callerNode);
    }
  }
  /**
   * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/typeChecker.jrag:85
   * @apilevel internal
   * @return {@code true} if this node has an equation for the inherited attribute isDeclOut
   */
  protected boolean canDefine_isDeclOut(ASTNode _callerNode, ASTNode _childNode) {
    return true;
  }

}
