/* This file was generated with JastAdd2 (http://jastadd.org) version 2.3.4 */
package lang.ast;
import java.util.*;
import java.io.PrintStream;
import java.util.Set;
import java.util.TreeSet;
import java.util.ArrayList;
import java.io.ByteArrayOutputStream;
import java.lang.reflect.InvocationTargetException;
import java.util.HashSet;
/**
 * @ast node
 * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/lang.ast:30
 * @astdecl retStmt : simpStmt ::= [Expr];
 * @production retStmt : {@link simpStmt} ::= <span class="component">[{@link Expr}]</span>;

 */
public class retStmt extends simpStmt implements Cloneable {
  /**
   * @aspect CodeGen
   * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/CodeGen.jrag:166
   */
  public void genCode(PrintStream out,IdDecl id) {
		if(hasExpr()){
			getExpr().genEval(out,id); // stores result in RAX
		}
		if(id!=null){
			out.println("        jmp " +id.getID()+ enclosingFunction().getIdDecl().getID() + "_ret");
		}
		else{
			out.println("        jmp " + enclosingFunction().getIdDecl().getID() + "_ret");

		}
	}
  /**
   * @declaredat ASTNode:1
   */
  public retStmt() {
    super();
  }
  /**
   * Initializes the child array to the correct size.
   * Initializes List and Opt nta children.
   * @apilevel internal
   * @ast method
   * @declaredat ASTNode:10
   */
  public void init$Children() {
    children = new ASTNode[1];
    setChild(new Opt(), 0);
  }
  /**
   * @declaredat ASTNode:14
   */
  @ASTNodeAnnotation.Constructor(
    name = {"Expr"},
    type = {"Opt<Expr>"},
    kind = {"Opt"}
  )
  public retStmt(Opt<Expr> p0) {
    setChild(p0, 0);
  }
  /** @apilevel low-level 
   * @declaredat ASTNode:23
   */
  protected int numChildren() {
    return 1;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:27
   */
  public void flushAttrCache() {
    super.flushAttrCache();
    current_reset();
    retType_reset();
    enclosingFunction_reset();
    expRet_reset();
  }
  /** @apilevel internal 
   * @declaredat ASTNode:35
   */
  public void flushCollectionCache() {
    super.flushCollectionCache();

  }
  /** @apilevel internal 
   * @declaredat ASTNode:40
   */
  public retStmt clone() throws CloneNotSupportedException {
    retStmt node = (retStmt) super.clone();
    return node;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:45
   */
  public retStmt copy() {
    try {
      retStmt node = (retStmt) clone();
      node.parent = null;
      if (children != null) {
        node.children = (ASTNode[]) children.clone();
      }
      return node;
    } catch (CloneNotSupportedException e) {
      throw new Error("Error: clone not supported for " + getClass().getName());
    }
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @deprecated Please use treeCopy or treeCopyNoTransform instead
   * @declaredat ASTNode:64
   */
  @Deprecated
  public retStmt fullCopy() {
    return treeCopyNoTransform();
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @declaredat ASTNode:74
   */
  public retStmt treeCopyNoTransform() {
    retStmt tree = (retStmt) copy();
    if (children != null) {
      for (int i = 0; i < children.length; ++i) {
        ASTNode child = (ASTNode) children[i];
        if (child != null) {
          child = child.treeCopyNoTransform();
          tree.setChild(child, i);
        }
      }
    }
    return tree;
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The subtree of this node is traversed to trigger rewrites before copy.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @declaredat ASTNode:94
   */
  public retStmt treeCopy() {
    retStmt tree = (retStmt) copy();
    if (children != null) {
      for (int i = 0; i < children.length; ++i) {
        ASTNode child = (ASTNode) getChild(i);
        if (child != null) {
          child = child.treeCopy();
          tree.setChild(child, i);
        }
      }
    }
    return tree;
  }
  /**
   * Replaces the optional node for the Expr child. This is the <code>Opt</code>
   * node containing the child Expr, not the actual child!
   * @param opt The new node to be used as the optional node for the Expr child.
   * @apilevel low-level
   */
  public retStmt setExprOpt(Opt<Expr> opt) {
    setChild(opt, 0);
    return this;
  }
  /**
   * Replaces the (optional) Expr child.
   * @param node The new node to be used as the Expr child.
   * @apilevel high-level
   */
  public retStmt setExpr(Expr node) {
    getExprOpt().setChild(node, 0);
    return this;
  }
  /**
   * Check whether the optional Expr child exists.
   * @return {@code true} if the optional Expr child exists, {@code false} if it does not.
   * @apilevel high-level
   */
  public boolean hasExpr() {
    return getExprOpt().getNumChild() != 0;
  }
  /**
   * Retrieves the (optional) Expr child.
   * @return The Expr child, if it exists. Returns {@code null} otherwise.
   * @apilevel low-level
   */
  public Expr getExpr() {
    return (Expr) getExprOpt().getChild(0);
  }
  /**
   * Retrieves the optional node for the Expr child. This is the <code>Opt</code> node containing the child Expr, not the actual child!
   * @return The optional node for child the Expr child.
   * @apilevel low-level
   */
  @ASTNodeAnnotation.OptChild(name="Expr")
  public Opt<Expr> getExprOpt() {
    return (Opt<Expr>) getChild(0);
  }
  /**
   * Retrieves the optional node for child Expr. This is the <code>Opt</code> node containing the child Expr, not the actual child!
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The optional node for child Expr.
   * @apilevel low-level
   */
  public Opt<Expr> getExprOptNoTransform() {
    return (Opt<Expr>) getChildNoTransform(0);
  }
/** @apilevel internal */
protected boolean current_visited = false;
  /** @apilevel internal */
  private void current_reset() {
    current_computed = false;
    
    current_value = null;
    current_visited = false;
  }
  /** @apilevel internal */
  protected boolean current_computed = false;

  /** @apilevel internal */
  protected Typen current_value;

  /**
   * @attribute syn
   * @aspect NameAnalysis
   * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/nameanalysis.jrag:190
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="NameAnalysis", declaredAt="/home/tobias_carlsson/chocopy-tobias/src/jastadd/nameanalysis.jrag:190")
  public Typen current() {
    ASTState state = state();
    if (current_computed) {
      return current_value;
    }
    if (current_visited) {
      throw new RuntimeException("Circular definition of attribute retStmt.current().");
    }
    current_visited = true;
    state().enterLazyAttribute();
    current_value = current_compute();
    current_computed = true;
    state().leaveLazyAttribute();
    current_visited = false;
    return current_value;
  }
  /** @apilevel internal */
  private Typen current_compute() {
  		if(hasExpr()){
  			return getExpr().types();
  		}
  		return unknownType();
  	}
/** @apilevel internal */
protected boolean retType_visited = false;
  /** @apilevel internal */
  private void retType_reset() {
    retType_computed = false;
    
    retType_value = null;
    retType_visited = false;
  }
  /** @apilevel internal */
  protected boolean retType_computed = false;

  /** @apilevel internal */
  protected Typen retType_value;

  /**
   * @attribute syn
   * @aspect typeChecker
   * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/typeChecker.jrag:12
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="typeChecker", declaredAt="/home/tobias_carlsson/chocopy-tobias/src/jastadd/typeChecker.jrag:12")
  public Typen retType() {
    ASTState state = state();
    if (retType_computed) {
      return retType_value;
    }
    if (retType_visited) {
      throw new RuntimeException("Circular definition of attribute retStmt.retType().");
    }
    retType_visited = true;
    state().enterLazyAttribute();
    retType_value = retType_compute();
    retType_computed = true;
    state().leaveLazyAttribute();
    retType_visited = false;
    return retType_value;
  }
  /** @apilevel internal */
  private Typen retType_compute() {
  		if(hasExpr()){
  			return getExpr().types();
  		}
  		return unknownType();
  	}
  /**
   * @attribute inh
   * @aspect CallGraph
   * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/CallGraph.jrag:7
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.INH)
  @ASTNodeAnnotation.Source(aspect="CallGraph", declaredAt="/home/tobias_carlsson/chocopy-tobias/src/jastadd/CallGraph.jrag:7")
  public func_def enclosingFunction() {
    ASTState state = state();
    if (enclosingFunction_computed) {
      return enclosingFunction_value;
    }
    if (enclosingFunction_visited) {
      throw new RuntimeException("Circular definition of attribute retStmt.enclosingFunction().");
    }
    enclosingFunction_visited = true;
    state().enterLazyAttribute();
    enclosingFunction_value = getParent().Define_enclosingFunction(this, null);
    enclosingFunction_computed = true;
    state().leaveLazyAttribute();
    enclosingFunction_visited = false;
    return enclosingFunction_value;
  }
/** @apilevel internal */
protected boolean enclosingFunction_visited = false;
  /** @apilevel internal */
  private void enclosingFunction_reset() {
    enclosingFunction_computed = false;
    
    enclosingFunction_value = null;
    enclosingFunction_visited = false;
  }
  /** @apilevel internal */
  protected boolean enclosingFunction_computed = false;

  /** @apilevel internal */
  protected func_def enclosingFunction_value;

  /**
   * @attribute inh
   * @aspect typeChecker
   * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/typeChecker.jrag:11
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.INH)
  @ASTNodeAnnotation.Source(aspect="typeChecker", declaredAt="/home/tobias_carlsson/chocopy-tobias/src/jastadd/typeChecker.jrag:11")
  public Typen expRet() {
    ASTState state = state();
    if (expRet_computed) {
      return expRet_value;
    }
    if (expRet_visited) {
      throw new RuntimeException("Circular definition of attribute retStmt.expRet().");
    }
    expRet_visited = true;
    state().enterLazyAttribute();
    expRet_value = getParent().Define_expRet(this, null);
    expRet_computed = true;
    state().leaveLazyAttribute();
    expRet_visited = false;
    return expRet_value;
  }
/** @apilevel internal */
protected boolean expRet_visited = false;
  /** @apilevel internal */
  private void expRet_reset() {
    expRet_computed = false;
    
    expRet_value = null;
    expRet_visited = false;
  }
  /** @apilevel internal */
  protected boolean expRet_computed = false;

  /** @apilevel internal */
  protected Typen expRet_value;

  /** @apilevel internal */
  protected void collect_contributors_Program_errors(Program _root, java.util.Map<ASTNode, java.util.Set<ASTNode>> _map) {
    // @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/Errors.jrag:128
    if (!expRet().compatibleType(retType())) {
      {
        Program target = (Program) (program());
        java.util.Set<ASTNode> contributors = _map.get(target);
        if (contributors == null) {
          contributors = new java.util.LinkedHashSet<ASTNode>();
          _map.put((ASTNode) target, contributors);
        }
        contributors.add(this);
      }
    }
    super.collect_contributors_Program_errors(_root, _map);
  }
  /** @apilevel internal */
  protected void contributeTo_Program_errors(Set<ErrorMessage> collection) {
    super.contributeTo_Program_errors(collection);
    if (!expRet().compatibleType(retType())) {
      collection.add(error("Expected " + expRet() + " but received " + retType()));
    }
  }

}
