/* This file was generated with JastAdd2 (http://jastadd.org) version 2.3.4 */
package lang.ast;
import java.util.*;
import java.io.PrintStream;
import java.util.Set;
import java.util.TreeSet;
import java.util.ArrayList;
import java.io.ByteArrayOutputStream;
import java.lang.reflect.InvocationTargetException;
import java.util.HashSet;
/**
 * @ast node
 * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/lang.ast:24
 * @astdecl ifStmt : stmt ::= if:cexpr ifblock:Block elseif:exprBlock* [optExprBlock];
 * @production ifStmt : {@link stmt} ::= <span class="component">if:{@link cexpr}</span> <span class="component">ifblock:{@link Block}</span> <span class="component">elseif:{@link exprBlock}*</span> <span class="component">[{@link optExprBlock}]</span>;

 */
public class ifStmt extends stmt implements Cloneable {
  /**
   * @aspect CodeGen
   * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/CodeGen.jrag:413
   */
  public void genCode(PrintStream out, IdDecl id) {

		if(id!=null){
			int count =1;
		if(haselseif()){
			getif().genConditionalJump(out,id, id.getID()+ globalStmtId() + count + "elseif" );
		}
		else if(hasoptExprBlock()){
			getif().genConditionalJump(out,id,id.getID()+ globalStmtId() + "else");
		}
		else{
			getif().genConditionalJump(out,id,id.getID()+ globalStmtId() + "end_if");
		}
		getifblock().genCode(out,id);
		out.println("        jmp " + id.getID()+globalStmtId() + "end_if");
		for (exprBlock eb :  getelseifList() ) {
			out.println(id.getID()+globalStmtId() + count + "elseif:");
			if(count == getNumelseif()){
				if(hasoptExprBlock()){
					eb.getcexpr().genConditionalJump(out,id, id.getID()+globalStmtId() + "else");
				}
				else{
					eb.getcexpr().genConditionalJump(out,id, id.getID()+globalStmtId() + "end_if");
				}
			}else{
				eb.getcexpr().genConditionalJump(out,id, id.getID()+globalStmtId() + (count+1) + "elseif" );
			}
			eb.genCode(out,id);
			out.println("        jmp " + id.getID()+globalStmtId() + "end_if");
			count=count+1;
		}
		if(hasoptExprBlock()){
			out.println(id.getID()+globalStmtId() + "else:");
			getoptExprBlock().genCode(out,id);
			out.println("        jmp " + id.getID()+globalStmtId() + "end_if");
		}
		out.println(id.getID()+globalStmtId() + "end_if:");
		}






		
		else{
		int count =1;
		if(haselseif()){
			getif().genConditionalJump(out,id, globalStmtId() + count + "elseif" );
		}
		else if(hasoptExprBlock()){
			getif().genConditionalJump(out,id, globalStmtId() + "else");
		}
		else{
			getif().genConditionalJump(out,id, globalStmtId() + "end_if");
		}
		getifblock().genCode(out,id);
		out.println("        jmp " + globalStmtId() + "end_if");
		for (exprBlock eb :  getelseifList() ) {
			out.println(globalStmtId() + count + "elseif:");
			if(count == getNumelseif()){
				if(hasoptExprBlock()){
					eb.getcexpr().genConditionalJump(out,id, globalStmtId() + "else");
				}
				else{
					eb.getcexpr().genConditionalJump(out,id, globalStmtId() + "end_if");
				}
			}else{
				eb.getcexpr().genConditionalJump(out,id, globalStmtId() + (count+1) + "elseif" );
			}
			eb.genCode(out,id);
			out.println("        jmp " + globalStmtId() + "end_if");
			count=count+1;
		}
		if(hasoptExprBlock()){
			out.println(globalStmtId() + "else:");
			getoptExprBlock().genCode(out,id);
			out.println("        jmp " + globalStmtId() + "end_if");
		}
		out.println(globalStmtId() + "end_if:");
		}
	}
  /**
   * @declaredat ASTNode:1
   */
  public ifStmt() {
    super();
  }
  /**
   * Initializes the child array to the correct size.
   * Initializes List and Opt nta children.
   * @apilevel internal
   * @ast method
   * @declaredat ASTNode:10
   */
  public void init$Children() {
    children = new ASTNode[4];
    setChild(new List(), 2);
    setChild(new Opt(), 3);
  }
  /**
   * @declaredat ASTNode:15
   */
  @ASTNodeAnnotation.Constructor(
    name = {"if", "ifblock", "elseif", "optExprBlock"},
    type = {"cexpr", "Block", "List<exprBlock>", "Opt<optExprBlock>"},
    kind = {"Child", "Child", "List", "Opt"}
  )
  public ifStmt(cexpr p0, Block p1, List<exprBlock> p2, Opt<optExprBlock> p3) {
    setChild(p0, 0);
    setChild(p1, 1);
    setChild(p2, 2);
    setChild(p3, 3);
  }
  /** @apilevel low-level 
   * @declaredat ASTNode:27
   */
  protected int numChildren() {
    return 4;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:31
   */
  public void flushAttrCache() {
    super.flushAttrCache();
    globalStmtId_reset();
    types_reset();
    expectedType_reset();
  }
  /** @apilevel internal 
   * @declaredat ASTNode:38
   */
  public void flushCollectionCache() {
    super.flushCollectionCache();

  }
  /** @apilevel internal 
   * @declaredat ASTNode:43
   */
  public ifStmt clone() throws CloneNotSupportedException {
    ifStmt node = (ifStmt) super.clone();
    return node;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:48
   */
  public ifStmt copy() {
    try {
      ifStmt node = (ifStmt) clone();
      node.parent = null;
      if (children != null) {
        node.children = (ASTNode[]) children.clone();
      }
      return node;
    } catch (CloneNotSupportedException e) {
      throw new Error("Error: clone not supported for " + getClass().getName());
    }
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @deprecated Please use treeCopy or treeCopyNoTransform instead
   * @declaredat ASTNode:67
   */
  @Deprecated
  public ifStmt fullCopy() {
    return treeCopyNoTransform();
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @declaredat ASTNode:77
   */
  public ifStmt treeCopyNoTransform() {
    ifStmt tree = (ifStmt) copy();
    if (children != null) {
      for (int i = 0; i < children.length; ++i) {
        ASTNode child = (ASTNode) children[i];
        if (child != null) {
          child = child.treeCopyNoTransform();
          tree.setChild(child, i);
        }
      }
    }
    return tree;
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The subtree of this node is traversed to trigger rewrites before copy.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @declaredat ASTNode:97
   */
  public ifStmt treeCopy() {
    ifStmt tree = (ifStmt) copy();
    if (children != null) {
      for (int i = 0; i < children.length; ++i) {
        ASTNode child = (ASTNode) getChild(i);
        if (child != null) {
          child = child.treeCopy();
          tree.setChild(child, i);
        }
      }
    }
    return tree;
  }
  /**
   * Replaces the if child.
   * @param node The new node to replace the if child.
   * @apilevel high-level
   */
  public ifStmt setif(cexpr node) {
    setChild(node, 0);
    return this;
  }
  /**
   * Retrieves the if child.
   * @return The current node used as the if child.
   * @apilevel high-level
   */
  @ASTNodeAnnotation.Child(name="if")
  public cexpr getif() {
    return (cexpr) getChild(0);
  }
  /**
   * Retrieves the if child.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The current node used as the if child.
   * @apilevel low-level
   */
  public cexpr getifNoTransform() {
    return (cexpr) getChildNoTransform(0);
  }
  /**
   * Replaces the ifblock child.
   * @param node The new node to replace the ifblock child.
   * @apilevel high-level
   */
  public ifStmt setifblock(Block node) {
    setChild(node, 1);
    return this;
  }
  /**
   * Retrieves the ifblock child.
   * @return The current node used as the ifblock child.
   * @apilevel high-level
   */
  @ASTNodeAnnotation.Child(name="ifblock")
  public Block getifblock() {
    return (Block) getChild(1);
  }
  /**
   * Retrieves the ifblock child.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The current node used as the ifblock child.
   * @apilevel low-level
   */
  public Block getifblockNoTransform() {
    return (Block) getChildNoTransform(1);
  }
  /**
   * Replaces the elseif list.
   * @param list The new list node to be used as the elseif list.
   * @apilevel high-level
   */
  public ifStmt setelseifList(List<exprBlock> list) {
    setChild(list, 2);
    return this;
  }
  /**
   * Retrieves the number of children in the elseif list.
   * @return Number of children in the elseif list.
   * @apilevel high-level
   */
  public int getNumelseif() {
    return getelseifList().getNumChild();
  }
  /**
   * Retrieves the number of children in the elseif list.
   * Calling this method will not trigger rewrites.
   * @return Number of children in the elseif list.
   * @apilevel low-level
   */
  public int getNumelseifNoTransform() {
    return getelseifListNoTransform().getNumChildNoTransform();
  }
  /**
   * Retrieves the element at index {@code i} in the elseif list.
   * @param i Index of the element to return.
   * @return The element at position {@code i} in the elseif list.
   * @apilevel high-level
   */
  public exprBlock getelseif(int i) {
    return (exprBlock) getelseifList().getChild(i);
  }
  /**
   * Check whether the elseif list has any children.
   * @return {@code true} if it has at least one child, {@code false} otherwise.
   * @apilevel high-level
   */
  public boolean haselseif() {
    return getelseifList().getNumChild() != 0;
  }
  /**
   * Append an element to the elseif list.
   * @param node The element to append to the elseif list.
   * @apilevel high-level
   */
  public ifStmt addelseif(exprBlock node) {
    List<exprBlock> list = (parent == null) ? getelseifListNoTransform() : getelseifList();
    list.addChild(node);
    return this;
  }
  /** @apilevel low-level 
   */
  public ifStmt addelseifNoTransform(exprBlock node) {
    List<exprBlock> list = getelseifListNoTransform();
    list.addChild(node);
    return this;
  }
  /**
   * Replaces the elseif list element at index {@code i} with the new node {@code node}.
   * @param node The new node to replace the old list element.
   * @param i The list index of the node to be replaced.
   * @apilevel high-level
   */
  public ifStmt setelseif(exprBlock node, int i) {
    List<exprBlock> list = getelseifList();
    list.setChild(node, i);
    return this;
  }
  /**
   * Retrieves the elseif list.
   * @return The node representing the elseif list.
   * @apilevel high-level
   */
  @ASTNodeAnnotation.ListChild(name="elseif")
  public List<exprBlock> getelseifList() {
    List<exprBlock> list = (List<exprBlock>) getChild(2);
    return list;
  }
  /**
   * Retrieves the elseif list.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The node representing the elseif list.
   * @apilevel low-level
   */
  public List<exprBlock> getelseifListNoTransform() {
    return (List<exprBlock>) getChildNoTransform(2);
  }
  /**
   * @return the element at index {@code i} in the elseif list without
   * triggering rewrites.
   */
  public exprBlock getelseifNoTransform(int i) {
    return (exprBlock) getelseifListNoTransform().getChildNoTransform(i);
  }
  /**
   * Retrieves the elseif list.
   * @return The node representing the elseif list.
   * @apilevel high-level
   */
  public List<exprBlock> getelseifs() {
    return getelseifList();
  }
  /**
   * Retrieves the elseif list.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The node representing the elseif list.
   * @apilevel low-level
   */
  public List<exprBlock> getelseifsNoTransform() {
    return getelseifListNoTransform();
  }
  /**
   * Replaces the optional node for the optExprBlock child. This is the <code>Opt</code>
   * node containing the child optExprBlock, not the actual child!
   * @param opt The new node to be used as the optional node for the optExprBlock child.
   * @apilevel low-level
   */
  public ifStmt setoptExprBlockOpt(Opt<optExprBlock> opt) {
    setChild(opt, 3);
    return this;
  }
  /**
   * Replaces the (optional) optExprBlock child.
   * @param node The new node to be used as the optExprBlock child.
   * @apilevel high-level
   */
  public ifStmt setoptExprBlock(optExprBlock node) {
    getoptExprBlockOpt().setChild(node, 0);
    return this;
  }
  /**
   * Check whether the optional optExprBlock child exists.
   * @return {@code true} if the optional optExprBlock child exists, {@code false} if it does not.
   * @apilevel high-level
   */
  public boolean hasoptExprBlock() {
    return getoptExprBlockOpt().getNumChild() != 0;
  }
  /**
   * Retrieves the (optional) optExprBlock child.
   * @return The optExprBlock child, if it exists. Returns {@code null} otherwise.
   * @apilevel low-level
   */
  public optExprBlock getoptExprBlock() {
    return (optExprBlock) getoptExprBlockOpt().getChild(0);
  }
  /**
   * Retrieves the optional node for the optExprBlock child. This is the <code>Opt</code> node containing the child optExprBlock, not the actual child!
   * @return The optional node for child the optExprBlock child.
   * @apilevel low-level
   */
  @ASTNodeAnnotation.OptChild(name="optExprBlock")
  public Opt<optExprBlock> getoptExprBlockOpt() {
    return (Opt<optExprBlock>) getChild(3);
  }
  /**
   * Retrieves the optional node for child optExprBlock. This is the <code>Opt</code> node containing the child optExprBlock, not the actual child!
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The optional node for child optExprBlock.
   * @apilevel low-level
   */
  public Opt<optExprBlock> getoptExprBlockOptNoTransform() {
    return (Opt<optExprBlock>) getChildNoTransform(3);
  }
/** @apilevel internal */
protected boolean globalStmtId_visited = false;
  /** @apilevel internal */
  private void globalStmtId_reset() {
    globalStmtId_computed = false;
    
    globalStmtId_value = null;
    globalStmtId_visited = false;
  }
  /** @apilevel internal */
  protected boolean globalStmtId_computed = false;

  /** @apilevel internal */
  protected String globalStmtId_value;

  /**
   * @attribute syn
   * @aspect UniqueName
   * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/genUniqueName.jrag:13
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="UniqueName", declaredAt="/home/tobias_carlsson/chocopy-tobias/src/jastadd/genUniqueName.jrag:13")
  public String globalStmtId() {
    ASTState state = state();
    if (globalStmtId_computed) {
      return globalStmtId_value;
    }
    if (globalStmtId_visited) {
      throw new RuntimeException("Circular definition of attribute ifStmt.globalStmtId().");
    }
    globalStmtId_visited = true;
    state().enterLazyAttribute();
    globalStmtId_value = "ifstmt" +'_' + stmtId();
    globalStmtId_computed = true;
    state().leaveLazyAttribute();
    globalStmtId_visited = false;
    return globalStmtId_value;
  }
/** @apilevel internal */
protected boolean types_visited = false;
  /** @apilevel internal */
  private void types_reset() {
    types_computed = false;
    
    types_value = null;
    types_visited = false;
  }
  /** @apilevel internal */
  protected boolean types_computed = false;

  /** @apilevel internal */
  protected Typen types_value;

  /**
   * @attribute syn
   * @aspect typeChecker
   * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/typeChecker.jrag:56
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="typeChecker", declaredAt="/home/tobias_carlsson/chocopy-tobias/src/jastadd/typeChecker.jrag:56")
  public Typen types() {
    ASTState state = state();
    if (types_computed) {
      return types_value;
    }
    if (types_visited) {
      throw new RuntimeException("Circular definition of attribute ifStmt.types().");
    }
    types_visited = true;
    state().enterLazyAttribute();
    types_value = getif().types();
    types_computed = true;
    state().leaveLazyAttribute();
    types_visited = false;
    return types_value;
  }
/** @apilevel internal */
protected boolean expectedType_visited = false;
  /** @apilevel internal */
  private void expectedType_reset() {
    expectedType_computed = false;
    
    expectedType_value = null;
    expectedType_visited = false;
  }
  /** @apilevel internal */
  protected boolean expectedType_computed = false;

  /** @apilevel internal */
  protected Typen expectedType_value;

  /**
   * @attribute syn
   * @aspect typeChecker
   * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/typeChecker.jrag:82
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="typeChecker", declaredAt="/home/tobias_carlsson/chocopy-tobias/src/jastadd/typeChecker.jrag:82")
  public Typen expectedType() {
    ASTState state = state();
    if (expectedType_computed) {
      return expectedType_value;
    }
    if (expectedType_visited) {
      throw new RuntimeException("Circular definition of attribute ifStmt.expectedType().");
    }
    expectedType_visited = true;
    state().enterLazyAttribute();
    expectedType_value = boolType();
    expectedType_computed = true;
    state().leaveLazyAttribute();
    expectedType_visited = false;
    return expectedType_value;
  }
  /** @apilevel internal */
  protected void collect_contributors_Program_errors(Program _root, java.util.Map<ASTNode, java.util.Set<ASTNode>> _map) {
    // @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/Errors.jrag:97
    if (!types().compatibleType(expectedType())) {
      {
        Program target = (Program) (program());
        java.util.Set<ASTNode> contributors = _map.get(target);
        if (contributors == null) {
          contributors = new java.util.LinkedHashSet<ASTNode>();
          _map.put((ASTNode) target, contributors);
        }
        contributors.add(this);
      }
    }
    super.collect_contributors_Program_errors(_root, _map);
  }
  /** @apilevel internal */
  protected void contributeTo_Program_errors(Set<ErrorMessage> collection) {
    super.contributeTo_Program_errors(collection);
    if (!types().compatibleType(expectedType())) {
      collection.add(error("Expected: '" +expectedType() + "' but recieved: '"+ types()+"'"));
    }
  }

}
