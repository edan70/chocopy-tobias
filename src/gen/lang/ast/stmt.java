/* This file was generated with JastAdd2 (http://jastadd.org) version 2.3.4 */
package lang.ast;
import java.util.*;
import java.io.PrintStream;
import java.util.Set;
import java.util.TreeSet;
import java.util.ArrayList;
import java.io.ByteArrayOutputStream;
import java.lang.reflect.InvocationTargetException;
import java.util.HashSet;
/**
 * @ast node
 * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/lang.ast:21
 * @astdecl stmt : ASTNode;
 * @production stmt : {@link ASTNode};

 */
public abstract class stmt extends ASTNode<ASTNode> implements Cloneable {
  /**
   * @aspect CodeGen
   * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/CodeGen.jrag:164
   */
  abstract public void genCode(PrintStream out,IdDecl id);
  /**
   * @declaredat ASTNode:1
   */
  public stmt() {
    super();
  }
  /**
   * Initializes the child array to the correct size.
   * Initializes List and Opt nta children.
   * @apilevel internal
   * @ast method
   * @declaredat ASTNode:10
   */
  public void init$Children() {
  }
  /** @apilevel low-level 
   * @declaredat ASTNode:13
   */
  protected int numChildren() {
    return 0;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:17
   */
  public void flushAttrCache() {
    super.flushAttrCache();
    globalStmtId_reset();
    lookup_String_reset();
    enclosingFunction_reset();
    stmtId_reset();
    globalLookup_String_reset();
    assStmt_reset();
    usedId_reset();
  }
  /** @apilevel internal 
   * @declaredat ASTNode:28
   */
  public void flushCollectionCache() {
    super.flushCollectionCache();

  }
  /** @apilevel internal 
   * @declaredat ASTNode:33
   */
  public stmt clone() throws CloneNotSupportedException {
    stmt node = (stmt) super.clone();
    return node;
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @deprecated Please use treeCopy or treeCopyNoTransform instead
   * @declaredat ASTNode:44
   */
  @Deprecated
  public abstract stmt fullCopy();
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @declaredat ASTNode:52
   */
  public abstract stmt treeCopyNoTransform();
  /**
   * Create a deep copy of the AST subtree at this node.
   * The subtree of this node is traversed to trigger rewrites before copy.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @declaredat ASTNode:60
   */
  public abstract stmt treeCopy();
/** @apilevel internal */
protected boolean globalStmtId_visited = false;
  /** @apilevel internal */
  private void globalStmtId_reset() {
    globalStmtId_computed = false;
    
    globalStmtId_value = null;
    globalStmtId_visited = false;
  }
  /** @apilevel internal */
  protected boolean globalStmtId_computed = false;

  /** @apilevel internal */
  protected String globalStmtId_value;

  /**
   * @attribute syn
   * @aspect UniqueName
   * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/genUniqueName.jrag:14
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="UniqueName", declaredAt="/home/tobias_carlsson/chocopy-tobias/src/jastadd/genUniqueName.jrag:14")
  public String globalStmtId() {
    ASTState state = state();
    if (globalStmtId_computed) {
      return globalStmtId_value;
    }
    if (globalStmtId_visited) {
      throw new RuntimeException("Circular definition of attribute stmt.globalStmtId().");
    }
    globalStmtId_visited = true;
    state().enterLazyAttribute();
    globalStmtId_value = '_' + stmtId();
    globalStmtId_computed = true;
    state().leaveLazyAttribute();
    globalStmtId_visited = false;
    return globalStmtId_value;
  }
/** @apilevel internal */
protected java.util.Set lookup_String_visited;
  /** @apilevel internal */
  private void lookup_String_reset() {
    lookup_String_values = null;
    lookup_String_visited = null;
  }
  /** @apilevel internal */
  protected java.util.Map lookup_String_values;

  /**
   * @attribute syn
   * @aspect NameAnalysis
   * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/nameanalysis.jrag:244
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="NameAnalysis", declaredAt="/home/tobias_carlsson/chocopy-tobias/src/jastadd/nameanalysis.jrag:197")
  public IdDecl lookup(String name) {
    Object _parameters = name;
    if (lookup_String_visited == null) lookup_String_visited = new java.util.HashSet(4);
    if (lookup_String_values == null) lookup_String_values = new java.util.HashMap(4);
    ASTState state = state();
    if (lookup_String_values.containsKey(_parameters)) {
      return (IdDecl) lookup_String_values.get(_parameters);
    }
    if (lookup_String_visited.contains(_parameters)) {
      throw new RuntimeException("Circular definition of attribute stmt.lookup(String).");
    }
    lookup_String_visited.add(_parameters);
    state().enterLazyAttribute();
    IdDecl lookup_String_value = globalLookup(name);
    lookup_String_values.put(_parameters, lookup_String_value);
    state().leaveLazyAttribute();
    lookup_String_visited.remove(_parameters);
    return lookup_String_value;
  }
  /**
   * @attribute inh
   * @aspect CallGraph
   * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/CallGraph.jrag:6
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.INH)
  @ASTNodeAnnotation.Source(aspect="CallGraph", declaredAt="/home/tobias_carlsson/chocopy-tobias/src/jastadd/CallGraph.jrag:6")
  public func_def enclosingFunction() {
    ASTState state = state();
    if (enclosingFunction_computed) {
      return enclosingFunction_value;
    }
    if (enclosingFunction_visited) {
      throw new RuntimeException("Circular definition of attribute stmt.enclosingFunction().");
    }
    enclosingFunction_visited = true;
    state().enterLazyAttribute();
    enclosingFunction_value = getParent().Define_enclosingFunction(this, null);
    enclosingFunction_computed = true;
    state().leaveLazyAttribute();
    enclosingFunction_visited = false;
    return enclosingFunction_value;
  }
/** @apilevel internal */
protected boolean enclosingFunction_visited = false;
  /** @apilevel internal */
  private void enclosingFunction_reset() {
    enclosingFunction_computed = false;
    
    enclosingFunction_value = null;
    enclosingFunction_visited = false;
  }
  /** @apilevel internal */
  protected boolean enclosingFunction_computed = false;

  /** @apilevel internal */
  protected func_def enclosingFunction_value;

  /**
   * @attribute inh
   * @aspect UniqueName
   * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/genUniqueName.jrag:3
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.INH)
  @ASTNodeAnnotation.Source(aspect="UniqueName", declaredAt="/home/tobias_carlsson/chocopy-tobias/src/jastadd/genUniqueName.jrag:3")
  public String stmtId() {
    ASTState state = state();
    if (stmtId_computed) {
      return stmtId_value;
    }
    if (stmtId_visited) {
      throw new RuntimeException("Circular definition of attribute stmt.stmtId().");
    }
    stmtId_visited = true;
    state().enterLazyAttribute();
    stmtId_value = getParent().Define_stmtId(this, null);
    stmtId_computed = true;
    state().leaveLazyAttribute();
    stmtId_visited = false;
    return stmtId_value;
  }
/** @apilevel internal */
protected boolean stmtId_visited = false;
  /** @apilevel internal */
  private void stmtId_reset() {
    stmtId_computed = false;
    
    stmtId_value = null;
    stmtId_visited = false;
  }
  /** @apilevel internal */
  protected boolean stmtId_computed = false;

  /** @apilevel internal */
  protected String stmtId_value;

  /**
   * @attribute inh
   * @aspect NameAnalysis
   * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/nameanalysis.jrag:337
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.INH)
  @ASTNodeAnnotation.Source(aspect="NameAnalysis", declaredAt="/home/tobias_carlsson/chocopy-tobias/src/jastadd/nameanalysis.jrag:337")
  public IdDecl globalLookup(String name) {
    Object _parameters = name;
    if (globalLookup_String_visited == null) globalLookup_String_visited = new java.util.HashSet(4);
    if (globalLookup_String_values == null) globalLookup_String_values = new java.util.HashMap(4);
    ASTState state = state();
    if (globalLookup_String_values.containsKey(_parameters)) {
      return (IdDecl) globalLookup_String_values.get(_parameters);
    }
    if (globalLookup_String_visited.contains(_parameters)) {
      throw new RuntimeException("Circular definition of attribute stmt.globalLookup(String).");
    }
    globalLookup_String_visited.add(_parameters);
    state().enterLazyAttribute();
    IdDecl globalLookup_String_value = getParent().Define_globalLookup(this, null, name);
    globalLookup_String_values.put(_parameters, globalLookup_String_value);
    state().leaveLazyAttribute();
    globalLookup_String_visited.remove(_parameters);
    return globalLookup_String_value;
  }
/** @apilevel internal */
protected java.util.Set globalLookup_String_visited;
  /** @apilevel internal */
  private void globalLookup_String_reset() {
    globalLookup_String_values = null;
    globalLookup_String_visited = null;
  }
  /** @apilevel internal */
  protected java.util.Map globalLookup_String_values;

  /**
   * @attribute inh
   * @aspect typeChecker
   * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/typeChecker.jrag:102
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.INH)
  @ASTNodeAnnotation.Source(aspect="typeChecker", declaredAt="/home/tobias_carlsson/chocopy-tobias/src/jastadd/typeChecker.jrag:102")
  public boolean assStmt() {
    ASTState state = state();
    if (assStmt_computed) {
      return assStmt_value;
    }
    if (assStmt_visited) {
      throw new RuntimeException("Circular definition of attribute stmt.assStmt().");
    }
    assStmt_visited = true;
    state().enterLazyAttribute();
    assStmt_value = getParent().Define_assStmt(this, null);
    assStmt_computed = true;
    state().leaveLazyAttribute();
    assStmt_visited = false;
    return assStmt_value;
  }
/** @apilevel internal */
protected boolean assStmt_visited = false;
  /** @apilevel internal */
  private void assStmt_reset() {
    assStmt_computed = false;
    assStmt_visited = false;
  }
  /** @apilevel internal */
  protected boolean assStmt_computed = false;

  /** @apilevel internal */
  protected boolean assStmt_value;

  /**
   * @attribute inh
   * @aspect typeChecker
   * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/typeChecker.jrag:106
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.INH)
  @ASTNodeAnnotation.Source(aspect="typeChecker", declaredAt="/home/tobias_carlsson/chocopy-tobias/src/jastadd/typeChecker.jrag:106")
  public String usedId() {
    ASTState state = state();
    if (usedId_computed) {
      return usedId_value;
    }
    if (usedId_visited) {
      throw new RuntimeException("Circular definition of attribute stmt.usedId().");
    }
    usedId_visited = true;
    state().enterLazyAttribute();
    usedId_value = getParent().Define_usedId(this, null);
    usedId_computed = true;
    state().leaveLazyAttribute();
    usedId_visited = false;
    return usedId_value;
  }
/** @apilevel internal */
protected boolean usedId_visited = false;
  /** @apilevel internal */
  private void usedId_reset() {
    usedId_computed = false;
    
    usedId_value = null;
    usedId_visited = false;
  }
  /** @apilevel internal */
  protected boolean usedId_computed = false;

  /** @apilevel internal */
  protected String usedId_value;

  /**
   * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/genUniqueName.jrag:4
   * @apilevel internal
   */
  public String Define_stmtId(ASTNode _callerNode, ASTNode _childNode) {
    int childIndex = this.getIndexOfChild(_callerNode);
    return stmtId();
  }
  /**
   * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/genUniqueName.jrag:4
   * @apilevel internal
   * @return {@code true} if this node has an equation for the inherited attribute stmtId
   */
  protected boolean canDefine_stmtId(ASTNode _callerNode, ASTNode _childNode) {
    return true;
  }

}
