/* This file was generated with JastAdd2 (http://jastadd.org) version 2.3.4 */
package lang.ast;
import java.util.*;
import java.io.PrintStream;
import java.util.Set;
import java.util.TreeSet;
import java.util.ArrayList;
import java.io.ByteArrayOutputStream;
import java.lang.reflect.InvocationTargetException;
import java.util.HashSet;
/**
 * @ast node
 * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/lang.ast:50
 * @astdecl numsD : idOrInt ::= <NUMERAL:String>;
 * @production numsD : {@link idOrInt} ::= <span class="component">&lt;NUMERAL:{@link String}&gt;</span>;

 */
public class numsD extends idOrInt implements Cloneable {
  /**
   * @aspect CodeGen
   * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/CodeGen.jrag:360
   */
  public void genEval(PrintStream out, IdDecl id) {
		out.println("        movq $" + getNUMERAL() + ", %rax");
	}
  /**
   * @declaredat ASTNode:1
   */
  public numsD() {
    super();
  }
  /**
   * Initializes the child array to the correct size.
   * Initializes List and Opt nta children.
   * @apilevel internal
   * @ast method
   * @declaredat ASTNode:10
   */
  public void init$Children() {
  }
  /**
   * @declaredat ASTNode:12
   */
  @ASTNodeAnnotation.Constructor(
    name = {"NUMERAL"},
    type = {"String"},
    kind = {"Token"}
  )
  public numsD(String p0) {
    setNUMERAL(p0);
  }
  /**
   * @declaredat ASTNode:20
   */
  public numsD(beaver.Symbol p0) {
    setNUMERAL(p0);
  }
  /** @apilevel low-level 
   * @declaredat ASTNode:24
   */
  protected int numChildren() {
    return 0;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:28
   */
  public void flushAttrCache() {
    super.flushAttrCache();
    types_reset();
  }
  /** @apilevel internal 
   * @declaredat ASTNode:33
   */
  public void flushCollectionCache() {
    super.flushCollectionCache();

  }
  /** @apilevel internal 
   * @declaredat ASTNode:38
   */
  public numsD clone() throws CloneNotSupportedException {
    numsD node = (numsD) super.clone();
    return node;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:43
   */
  public numsD copy() {
    try {
      numsD node = (numsD) clone();
      node.parent = null;
      if (children != null) {
        node.children = (ASTNode[]) children.clone();
      }
      return node;
    } catch (CloneNotSupportedException e) {
      throw new Error("Error: clone not supported for " + getClass().getName());
    }
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @deprecated Please use treeCopy or treeCopyNoTransform instead
   * @declaredat ASTNode:62
   */
  @Deprecated
  public numsD fullCopy() {
    return treeCopyNoTransform();
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @declaredat ASTNode:72
   */
  public numsD treeCopyNoTransform() {
    numsD tree = (numsD) copy();
    if (children != null) {
      for (int i = 0; i < children.length; ++i) {
        ASTNode child = (ASTNode) children[i];
        if (child != null) {
          child = child.treeCopyNoTransform();
          tree.setChild(child, i);
        }
      }
    }
    return tree;
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The subtree of this node is traversed to trigger rewrites before copy.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @declaredat ASTNode:92
   */
  public numsD treeCopy() {
    numsD tree = (numsD) copy();
    if (children != null) {
      for (int i = 0; i < children.length; ++i) {
        ASTNode child = (ASTNode) getChild(i);
        if (child != null) {
          child = child.treeCopy();
          tree.setChild(child, i);
        }
      }
    }
    return tree;
  }
  /**
   * Replaces the lexeme NUMERAL.
   * @param value The new value for the lexeme NUMERAL.
   * @apilevel high-level
   */
  public numsD setNUMERAL(String value) {
    tokenString_NUMERAL = value;
    return this;
  }
  /** @apilevel internal 
   */
  protected String tokenString_NUMERAL;
  /**
   */
  public int NUMERALstart;
  /**
   */
  public int NUMERALend;
  /**
   * JastAdd-internal setter for lexeme NUMERAL using the Beaver parser.
   * @param symbol Symbol containing the new value for the lexeme NUMERAL
   * @apilevel internal
   */
  public numsD setNUMERAL(beaver.Symbol symbol) {
    if (symbol.value != null && !(symbol.value instanceof String))
    throw new UnsupportedOperationException("setNUMERAL is only valid for String lexemes");
    tokenString_NUMERAL = (String)symbol.value;
    NUMERALstart = symbol.getStart();
    NUMERALend = symbol.getEnd();
    return this;
  }
  /**
   * Retrieves the value for the lexeme NUMERAL.
   * @return The value for the lexeme NUMERAL.
   * @apilevel high-level
   */
  @ASTNodeAnnotation.Token(name="NUMERAL")
  public String getNUMERAL() {
    return tokenString_NUMERAL != null ? tokenString_NUMERAL : "";
  }
/** @apilevel internal */
protected boolean types_visited = false;
  /** @apilevel internal */
  private void types_reset() {
    types_computed = false;
    
    types_value = null;
    types_visited = false;
  }
  /** @apilevel internal */
  protected boolean types_computed = false;

  /** @apilevel internal */
  protected Typen types_value;

  /**
   * @attribute syn
   * @aspect typeChecker
   * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/typeChecker.jrag:76
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="typeChecker", declaredAt="/home/tobias_carlsson/chocopy-tobias/src/jastadd/typeChecker.jrag:76")
  public Typen types() {
    ASTState state = state();
    if (types_computed) {
      return types_value;
    }
    if (types_visited) {
      throw new RuntimeException("Circular definition of attribute numsD.types().");
    }
    types_visited = true;
    state().enterLazyAttribute();
    types_value = intType();
    types_computed = true;
    state().leaveLazyAttribute();
    types_visited = false;
    return types_value;
  }

}
