/* This file was generated with JastAdd2 (http://jastadd.org) version 2.3.4 */
package lang.ast;
import java.util.*;
import java.io.PrintStream;
import java.util.Set;
import java.util.TreeSet;
import java.util.ArrayList;
import java.io.ByteArrayOutputStream;
import java.lang.reflect.InvocationTargetException;
import java.util.HashSet;
/**
 * @ast node
 * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/lang.ast:7
 * @astdecl class_body : ASTNode ::= var_def optVf:var_def* optfunc:func_def*;
 * @production class_body : {@link ASTNode} ::= <span class="component">{@link var_def}</span> <span class="component">optVf:{@link var_def}*</span> <span class="component">optfunc:{@link func_def}*</span>;

 */
public class class_body extends ASTNode<ASTNode> implements Cloneable {
  /**
   * @aspect CodeGen
   * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/CodeGen.jrag:251
   */
  public void genCode(PrintStream out, IdDecl id) {

		getvar_def().genCode(out,id);

		if(hasoptVf()){
			for(var_def v: getoptVfs()){
				v.genCode(out,id);
			}
		}
	}
  /**
   * @declaredat ASTNode:1
   */
  public class_body() {
    super();
  }
  /**
   * Initializes the child array to the correct size.
   * Initializes List and Opt nta children.
   * @apilevel internal
   * @ast method
   * @declaredat ASTNode:10
   */
  public void init$Children() {
    children = new ASTNode[3];
    setChild(new List(), 1);
    setChild(new List(), 2);
  }
  /**
   * @declaredat ASTNode:15
   */
  @ASTNodeAnnotation.Constructor(
    name = {"var_def", "optVf", "optfunc"},
    type = {"var_def", "List<var_def>", "List<func_def>"},
    kind = {"Child", "List", "List"}
  )
  public class_body(var_def p0, List<var_def> p1, List<func_def> p2) {
    setChild(p0, 0);
    setChild(p1, 1);
    setChild(p2, 2);
  }
  /** @apilevel low-level 
   * @declaredat ASTNode:26
   */
  protected int numChildren() {
    return 3;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:30
   */
  public void flushAttrCache() {
    super.flushAttrCache();

  }
  /** @apilevel internal 
   * @declaredat ASTNode:35
   */
  public void flushCollectionCache() {
    super.flushCollectionCache();

  }
  /** @apilevel internal 
   * @declaredat ASTNode:40
   */
  public class_body clone() throws CloneNotSupportedException {
    class_body node = (class_body) super.clone();
    return node;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:45
   */
  public class_body copy() {
    try {
      class_body node = (class_body) clone();
      node.parent = null;
      if (children != null) {
        node.children = (ASTNode[]) children.clone();
      }
      return node;
    } catch (CloneNotSupportedException e) {
      throw new Error("Error: clone not supported for " + getClass().getName());
    }
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @deprecated Please use treeCopy or treeCopyNoTransform instead
   * @declaredat ASTNode:64
   */
  @Deprecated
  public class_body fullCopy() {
    return treeCopyNoTransform();
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @declaredat ASTNode:74
   */
  public class_body treeCopyNoTransform() {
    class_body tree = (class_body) copy();
    if (children != null) {
      for (int i = 0; i < children.length; ++i) {
        ASTNode child = (ASTNode) children[i];
        if (child != null) {
          child = child.treeCopyNoTransform();
          tree.setChild(child, i);
        }
      }
    }
    return tree;
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The subtree of this node is traversed to trigger rewrites before copy.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @declaredat ASTNode:94
   */
  public class_body treeCopy() {
    class_body tree = (class_body) copy();
    if (children != null) {
      for (int i = 0; i < children.length; ++i) {
        ASTNode child = (ASTNode) getChild(i);
        if (child != null) {
          child = child.treeCopy();
          tree.setChild(child, i);
        }
      }
    }
    return tree;
  }
  /**
   * Replaces the var_def child.
   * @param node The new node to replace the var_def child.
   * @apilevel high-level
   */
  public class_body setvar_def(var_def node) {
    setChild(node, 0);
    return this;
  }
  /**
   * Retrieves the var_def child.
   * @return The current node used as the var_def child.
   * @apilevel high-level
   */
  @ASTNodeAnnotation.Child(name="var_def")
  public var_def getvar_def() {
    return (var_def) getChild(0);
  }
  /**
   * Retrieves the var_def child.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The current node used as the var_def child.
   * @apilevel low-level
   */
  public var_def getvar_defNoTransform() {
    return (var_def) getChildNoTransform(0);
  }
  /**
   * Replaces the optVf list.
   * @param list The new list node to be used as the optVf list.
   * @apilevel high-level
   */
  public class_body setoptVfList(List<var_def> list) {
    setChild(list, 1);
    return this;
  }
  /**
   * Retrieves the number of children in the optVf list.
   * @return Number of children in the optVf list.
   * @apilevel high-level
   */
  public int getNumoptVf() {
    return getoptVfList().getNumChild();
  }
  /**
   * Retrieves the number of children in the optVf list.
   * Calling this method will not trigger rewrites.
   * @return Number of children in the optVf list.
   * @apilevel low-level
   */
  public int getNumoptVfNoTransform() {
    return getoptVfListNoTransform().getNumChildNoTransform();
  }
  /**
   * Retrieves the element at index {@code i} in the optVf list.
   * @param i Index of the element to return.
   * @return The element at position {@code i} in the optVf list.
   * @apilevel high-level
   */
  public var_def getoptVf(int i) {
    return (var_def) getoptVfList().getChild(i);
  }
  /**
   * Check whether the optVf list has any children.
   * @return {@code true} if it has at least one child, {@code false} otherwise.
   * @apilevel high-level
   */
  public boolean hasoptVf() {
    return getoptVfList().getNumChild() != 0;
  }
  /**
   * Append an element to the optVf list.
   * @param node The element to append to the optVf list.
   * @apilevel high-level
   */
  public class_body addoptVf(var_def node) {
    List<var_def> list = (parent == null) ? getoptVfListNoTransform() : getoptVfList();
    list.addChild(node);
    return this;
  }
  /** @apilevel low-level 
   */
  public class_body addoptVfNoTransform(var_def node) {
    List<var_def> list = getoptVfListNoTransform();
    list.addChild(node);
    return this;
  }
  /**
   * Replaces the optVf list element at index {@code i} with the new node {@code node}.
   * @param node The new node to replace the old list element.
   * @param i The list index of the node to be replaced.
   * @apilevel high-level
   */
  public class_body setoptVf(var_def node, int i) {
    List<var_def> list = getoptVfList();
    list.setChild(node, i);
    return this;
  }
  /**
   * Retrieves the optVf list.
   * @return The node representing the optVf list.
   * @apilevel high-level
   */
  @ASTNodeAnnotation.ListChild(name="optVf")
  public List<var_def> getoptVfList() {
    List<var_def> list = (List<var_def>) getChild(1);
    return list;
  }
  /**
   * Retrieves the optVf list.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The node representing the optVf list.
   * @apilevel low-level
   */
  public List<var_def> getoptVfListNoTransform() {
    return (List<var_def>) getChildNoTransform(1);
  }
  /**
   * @return the element at index {@code i} in the optVf list without
   * triggering rewrites.
   */
  public var_def getoptVfNoTransform(int i) {
    return (var_def) getoptVfListNoTransform().getChildNoTransform(i);
  }
  /**
   * Retrieves the optVf list.
   * @return The node representing the optVf list.
   * @apilevel high-level
   */
  public List<var_def> getoptVfs() {
    return getoptVfList();
  }
  /**
   * Retrieves the optVf list.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The node representing the optVf list.
   * @apilevel low-level
   */
  public List<var_def> getoptVfsNoTransform() {
    return getoptVfListNoTransform();
  }
  /**
   * Replaces the optfunc list.
   * @param list The new list node to be used as the optfunc list.
   * @apilevel high-level
   */
  public class_body setoptfuncList(List<func_def> list) {
    setChild(list, 2);
    return this;
  }
  /**
   * Retrieves the number of children in the optfunc list.
   * @return Number of children in the optfunc list.
   * @apilevel high-level
   */
  public int getNumoptfunc() {
    return getoptfuncList().getNumChild();
  }
  /**
   * Retrieves the number of children in the optfunc list.
   * Calling this method will not trigger rewrites.
   * @return Number of children in the optfunc list.
   * @apilevel low-level
   */
  public int getNumoptfuncNoTransform() {
    return getoptfuncListNoTransform().getNumChildNoTransform();
  }
  /**
   * Retrieves the element at index {@code i} in the optfunc list.
   * @param i Index of the element to return.
   * @return The element at position {@code i} in the optfunc list.
   * @apilevel high-level
   */
  public func_def getoptfunc(int i) {
    return (func_def) getoptfuncList().getChild(i);
  }
  /**
   * Check whether the optfunc list has any children.
   * @return {@code true} if it has at least one child, {@code false} otherwise.
   * @apilevel high-level
   */
  public boolean hasoptfunc() {
    return getoptfuncList().getNumChild() != 0;
  }
  /**
   * Append an element to the optfunc list.
   * @param node The element to append to the optfunc list.
   * @apilevel high-level
   */
  public class_body addoptfunc(func_def node) {
    List<func_def> list = (parent == null) ? getoptfuncListNoTransform() : getoptfuncList();
    list.addChild(node);
    return this;
  }
  /** @apilevel low-level 
   */
  public class_body addoptfuncNoTransform(func_def node) {
    List<func_def> list = getoptfuncListNoTransform();
    list.addChild(node);
    return this;
  }
  /**
   * Replaces the optfunc list element at index {@code i} with the new node {@code node}.
   * @param node The new node to replace the old list element.
   * @param i The list index of the node to be replaced.
   * @apilevel high-level
   */
  public class_body setoptfunc(func_def node, int i) {
    List<func_def> list = getoptfuncList();
    list.setChild(node, i);
    return this;
  }
  /**
   * Retrieves the optfunc list.
   * @return The node representing the optfunc list.
   * @apilevel high-level
   */
  @ASTNodeAnnotation.ListChild(name="optfunc")
  public List<func_def> getoptfuncList() {
    List<func_def> list = (List<func_def>) getChild(2);
    return list;
  }
  /**
   * Retrieves the optfunc list.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The node representing the optfunc list.
   * @apilevel low-level
   */
  public List<func_def> getoptfuncListNoTransform() {
    return (List<func_def>) getChildNoTransform(2);
  }
  /**
   * @return the element at index {@code i} in the optfunc list without
   * triggering rewrites.
   */
  public func_def getoptfuncNoTransform(int i) {
    return (func_def) getoptfuncListNoTransform().getChildNoTransform(i);
  }
  /**
   * Retrieves the optfunc list.
   * @return The node representing the optfunc list.
   * @apilevel high-level
   */
  public List<func_def> getoptfuncs() {
    return getoptfuncList();
  }
  /**
   * Retrieves the optfunc list.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The node representing the optfunc list.
   * @apilevel low-level
   */
  public List<func_def> getoptfuncsNoTransform() {
    return getoptfuncListNoTransform();
  }
  /**
   * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/nameanalysis.jrag:224
   * @apilevel internal
   */
  public IdDecl Define_lookupNN(ASTNode _callerNode, ASTNode _childNode, String name) {
    int childIndex = this.getIndexOfChild(_callerNode);
    {
    		if(getvar_def().gettypedVar().getIdDecl().getID().equals(name)){
    			return getvar_def().gettypedVar().getIdDecl();
    		}
    		for(var_def vd : getoptVfs()){
    			if(vd.gettypedVar().getIdDecl().getID().equals(name)){
    			return vd.gettypedVar().getIdDecl();
    		}
    		}
    		return unknownDecl();
    	}
  }
  /**
   * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/nameanalysis.jrag:224
   * @apilevel internal
   * @return {@code true} if this node has an equation for the inherited attribute lookupNN
   */
  protected boolean canDefine_lookupNN(ASTNode _callerNode, ASTNode _childNode, String name) {
    return true;
  }
  /**
   * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/nameanalysis.jrag:334
   * @apilevel internal
   */
  public IdDecl Define_globalLookup(ASTNode _callerNode, ASTNode _childNode, String name) {
    if (_callerNode == getoptfuncListNoTransform()) {
      // @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/nameanalysis.jrag:257
      int i = _callerNode.getIndexOfChild(_childNode);
      {
      		if(getvar_def().gettypedVar().getIdDecl().getID().equals(name)){
      			return getvar_def().gettypedVar().getIdDecl();
      		}
      		for(var_def f : getoptVfs()){
      			if(f.gettypedVar().getIdDecl().getID().equals(name)){
      				return f.gettypedVar().getIdDecl();
      			}
      		}
      		return unknownDecl();
      	}
    }
    else {
      return getParent().Define_globalLookup(this, _callerNode, name);
    }
  }
  /**
   * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/nameanalysis.jrag:334
   * @apilevel internal
   * @return {@code true} if this node has an equation for the inherited attribute globalLookup
   */
  protected boolean canDefine_globalLookup(ASTNode _callerNode, ASTNode _childNode, String name) {
    return true;
  }
  /**
   * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/nameanalysis.jrag:283
   * @apilevel internal
   */
  public IdDecl Define_localLookups(ASTNode _callerNode, ASTNode _childNode, String name) {
    if (_callerNode == getoptfuncListNoTransform()) {
      // @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/nameanalysis.jrag:287
      int i = _callerNode.getIndexOfChild(_childNode);
      {
      		if(getvar_def().gettypedVar().getIdDecl().getID().equals(name)){
      			return getvar_def().gettypedVar().getIdDecl();
      		}
      		for(var_def f : getoptVfs()){
      			if(f.gettypedVar().getIdDecl().getID().equals(name)){
      				return f.gettypedVar().getIdDecl();
      			}
      		}
      		return unknownDecl();
      	}
    }
    else {
      return getParent().Define_localLookups(this, _callerNode, name);
    }
  }
  /**
   * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/nameanalysis.jrag:283
   * @apilevel internal
   * @return {@code true} if this node has an equation for the inherited attribute localLookups
   */
  protected boolean canDefine_localLookups(ASTNode _callerNode, ASTNode _childNode, String name) {
    return true;
  }
  /**
   * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/paramAdrInd.jrag:52
   * @apilevel internal
   */
  public int Define_localIndexc(ASTNode _callerNode, ASTNode _childNode) {
    if (_callerNode == getoptVfListNoTransform()) {
      // @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/paramAdrInd.jrag:55
      int i = _callerNode.getIndexOfChild(_childNode);
      return i+2;
    }
    else if (_callerNode == getvar_defNoTransform()) {
      // @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/paramAdrInd.jrag:54
      return 1;
    }
    else {
      return getParent().Define_localIndexc(this, _callerNode);
    }
  }
  /**
   * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/paramAdrInd.jrag:52
   * @apilevel internal
   * @return {@code true} if this node has an equation for the inherited attribute localIndexc
   */
  protected boolean canDefine_localIndexc(ASTNode _callerNode, ASTNode _childNode) {
    return true;
  }
  /**
   * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/paramAdrInd.jrag:70
   * @apilevel internal
   */
  public boolean Define_insideClass(ASTNode _callerNode, ASTNode _childNode) {
    if (_callerNode == getvar_defNoTransform()) {
      // @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/paramAdrInd.jrag:78
      return true;
    }
    else {
      return getParent().Define_insideClass(this, _callerNode);
    }
  }
  /**
   * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/paramAdrInd.jrag:70
   * @apilevel internal
   * @return {@code true} if this node has an equation for the inherited attribute insideClass
   */
  protected boolean canDefine_insideClass(ASTNode _callerNode, ASTNode _childNode) {
    return true;
  }
  /**
   * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/paramAdrInd.jrag:80
   * @apilevel internal
   */
  public int Define_localIndexe(ASTNode _callerNode, ASTNode _childNode) {
    if (_callerNode == getoptVfListNoTransform()) {
      // @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/paramAdrInd.jrag:83
      int i = _callerNode.getIndexOfChild(_childNode);
      return localIndex()-1-i;
    }
    else if (_callerNode == getvar_defNoTransform()) {
      // @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/paramAdrInd.jrag:82
      return localIndex();
    }
    else {
      return getParent().Define_localIndexe(this, _callerNode);
    }
  }
  /**
   * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/paramAdrInd.jrag:80
   * @apilevel internal
   * @return {@code true} if this node has an equation for the inherited attribute localIndexe
   */
  protected boolean canDefine_localIndexe(ASTNode _callerNode, ASTNode _childNode) {
    return true;
  }
  /**
   * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/rndFix.jrag:8
   * @apilevel internal
   */
  public boolean Define_insideCl(ASTNode _callerNode, ASTNode _childNode) {
    if (_callerNode == getvar_defNoTransform()) {
      // @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/rndFix.jrag:14
      return true;
    }
    else {
      int childIndex = this.getIndexOfChild(_callerNode);
      return true;
    }
  }
  /**
   * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/rndFix.jrag:8
   * @apilevel internal
   * @return {@code true} if this node has an equation for the inherited attribute insideCl
   */
  protected boolean canDefine_insideCl(ASTNode _callerNode, ASTNode _childNode) {
    return true;
  }
  /**
   * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/typeChecker.jrag:21
   * @apilevel internal
   */
  public Typen Define_types(ASTNode _callerNode, ASTNode _childNode) {
    if (_callerNode == getoptVfListNoTransform()) {
      // @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/typeChecker.jrag:100
      int i = _callerNode.getIndexOfChild(_childNode);
      return getoptVf(i).gettypedVar().getTypen();
    }
    else {
      return getParent().Define_types(this, _callerNode);
    }
  }
  /**
   * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/typeChecker.jrag:21
   * @apilevel internal
   * @return {@code true} if this node has an equation for the inherited attribute types
   */
  protected boolean canDefine_types(ASTNode _callerNode, ASTNode _childNode) {
    return true;
  }

}
