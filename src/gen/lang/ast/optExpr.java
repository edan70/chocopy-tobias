/* This file was generated with JastAdd2 (http://jastadd.org) version 2.3.4 */
package lang.ast;
import java.util.*;
import java.io.PrintStream;
import java.util.Set;
import java.util.TreeSet;
import java.util.ArrayList;
import java.io.ByteArrayOutputStream;
import java.lang.reflect.InvocationTargetException;
import java.util.HashSet;
/**
 * @ast node
 * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/lang.ast:33
 * @astdecl optExpr : Expr ::= Expr Opt:Expr*;
 * @production optExpr : {@link Expr} ::= <span class="component">{@link Expr}</span> <span class="component">Opt:{@link Expr}*</span>;

 */
public class optExpr extends Expr implements Cloneable {
  /**
   * @aspect CodeGen
   * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/CodeGen.jrag:575
   */
  public void genEval(PrintStream out, IdDecl id) {
		getExpr().genEval(out,id);
		for (Expr eb :  getOpts() ){
			eb.genEval(out,id);
		}
	}
  /**
   * @declaredat ASTNode:1
   */
  public optExpr() {
    super();
  }
  /**
   * Initializes the child array to the correct size.
   * Initializes List and Opt nta children.
   * @apilevel internal
   * @ast method
   * @declaredat ASTNode:10
   */
  public void init$Children() {
    children = new ASTNode[2];
    setChild(new List(), 1);
  }
  /**
   * @declaredat ASTNode:14
   */
  @ASTNodeAnnotation.Constructor(
    name = {"Expr", "Opt"},
    type = {"Expr", "List<Expr>"},
    kind = {"Child", "List"}
  )
  public optExpr(Expr p0, List<Expr> p1) {
    setChild(p0, 0);
    setChild(p1, 1);
  }
  /** @apilevel low-level 
   * @declaredat ASTNode:24
   */
  protected int numChildren() {
    return 2;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:28
   */
  public void flushAttrCache() {
    super.flushAttrCache();
    types_reset();
  }
  /** @apilevel internal 
   * @declaredat ASTNode:33
   */
  public void flushCollectionCache() {
    super.flushCollectionCache();

  }
  /** @apilevel internal 
   * @declaredat ASTNode:38
   */
  public optExpr clone() throws CloneNotSupportedException {
    optExpr node = (optExpr) super.clone();
    return node;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:43
   */
  public optExpr copy() {
    try {
      optExpr node = (optExpr) clone();
      node.parent = null;
      if (children != null) {
        node.children = (ASTNode[]) children.clone();
      }
      return node;
    } catch (CloneNotSupportedException e) {
      throw new Error("Error: clone not supported for " + getClass().getName());
    }
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @deprecated Please use treeCopy or treeCopyNoTransform instead
   * @declaredat ASTNode:62
   */
  @Deprecated
  public optExpr fullCopy() {
    return treeCopyNoTransform();
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @declaredat ASTNode:72
   */
  public optExpr treeCopyNoTransform() {
    optExpr tree = (optExpr) copy();
    if (children != null) {
      for (int i = 0; i < children.length; ++i) {
        ASTNode child = (ASTNode) children[i];
        if (child != null) {
          child = child.treeCopyNoTransform();
          tree.setChild(child, i);
        }
      }
    }
    return tree;
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The subtree of this node is traversed to trigger rewrites before copy.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @declaredat ASTNode:92
   */
  public optExpr treeCopy() {
    optExpr tree = (optExpr) copy();
    if (children != null) {
      for (int i = 0; i < children.length; ++i) {
        ASTNode child = (ASTNode) getChild(i);
        if (child != null) {
          child = child.treeCopy();
          tree.setChild(child, i);
        }
      }
    }
    return tree;
  }
  /**
   * Replaces the Expr child.
   * @param node The new node to replace the Expr child.
   * @apilevel high-level
   */
  public optExpr setExpr(Expr node) {
    setChild(node, 0);
    return this;
  }
  /**
   * Retrieves the Expr child.
   * @return The current node used as the Expr child.
   * @apilevel high-level
   */
  @ASTNodeAnnotation.Child(name="Expr")
  public Expr getExpr() {
    return (Expr) getChild(0);
  }
  /**
   * Retrieves the Expr child.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The current node used as the Expr child.
   * @apilevel low-level
   */
  public Expr getExprNoTransform() {
    return (Expr) getChildNoTransform(0);
  }
  /**
   * Replaces the Opt list.
   * @param list The new list node to be used as the Opt list.
   * @apilevel high-level
   */
  public optExpr setOptList(List<Expr> list) {
    setChild(list, 1);
    return this;
  }
  /**
   * Retrieves the number of children in the Opt list.
   * @return Number of children in the Opt list.
   * @apilevel high-level
   */
  public int getNumOpt() {
    return getOptList().getNumChild();
  }
  /**
   * Retrieves the number of children in the Opt list.
   * Calling this method will not trigger rewrites.
   * @return Number of children in the Opt list.
   * @apilevel low-level
   */
  public int getNumOptNoTransform() {
    return getOptListNoTransform().getNumChildNoTransform();
  }
  /**
   * Retrieves the element at index {@code i} in the Opt list.
   * @param i Index of the element to return.
   * @return The element at position {@code i} in the Opt list.
   * @apilevel high-level
   */
  public Expr getOpt(int i) {
    return (Expr) getOptList().getChild(i);
  }
  /**
   * Check whether the Opt list has any children.
   * @return {@code true} if it has at least one child, {@code false} otherwise.
   * @apilevel high-level
   */
  public boolean hasOpt() {
    return getOptList().getNumChild() != 0;
  }
  /**
   * Append an element to the Opt list.
   * @param node The element to append to the Opt list.
   * @apilevel high-level
   */
  public optExpr addOpt(Expr node) {
    List<Expr> list = (parent == null) ? getOptListNoTransform() : getOptList();
    list.addChild(node);
    return this;
  }
  /** @apilevel low-level 
   */
  public optExpr addOptNoTransform(Expr node) {
    List<Expr> list = getOptListNoTransform();
    list.addChild(node);
    return this;
  }
  /**
   * Replaces the Opt list element at index {@code i} with the new node {@code node}.
   * @param node The new node to replace the old list element.
   * @param i The list index of the node to be replaced.
   * @apilevel high-level
   */
  public optExpr setOpt(Expr node, int i) {
    List<Expr> list = getOptList();
    list.setChild(node, i);
    return this;
  }
  /**
   * Retrieves the Opt list.
   * @return The node representing the Opt list.
   * @apilevel high-level
   */
  @ASTNodeAnnotation.ListChild(name="Opt")
  public List<Expr> getOptList() {
    List<Expr> list = (List<Expr>) getChild(1);
    return list;
  }
  /**
   * Retrieves the Opt list.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The node representing the Opt list.
   * @apilevel low-level
   */
  public List<Expr> getOptListNoTransform() {
    return (List<Expr>) getChildNoTransform(1);
  }
  /**
   * @return the element at index {@code i} in the Opt list without
   * triggering rewrites.
   */
  public Expr getOptNoTransform(int i) {
    return (Expr) getOptListNoTransform().getChildNoTransform(i);
  }
  /**
   * Retrieves the Opt list.
   * @return The node representing the Opt list.
   * @apilevel high-level
   */
  public List<Expr> getOpts() {
    return getOptList();
  }
  /**
   * Retrieves the Opt list.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The node representing the Opt list.
   * @apilevel low-level
   */
  public List<Expr> getOptsNoTransform() {
    return getOptListNoTransform();
  }
/** @apilevel internal */
protected boolean types_visited = false;
  /** @apilevel internal */
  private void types_reset() {
    types_computed = false;
    
    types_value = null;
    types_visited = false;
  }
  /** @apilevel internal */
  protected boolean types_computed = false;

  /** @apilevel internal */
  protected Typen types_value;

  /**
   * @attribute syn
   * @aspect typeChecker
   * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/typeChecker.jrag:64
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="typeChecker", declaredAt="/home/tobias_carlsson/chocopy-tobias/src/jastadd/typeChecker.jrag:64")
  public Typen types() {
    ASTState state = state();
    if (types_computed) {
      return types_value;
    }
    if (types_visited) {
      throw new RuntimeException("Circular definition of attribute optExpr.types().");
    }
    types_visited = true;
    state().enterLazyAttribute();
    types_value = getExpr().types();
    types_computed = true;
    state().leaveLazyAttribute();
    types_visited = false;
    return types_value;
  }
  /**
   * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/paramAdrInd.jrag:29
   * @apilevel internal
   */
  public int Define_localIndex(ASTNode _callerNode, ASTNode _childNode) {
    if (_callerNode == getOptListNoTransform()) {
      // @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/paramAdrInd.jrag:34
      int i = _callerNode.getIndexOfChild(_childNode);
      return localIndex();
    }
    else if (_callerNode == getExprNoTransform()) {
      // @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/paramAdrInd.jrag:33
      return localIndex();
    }
    else {
      return getParent().Define_localIndex(this, _callerNode);
    }
  }
  /**
   * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/paramAdrInd.jrag:29
   * @apilevel internal
   * @return {@code true} if this node has an equation for the inherited attribute localIndex
   */
  protected boolean canDefine_localIndex(ASTNode _callerNode, ASTNode _childNode) {
    return true;
  }

}
