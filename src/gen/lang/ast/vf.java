/* This file was generated with JastAdd2 (http://jastadd.org) version 2.3.4 */
package lang.ast;
import java.util.*;
import java.io.PrintStream;
import java.util.Set;
import java.util.TreeSet;
import java.util.ArrayList;
import java.io.ByteArrayOutputStream;
import java.lang.reflect.InvocationTargetException;
import java.util.HashSet;
/**
 * @ast node
 * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/lang.ast:9
 * @astdecl vf : ASTNode;
 * @production vf : {@link ASTNode};

 */
public abstract class vf extends ASTNode<ASTNode> implements Cloneable {
  /**
   * @aspect CodeGen
   * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/CodeGen.jrag:261
   */
  abstract public void genCode(PrintStream out, IdDecl id);
  /**
   * @declaredat ASTNode:1
   */
  public vf() {
    super();
  }
  /**
   * Initializes the child array to the correct size.
   * Initializes List and Opt nta children.
   * @apilevel internal
   * @ast method
   * @declaredat ASTNode:10
   */
  public void init$Children() {
  }
  /** @apilevel low-level 
   * @declaredat ASTNode:13
   */
  protected int numChildren() {
    return 0;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:17
   */
  public void flushAttrCache() {
    super.flushAttrCache();
    localIndexc_reset();
  }
  /** @apilevel internal 
   * @declaredat ASTNode:22
   */
  public void flushCollectionCache() {
    super.flushCollectionCache();

  }
  /** @apilevel internal 
   * @declaredat ASTNode:27
   */
  public vf clone() throws CloneNotSupportedException {
    vf node = (vf) super.clone();
    return node;
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @deprecated Please use treeCopy or treeCopyNoTransform instead
   * @declaredat ASTNode:38
   */
  @Deprecated
  public abstract vf fullCopy();
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @declaredat ASTNode:46
   */
  public abstract vf treeCopyNoTransform();
  /**
   * Create a deep copy of the AST subtree at this node.
   * The subtree of this node is traversed to trigger rewrites before copy.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @declaredat ASTNode:54
   */
  public abstract vf treeCopy();
  /**
   * @attribute inh
   * @aspect paramAdrInd
   * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/paramAdrInd.jrag:50
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.INH)
  @ASTNodeAnnotation.Source(aspect="paramAdrInd", declaredAt="/home/tobias_carlsson/chocopy-tobias/src/jastadd/paramAdrInd.jrag:50")
  public int localIndexc() {
    ASTState state = state();
    if (localIndexc_computed) {
      return localIndexc_value;
    }
    if (localIndexc_visited) {
      throw new RuntimeException("Circular definition of attribute vf.localIndexc().");
    }
    localIndexc_visited = true;
    state().enterLazyAttribute();
    localIndexc_value = getParent().Define_localIndexc(this, null);
    localIndexc_computed = true;
    state().leaveLazyAttribute();
    localIndexc_visited = false;
    return localIndexc_value;
  }
/** @apilevel internal */
protected boolean localIndexc_visited = false;
  /** @apilevel internal */
  private void localIndexc_reset() {
    localIndexc_computed = false;
    localIndexc_visited = false;
  }
  /** @apilevel internal */
  protected boolean localIndexc_computed = false;

  /** @apilevel internal */
  protected int localIndexc_value;

  /**
   * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/paramAdrInd.jrag:52
   * @apilevel internal
   */
  public int Define_localIndexc(ASTNode _callerNode, ASTNode _childNode) {
    int childIndex = this.getIndexOfChild(_callerNode);
    return localIndexc();
  }
  /**
   * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/paramAdrInd.jrag:52
   * @apilevel internal
   * @return {@code true} if this node has an equation for the inherited attribute localIndexc
   */
  protected boolean canDefine_localIndexc(ASTNode _callerNode, ASTNode _childNode) {
    return true;
  }

}
