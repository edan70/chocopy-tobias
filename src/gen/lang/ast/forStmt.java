/* This file was generated with JastAdd2 (http://jastadd.org) version 2.3.4 */
package lang.ast;
import java.util.*;
import java.io.PrintStream;
import java.util.Set;
import java.util.TreeSet;
import java.util.ArrayList;
import java.io.ByteArrayOutputStream;
import java.lang.reflect.InvocationTargetException;
import java.util.HashSet;
/**
 * @ast node
 * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/lang.ast:23
 * @astdecl forStmt : stmt ::= IdUse in:Expr then:Block;
 * @production forStmt : {@link stmt} ::= <span class="component">{@link IdUse}</span> <span class="component">in:{@link Expr}</span> <span class="component">then:{@link Block}</span>;

 */
public class forStmt extends stmt implements Cloneable {
  /**
   * @aspect CodeGen
   * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/CodeGen.jrag:178
   */
  public void genCode(PrintStream out,IdDecl id) {   
	}
  /**
   * @declaredat ASTNode:1
   */
  public forStmt() {
    super();
  }
  /**
   * Initializes the child array to the correct size.
   * Initializes List and Opt nta children.
   * @apilevel internal
   * @ast method
   * @declaredat ASTNode:10
   */
  public void init$Children() {
    children = new ASTNode[3];
  }
  /**
   * @declaredat ASTNode:13
   */
  @ASTNodeAnnotation.Constructor(
    name = {"IdUse", "in", "then"},
    type = {"IdUse", "Expr", "Block"},
    kind = {"Child", "Child", "Child"}
  )
  public forStmt(IdUse p0, Expr p1, Block p2) {
    setChild(p0, 0);
    setChild(p1, 1);
    setChild(p2, 2);
  }
  /** @apilevel low-level 
   * @declaredat ASTNode:24
   */
  protected int numChildren() {
    return 3;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:28
   */
  public void flushAttrCache() {
    super.flushAttrCache();

  }
  /** @apilevel internal 
   * @declaredat ASTNode:33
   */
  public void flushCollectionCache() {
    super.flushCollectionCache();

  }
  /** @apilevel internal 
   * @declaredat ASTNode:38
   */
  public forStmt clone() throws CloneNotSupportedException {
    forStmt node = (forStmt) super.clone();
    return node;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:43
   */
  public forStmt copy() {
    try {
      forStmt node = (forStmt) clone();
      node.parent = null;
      if (children != null) {
        node.children = (ASTNode[]) children.clone();
      }
      return node;
    } catch (CloneNotSupportedException e) {
      throw new Error("Error: clone not supported for " + getClass().getName());
    }
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @deprecated Please use treeCopy or treeCopyNoTransform instead
   * @declaredat ASTNode:62
   */
  @Deprecated
  public forStmt fullCopy() {
    return treeCopyNoTransform();
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @declaredat ASTNode:72
   */
  public forStmt treeCopyNoTransform() {
    forStmt tree = (forStmt) copy();
    if (children != null) {
      for (int i = 0; i < children.length; ++i) {
        ASTNode child = (ASTNode) children[i];
        if (child != null) {
          child = child.treeCopyNoTransform();
          tree.setChild(child, i);
        }
      }
    }
    return tree;
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The subtree of this node is traversed to trigger rewrites before copy.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @declaredat ASTNode:92
   */
  public forStmt treeCopy() {
    forStmt tree = (forStmt) copy();
    if (children != null) {
      for (int i = 0; i < children.length; ++i) {
        ASTNode child = (ASTNode) getChild(i);
        if (child != null) {
          child = child.treeCopy();
          tree.setChild(child, i);
        }
      }
    }
    return tree;
  }
  /**
   * Replaces the IdUse child.
   * @param node The new node to replace the IdUse child.
   * @apilevel high-level
   */
  public forStmt setIdUse(IdUse node) {
    setChild(node, 0);
    return this;
  }
  /**
   * Retrieves the IdUse child.
   * @return The current node used as the IdUse child.
   * @apilevel high-level
   */
  @ASTNodeAnnotation.Child(name="IdUse")
  public IdUse getIdUse() {
    return (IdUse) getChild(0);
  }
  /**
   * Retrieves the IdUse child.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The current node used as the IdUse child.
   * @apilevel low-level
   */
  public IdUse getIdUseNoTransform() {
    return (IdUse) getChildNoTransform(0);
  }
  /**
   * Replaces the in child.
   * @param node The new node to replace the in child.
   * @apilevel high-level
   */
  public forStmt setin(Expr node) {
    setChild(node, 1);
    return this;
  }
  /**
   * Retrieves the in child.
   * @return The current node used as the in child.
   * @apilevel high-level
   */
  @ASTNodeAnnotation.Child(name="in")
  public Expr getin() {
    return (Expr) getChild(1);
  }
  /**
   * Retrieves the in child.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The current node used as the in child.
   * @apilevel low-level
   */
  public Expr getinNoTransform() {
    return (Expr) getChildNoTransform(1);
  }
  /**
   * Replaces the then child.
   * @param node The new node to replace the then child.
   * @apilevel high-level
   */
  public forStmt setthen(Block node) {
    setChild(node, 2);
    return this;
  }
  /**
   * Retrieves the then child.
   * @return The current node used as the then child.
   * @apilevel high-level
   */
  @ASTNodeAnnotation.Child(name="then")
  public Block getthen() {
    return (Block) getChild(2);
  }
  /**
   * Retrieves the then child.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The current node used as the then child.
   * @apilevel low-level
   */
  public Block getthenNoTransform() {
    return (Block) getChildNoTransform(2);
  }
  /**
   * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/nameanalysis.jrag:207
   * @apilevel internal
   */
  public IdDecl Define_lookup(ASTNode _callerNode, ASTNode _childNode, String name) {
    if (_callerNode == getIdUseNoTransform()) {
      // @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/nameanalysis.jrag:242
      return globalLookup(name);
    }
    else {
      return getParent().Define_lookup(this, _callerNode, name);
    }
  }
  /**
   * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/nameanalysis.jrag:207
   * @apilevel internal
   * @return {@code true} if this node has an equation for the inherited attribute lookup
   */
  protected boolean canDefine_lookup(ASTNode _callerNode, ASTNode _childNode, String name) {
    return true;
  }

}
