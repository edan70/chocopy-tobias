/* This file was generated with JastAdd2 (http://jastadd.org) version 2.3.4 */
package lang.ast;
import java.util.*;
import java.io.PrintStream;
import java.util.Set;
import java.util.TreeSet;
import java.util.ArrayList;
import java.io.ByteArrayOutputStream;
import java.lang.reflect.InvocationTargetException;
import java.util.HashSet;
/**
 * @ast node
 * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/lang.ast:26
 * @astdecl assignStmt : stmt ::= Target Expr;
 * @production assignStmt : {@link stmt} ::= <span class="component">{@link Target}</span> <span class="component">{@link Expr}</span>;

 */
public class assignStmt extends stmt implements Cloneable {
  /**
   * @aspect CodeGen
   * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/CodeGen.jrag:383
   */
  public void genCode(PrintStream out,IdDecl id) {
		getExpr().genEval(out,id);
		if(insideCl()&&insideFunc())
		{	int ch =0;
		if(id!=null){
			int count =id.nbrLoc()-1;

			for(Map.Entry mapElement : id.objvars().entrySet()){
				int ind = (int)mapElement.getValue(); 
				var_def vd = (var_def)mapElement.getKey(); 
				if(vd.gettypedVar().getIdDecl().getID().equals(getTarget().getIdUse().getID())){

					out.println("        movq %rax, " +"-"+((ind)*8)+"(%rbp)");
					ch=1;
				}
				count--;
			}
		}
		if(ch==0){				
			out.println("        movq %rax, " + getTarget().getIdUse().decl().address());
		}
		}
		else if(insideCl())
		{
			out.println("        movq %rax, " + getTarget().getIdUse().decl().addresse());
		}else{
			out.println("        movq %rax, " + getTarget().getIdUse().decl().address());
		}
	}
  /**
   * @declaredat ASTNode:1
   */
  public assignStmt() {
    super();
  }
  /**
   * Initializes the child array to the correct size.
   * Initializes List and Opt nta children.
   * @apilevel internal
   * @ast method
   * @declaredat ASTNode:10
   */
  public void init$Children() {
    children = new ASTNode[2];
  }
  /**
   * @declaredat ASTNode:13
   */
  @ASTNodeAnnotation.Constructor(
    name = {"Target", "Expr"},
    type = {"Target", "Expr"},
    kind = {"Child", "Child"}
  )
  public assignStmt(Target p0, Expr p1) {
    setChild(p0, 0);
    setChild(p1, 1);
  }
  /** @apilevel low-level 
   * @declaredat ASTNode:23
   */
  protected int numChildren() {
    return 2;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:27
   */
  public void flushAttrCache() {
    super.flushAttrCache();
    types_reset();
    assStmt_reset();
    usedId_reset();
    expectedType_reset();
    globalLookup_String_reset();
    insideFunc_reset();
    insideCl_reset();
    isDeclOut_reset();
  }
  /** @apilevel internal 
   * @declaredat ASTNode:39
   */
  public void flushCollectionCache() {
    super.flushCollectionCache();

  }
  /** @apilevel internal 
   * @declaredat ASTNode:44
   */
  public assignStmt clone() throws CloneNotSupportedException {
    assignStmt node = (assignStmt) super.clone();
    return node;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:49
   */
  public assignStmt copy() {
    try {
      assignStmt node = (assignStmt) clone();
      node.parent = null;
      if (children != null) {
        node.children = (ASTNode[]) children.clone();
      }
      return node;
    } catch (CloneNotSupportedException e) {
      throw new Error("Error: clone not supported for " + getClass().getName());
    }
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @deprecated Please use treeCopy or treeCopyNoTransform instead
   * @declaredat ASTNode:68
   */
  @Deprecated
  public assignStmt fullCopy() {
    return treeCopyNoTransform();
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @declaredat ASTNode:78
   */
  public assignStmt treeCopyNoTransform() {
    assignStmt tree = (assignStmt) copy();
    if (children != null) {
      for (int i = 0; i < children.length; ++i) {
        ASTNode child = (ASTNode) children[i];
        if (child != null) {
          child = child.treeCopyNoTransform();
          tree.setChild(child, i);
        }
      }
    }
    return tree;
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The subtree of this node is traversed to trigger rewrites before copy.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @declaredat ASTNode:98
   */
  public assignStmt treeCopy() {
    assignStmt tree = (assignStmt) copy();
    if (children != null) {
      for (int i = 0; i < children.length; ++i) {
        ASTNode child = (ASTNode) getChild(i);
        if (child != null) {
          child = child.treeCopy();
          tree.setChild(child, i);
        }
      }
    }
    return tree;
  }
  /**
   * Replaces the Target child.
   * @param node The new node to replace the Target child.
   * @apilevel high-level
   */
  public assignStmt setTarget(Target node) {
    setChild(node, 0);
    return this;
  }
  /**
   * Retrieves the Target child.
   * @return The current node used as the Target child.
   * @apilevel high-level
   */
  @ASTNodeAnnotation.Child(name="Target")
  public Target getTarget() {
    return (Target) getChild(0);
  }
  /**
   * Retrieves the Target child.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The current node used as the Target child.
   * @apilevel low-level
   */
  public Target getTargetNoTransform() {
    return (Target) getChildNoTransform(0);
  }
  /**
   * Replaces the Expr child.
   * @param node The new node to replace the Expr child.
   * @apilevel high-level
   */
  public assignStmt setExpr(Expr node) {
    setChild(node, 1);
    return this;
  }
  /**
   * Retrieves the Expr child.
   * @return The current node used as the Expr child.
   * @apilevel high-level
   */
  @ASTNodeAnnotation.Child(name="Expr")
  public Expr getExpr() {
    return (Expr) getChild(1);
  }
  /**
   * Retrieves the Expr child.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The current node used as the Expr child.
   * @apilevel low-level
   */
  public Expr getExprNoTransform() {
    return (Expr) getChildNoTransform(1);
  }
/** @apilevel internal */
protected boolean types_visited = false;
  /** @apilevel internal */
  private void types_reset() {
    types_computed = false;
    
    types_value = null;
    types_visited = false;
  }
  /** @apilevel internal */
  protected boolean types_computed = false;

  /** @apilevel internal */
  protected Typen types_value;

  /**
   * @attribute syn
   * @aspect typeChecker
   * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/typeChecker.jrag:54
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="typeChecker", declaredAt="/home/tobias_carlsson/chocopy-tobias/src/jastadd/typeChecker.jrag:54")
  public Typen types() {
    ASTState state = state();
    if (types_computed) {
      return types_value;
    }
    if (types_visited) {
      throw new RuntimeException("Circular definition of attribute assignStmt.types().");
    }
    types_visited = true;
    state().enterLazyAttribute();
    types_value = getExpr().types();
    types_computed = true;
    state().leaveLazyAttribute();
    types_visited = false;
    return types_value;
  }
/** @apilevel internal */
protected boolean assStmt_visited = false;
  /** @apilevel internal */
  private void assStmt_reset() {
    assStmt_computed = false;
    assStmt_visited = false;
  }
  /** @apilevel internal */
  protected boolean assStmt_computed = false;

  /** @apilevel internal */
  protected boolean assStmt_value;

  /**
   * @attribute syn
   * @aspect typeChecker
   * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/typeChecker.jrag:104
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="typeChecker", declaredAt="/home/tobias_carlsson/chocopy-tobias/src/jastadd/typeChecker.jrag:104")
  public boolean assStmt() {
    ASTState state = state();
    if (assStmt_computed) {
      return assStmt_value;
    }
    if (assStmt_visited) {
      throw new RuntimeException("Circular definition of attribute assignStmt.assStmt().");
    }
    assStmt_visited = true;
    state().enterLazyAttribute();
    assStmt_value = true;
    assStmt_computed = true;
    state().leaveLazyAttribute();
    assStmt_visited = false;
    return assStmt_value;
  }
/** @apilevel internal */
protected boolean usedId_visited = false;
  /** @apilevel internal */
  private void usedId_reset() {
    usedId_computed = false;
    
    usedId_value = null;
    usedId_visited = false;
  }
  /** @apilevel internal */
  protected boolean usedId_computed = false;

  /** @apilevel internal */
  protected String usedId_value;

  /**
   * @attribute syn
   * @aspect typeChecker
   * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/typeChecker.jrag:109
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="typeChecker", declaredAt="/home/tobias_carlsson/chocopy-tobias/src/jastadd/typeChecker.jrag:109")
  public String usedId() {
    ASTState state = state();
    if (usedId_computed) {
      return usedId_value;
    }
    if (usedId_visited) {
      throw new RuntimeException("Circular definition of attribute assignStmt.usedId().");
    }
    usedId_visited = true;
    state().enterLazyAttribute();
    usedId_value = getTarget().getIdUse().getID();
    usedId_computed = true;
    state().leaveLazyAttribute();
    usedId_visited = false;
    return usedId_value;
  }
/** @apilevel internal */
protected boolean expectedType_visited = false;
  /** @apilevel internal */
  private void expectedType_reset() {
    expectedType_computed = false;
    
    expectedType_value = null;
    expectedType_visited = false;
  }
  /** @apilevel internal */
  protected boolean expectedType_computed = false;

  /** @apilevel internal */
  protected Typen expectedType_value;

  /**
   * @attribute syn
   * @aspect typeChecker
   * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/typeChecker.jrag:111
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="typeChecker", declaredAt="/home/tobias_carlsson/chocopy-tobias/src/jastadd/typeChecker.jrag:111")
  public Typen expectedType() {
    ASTState state = state();
    if (expectedType_computed) {
      return expectedType_value;
    }
    if (expectedType_visited) {
      throw new RuntimeException("Circular definition of attribute assignStmt.expectedType().");
    }
    expectedType_visited = true;
    state().enterLazyAttribute();
    expectedType_value = getTarget().getIdUse().expectedType();
    expectedType_computed = true;
    state().leaveLazyAttribute();
    expectedType_visited = false;
    return expectedType_value;
  }
  /**
   * @attribute inh
   * @aspect NameAnalysis
   * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/nameanalysis.jrag:338
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.INH)
  @ASTNodeAnnotation.Source(aspect="NameAnalysis", declaredAt="/home/tobias_carlsson/chocopy-tobias/src/jastadd/nameanalysis.jrag:338")
  public IdDecl globalLookup(String name) {
    Object _parameters = name;
    if (globalLookup_String_visited == null) globalLookup_String_visited = new java.util.HashSet(4);
    if (globalLookup_String_values == null) globalLookup_String_values = new java.util.HashMap(4);
    ASTState state = state();
    if (globalLookup_String_values.containsKey(_parameters)) {
      return (IdDecl) globalLookup_String_values.get(_parameters);
    }
    if (globalLookup_String_visited.contains(_parameters)) {
      throw new RuntimeException("Circular definition of attribute assignStmt.globalLookup(String).");
    }
    globalLookup_String_visited.add(_parameters);
    state().enterLazyAttribute();
    IdDecl globalLookup_String_value = getParent().Define_globalLookup(this, null, name);
    globalLookup_String_values.put(_parameters, globalLookup_String_value);
    state().leaveLazyAttribute();
    globalLookup_String_visited.remove(_parameters);
    return globalLookup_String_value;
  }
/** @apilevel internal */
protected java.util.Set globalLookup_String_visited;
  /** @apilevel internal */
  private void globalLookup_String_reset() {
    globalLookup_String_values = null;
    globalLookup_String_visited = null;
  }
  /** @apilevel internal */
  protected java.util.Map globalLookup_String_values;

  /**
   * @attribute inh
   * @aspect paramAdrInd
   * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/paramAdrInd.jrag:21
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.INH)
  @ASTNodeAnnotation.Source(aspect="paramAdrInd", declaredAt="/home/tobias_carlsson/chocopy-tobias/src/jastadd/paramAdrInd.jrag:21")
  public boolean insideFunc() {
    ASTState state = state();
    if (insideFunc_computed) {
      return insideFunc_value;
    }
    if (insideFunc_visited) {
      throw new RuntimeException("Circular definition of attribute assignStmt.insideFunc().");
    }
    insideFunc_visited = true;
    state().enterLazyAttribute();
    insideFunc_value = getParent().Define_insideFunc(this, null);
    insideFunc_computed = true;
    state().leaveLazyAttribute();
    insideFunc_visited = false;
    return insideFunc_value;
  }
/** @apilevel internal */
protected boolean insideFunc_visited = false;
  /** @apilevel internal */
  private void insideFunc_reset() {
    insideFunc_computed = false;
    insideFunc_visited = false;
  }
  /** @apilevel internal */
  protected boolean insideFunc_computed = false;

  /** @apilevel internal */
  protected boolean insideFunc_value;

  /**
   * @attribute inh
   * @aspect NameAnalysis
   * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/rndFix.jrag:9
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.INH)
  @ASTNodeAnnotation.Source(aspect="NameAnalysis", declaredAt="/home/tobias_carlsson/chocopy-tobias/src/jastadd/rndFix.jrag:9")
  public boolean insideCl() {
    ASTState state = state();
    if (insideCl_computed) {
      return insideCl_value;
    }
    if (insideCl_visited) {
      throw new RuntimeException("Circular definition of attribute assignStmt.insideCl().");
    }
    insideCl_visited = true;
    state().enterLazyAttribute();
    insideCl_value = getParent().Define_insideCl(this, null);
    insideCl_computed = true;
    state().leaveLazyAttribute();
    insideCl_visited = false;
    return insideCl_value;
  }
/** @apilevel internal */
protected boolean insideCl_visited = false;
  /** @apilevel internal */
  private void insideCl_reset() {
    insideCl_computed = false;
    insideCl_visited = false;
  }
  /** @apilevel internal */
  protected boolean insideCl_computed = false;

  /** @apilevel internal */
  protected boolean insideCl_value;

  /**
   * @attribute inh
   * @aspect typeChecker
   * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/typeChecker.jrag:85
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.INH)
  @ASTNodeAnnotation.Source(aspect="typeChecker", declaredAt="/home/tobias_carlsson/chocopy-tobias/src/jastadd/typeChecker.jrag:85")
  public boolean isDeclOut() {
    ASTState state = state();
    if (isDeclOut_computed) {
      return isDeclOut_value;
    }
    if (isDeclOut_visited) {
      throw new RuntimeException("Circular definition of attribute assignStmt.isDeclOut().");
    }
    isDeclOut_visited = true;
    state().enterLazyAttribute();
    isDeclOut_value = getParent().Define_isDeclOut(this, null);
    isDeclOut_computed = true;
    state().leaveLazyAttribute();
    isDeclOut_visited = false;
    return isDeclOut_value;
  }
/** @apilevel internal */
protected boolean isDeclOut_visited = false;
  /** @apilevel internal */
  private void isDeclOut_reset() {
    isDeclOut_computed = false;
    isDeclOut_visited = false;
  }
  /** @apilevel internal */
  protected boolean isDeclOut_computed = false;

  /** @apilevel internal */
  protected boolean isDeclOut_value;

  /**
   * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/nameanalysis.jrag:372
   * @apilevel internal
   */
  public boolean Define_canDeclare(ASTNode _callerNode, ASTNode _childNode) {
    if (_callerNode == getTargetNoTransform()) {
      // @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/nameanalysis.jrag:373
      return getTarget().getIdUse().decl().possAssign();
    }
    else {
      return getParent().Define_canDeclare(this, _callerNode);
    }
  }
  /**
   * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/nameanalysis.jrag:372
   * @apilevel internal
   * @return {@code true} if this node has an equation for the inherited attribute canDeclare
   */
  protected boolean canDefine_canDeclare(ASTNode _callerNode, ASTNode _childNode) {
    return true;
  }
  /** @apilevel internal */
  protected void collect_contributors_Program_errors(Program _root, java.util.Map<ASTNode, java.util.Set<ASTNode>> _map) {
    // @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/Errors.jrag:120
    if (!types().compatibleType(expectedType()) && !getTarget().getIdUse().decl().isUnknown()) {
      {
        Program target = (Program) (program());
        java.util.Set<ASTNode> contributors = _map.get(target);
        if (contributors == null) {
          contributors = new java.util.LinkedHashSet<ASTNode>();
          _map.put((ASTNode) target, contributors);
        }
        contributors.add(this);
      }
    }
    // @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/Errors.jrag:134
    if ((getTarget().getIdUse().lookupIt(getTarget().getIdUse().getID())== unknownDecl()) && globalLookup(getTarget().getIdUse().getID())!=unknownDecl()) {
      {
        Program target = (Program) (program());
        java.util.Set<ASTNode> contributors = _map.get(target);
        if (contributors == null) {
          contributors = new java.util.LinkedHashSet<ASTNode>();
          _map.put((ASTNode) target, contributors);
        }
        contributors.add(this);
      }
    }
    super.collect_contributors_Program_errors(_root, _map);
  }
  /** @apilevel internal */
  protected void contributeTo_Program_errors(Set<ErrorMessage> collection) {
    super.contributeTo_Program_errors(collection);
    if (!types().compatibleType(expectedType()) && !getTarget().getIdUse().decl().isUnknown()) {
      collection.add(error("Expected " + expectedType() + " but received " + types()));
    }
    if ((getTarget().getIdUse().lookupIt(getTarget().getIdUse().getID())== unknownDecl()) && globalLookup(getTarget().getIdUse().getID())!=unknownDecl()) {
      collection.add(error("You cannot assign a value to this symbol: '" + getTarget().getIdUse().getID() +"', since it is not declared here  "));
    }
  }

}
