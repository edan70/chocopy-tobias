/* This file was generated with JastAdd2 (http://jastadd.org) version 2.3.4 */
package lang.ast;
import java.util.*;
import java.io.PrintStream;
import java.util.Set;
import java.util.TreeSet;
import java.util.ArrayList;
import java.io.ByteArrayOutputStream;
import java.lang.reflect.InvocationTargetException;
import java.util.HashSet;
/**
 * @ast node
 * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/lang.ast:72
 * @astdecl typedVar : ASTNode ::= IdDecl Typen;
 * @production typedVar : {@link ASTNode} ::= <span class="component">{@link IdDecl}</span> <span class="component">{@link Typen}</span>;

 */
public class typedVar extends ASTNode<ASTNode> implements Cloneable {
  /**
   * @declaredat ASTNode:1
   */
  public typedVar() {
    super();
  }
  /**
   * Initializes the child array to the correct size.
   * Initializes List and Opt nta children.
   * @apilevel internal
   * @ast method
   * @declaredat ASTNode:10
   */
  public void init$Children() {
    children = new ASTNode[2];
  }
  /**
   * @declaredat ASTNode:13
   */
  @ASTNodeAnnotation.Constructor(
    name = {"IdDecl", "Typen"},
    type = {"IdDecl", "Typen"},
    kind = {"Child", "Child"}
  )
  public typedVar(IdDecl p0, Typen p1) {
    setChild(p0, 0);
    setChild(p1, 1);
  }
  /** @apilevel low-level 
   * @declaredat ASTNode:23
   */
  protected int numChildren() {
    return 2;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:27
   */
  public void flushAttrCache() {
    super.flushAttrCache();
    lookupCl_String_reset();
    lookupNN_String_reset();
    localLookups_String_reset();
    globalLookup_String_reset();
    insideFunc_reset();
    localIndex_reset();
    localIndexc_reset();
    insideClass_reset();
    types_reset();
  }
  /** @apilevel internal 
   * @declaredat ASTNode:40
   */
  public void flushCollectionCache() {
    super.flushCollectionCache();

  }
  /** @apilevel internal 
   * @declaredat ASTNode:45
   */
  public typedVar clone() throws CloneNotSupportedException {
    typedVar node = (typedVar) super.clone();
    return node;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:50
   */
  public typedVar copy() {
    try {
      typedVar node = (typedVar) clone();
      node.parent = null;
      if (children != null) {
        node.children = (ASTNode[]) children.clone();
      }
      return node;
    } catch (CloneNotSupportedException e) {
      throw new Error("Error: clone not supported for " + getClass().getName());
    }
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @deprecated Please use treeCopy or treeCopyNoTransform instead
   * @declaredat ASTNode:69
   */
  @Deprecated
  public typedVar fullCopy() {
    return treeCopyNoTransform();
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @declaredat ASTNode:79
   */
  public typedVar treeCopyNoTransform() {
    typedVar tree = (typedVar) copy();
    if (children != null) {
      for (int i = 0; i < children.length; ++i) {
        ASTNode child = (ASTNode) children[i];
        if (child != null) {
          child = child.treeCopyNoTransform();
          tree.setChild(child, i);
        }
      }
    }
    return tree;
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The subtree of this node is traversed to trigger rewrites before copy.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @declaredat ASTNode:99
   */
  public typedVar treeCopy() {
    typedVar tree = (typedVar) copy();
    if (children != null) {
      for (int i = 0; i < children.length; ++i) {
        ASTNode child = (ASTNode) getChild(i);
        if (child != null) {
          child = child.treeCopy();
          tree.setChild(child, i);
        }
      }
    }
    return tree;
  }
  /**
   * Replaces the IdDecl child.
   * @param node The new node to replace the IdDecl child.
   * @apilevel high-level
   */
  public typedVar setIdDecl(IdDecl node) {
    setChild(node, 0);
    return this;
  }
  /**
   * Retrieves the IdDecl child.
   * @return The current node used as the IdDecl child.
   * @apilevel high-level
   */
  @ASTNodeAnnotation.Child(name="IdDecl")
  public IdDecl getIdDecl() {
    return (IdDecl) getChild(0);
  }
  /**
   * Retrieves the IdDecl child.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The current node used as the IdDecl child.
   * @apilevel low-level
   */
  public IdDecl getIdDeclNoTransform() {
    return (IdDecl) getChildNoTransform(0);
  }
  /**
   * Replaces the Typen child.
   * @param node The new node to replace the Typen child.
   * @apilevel high-level
   */
  public typedVar setTypen(Typen node) {
    setChild(node, 1);
    return this;
  }
  /**
   * Retrieves the Typen child.
   * @return The current node used as the Typen child.
   * @apilevel high-level
   */
  @ASTNodeAnnotation.Child(name="Typen")
  public Typen getTypen() {
    return (Typen) getChild(1);
  }
  /**
   * Retrieves the Typen child.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The current node used as the Typen child.
   * @apilevel low-level
   */
  public Typen getTypenNoTransform() {
    return (Typen) getChildNoTransform(1);
  }
  /**
   * @attribute inh
   * @aspect NameAnalysis
   * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/nameanalysis.jrag:13
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.INH)
  @ASTNodeAnnotation.Source(aspect="NameAnalysis", declaredAt="/home/tobias_carlsson/chocopy-tobias/src/jastadd/nameanalysis.jrag:13")
  public class_def lookupCl(String name) {
    Object _parameters = name;
    if (lookupCl_String_visited == null) lookupCl_String_visited = new java.util.HashSet(4);
    if (lookupCl_String_values == null) lookupCl_String_values = new java.util.HashMap(4);
    ASTState state = state();
    if (lookupCl_String_values.containsKey(_parameters)) {
      return (class_def) lookupCl_String_values.get(_parameters);
    }
    if (lookupCl_String_visited.contains(_parameters)) {
      throw new RuntimeException("Circular definition of attribute typedVar.lookupCl(String).");
    }
    lookupCl_String_visited.add(_parameters);
    state().enterLazyAttribute();
    class_def lookupCl_String_value = getParent().Define_lookupCl(this, null, name);
    lookupCl_String_values.put(_parameters, lookupCl_String_value);
    state().leaveLazyAttribute();
    lookupCl_String_visited.remove(_parameters);
    return lookupCl_String_value;
  }
/** @apilevel internal */
protected java.util.Set lookupCl_String_visited;
  /** @apilevel internal */
  private void lookupCl_String_reset() {
    lookupCl_String_values = null;
    lookupCl_String_visited = null;
  }
  /** @apilevel internal */
  protected java.util.Map lookupCl_String_values;

  /**
   * @attribute inh
   * @aspect NameAnalysis
   * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/nameanalysis.jrag:224
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.INH)
  @ASTNodeAnnotation.Source(aspect="NameAnalysis", declaredAt="/home/tobias_carlsson/chocopy-tobias/src/jastadd/nameanalysis.jrag:224")
  public IdDecl lookupNN(String name) {
    Object _parameters = name;
    if (lookupNN_String_visited == null) lookupNN_String_visited = new java.util.HashSet(4);
    if (lookupNN_String_values == null) lookupNN_String_values = new java.util.HashMap(4);
    ASTState state = state();
    if (lookupNN_String_values.containsKey(_parameters)) {
      return (IdDecl) lookupNN_String_values.get(_parameters);
    }
    if (lookupNN_String_visited.contains(_parameters)) {
      throw new RuntimeException("Circular definition of attribute typedVar.lookupNN(String).");
    }
    lookupNN_String_visited.add(_parameters);
    state().enterLazyAttribute();
    IdDecl lookupNN_String_value = getParent().Define_lookupNN(this, null, name);
    lookupNN_String_values.put(_parameters, lookupNN_String_value);
    state().leaveLazyAttribute();
    lookupNN_String_visited.remove(_parameters);
    return lookupNN_String_value;
  }
/** @apilevel internal */
protected java.util.Set lookupNN_String_visited;
  /** @apilevel internal */
  private void lookupNN_String_reset() {
    lookupNN_String_values = null;
    lookupNN_String_visited = null;
  }
  /** @apilevel internal */
  protected java.util.Map lookupNN_String_values;

  /**
   * @attribute inh
   * @aspect NameAnalysis
   * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/nameanalysis.jrag:283
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.INH)
  @ASTNodeAnnotation.Source(aspect="NameAnalysis", declaredAt="/home/tobias_carlsson/chocopy-tobias/src/jastadd/nameanalysis.jrag:283")
  public IdDecl localLookups(String name) {
    Object _parameters = name;
    if (localLookups_String_visited == null) localLookups_String_visited = new java.util.HashSet(4);
    if (localLookups_String_values == null) localLookups_String_values = new java.util.HashMap(4);
    ASTState state = state();
    if (localLookups_String_values.containsKey(_parameters)) {
      return (IdDecl) localLookups_String_values.get(_parameters);
    }
    if (localLookups_String_visited.contains(_parameters)) {
      throw new RuntimeException("Circular definition of attribute typedVar.localLookups(String).");
    }
    localLookups_String_visited.add(_parameters);
    state().enterLazyAttribute();
    IdDecl localLookups_String_value = getParent().Define_localLookups(this, null, name);
    localLookups_String_values.put(_parameters, localLookups_String_value);
    state().leaveLazyAttribute();
    localLookups_String_visited.remove(_parameters);
    return localLookups_String_value;
  }
/** @apilevel internal */
protected java.util.Set localLookups_String_visited;
  /** @apilevel internal */
  private void localLookups_String_reset() {
    localLookups_String_values = null;
    localLookups_String_visited = null;
  }
  /** @apilevel internal */
  protected java.util.Map localLookups_String_values;

  /**
   * @attribute inh
   * @aspect NameAnalysis
   * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/nameanalysis.jrag:332
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.INH)
  @ASTNodeAnnotation.Source(aspect="NameAnalysis", declaredAt="/home/tobias_carlsson/chocopy-tobias/src/jastadd/nameanalysis.jrag:332")
  public IdDecl globalLookup(String name) {
    Object _parameters = name;
    if (globalLookup_String_visited == null) globalLookup_String_visited = new java.util.HashSet(4);
    if (globalLookup_String_values == null) globalLookup_String_values = new java.util.HashMap(4);
    ASTState state = state();
    if (globalLookup_String_values.containsKey(_parameters)) {
      return (IdDecl) globalLookup_String_values.get(_parameters);
    }
    if (globalLookup_String_visited.contains(_parameters)) {
      throw new RuntimeException("Circular definition of attribute typedVar.globalLookup(String).");
    }
    globalLookup_String_visited.add(_parameters);
    state().enterLazyAttribute();
    IdDecl globalLookup_String_value = getParent().Define_globalLookup(this, null, name);
    globalLookup_String_values.put(_parameters, globalLookup_String_value);
    state().leaveLazyAttribute();
    globalLookup_String_visited.remove(_parameters);
    return globalLookup_String_value;
  }
/** @apilevel internal */
protected java.util.Set globalLookup_String_visited;
  /** @apilevel internal */
  private void globalLookup_String_reset() {
    globalLookup_String_values = null;
    globalLookup_String_visited = null;
  }
  /** @apilevel internal */
  protected java.util.Map globalLookup_String_values;

  /**
   * @attribute inh
   * @aspect paramAdrInd
   * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/paramAdrInd.jrag:22
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.INH)
  @ASTNodeAnnotation.Source(aspect="paramAdrInd", declaredAt="/home/tobias_carlsson/chocopy-tobias/src/jastadd/paramAdrInd.jrag:22")
  public boolean insideFunc() {
    ASTState state = state();
    if (insideFunc_computed) {
      return insideFunc_value;
    }
    if (insideFunc_visited) {
      throw new RuntimeException("Circular definition of attribute typedVar.insideFunc().");
    }
    insideFunc_visited = true;
    state().enterLazyAttribute();
    insideFunc_value = getParent().Define_insideFunc(this, null);
    insideFunc_computed = true;
    state().leaveLazyAttribute();
    insideFunc_visited = false;
    return insideFunc_value;
  }
/** @apilevel internal */
protected boolean insideFunc_visited = false;
  /** @apilevel internal */
  private void insideFunc_reset() {
    insideFunc_computed = false;
    insideFunc_visited = false;
  }
  /** @apilevel internal */
  protected boolean insideFunc_computed = false;

  /** @apilevel internal */
  protected boolean insideFunc_value;

  /**
   * @attribute inh
   * @aspect paramAdrInd
   * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/paramAdrInd.jrag:31
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.INH)
  @ASTNodeAnnotation.Source(aspect="paramAdrInd", declaredAt="/home/tobias_carlsson/chocopy-tobias/src/jastadd/paramAdrInd.jrag:31")
  public int localIndex() {
    ASTState state = state();
    if (localIndex_computed) {
      return localIndex_value;
    }
    if (localIndex_visited) {
      throw new RuntimeException("Circular definition of attribute typedVar.localIndex().");
    }
    localIndex_visited = true;
    state().enterLazyAttribute();
    localIndex_value = getParent().Define_localIndex(this, null);
    localIndex_computed = true;
    state().leaveLazyAttribute();
    localIndex_visited = false;
    return localIndex_value;
  }
/** @apilevel internal */
protected boolean localIndex_visited = false;
  /** @apilevel internal */
  private void localIndex_reset() {
    localIndex_computed = false;
    localIndex_visited = false;
  }
  /** @apilevel internal */
  protected boolean localIndex_computed = false;

  /** @apilevel internal */
  protected int localIndex_value;

  /**
   * @attribute inh
   * @aspect paramAdrInd
   * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/paramAdrInd.jrag:51
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.INH)
  @ASTNodeAnnotation.Source(aspect="paramAdrInd", declaredAt="/home/tobias_carlsson/chocopy-tobias/src/jastadd/paramAdrInd.jrag:51")
  public int localIndexc() {
    ASTState state = state();
    if (localIndexc_computed) {
      return localIndexc_value;
    }
    if (localIndexc_visited) {
      throw new RuntimeException("Circular definition of attribute typedVar.localIndexc().");
    }
    localIndexc_visited = true;
    state().enterLazyAttribute();
    localIndexc_value = getParent().Define_localIndexc(this, null);
    localIndexc_computed = true;
    state().leaveLazyAttribute();
    localIndexc_visited = false;
    return localIndexc_value;
  }
/** @apilevel internal */
protected boolean localIndexc_visited = false;
  /** @apilevel internal */
  private void localIndexc_reset() {
    localIndexc_computed = false;
    localIndexc_visited = false;
  }
  /** @apilevel internal */
  protected boolean localIndexc_computed = false;

  /** @apilevel internal */
  protected int localIndexc_value;

  /**
   * @attribute inh
   * @aspect paramAdrInd
   * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/paramAdrInd.jrag:74
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.INH)
  @ASTNodeAnnotation.Source(aspect="paramAdrInd", declaredAt="/home/tobias_carlsson/chocopy-tobias/src/jastadd/paramAdrInd.jrag:74")
  public boolean insideClass() {
    ASTState state = state();
    if (insideClass_computed) {
      return insideClass_value;
    }
    if (insideClass_visited) {
      throw new RuntimeException("Circular definition of attribute typedVar.insideClass().");
    }
    insideClass_visited = true;
    state().enterLazyAttribute();
    insideClass_value = getParent().Define_insideClass(this, null);
    insideClass_computed = true;
    state().leaveLazyAttribute();
    insideClass_visited = false;
    return insideClass_value;
  }
/** @apilevel internal */
protected boolean insideClass_visited = false;
  /** @apilevel internal */
  private void insideClass_reset() {
    insideClass_computed = false;
    insideClass_visited = false;
  }
  /** @apilevel internal */
  protected boolean insideClass_computed = false;

  /** @apilevel internal */
  protected boolean insideClass_value;

  /**
   * @attribute inh
   * @aspect typeChecker
   * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/typeChecker.jrag:22
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.INH)
  @ASTNodeAnnotation.Source(aspect="typeChecker", declaredAt="/home/tobias_carlsson/chocopy-tobias/src/jastadd/typeChecker.jrag:22")
  public Typen types() {
    ASTState state = state();
    if (types_computed) {
      return types_value;
    }
    if (types_visited) {
      throw new RuntimeException("Circular definition of attribute typedVar.types().");
    }
    types_visited = true;
    state().enterLazyAttribute();
    types_value = getParent().Define_types(this, null);
    types_computed = true;
    state().leaveLazyAttribute();
    types_visited = false;
    return types_value;
  }
/** @apilevel internal */
protected boolean types_visited = false;
  /** @apilevel internal */
  private void types_reset() {
    types_computed = false;
    
    types_value = null;
    types_visited = false;
  }
  /** @apilevel internal */
  protected boolean types_computed = false;

  /** @apilevel internal */
  protected Typen types_value;

  /**
   * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/nameanalysis.jrag:254
   * @apilevel internal
   */
  public IdDecl Define_loclookup(ASTNode _callerNode, ASTNode _childNode, String name) {
    if (_callerNode == getIdDeclNoTransform()) {
      // @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/nameanalysis.jrag:166
      {
      		IdDecl id = globalLookup(getTypen().idif());
      		return id;
      	}
    }
    else {
      return getParent().Define_loclookup(this, _callerNode, name);
    }
  }
  /**
   * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/nameanalysis.jrag:254
   * @apilevel internal
   * @return {@code true} if this node has an equation for the inherited attribute loclookup
   */
  protected boolean canDefine_loclookup(ASTNode _callerNode, ASTNode _childNode, String name) {
    return true;
  }
  /**
   * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/nameanalysis.jrag:207
   * @apilevel internal
   */
  public IdDecl Define_lookup(ASTNode _callerNode, ASTNode _childNode, String name) {
    if (_callerNode == getIdDeclNoTransform()) {
      // @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/nameanalysis.jrag:212
      {
      		if(insideClass()){
      			if(insideFunc()){
      			return globalLookup(name);
      			}
      			return lookupNN(name);
      		}
      		return globalLookup(name);
      	}
    }
    else {
      return getParent().Define_lookup(this, _callerNode, name);
    }
  }
  /**
   * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/nameanalysis.jrag:207
   * @apilevel internal
   * @return {@code true} if this node has an equation for the inherited attribute lookup
   */
  protected boolean canDefine_lookup(ASTNode _callerNode, ASTNode _childNode, String name) {
    return true;
  }
  /**
   * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/nameanalysis.jrag:351
   * @apilevel internal
   */
  public boolean Define_possAssign(ASTNode _callerNode, ASTNode _childNode) {
    if (_callerNode == getIdDeclNoTransform()) {
      // @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/nameanalysis.jrag:353
      return true;
    }
    else {
      return getParent().Define_possAssign(this, _callerNode);
    }
  }
  /**
   * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/nameanalysis.jrag:351
   * @apilevel internal
   * @return {@code true} if this node has an equation for the inherited attribute possAssign
   */
  protected boolean canDefine_possAssign(ASTNode _callerNode, ASTNode _childNode) {
    return true;
  }
  /**
   * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/paramAdrInd.jrag:52
   * @apilevel internal
   */
  public int Define_localIndexc(ASTNode _callerNode, ASTNode _childNode) {
    if (_callerNode == getIdDeclNoTransform()) {
      // @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/paramAdrInd.jrag:57
      return localIndexc();
    }
    else {
      return getParent().Define_localIndexc(this, _callerNode);
    }
  }
  /**
   * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/paramAdrInd.jrag:52
   * @apilevel internal
   * @return {@code true} if this node has an equation for the inherited attribute localIndexc
   */
  protected boolean canDefine_localIndexc(ASTNode _callerNode, ASTNode _childNode) {
    return true;
  }
  /**
   * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/paramAdrInd.jrag:124
   * @apilevel internal
   */
  public HashMap<var_def, Integer> Define_objvars(ASTNode _callerNode, ASTNode _childNode) {
    if (_callerNode == getIdDeclNoTransform()) {
      // @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/paramAdrInd.jrag:163
      {
      		List<var_def> tList = 	  globalLookup(getTypen().idif()).locvars();
      		List<var_def> fList = 	  globalLookup(getTypen().idif()).locvarsF();
      		HashMap<var_def, Integer> hash_map = new HashMap<var_def, Integer>(); 
      		int count = 0;
      		for(var_def vf : tList){
      			hash_map.put(vf, getIdDecl().localIndex() - count); 
      			count++;
      		}
      		for(var_def vf : fList){
      			hash_map.put(vf, getIdDecl().localIndex() - count); 
      			count++;	
      		}
      		return hash_map;
      	}
    }
    else {
      return getParent().Define_objvars(this, _callerNode);
    }
  }
  /**
   * @declaredat /home/tobias_carlsson/chocopy-tobias/src/jastadd/paramAdrInd.jrag:124
   * @apilevel internal
   * @return {@code true} if this node has an equation for the inherited attribute objvars
   */
  protected boolean canDefine_objvars(ASTNode _callerNode, ASTNode _childNode) {
    return true;
  }

}
