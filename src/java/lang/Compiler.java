package lang;

import java.io.FileReader;
import java.io.IOException;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.Buffer;
import java.nio.ByteBuffer;
import java.nio.file.FileSystem;
import java.nio.file.FileSystems;
import java.nio.file.LinkOption;
import java.nio.file.Path;
import java.nio.file.Paths;
import beaver.Parser.Exception;
import java.util.*;

import lang.ast.Program;
import lang.ast.LangParser;
import lang.ast.LangScanner;
import lang.PreProc;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import lang.ast.ErrorMessage;
import java.io.File; // Import the File class

/**
 * Dumps the parsed Abstract Syntax Tree of a Calc program.
 */
public class Compiler {
	/**
	 * Entry point
	 * 
	 * @param args
	 */

	public static Object DrAST_root_node; // Enable debugging with DrAST

	public static void main(String[] args) {
		try {
			if (args.length != 1) {
				System.err.println("You must specify a source file on the command line!");
				printUsage();
				System.exit(1);
				return;
			}

			String filename = args[0];
			PreProc preProcessor = new PreProc();
						//preProcessor.fixSynt(filename);

			LangScanner scanner = new LangScanner(new FileReader(preProcessor.fixSynt(filename)));
			LangParser parser = new LangParser();
			Program program = (Program) parser.parse(scanner);
			//System.out.println(program.dumpTree());
			DrAST_root_node = program; // Enable debugging with DrAST

		if (!program.errors().isEmpty()) {
        System.err.println();
        System.err.println("Errors: ");
        for (ErrorMessage e: program.errors()) {
          System.err.println("- " + e);
          System.exit(1);
        }
      }else {
        program.genCode(System.out,null);
      }
		} catch (FileNotFoundException e) {
			System.out.println("File not found!");
			System.exit(1);
		} catch (IOException e) {
			e.printStackTrace(System.err);
			System.exit(1);
		} catch (Exception e) {
			e.printStackTrace();
			System.exit(1);
		}
	}

	private static void printUsage() {
		System.err.println("Usage: DumpTree FILE");
		System.err.println("  where FILE is the file to be parsed");
	}
}
