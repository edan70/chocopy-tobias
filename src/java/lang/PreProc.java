
package lang;

import java.io.FileReader;
import java.io.IOException;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.Buffer;
import java.nio.ByteBuffer;
import java.nio.file.FileSystem;
import java.nio.file.FileSystems;

import java.nio.file.LinkOption;
import java.nio.file.Path;
import java.nio.file.Paths;
import beaver.Parser.Exception;
import java.util.*;
import lang.ast.Program;
import lang.ast.LangParser;
import lang.ast.LangScanner;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;

import java.io.File; // Import the File class

public class PreProc {

	private static final File TEST_DIRECTORY = new File("testfiles/parser");

	public String fixSynt(String filename) {
		try {
			List<String> newLines = new ArrayList<>();
			int indent_level = 0;
			int loc_indent = 0;
			int count = 0;
			
			Character prevchar = ' ';
			for (String line : Files.readAllLines(Paths.get(filename), StandardCharsets.UTF_8)) {
				count = 0;
				loc_indent = 0;
				int len = line.length();
				for (int i = 0; i < len; i++) {
					if (line.charAt(i) == ' ') {
						count++;
						if (count == 4) {
							loc_indent++;

							count = 0;
						}
					} else {

						if (loc_indent > indent_level) {
							StringBuilder sb = new StringBuilder();
							StringBuilder ind = new StringBuilder();


						
							for (int j = 0; j < loc_indent; j++) {

								ind.append("    ");
							}
							while (indent_level < loc_indent) {
								indent_level++;

								sb.append("			indent	");
							}

							if(lookForDecl(line,i)){
								sb.append("			idDecl  ");
							
							}

							newLines.add(sb.toString() +line.trim() + "	newline	");
							indent_level = loc_indent;
						} else if (loc_indent < indent_level) {
							StringBuilder sb = new StringBuilder();
							StringBuilder ind = new StringBuilder();
							if (loc_indent == 0) {
								if (count == 0) {
									ind.append(line);
									while (indent_level > loc_indent) {
									indent_level--;
									sb.append("		dedent	");
									}
								}else {
									for (int j = 0; j < count; j++) {
										ind.append(" ");
									}
									while (indent_level > loc_indent) {
									indent_level--;
									sb.append("		dedent	");
									}
								}
							} else {
								for (int j = 0; j < loc_indent; j++) {
									ind.append("    ");
								}
								while (indent_level > loc_indent) {
									indent_level--;
									sb.append("		dedent	");
								}
							}
							if(lookForDecl(line,i)){
								sb.append("idDecl ");
							
							}

							newLines.add(sb.toString() + line + " newline" );
							indent_level = loc_indent;
						} else if (count > 0) {
							StringBuilder sb = new StringBuilder();
							StringBuilder ind = new StringBuilder();
							for (int j = 0; j < count; j++) {

								ind.append(" ");
								sb.append(" space	");
							}
							if(lookForDecl(line,i)){
								sb.append("idDecl ");
							
							}
							newLines.add(sb.toString() + line.trim() +  "  newline	");
						}else{

							if(lookForDecl(line,i)){
							
							newLines.add(line.replace(line,"idDecl " +line.trim() + "  newline	"));

							
							}
							else{
							newLines.add(line.replace(line, line.trim() + "  newline	"));
							}
						}
						break;
					}
					prevchar = line.charAt(i);

				}
				if (line.trim().length() > 0) {

				

				}
				else{
										newLines.add(line);

				}
			}
			StringBuilder sb = new StringBuilder();
			for (int j = 0; j < indent_level; j++) {
				sb.append("		dedent	");
			}
			newLines.add(sb.toString());
			
			String newFile = "";
			int index = filename.lastIndexOf('.');
			if (index != -1) {
				newFile = filename.substring(0, index) + ".indent";
			} else {
				newFile = filename + ".indent";
			}

			Path root = FileSystems.getDefault().getPath("").toAbsolutePath();
			Path filePath = Paths.get(root.toString(), newFile);
			Files.write(filePath, newLines, StandardCharsets.UTF_8);
			return filePath.toString();

		} catch (FileNotFoundException e) {
			System.out.println("File not found!");
			System.exit(1);
		} catch (IOException e) {
			e.printStackTrace(System.err);
			System.exit(1);
		}
		return null;
	}
	public boolean lookForDecl(String line, int iSt){
		for(int i = iSt+1; i< line.length();i++){
			if(line.charAt(i)==' '){
				return false;
			}
			else if(line.charAt(i)==':'){
				for(int j = i+1; j <line.length();j++){
					if(line.charAt(j)=='='){
						return true;

					}
				

				}
			}
		}
		return false;
	}
}
