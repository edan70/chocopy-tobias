package lang;

import java.io.File;

import org.junit.Test;

import beaver.Parser.Exception;
import java.util.*;
import lang.ast.Program;
import lang.ast.LangParser;
import lang.PreProc;
import lang.ast.LangScanner;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;

import java.io.File;
import org.junit.Test;

import lang.ast.Program;

public class ParseTests {
	/** Directory where the test input files are stored. */
	private static final File TEST_DIRECTORY = new File("testfiles/parser");

	@Test
	public void testIfWhileForStmt() {
		PreProc preProcessor = new PreProc();
		for (int i = 1; i <= 4; i++) {
			String tempFile = "testfiles/parser/testSc" + i + ".in";
			preProcessor.fixSynt(tempFile);
			Util.testValidSyntax(TEST_DIRECTORY, "testSc" + i + ".indent");

		}
	}

	@Test
	public void testSimpStmt() {
		PreProc preProcessor = new PreProc();
		for (int i = 1; i <= 3; i++) {
			String tempFile = "testfiles/parser/testSt" + i + ".in";
			preProcessor.fixSynt(tempFile);
			Util.testValidSyntax(TEST_DIRECTORY, "testSt" + i + ".indent");

		}
	}

	@Test
	public void testErrorSyntax() {
		PreProc preProcessor = new PreProc();
		for (int i = 1; i <= 4; i++) {
			String tempFile = "testfiles/parser/testError" + i + ".in";
			preProcessor.fixSynt(tempFile);
			
			Util.testSyntaxError(TEST_DIRECTORY, "testError" + i + ".indent");

		}
	}
}
